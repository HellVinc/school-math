<?php

namespace api\modules\v2\controllers;

use common\models\Task;
use Yii;
use common\models\Bookmark;
use common\models\search\BookmarkSearch;


use yii\behaviors;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookmarkController implements the CRUD actions for Bookmark model.
 */
class BookmarkController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'for-user',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'for-user',
            ],
            'rules' => [
                [
                    'actions' => [

                        'for-user',

                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [

                'for-user' => ['get'],

            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }


    /**
     * Displays a single Bookmark model.
     * Bookmark/for-user
     * get
     *
     * @return mixed
     */
    public function actionForUser()
    {
//        $model = (new Query())
////            ->asArray()
//            ->select(['task_id AS id', 'explanation', 'text', 'book_id', 'bookmark.created_by AS created_by', 'bookmark.created_at AS created_at', 'bookmark.updated_at AS updated_at', 'bookmark.deleted AS deleted'])
//            ->from('bookmark')
//            ->leftJoin('task', 'task.id = task_id')
//            ->where(['bookmark.created_by' => Yii::$app->user->id])
//
//            ->all();


        $model = new BookmarkSearch();
        $model->created_by = Yii::$app->user->id;
        $result = $model->searchAll(Yii::$app->request->get());
        $result = $model->all_fields($result);

        if ($result) {
//            foreach ($models as $one){
//                $one->explanation = $one->task->explanation;
//            }
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Finds the Bookmark model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Bookmark the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Bookmark::findOne($id)) !== null) {
            if ($model->deleted == Bookmark::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
