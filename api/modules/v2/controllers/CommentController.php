<?php

namespace api\modules\v2\controllers;

use common\models\Bookmark;
use common\models\Task;
use Yii;
use common\models\Message;
use common\models\Comment;
use common\models\Role;
use common\models\search\CommentSearch;
use common\models\search\MessageSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',

                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['learner', 'tutor'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all-message' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }

    /**
     * Lists all Comment models.
     * Comment/all-message
     * get
     * comment_id
     *
     * @return mixed
     */
    public function actionAllMessage()
    {
        if (Yii::$app->request->get('comment_id')) {
            Message::changeStatus(Yii::$app->request->get('comment_id'));
            $model = new MessageSearch();
            $result = $model->searchAll(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        } else {
            return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
        }
    }

    /**
     * comment/all
     * get
     * task_id
     * @return array|bool
     */

    public function actionAll()
    {
        if (Yii::$app->request->get('task_id')) {
            $model = new CommentSearch();
            $task  = Task::findOne(['id' => Yii::$app->request->get('task_id')]);
            if (!($bookmark = Bookmark::saveOne($task->theme->section->book->id, $task->id))) {
                return $bookmark->getErrors();
            }

//            if(Yii::$app->user->identity->role_id == Role::LEARNER) {
//                $model->created_by = Yii::$app->user->id;
//            }
            $result = $model->searchAll(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        } else {
            return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
        }
    }

    /**
     * Displays a single Comment model.
     * Comment/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Comment model.
     * Comment/create
     * post
     *
     * @property integer $task_id +
     * @property integer $comment_id
     * @property string $send_to +
     * @property string $text +
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Message();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
            return $model->one_fields();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Comment model.
     * Comment/update
     * post
     *
     * @property integer $id +
     * @property integer $task_id +
     * @property double $result
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->created_by == Yii::$app->user->id) {
            if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
                return $model->one_fields();
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Deletes an existing Comment model.
     * Comment/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->created_by == Yii::$app->user->id) {
            if ($model->remove()  && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }


    public function actionDeleteMessage()
    {

        $model = Message::findOne(Yii::$app->request->getBodyParams('id'));
        if ($model->deleted == Message::NOT_DELETED) {
            if ($model->send_from == Yii::$app->user->id) {
                if ($model->remove() && !$model->getErrors()) {
                    return true;
                }
                return $model->getErrors();
            }
            return ['error' => Yii::t('msg/error', 'Changes not saved')];
        } else {
            throw new NotFoundHttpException('The record was archived.');
        }

    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            if ($model->deleted == Comment::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
