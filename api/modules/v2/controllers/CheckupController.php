<?php

namespace api\modules\v2\controllers;


use Yii;
use common\models\Checkup;
use common\models\Control;
use common\models\search\CheckupSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * CheckupController implements the CRUD actions for Checkup model.
 */
class CheckupController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',

            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'lowest-rating' => ['get'],
                'all' => ['get'],
                'one' => ['get'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }


    /**
     * Lists all Checkup models.
     * Checkup/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new CheckupSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Checkup model.
     * checkup/one
     * get
     *
     * id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionOne()
    {
        $checkup = $this->findModel(Yii::$app->request->get('id'));
        $done_task = $checkup->doneTask;

        //если просматривает автор решения, изменить статус проверки на просмотреный
        if($done_task->created_by == Yii::$app->user->id){
            $done_task->statusViewed();
        }
        $task = $done_task->task;
        return [
            'task' => $task,
            'done_task' => $done_task,
            'checkup' => $checkup->one_fields()
        ];

    }

    /**
     * Finds the Checkup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Checkup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Checkup::findOne($id)) !== null) {
            if ($model->deleted == Checkup::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
