<?php

namespace api\modules\v2\controllers;

use common\models\MyDiscipline;
use common\models\search\UserSearch;
use Yii;
use common\models\User;
use common\models\Role;
use common\models\MyClass;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
//            'class' => CompositeAuth::className(),
//            'authMethods' => [
//                QueryParamAuth::className(),
//            ],
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all-for-management',
                'all',
                'one',
                'todo',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all-for-management',
                'all',
                'one',
                'todo',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all-for-management',
                        'all',
                        'one',
                        'todo',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all-for-management' => ['get'],
                'one' => ['get'],
                'todo' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }


    /**
     * Updates an existing User model.
     * user/update
     * post
     *
     * id
     *
     *
     *  * $request['my_classes'] = [
     * '0' => [
     * 'number' => 5,
     * 'letter' => 'A'
     * ] ,
     * '1' => [
     * 'number' => 3,
     * 'letter' => 'A'
     * ]
     * ];
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $model->updated_at =time();
            if ($model->saveModel() && !$model->getErrors()) {
                //если заданы класы для юзера
                $trans->commit();
                return $model->one_fields();
            }else{
                $trans->rollback();
            }
        }
        return $model->getErrors();
    }

    public function actionTodo(){
        $user = Yii::$app->user->identity;
        $done_error = $user->getDoneErrorTasks();
        $tasks = $user->getPushTask();
        return [
            'done_error' => $done_error,
            'message' => $done_error,
            'tasks' => $tasks
            ];
    }


    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            if ($model->deleted == User::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

        }
    }
}
