<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\BookDiscipline;
use common\models\search\BookDisciplineSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookDisciplineController implements the CRUD actions for BookDiscipline model.
 */
class BookDisciplineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],

            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }

    /**
     * Lists all BookDiscipline models.
     * book-discipline/all
     * get
     *
     * discipline_id
     *
     *
     * @return mixed
     */
    public function actionAll()
    {
        if (Yii::$app->request->get('discipline_id')) {
            $model = new BookDisciplineSearch();
            $result = $model->searchAll(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

}
