<?php

namespace api\modules\v2\controllers;

use common\models\MyPupil;
use common\models\Subscription;
use common\models\Transfer;
use Yii;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SubscriptionController implements the CRUD actions for Subscription model.
 */
class SubscriptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'check',
                'create',
                'one',
                'all',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                        'all',
                        'create',
                    ],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'check' => ['get'],
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],

            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }

    /**
     * Lists all Subscription models.
     * Subtask/all
     * get
     * theme_id
     * @return mixed
     */
    public function actionAll()
    {
        return Transfer::searchModel(Yii::$app->request->get());
    }

    /**
     * Creates a new Transfer model.
     * Transfer/create
     * post
     *
     * user_id
     * summa
     * type
     *
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request->post();
        if ($request) {
            $model = new Transfer();
            if ($model->load($request, "")) {
                $trans = Yii::$app->db->beginTransaction();
                $model->type = $model::SUBSCRIBTION;
                $model->user_id = Yii::$app->user->id;
                $result = $model->saveModel();
                if ($result['error']) {
                    $trans->rollback();
                    return $result;
                }
                $subscr = new Subscription();
                $result = $subscr->saveModel($model);
                if ($result['error']) {
                    $trans->rollback();
                    return $result;
                }

                $trans->commit();
                return $model;
            }
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Displays a single Subtask model.
     * Subtask/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    public function actionCheck()
    {
        if ($result = Subscription::findOne([
            'user_id' => Yii::$app->user->id,
            'deleted' => Subscription::NOT_DELETED
        ])
        ) {
            return $result;
        }
        return false;
    }

    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        $myPupil = MyPupil::findOne(['pupil_id' => $model->user_id]);
        if ($model->remove() && $myPupil->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }


    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscription the loaded model
     * @throws NotFoundHttpException if the model cannot be foundh
     */
    protected function findModel($id)
    {
        if (($model = Subscription::findOne($id)) !== null) {
            if ($model->deleted == Subscription::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}