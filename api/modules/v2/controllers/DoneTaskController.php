<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\DoneTask;
use common\models\search\DoneTaskSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * DoneTaskController implements the CRUD actions for DoneTask model.
 */
class DoneTaskController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['learner','tutor'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }

    /**
     * Lists all DoneTask models.
     * DoneTask/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new DoneTaskSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single DoneTask model.
     * DoneTask/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new DoneTask model.
     * done-task/create
     * post
     *
     * @property integer $task_id +
     * @property double $answer
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DoneTask();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->checkResult() && $model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing DoneTask model.
     * DoneTask/update
     * post
     *
     * @property integer $id +
     * @property integer $task_id +
     * @property double $result
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->checkResult() && $model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing DoneTask model.
     * DoneTask/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->created_by == Yii::$app->user->id) {
            if ($model->remove() && $model->removeFiles() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Finds the DoneTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DoneTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DoneTask::findOne($id)) !== null) {
            if ($model->deleted == DoneTask::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
