<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\MyBook;
use common\models\search\MyBookSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * MyBookController implements the CRUD actions for MyBook model.
 */
class MyBookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post', 'put'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all MyBook models.
     * my-Book/all
     * get
     *
     * discipline_id
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new MyBookSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single MyBook model.
     * my-Book/one
     * get
     * @property integer $id+
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new MyBook model.
     * my-Book/create
     * post
     *
     * @property integer $book_discipline_id+
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MyBook();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->saveWithCheck() && !$model->getErrors()) {
                return $model;
            }
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing MyBook model.
     * my-Book//update
     * post
     *
     * @property integer $id+
     * @property integer $book_discipline_id+
     * @property integer $book_id+
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->user_id == Yii::$app->user->id) {
            $model->book_discipline_id = Yii::$app->request->post('book_discipline_id');
            if ($model->save() && !$model->getErrors()) {
                return $model;
            }
            return $model->getErrors();
        }

    }

    /**
     * Deletes an existing MyBook model.
     * my-Book/delete
     * delete
     *
     * @property integer $id+
     * @return mixed
     */
    public
    function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->myDiscipline->user_id == Yii::$app->user->id) {
            if ($model->remove() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Finds the MyBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = MyBook::findOne($id)) !== null) {
            if ($model->deleted == MyBook::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
