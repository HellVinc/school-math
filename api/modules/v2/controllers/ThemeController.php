<?php

namespace api\modules\v2\controllers;

use Yii;
use common\models\Theme;
use common\models\search\ThemeSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ThemeController implements the CRUD actions for Theme model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }
    
    /**
     * Lists all Theme models.
     * Theme/all
     * get
     *
     * section_id
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new ThemeSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Theme model.
     * Theme/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
       return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Finds the Theme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null) {
            if ($model->deleted == Theme::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
