<?php
namespace api\modules\v2\controllers;

use common\models\search\SchoolSearch;
use common\models\User;
use common\models\LoginForm;
use common\models\CMail;
use common\models\Role;
use common\models\MyClass;
use common\models\MyDiscipline;


use Yii;
use yii\behaviors;
use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * Site controller
 */
class SiteController extends Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'login-key',
                'check'
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'signup',
                'login'
            ],
            'rules' => [
                [
                    'actions' => [
                        'signup',
                        'login'
                    ],
                    'allow' => true,
                    'roles' => ['?'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'index' => ['post'],
                'login-key' => ['get'],
                'get-inform' => ['get'],
                'signup' => ['post'],
                'login' => ['post'],
                'get-labels' => ['get'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }
    
    /**
     * site/login-key
     * get
     *
     * auth_key
     *
     * @return mixed
     */
    public function actionLoginKey()
    {
        $result = Yii::$app->user->identity->one_fields();
        $result['user']['auth_key'] = Yii::$app->user->identity->getAuthKey();
        return $result;
    }

    /**
     * site/login
     *
     * post
     *
     * phone
     * password
     *
     * @return $this|array|bool|null|static
     */

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post(), "")) {
            if ($model->login()) {
                $result = Yii::$app->user->identity->one_fields();
                $result['user']['auth_key'] = Yii::$app->user->identity->getAuthKey();
                return $result;

            } else {
                return ['error' => Yii::t('msg/error', 'Invalid login or password')];
            }
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**get all schools and sities for registration
     * site/get-inform
     * get
     *
     * @return array
     */
    public function actionGetInform()
    {
        if (Yii::$app->request->get('sity_name')) {
            $model = new SchoolSearch();
            $schools = $model->searchAll(Yii::$app->request->get());
            return $schools ?
                [
                    'schools' => $model->all_fields($schools),
                ]
                : $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    public function actionGetLabels()
    {
        if (Yii::$app->request->get('model')) {
            $model = "common\\models\\" . Yii::$app->request->get('model');


            return ['label' => @$model::attributeLabels()];
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * site/signup
     * post
     *
     * phone +
     * password +
     * email +
     * first_name +
     * second_name
     * last_name +
     * image_file
     *
     * role_id + [1-5]
     *
     * if role = [1,2]
     *
     * school_id +
     * sity_id +
     *
     *
     * @return array|User|null
     */

    public function actionSignup()
    {
        $model = new User();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->signup() && !$model->getErrors()) {
                $trans->commit();
                if (Yii::$app->getUser()->login($model)) {
                    $result = User::findOne(Yii::$app->user->id)->one_fields();
                    $result['auth_key'] = Yii::$app->user->identity->getAuthKey();
                    return $result;
                }
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }


    /**
     * site/forgot-password
     *   post
     * phone
     *
     * @return array|string
     */

    public function actionForgotPassword()
    {
        $request = Yii::$app->request->post();
        if ($request['phone']) {
            $user = User::findByPhone($request['phone']);
            if ($user) {
                $new_pass = Yii::$app->security->generateRandomString(8);
                $user->setScenario('save');
                $user->setPassword($new_pass);

                if ($user->save()) {
                    if (CMail::ForgotPassword($user, $new_pass)) {
                        return ['message' => Yii::t('msg/message', 'Check your email for further instructions')];
                    } else {
                        return ['error' => Yii::t('msg/error', "Letter don't sent")];
                    }
                } else {
                    return Yii::$app->errors->modelError($user);
                }
            } else {
                return ['error' => Yii::t('msg/error', 'User not found')];
            }
        } else {
            return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
        }
    }

    public function actionCheck()
    {
        return User::lastColumn();
    }

}
