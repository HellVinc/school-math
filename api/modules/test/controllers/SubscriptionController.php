<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Subscription;
use common\models\search\SubscriptionSearch;


use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SubscriptionController implements the CRUD actions for Subscription model.
 */
class SubscriptionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Subscription models.
     * Subscription/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new SubscriptionSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Subscription model.
     * Subscription/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    public function actionCreate()
    {
        $model = new Subscription();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing Subscription model.
     * Subscription/update
     * post
     *
     * id
     * date_from
     * date_to
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Subscription model.
     * Subscription/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Subscription model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subscription the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subscription::findOne(['id' => $id, 'deleted' => Subscription::NOT_DELETED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
