<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Sity;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SityController implements the CRUD actions for Sity model.
 */
class SityController extends Controller
{
    //TODO
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Sity models.
     * sity/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        return Sity::searchModel(Yii::$app->request->get());
    }

    /**
     * sity/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $request = Yii::$app->request->get();
        if ($request['id']) {
            return $this->findModel($request['id']);
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Creates a new Sity model.
     *sity/create
     * post
     * name
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request->post();
        if ($request) {
            $model = new Sity();
            if ($model->load($request, "")) {
                return $model->saveModel();
            }
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Updates an existing Sity model.
     *sity/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {

        $request = Yii::$app->request->post();
        if ($request['id']) {
            $model = $this->findModel($request['id']);
            if ($model->load($request, "")) {
                return $model->saveModel();
            }
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Deletes an existing Sity model.
     *
     * sity/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $request = Yii::$app->request->getBodyParams();
        if ($request['id']) {
            $model = $this->findModel($request['id']);
            $model->deleted = Sity::DELETED;
            return $model->saveModel();

        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Finds the Sity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sity::findOne(['id' => $id, 'deleted' => Sity::NOT_DELETED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
