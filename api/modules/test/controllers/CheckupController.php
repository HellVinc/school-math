<?php

namespace api\modules\v1\controllers;


use Yii;
use common\models\Checkup;
use common\models\Control;
use common\models\search\CheckupSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * CheckupController implements the CRUD actions for Checkup model.
 */
class CheckupController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
                'lowest-rating'
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
                'lowest-rating'
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['tutor'],

                ],
                [
                    'actions' => [

                        'lowest-rating'
                    ],
                    'allow' => true,
                    'roles' => ['curator'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'lowest-rating' => ['get'],
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * выбираем записи для проверки репетиторов с низким рейтингом
     * checkup/lowest-rating
     * get
     *
     * @return array
     */
    public function actionLowestRating()
    {
        return Checkup::searchLowest(Yii::$app->request->get());
    }

    /**
     * Lists all Checkup models.
     * Checkup/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new CheckupSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Checkup model.
     * checkup/one
     * get
     *
     * id
     *
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionOne()
    {
        $checkup = $this->findModel(Yii::$app->request->get('id'));
        $done_task = $checkup->doneTask;
        $task = $done_task->task;
        return [
            'task' => $task,
            'done_task' => $done_task,
            'checkup' => $checkup->one_fields()
        ];

    }


    /**
     * Creates a new Checkup model.
     * Checkup/create
     * post
     *
     * done_task_id
     * text+
     * date
     *
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Checkup();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            //проверяем рейтинг репетитора для изменения статуса исполененого задания
            if ($model->checkByRating() && !$model->getErrors()) {
                //если в процессе возникли ошибки
                return $model;
            }
        }
        return $model->getErrors();

    }

    /**
     * Updates an existing Checkup model.
     * Checkup/update
     * post
     *
     * id +
     *
     * done_task_id
     * text+
     *
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            //проверяем рейтинг репетитора для изменения статуса исполененого задания
            if ($model->checkByRating() && !$model->getErrors()) {
                //если в процессе возникли ошибки
                return $model;
            }
        }
        return $model->getErrors();

    }

    /**
     * Deletes an existing Checkup model.
     * Checkup/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Checkup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Checkup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Checkup::findOne($id)) !== null) {
            if ($model->deleted == Checkup::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
