<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Theme;
use common\models\search\ThemeSearch;


use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ThemeController implements the CRUD actions for Theme model.
 */
class ThemeController extends Controller
{

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Theme models.
     * theme/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new ThemeSearch();
        $result = $model->searchAll(Yii::$app->request->get());

        return [
            'section' => $model->section,
            'theme' => $result ? $model->all_fields($result) : $model->getErrors()
        ];


    }

    /**
     * Displays a single Theme model.
     * theme/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $this->findModel(Yii::$app->request->get('id'));
    }

    /**
     * Creates a new Theme model.
     * theme/create
     * post
     * name
     *
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new Theme();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing Theme model.
     * theme/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Deletes an existing Theme model.
     * theme/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
    }

    /**
     * Finds the Theme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null) {
            if ($model->deleted == Theme::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
