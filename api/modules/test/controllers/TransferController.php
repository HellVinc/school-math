<?php

namespace api\modules\v1\controllers;

use common\models\Subscription;
use Yii;
use common\models\Transfer;


use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * TransferController implements the CRUD actions for Transfer model.
 */
class TransferController extends Controller
{
    //TODO

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Transfer models.
     * Transfer/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        return Transfer::searchModel(Yii::$app->request->get());
    }

    /**
     * Displays a single Transfer model.
     * Transfer/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $request = Yii::$app->request->get();
        if ($request['id']) {
            $result  = $this->findModel($request['id']);
            return $result;
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Creates a new Transfer model.
     * Transfer/create
     * post
     *
     * user_id
     * summa
     * type
     *
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request->post();
        if ($request) {
            $model = new Transfer();
            if ($model->load($request, "")) {
                $trans = Yii::$app->db->beginTransaction();
                $result = $model->saveModel();
                if($result['error']){
                    $trans->rollback();
                    return  $result;
                }
                if($request['type'] == Transfer::SUBSCRIBTION){
                    $result = Subscription::saveModel($model);
                    if($result['error']){
                        $trans->rollback();
                        return  $result;
                    }
                }
                $trans->commit();
                return $model;
            }
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Updates an existing Transfer model.
     * Transfer/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        //TODO
        $request = Yii::$app->request->post();
        if ($request['id']) {
            $model = $this->findModel($request['id']);
            if ($model->load($request, "")) {
                return $model->saveModel();
            }
        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Deletes an existing Transfer model.
     * Transfer/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        //TODO
        $request = Yii::$app->request->getBodyParams();
        if ($request['id']) {
            $model = $this->findModel($request['id']);
            $model->deleted = Transfer::DELETED;
            return $model->saveModel();

        }
        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
    }

    /**
     * Finds the Transfer model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Transfer the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Transfer::findOne(['id' => $id, 'deleted' => Transfer::NOT_DELETED])) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
