<?php

namespace api\modules\v1\controllers;

use common\models\BookDiscipline;
use common\models\search\BookSearch;
use Yii;
use common\models\Book;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'free' => ['get'],
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all free Book models.
     * book/free
     * get
     *
     * discipline_id
     *
     * @return mixed
     */

    public function actionFree()
    {
        if (Yii::$app->request->get('discipline_id')) {
            $model = new BookSearch();
            $result = $model->searchFree(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Lists all Book models.
     * book/all
     * get
     *
     * discipline_id
     *
     * @return mixed
     */

    public function actionAll()
    {
        $model = new BookSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Book model.
     * book/one
     * get
     * id
     *
     * @return mixed
     */

    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        return [
            'book' => $model->one_fields(),
            'label' => @$model->attributeLabels()
        ];
    }

    /**
     * Creates a new Book model.
     *
     * book/create
     * post
     *
     * name+
     * author+
     * class+
     * year+
     * edition+
     * notes
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing Book model.
     * book/update
     * post
     *
     * id
     * name+
     * author+
     * class+
     * year+
     * edition+
     * notes
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Deletes an existing Book model.
     * book/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            if ($model->deleted == Book::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
