<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\File;
use common\models\Task;
use common\models\search\TaskSearch;
use common\models\UploadFile;

use yii\web\UploadedFile;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

use yii\helpers\FileHelper;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{

    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
                'delete-file',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [

                        'one',
                        'create',
                        'update',
                        'delete',
                        'delete-file',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'add-file' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
                'delete-file' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Task models.
     * Task/all
     * get
     *
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new TaskSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return [
            'theme' => $model->theme,
            'task' => $result ? $model->all_fields($result) : $model->getErrors()
        ];
    }

    /**
     * Displays a single Task model.
     * Task/one
     * get
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        return [
            'task' => $model->one_fields(),
            'label' => $model->attributeLabels()
        ];
    }

    /**
     * Creates a new Task model.
     * Task/create
     * post
     *
     * class+
     * level_id+
     * theme_id+
     * text+
     * result
     * explanation
     * file
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();
        $trans = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            $trans->commit();
            return $model;
        }
        $trans->rollback();
        $model->getErrors();

    }

    /**
     * Updates an existing Task model.
     * Task/update
     * post
     *
     * id +
     * class+
     * level_id+
     * theme_id+
     * text+
     * result
     * explanation
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            $trans->commit();
            return $model;
        }
        $trans->rollback();
        $model->getErrors();
    }

    /**
     * Deletes an existing Task model.
     * Task/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->remove() && !$model->getErrors()) {
            $trans->commit();
            return true;
        }
        $trans->rollback();
        $model->getErrors();
    }

    /**
     * Deletes file an existing Task model.
     * Task/delete-file
     * delete
     *
     * id
     * @return array|bool
     * @throws \yii\base\ErrorException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionDeleteFile()
    {
        $file = File::findOne(['id' => Yii::$app->request->getBodyParams('id'), 'deleted' => File::NOT_DELETED]);
        if ($file){
            if($file->remove() && !$file->getErrors() ) {
                unlink($file->fileDir);
                return true;
            }
            return $file->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            if ($model->deleted == Task::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
