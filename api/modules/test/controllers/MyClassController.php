<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\MyClass;
use common\models\search\MyClassSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * MyClassController implements the CRUD actions for MyClass model.
 */
class MyClassController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'create',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'create',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'create',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'create' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all MyClass models.
     * my-class/all
     * get
     * school_id
     * number
     * letter
     *
     * discipline_id
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new MyClassSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }


    /**
     * Creates a new MyClass model.
     * my-class/create
     * post
     *
     * user_id+
     * number
     * letter
     *
     * /**
     * @return array|bool
     */
    public function actionCreate()
    {
        $model = new MyClass();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->saveModel() && !$model->getErrors()) {
                return $model->one_fields();
            }
        }
        return $model->getErrors();

    }


    /**
     * Deletes an existing MyClass model.
     * my-class/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->user_id == Yii::$app->user->id) {
            if ($model->remove() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Finds the MyClass model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyClass the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyClass::findOne($id)) !== null) {
            if ($model->deleted == MyClass::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
