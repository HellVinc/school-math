<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Level;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;
/**

/**
 * LevelController implements the CRUD actions for Level model.
 */
class LevelController extends Controller
{
    //TODO
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Level models.
     * level/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        return Level::searchModel(Yii::$app->request->get());
    }

//    /**
//     * Displays a single Level model.
//     * level/one
//     * get
//     * id
//     *
//     * @return mixed
//     */
//    public function actionOne()
//    {
//        $request = Yii::$app->request->get();
//        if ($request['id']) {
//            return $this->findModel($request['id']);
//        }
//        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
//    }
//
//    /**
//     * Creates a new Level model.
//     * level/create
//     * post
//     *
//     * name+
//     * @return mixed
//     */
//    public function actionCreate()
//    {
//        $request = Yii::$app->request->post();
//        if ($request) {
//            $model = new Level();
//            if ($model->load($request, "")) {
//                return $model->saveModel();
//            }
//        }
//        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
//    }
//
//    /**
//     * Updates an existing Level model.
//     * level//update
//     * post
//     *
//     * book_id+
//     * name+
//     * @return mixed
//     */
//    public function actionUpdate()
//    {
//        $request = Yii::$app->request->post();
//        if ($request['id']) {
//            $model = $this->findModel($request['id']);
//            if ($model->load($request, "")) {
//                return $model->saveModel();
//            }
//        }
//        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
//    }
//
//    /**
//     * Deletes an existing Level model.
//     * level/delete
//     * delete
//     *
//     * id
//     * @return mixed
//     */
//    public function actionDelete()
//    {
//        $request = Yii::$app->request->getBodyParams();
//        if ($request['id']) {
//            $model = $this->findModel($request['id']);
//            $model->deleted = Level::DELETED;
//            return $model->saveModel();
//
//        }
//        return ['error' =>  Yii::t('msg/error','Error. Bad request.')];
//    }
//
//    /**
//     * Finds the Level model based on its primary key value.
//     * If the model is not found, a 404 HTTP exception will be thrown.
//     * @param integer $id
//     * @return Level the loaded model
//     * @throws NotFoundHttpException if the model cannot be found
//     */
//    protected function findModel($id)
//    {
//        if (($model = Level::findOne(['id' => $id, 'deleted' => Level::NOT_DELETED])) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested page does not exist.');
//        }
//    }
}
