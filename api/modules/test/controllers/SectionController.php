<?php

namespace api\modules\v1\controllers;

use common\models\Book;
use common\models\Section;
use common\models\search\SectionSearch;


use Yii;
use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SectionController implements the CRUD actions for Section model.
 */
class SectionController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Section models.
     * section/all
     * get
     *
     * book_id
     * @return mixed
     */
    public function actionAll()
    {
        $model = new SectionSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? [
            'book' => $model->book,
            'section' => $model->all_fields($result)
        ]
            : $model->getErrors();
    }

    /**
     * Displays a single Section model.
     * section/one
     * get
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Section model.
     *
     * section/create
     * post
     *
     * book_id+
     * name+
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Section();
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing Section model.
     * section/update
     * post
     *
     * book_id+
     * name+
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Deletes an existing Section model.
     * section/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Section model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Section the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Section::findOne($id)) !== null) {
            if ($model->deleted == Section::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
