<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\DoneTask;
use common\models\search\DoneTaskSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * DoneTaskController implements the CRUD actions for DoneTask model.
 */
class DoneTaskController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['tutor'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all DoneTask models.
     * DoneTask/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new DoneTaskSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single DoneTask model.
     * DoneTask/one
     * get
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
       return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new DoneTask model.
     * DoneTask/create
     * post
     *
     * task_id
     * status+
     * result
     * date
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new DoneTask();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing DoneTask model.
     * DoneTask/update
     * post
     *
     * id +
     *
     * task_id
     * status+
     * result
     * date
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing DoneTask model.
     * DoneTask/delete
     * delete
     *
     * id
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the DoneTask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return DoneTask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DoneTask::findOne($id)) !== null) {
            if ($model->deleted == DoneTask::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
