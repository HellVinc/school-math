<?php

namespace api\modules\v1\controllers;

use common\models\MyDiscipline;
use common\models\search\UserSearch;
use Yii;
use common\models\User;
use common\models\Role;
use common\models\MyClass;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'all-for-management',
                'all',
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all-for-management',
                'all',
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all-for-management',
                        'all',
                        'one',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',

                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all-for-management' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all User models.
     * user/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new UserSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * user/all-for-management
     * get
     * @return array
     */

    public function actionAllForManagement()
    {
        $model = new UserSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->for_management_fields($result) : $model->getErrors();
    }


    /**
     * user/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        $user = $this->findModel(Yii::$app->request->get('id'))->one_fields();
        if ($user->role_id == Role::LEARNER && $user->checkSubscription()) {
            $result['user']['subscription'] = $user->subscription;
            $result['user']['balance'] = $user->checkBalance();
        }
        if ($user->myDisciplines) {
            $result['user']['discipline'] = $user->myDisciplines;
        }
        return $result;

    }

//    /**
//     * Creates a new user model.
//     *user/create
//     * post
//     *
//     * phone +
//     * email +
//     * first_name +
//     * second_name
//     * last_name +
//     * image_file
//     *
//     * role_id + [1-5]
//     *
//     *if role = [1,2]
//     *
//     * school_id +
//     * sity_id +
//     * class +
//     *
//     * $request['my_classes'] = [
//     * '0' => [
//     * 'number' => 5,
//     * 'letter' => 'A'
//     * ] ,
//     * '1' => [
//     * 'number' => 3,
//     * 'letter' => 'A'
//     * ]
//     * ];
//     * $request['my_discipline'] = [
//     * '0' => [
//     * 'discipline_id' => '1'
//     * 'teacher' => 'sdfc'
//     * ]
//     * ];
//     * @return mixed
//     */
//    public function actionCreate()
//    {
//
//        $model = new User();
//        if ($model->load(Yii::$app->request->post())) {
//            $trans = Yii::$app->db->beginTransaction();
//
//            //TODO random password
//            $model->password = '11111111';
//
//            if ($model->signup() && !$model->getErrors()) {
//
//                //если заданы класы для юзера
//                if (Yii::$app->request->post('my_classes')) {
//
//                    $save_class = MyClass::saveClasses($model->id, Yii::$app->request->post('my_classes'));
//                    if ($save_class->getErrors()) {
//                        $trans->rollback();
//                        return $save_class->getErrors();
//                    }
//                }
//                //если заданы дисциплины для юзера
//                if (Yii::$app->request->post('my_discipline')) {
//                    $save_discipline = MyDiscipline::saveDiscipline($model->id, Yii::$app->request->post('my_discipline'));
//                    if ($save_discipline->getErrors()) {
//                        $trans->rollback();
//                        return $save_discipline->getErrors();
//                    }
//                }
//
//                $trans->commit();
//                return $model->one_fields();
//            }
//
//            $trans->rollback();
//        }
//        return $model->getErrors();
//    }

    /**
     * Updates an existing User model.
     * user/update
     * post
     *
     * id
     *
     *
     *  * $request['my_classes'] = [
     * '0' => [
     * 'number' => 5,
     * 'letter' => 'A'
     * ] ,
     * '1' => [
     * 'number' => 3,
     * 'letter' => 'A'
     * ]
     * ];
     * @return mixed
     */
    public function actionUpdate()
    {

        $model = $this->findModel(Yii::$app->request->post('id'));


//        unset(Yii::$app->request->post('photo'));

        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && !$model->getErrors()) {
                //если заданы класы для юзера
                if (Yii::$app->request->post('classes')) {

                    $save_class = MyClass::saveClasses($model->id, Yii::$app->request->post('classes'));
                    if ($save_class->getErrors()) {
                        $trans->rollback();
                        return $save_class->getErrors();
                    }
                }
                //если заданы дисциплины для юзера
                if (Yii::$app->request->post('discipline')) {
                    $save_discipline = MyDiscipline::saveDiscipline($model->id, Yii::$app->request->post('discipline'));
                    if ($save_discipline->getErrors()) {
                        $trans->rollback();
                        return $save_discipline->getErrors();
                    }
                }

                $trans->commit();
                return $model->one_fields();
            }

            $trans->rollback();
        }
        return $model->getErrors();
    }

//    /**
//     * Deletes an existing User model.
//     *
//     * user/deleted
//     * delete
//     *
//     * id
//     *
//     * @return mixed
//     */
//    public function actionDelete()
//    {
//        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
//
//        if ($model->remove() && !$model->getErrors()) {
//            return true;
//        }
//        return $model->getErrors();
//
//    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyClass::findOne($id)) !== null) {
            if ($model->deleted == MyClass::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');

        }
    }
}
