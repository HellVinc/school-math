<?php

namespace api\modules\v3\controllers;

use common\models\Book;
use Yii;
use common\models\english\Translate;
use common\models\english\search\TranslateSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * TranslateController implements the CRUD actions for Translate model.
 */
class TranslateController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                QueryParamAuth::className(),
            ],
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                        'all',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],
            ],
        ];
        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'template' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['post'],
            ],
        ];
        return $behaviors;
    }

    /**
     * Lists all Translate models.
     * translate/all
     * get
     *
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new TranslateSearch();
        $model->user_id = Yii::$app->user->id;
        $arr = [];
        foreach (range('a', 'z') as $letter) {
            $model->en_letter = $letter;
            $result = $model->searchAll();
            if ($result['models']) {
                $arr[$letter] = $model->all_fields($result);
            }
        }
        return $arr ? ['dictionary' => $arr]
            : ['dictionary' => false];
    }

    /**
     * translate/template
     * get
     *
     * book_id
     * page
     *
     * @return bool|string
     * @throws NotFoundHttpException
     */

    public function actionTemplate()
    {
        $dir = dirname(Yii::getAlias('@app')) . "/book_template/" . Yii::$app->request->get('book_id');
        if (file_exists($dir)) {
            $count_page = count(glob($dir . "/*", GLOB_NOSORT));
            $book = Book::findOne(Yii::$app->request->get('book_id'));
            $result ['header'] = $book->name;
            $result ['count_page'] = $count_page;

            for ($i = 0; $i < $count_page; $i++) {
                $path_file = $dir;
                $num = $i+1;
                $path_file .= "/" . $num. ".html";
                $result['content'][$i] = file_get_contents($path_file);
            }
//            for ($i = 2; $i <= 500; $i++) {
//                $path_file = $dir;
//                $path_file .= "\\1.html";
//                $newfile = $dir."\\".$i.'.html';
//
//                if (!copy($path_file, $newfile)) {
//                    echo "не удалось скопировать $path_file...\n";
//                }
//            }
            return $result;
        } else {
            throw new NotFoundHttpException('The requested file does not exist.');
        }
    }

    /**
     * Displays a single Translate model.
     * Translate/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Translate model.
     * user-dictionary/create
     * post
     *
     * @property integer $translate_id+
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Translate();
        $model->user_id = Yii::$app->user->id;
        if ($model->load(Yii::$app->request->post()) && $model->saveWithCheck() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }


    /**
     * Deletes an existing Translate model.
     * user-dictionary/delete
     * delete
     *
     * @property integer $id +
     * @return array|bool
     * @throws NotFoundHttpException
     */
    public function actionDelete()
    {
        $model = new Translate;
        $model->user_id = Yii::$app->user->id;
        $model->translate_id = Yii::$app->request->post('translate_id');
        $result = $model->findModel();
        if ($result) {
            if ($result->remove() && !$result->getErrors()) {
                return true;
            }
            return $model->getErrors();
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }

    }

    /**
     * Finds the Translate model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Translate the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Translate::findOne($id)) !== null) {
            if ($model->deleted == Translate::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
