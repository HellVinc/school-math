<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Task;
use common\models\search\TaskSearch;


use yii\base\Theme;
use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Task models.
     * Task/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new TaskSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        $section = \common\models\Theme::findOne(Yii::$app->request->get('theme_id'));
        return $result ? [
            'tasks' => $model->all_fields($result),
            'topic' => $section
            ]
        : $model->getErrors();
    }

    /**
     * Displays a single Task model.
     * Task/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
       return [
           'task'=>$model->one_fields(),
           'label' =>@$model::attributeLabels()];
    }

    /**
     * Creates a new Task model.
     * Task/create
     * post
     *
     * @property integer $class+
     * @property integer $level_id+
     * @property integer $theme_id+
     * @property string $type+
     * @property string $text+
     * @property string $number
     * @property string $sheet
     * @property double $result
     * @property string $explanation
     * @property integer $push_status
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Task();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            $model->text = str_replace('<br></span>', ' ', Yii::$app->request->post('text'));
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Task model.
     * Task/update
     * post
     *
     * @property integer $id +
     * @property integer $class +
     * @property integer $level_id +
     * @property integer $theme_id +
     * @property string $type+
     * @property string $text+
     * @property string $number
     * @property string $sheet
     * @property double $result
     * @property string $explanation
     * @property integer $push_status
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();


            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Task model.
     * Task/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && $model->removeFiles() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            if ($model->deleted == Task::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
