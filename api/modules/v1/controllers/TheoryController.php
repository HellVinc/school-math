<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Theory;
use common\models\search\TheorySearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * TheoryController implements the CRUD actions for Theory model.
 */
class TheoryController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Theory models.
     * Theory/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new TheorySearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Theory model.
     * Theory/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
       return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Theory model.
     * Theory/create
     * post
     *
     * @property string $table +
     * @property integer $row_id +
     * @property string $title +
     * @property string $text +
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Theory();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Theory model.
     * Theory/update
     * post
     *
     * @property integer $id +
     * @property string $table
     * @property integer $row_id
     * @property string $title
     * @property string $text
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Theory model.
     * Theory/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && $model->removeFiles() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Theory model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theory the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theory::findOne($id)) !== null) {
            if ($model->deleted == Theory::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
