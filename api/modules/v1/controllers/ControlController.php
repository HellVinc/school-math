<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Control;
use common\models\search\ControlSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ControlController implements the CRUD actions for Control model.
 */
class ControlController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['curator'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Control models.
     * Control/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new ControlSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Control model.
     * Control/one
     * get
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionOne()
    {
        return $model = $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Control model.
     * Control/create
     * post
     *
     * @property integer $checkup_id +
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Control();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //проверяем рейтинг репетитора для изменения статуса исполененого задания
                if ($model->status == Control::RIGHT && $model->checkup->checkByRating() && !$model->getErrors()) {
                    //если в процессе не  возникли ошибки
                    $trans->commit();
                    return $model->one_fields();
                }
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Control model.
     * Control/update
     * post
     *
     * @property integer $id +
     * @property integer $checkup_id +
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //проверяем рейтинг репетитора для изменения статуса исполененого задания
                if ($model->status == Control::RIGHT && $model->checkup->checkByRating() && !$model->getErrors()) {
                    //если в процессе не  возникли ошибки
                    $trans->commit();
                    return $model->one_fields();
                }
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Control model.
     * Control/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && $model->removeFiles() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Control model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Control the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Control::findOne($id)) !== null) {
            if ($model->deleted == Control::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
