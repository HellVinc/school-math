<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\MyPupil;
use common\models\search\MyPupilSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * MyPupilController implements the CRUD actions for MyPupil model.
 */
class ManagementController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'create',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'create',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'create',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'create' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all MyPupil models.
     * management/all
     * get
     *
     * @property integer $user_id+
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new MyPupilSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }


    /**
     * Creates a new MyPupil model.
     * my-Pupil/create
     * post
     *
     * @property integer $pupil_id+
     * @property integer $user_id+
     * @property integer $type+
     *
     * @return mixed
     */
    public function actionCreate()
    {
//        return Yii::$app->request->post();
        $model = new MyPupil();
        $model->type = 1;
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }


    /**
     * Deletes an existing MyPupil model.
     * my-Pupil/delete
     * delete
     *
     * @property integer $id+
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the MyPupil model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyPupil the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyPupil::findOne($id)) !== null) {
            if ($model->deleted == MyPupil::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
