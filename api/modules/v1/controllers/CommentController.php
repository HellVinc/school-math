<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Message;
use common\models\Comment;
use common\models\Role;
use common\models\search\CommentSearch;
use common\models\search\MessageSearch;


use yii\behaviors;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * CommentController implements the CRUD actions for Comment model.
 */
class CommentController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',

                    ],
                    'allow' => true,
                    'roles' => ['@'],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'all-message' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
            ],
        ];

        return $behaviors;
    }

    public function beforeAction($action)
    {
        \Yii::$app->language = Yii::$app->request->get('lang');
        return parent::beforeAction($action);
    }

    /**
     * Lists all Comment models.
     * Comment/all-message
     * get
     * comment_id
     *
     * @return mixed
     */
    public function actionAllMessage()
    {
        Message::changeStatus(Yii::$app->request->get('comment_id'));
        $searchModel = new MessageSearch();
        if ($searchModel->load(['MessageSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {
            $dataProvider = $searchModel->search();
            $models = $dataProvider->getModels();
            return [
                'models' => ArrayHelper::toArray($models, [
                    'common\models\Message' => [
                        'id',
                        'comment_id',
                        'text',
                        'send_from',
                        'sender' => function ($model) {
                            return $model->sender->first_name . " " . $model->sender->last_name;
                        },
                        'send_to',
                        'receiver' => function ($model) {
                            if (isset($model->receiver->first_name)){
                                return $model->receiver->first_name . " " . $model->receiver->last_name;
                            }
                            return '';
                        },
                        'role_sender' => function ($model) {
                            /** @var $model Message */
                            return $model->sender->role->name;
                        },
                        'role_receiver' => function ($model) {
                            /** @var $model Message */
                            if (isset($model->receiver->role->name)){
                                return $model->receiver->role->name;
                            }else{
                                return ' ';
                            }
                        },
                        'time',
                        'files',
                        'avatar_sender' => function ($model) {
                            /** @var $model Message */
                            return $model->sender->photoPath;
                        },
                        'avatar_reciever' => function ($model) {
                            /** @var $model Message */
                            if (isset($model->receiver->photoPath)){
                                return $model->receiver->photoPath;
                            }else{
                                return Yii::$app->request->getHostInfo() . "/photo/users/empty.jpg";
                            }
                        },
                        'type',
                        'task_id' => function($model) {
                            /** @var $model Message */
                            return $model->comment->task_id;
                        }
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
    }

    /**
     * comment/all
     * get
     * task_id
     * @return array|bool
     */

    public function actionAll()
    {
        $searchModel = new MessageSearch();
        if ($searchModel->load(['MessageSearch' => Yii::$app->request->get()]) && $searchModel->validate()) {
            $dataProvider = $searchModel->search();
            $models = $dataProvider->getModels();
            return [
                'models' => ArrayHelper::toArray($models, [
                    'common\models\Message' => [
                        'id',
                        'comment_id',
                        'text',
                        'send_from',
                        'sender' => function ($model) {
                            return $model->sender->first_name . " " . $model->sender->last_name;
                        },
                        'send_to',
                        'receiver' => function ($model) {
                            if (isset($model->receiver->first_name)){
                                return $model->receiver->first_name . " " . $model->receiver->last_name;
                            }
                            return '';
                        },
                        'role_sender' => function ($model) {
                            /** @var $model Message */
                            return $model->sender->role->name;
                        },
                        'role_receiver' => function ($model) {
                            /** @var $model Message */
                            if (isset($model->receiver->role->name)){
                                return $model->receiver->role->name;
                            }else{
                                return ' ';
                            }
                        },
                        'time',
                        'files',
                        'avatar_sender' => function ($model) {
                            /** @var $model Message */
                            return $model->sender->photoPath;
                        },
                        'avatar_reciever' => function ($model) {
                            /** @var $model Message */
                            if (isset($model->receiver->photoPath)){
                                return $model->receiver->photoPath;
                            }else{
                                return Yii::$app->request->getHostInfo() . "/photo/users/empty.jpg";
                            }
                        },
                        'type',
                        'task_id' => function($model) {
                            /** @var $model Message */
                            return $model->comment->task_id;
                        }
                    ]
                ]),
                'count' => $dataProvider->query->count(),
                'currentPage' => $dataProvider->pagination->page,
                'pagesCount' => $dataProvider->pagination->pageCount
            ];
        }
//        $result = $model->searchAll(Yii::$app->request->get());
//        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Comment model.
     * Comment/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Comment model.
     * Comment/create
     * post
     *
     * @property integer $task_id +
     * @property integer $comment_id
     * @property string $send_to +
     * @property string $text +
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Message();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
            return $model->one_fields();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Comment model.
     * Comment/update
     * post
     *
     * @property integer $id +
     * @property integer $task_id +
     * @property double $result
     * @property integer $status +
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->created_by == Yii::$app->user->id) {
            if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
                return $model->one_fields();
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Deletes an existing Comment model.
     * Comment/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->created_by == Yii::$app->user->id) {
            if ($model->remove() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }


    public function actionDeleteMessage()
    {

        $model = Message::findOne(Yii::$app->request->getBodyParams('id'));
        if ($model->deleted == Message::NOT_DELETED) {
            if ($model->send_from == Yii::$app->user->id) {
                if ($model->remove() && !$model->getErrors()) {
                    return true;
                }
                return $model->getErrors();
            }
            return ['error' => Yii::t('msg/error', 'Changes not saved')];
        } else {
            throw new NotFoundHttpException('The record was archived.');
        }

    }

    /**
     * Finds the Comment model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Comment the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Comment::findOne($id)) !== null) {
            if ($model->deleted == Comment::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
