<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\MyDiscipline;
use common\models\search\MyDisciplineSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * MyDisciplineController implements the CRUD actions for MyDiscipline model.
 */
class MyDisciplineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all MyDiscipline models.
     * my-discipline/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new MyDisciplineSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single MyDiscipline model.
     * my-discipline/one
     * get
     *
     * @property integer $id+
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new MyDiscipline model.
     * my-discipline/create
     * post
     *
     * @property integer $user_id+
     * @property integer $discipline_id+
     * @property string $teacher
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MyDiscipline();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_id = Yii::$app->user->id;
            if ($model->saveWithCheckAndRestore() && !$model->getErrors()) {
                return $model;
            }
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing MyDiscipline model.
     * my-discipline//update
     * post
     *
     * @property integer $id+
     * @property integer $user_id+
     * @property integer $discipline_id+
     * @property string $teacher
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->user_id == Yii::$app->user->id) {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->saveWithCheckAndRestore() && !$model->getErrors()) {
                    return $model->one_fields();
                }
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Deletes an existing MyDiscipline model.
     * my-discipline/delete
     * delete
     *
     * @property integer $id+
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->user_id == Yii::$app->user->id) {
            if ($model->remove() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Finds the MyDiscipline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyDiscipline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = MyDiscipline::findOne($id)) !== null) {
            if ($model->deleted == MyDiscipline::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
