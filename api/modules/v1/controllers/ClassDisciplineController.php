<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\ClassDiscipline;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ClassDisciplineController implements the CRUD actions for ClassDiscipline model.
 */
class ClassDisciplineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'create',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'create',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'create',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'create' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }


    /**
     * Creates a new ClassDiscipline model.
     * Class-discipline/create
     * post
     *
     * @property integer $discipline_id+
     * @property integer $class+
     *
     * @return mixed
     */
    public function actionCreate()
    {

        $model = new ClassDiscipline();
        if ($model->load(Yii::$app->request->post()) && $model->saveWithCheck() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing ClassDiscipline model.
     * Class-discipline/delete
     * delete
     * @property integer $id+
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the ClassDiscipline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ClassDiscipline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ClassDiscipline::findOne($id)) !== null) {
            if ($model->deleted == ClassDiscipline::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
