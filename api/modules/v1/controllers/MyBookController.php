<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\MyBook;
use common\models\search\MyBookSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * MyBookController implements the CRUD actions for MyBook model.
 */
class MyBookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all MyBook models.
     * my-Book/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new MyBookSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single MyBook model.
     * my-Book/one
     * get
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new MyBook model.
     * my-Book/create
     * post
     *
     * user_id+
     * number
     * letter
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MyBook();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->saveModel() && !$model->getErrors()) {
                return $model->one_fields();
            }
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing MyBook model.
     * my-Book//update
     * post
     *
     * user_id+
     * number
     * letter
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->myDiscipline->user_id == Yii::$app->user->id) {
            if ($model->saveModel() && !$model->getErrors()) {
                return $model->one_fields();
            }
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Deletes an existing MyBook model.
     * my-Book/delete
     * delete
     *
     * id
     * @return mixed
     */
    public
    function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->myDiscipline->user_id == Yii::$app->user->id) {
            if ($model->remove() && !$model->getErrors()) {
                return true;
            }
            return $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Changes not saved')];
    }

    /**
     * Finds the MyBook model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MyBook the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected
    function findModel($id)
    {
        if (($model = MyBook::findOne($id)) !== null) {
            if ($model->deleted == MyBook::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
