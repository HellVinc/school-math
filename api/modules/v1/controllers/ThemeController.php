<?php

namespace api\modules\v1\controllers;

use common\models\Section;
use Yii;
use common\models\Theme;
use common\models\search\ThemeSearch;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * ThemeController implements the CRUD actions for Theme model.
 */
class ThemeController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Theme models.
     * Theme/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new ThemeSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        if(Yii::$app->request->get('section_id')){
            $section = Section::findOne(Yii::$app->request->get('section_id'));
        }
        return $result ? [
            'theme' => $model->all_fields($result),
            'section' => $section
            ]
            : $model->getErrors();
    }

    /**
     * Displays a single Theme model.
     * Theme/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
       return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Theme model.
     * Theme/create
     * post
     *
     * @property integer $class+
     * @property integer $level_id+
     * @property integer $theme_id+
     * @property string $type+
     * @property string $text+
     * @property string $number
     * @property string $sheet
     * @property double $result
     * @property string $explanation
     * @property integer $push_status
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Theme();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Theme model.
     * Theme/update
     * post
     *
     * @property integer $id +
     * @property integer $class +
     * @property integer $level_id +
     * @property integer $theme_id +
     * @property string $type+
     * @property string $text+
     * @property string $number
     * @property string $sheet
     * @property double $result
     * @property string $explanation
     * @property integer $push_status
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Theme model.
     * Theme/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Theme model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Theme the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Theme::findOne($id)) !== null) {
            if ($model->deleted == Theme::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
