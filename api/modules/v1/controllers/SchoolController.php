<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\School;
use common\models\search\SchoolSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SchoolController implements the CRUD actions for School model.
 */
class SchoolController extends Controller
{
    /**
     * @inheritdoc
     */
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'one',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],
                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all School models.
     * school/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new SchoolSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single School model.
     * school/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new School model.
     * school/create
     * post
     *
     * @property string $name +
     * @property integer $sity_id +
     * or
     * sity_name
     * region_name
     *
     * @property integer $house
     * @property integer $street
     * @property integer $phone
     * @property string $email
     * @property integer $director
     * @property integer $notes
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new School();
        $trans = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            $trans->commit();
            return $model;
        }
        $trans->rollback();
        return $model->getErrors();
    }

    /**
     * Updates an existing School model.
     * school/update
     * post
     *
     * @property integer $id +
     * @property integer $sity_id +
     * or
     * sity_name
     * region_name
     *
     * @property integer $house
     * @property integer $street
     * @property integer $phone
     * @property string $email
     * @property integer $director
     * @property integer $notes
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        $trans = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && !$model->getErrors()) {
            $trans->commit();
            return $model;
        }
        $trans->rollback();
        return $model->getErrors();
    }

    /**
     * Deletes an existing School model.
     * school/delete
     * delete
     *
     * @property integer $id +

     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the School model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return School the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = School::findOne($id)) !== null) {
            if ($model->deleted == School::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
