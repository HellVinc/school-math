<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Subtask;
use common\models\search\SubtaskSearch;

use common\models\Theme;
use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SubtaskController implements the CRUD actions for Subtask model.
 */
class SubtaskController extends Controller
{

    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                        'all',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [

                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Subtask models.
     * Subtask/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new SubtaskSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        $section = Theme::findOne(Yii::$app->request->get('theme_id'));
        return $result ? [
            'tasks' => $model->all_fields($result),
            'topic' => $section
        ]
            : $model->getErrors();
    }

    /**
     * Displays a single Subtask model.
     * Subtask/one
     * get
     * @property integer $id +
     *
     * @return mixed
     */
    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        return [
            'subtask'=>$model->one_fields(),
            'label' =>@$model::attributeLabels()];
    }

    /**
     * Creates a new Subtask model.
     * Subtask/create
     * post
     *
     * @property integer $Subtask_id
     * @property string $text
     * @property string $result
     * @property integer $number
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Subtask();
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();
            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Subtask model.
     * Subtask/update
     * post
     *
     * @property integer $id 
     * @property integer $Subtask_id
     * @property string $text
     * @property string $result
     * @property integer $number
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post())) {
            $trans = Yii::$app->db->beginTransaction();


            if ($model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
                //если в процессе не возникло ошибок
                $trans->commit();
                return $model->one_fields();
            }
            $trans->rollback();
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Subtask model.
     * Subtask/delete
     * delete
     *
     * @property integer $id +
     * @return mixed
     */
    public function actionDelete()
    {

        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && $model->removeFiles() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Subtask model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Subtask the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Subtask::findOne($id)) !== null) {
            if ($model->deleted == Subtask::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
