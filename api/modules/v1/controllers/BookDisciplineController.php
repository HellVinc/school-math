<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\BookDiscipline;
use common\models\search\BookDisciplineSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookDisciplineController implements the CRUD actions for BookDiscipline model.
 */
class BookDisciplineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'create',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all BookDiscipline models.
     * book-discipline/all
     * get
     *
     * @property integer $discipline_id
     *
     * @return mixed
     */
    public function actionAll()
    {
        if (Yii::$app->request->get('discipline_id')) {
            $model = new BookDisciplineSearch();
            $result = $model->searchAll(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Creates a new BookDiscipline model.
     * book-discipline/create
     * post
     *
     * @property integer $book_id+
     * @property integer $discipline_id+
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BookDiscipline();
        if ($model->load(Yii::$app->request->post()) && $model->saveWithCheck() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing BookDiscipline model.
     * book-discipline/delete
     * delete
     *
     * @property integer $id+
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the BookDiscipline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BookDiscipline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = BookDiscipline::findOne($id)) !== null) {
            if ($model->deleted == BookDiscipline::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
