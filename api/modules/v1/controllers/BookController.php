<?php

namespace api\modules\v1\controllers;

use common\models\BookDiscipline;
use common\models\search\BookSearch;
use Yii;
use common\models\Book;


use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * BookController implements the CRUD actions for Book model.
 */
class BookController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'all',
                'one',
                'update',
                'create',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'all',
                'one',
                'create',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'all',
                        'one',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'free' => ['get'],
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all free Book models.
     * book/free
     * get
     *
     * discipline_id
     *
     * @return mixed
     */

    public function actionFree()
    {
        if (Yii::$app->request->get('discipline_id')) {
            $model = new BookSearch();
            $result = $model->searchFree(Yii::$app->request->get());
            return $result ? $model->all_fields($result) : $model->getErrors();
        }
        return ['error' => Yii::t('msg/error', 'Error. Bad request.')];
    }

    /**
     * Lists all Book models.
     * book/all
     * get
     *
     * discipline_id
     *
     * @return mixed
     */

    public function actionAll()
    {
        $model = new BookSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Book model.
     * book/one
     * get
     * @property integer $id+
     *
     * @return mixed
     */

    public function actionOne()
    {
        $model = $this->findModel(Yii::$app->request->get('id'));
        return [
            'book' => $model->one_fields(),
            'label' => $model->attributeLabels()
        ];
    }

    /**
     * Creates a new Book model.
     *
     * book/create
     * post
     *
     * @property string $name+
     * @property string $author+
     * @property integer $class+
     * @property integer $edition+
     * @property integer $year+
     * @property string $notes
     * @property string $paid_content
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Book();
        if ($model->load(Yii::$app->request->post()) && $model->saveModel() && $model->checkFiles() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Book model.
     * book/update
     * post
     * @property integer $id+
     * @property string $name+
     * @property string $author+
     * @property integer $class+
     * @property integer $edition+
     * @property integer $year+
     * @property string $notes
     * @property string $paid_content
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->saveModel()&& $model->checkFiles() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Book model.
     * book/delete
     * delete
     *
     * @property integer $id+
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Book model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Book the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Book::findOne($id)) !== null) {
            if ($model->deleted == Book::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
