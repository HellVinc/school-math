<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Sity;
use common\models\search\SitySearch;

use yii\web\NotFoundHttpException;
use yii\behaviors;

use yii\rest\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * SityController implements the CRUD actions for Sity model.
 */
class SityController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [
                'one',
                'update',
                'delete',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [
                'one',
                'update',
                'delete',
            ],
            'rules' => [
                [
                    'actions' => [
                        'one',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                    ],
                    'allow' => true,
                    'roles' => ['admin'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
            ],
        ];

        return $behaviors;
    }

    /**
     * Lists all Sity models.
     * sity/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new SitySearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * sity/one
     * get
     *
     * id
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Sity model.
     *sity/create
     * post
     * name
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sity();
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Updates an existing Sity model.
     *sity/update
     * post
     *
     * id
     * name*
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->save() && !$model->getErrors()) {
            return $model;
        }
        $model->getErrors();
    }

    /**
     * Deletes an existing Sity model.
     *
     * sity/deleted
     * delete
     *
     * id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {
            return true;
        }
        return $model->getErrors();
    }

    /**
     * Finds the Sity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sity::findOne($id)) !== null) {
            if ($model->deleted == Sity::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
