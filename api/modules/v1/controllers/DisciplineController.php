<?php

namespace api\modules\v1\controllers;

use Yii;
use common\models\Discipline;
use common\models\File;
use common\models\search\DisciplineSearch;

use yii\behaviors;
use yii\web\NotFoundHttpException;
use yii\rest\Controller;

use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\filters\auth\QueryParamAuth;
use yii\filters\auth\CompositeAuth;

/**
 * DisciplineController implements the CRUD actions for Discipline model.
 */
class DisciplineController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            'tokenParam' => 'auth_key',
            'only' => [

                'one',
                'create',
                'update',
                'delete',
                'delete-one-file',
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'only' => [

                'one',
                'create',
                'update',
                'delete',
                'delete-one-file',
            ],
            'rules' => [
                [
                    'actions' => [

                        'one',
                    ],
                    'allow' => true,
                    'roles' => [
                        'tutor',
                        'teacher',
                        'curator',
                        'manager',
                        'admin'
                    ],

                ],

                [
                    'actions' => [
                        'create',
                        'update',
                        'delete',
                        'delete-one-file',
                    ],
                    'allow' => true,
                    'roles' => ['manager'],

                ],
            ],
        ];

        $behaviors['verbFilter'] = [
            'class' => VerbFilter::className(),
            'actions' => [
                'all' => ['get'],
                'one' => ['get'],
                'create' => ['post'],
                'update' => ['post'],
                'delete' => ['delete'],
                'delete-one-file' => ['delete'],
            ],
        ];

        return $behaviors;
    }



    /**
     * Lists all Discipline models.
     * discipline/all
     * get
     *
     * @return mixed
     */
    public function actionAll()
    {
        $model = new DisciplineSearch();
        $result = $model->searchAll(Yii::$app->request->get());
        return ['a', 'b'];
        return $result ? $model->all_fields($result) : $model->getErrors();
    }

    /**
     * Displays a single Discipline model.
     * discipline/one
     * get
     * @property integer $id+
     *
     * @return mixed
     */
    public function actionOne()
    {
        return $this->findModel(Yii::$app->request->get('id'))->one_fields();
    }

    /**
     * Creates a new Discipline model.
     * discipline/create
     * post
     *
     * @property string $name+
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Discipline();
        if ($model->load(Yii::$app->request->post()) && $model->checkModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Updates an existing Discipline model.
     * discipline//update
     * post
     *
     * @property integer $id+
     * @property string $name+
     * @return mixed
     */
    public function actionUpdate()
    {
        $model = $this->findModel(Yii::$app->request->post('id'));
        if ($model->load(Yii::$app->request->post()) && $model->checkModel() && !$model->getErrors()) {
            return $model;
        }
        return $model->getErrors();
    }

    /**
     * Deletes an existing Discipline model.
     * discipline/delete-one-file
     * delete
     *
     * @property integer $id+
     * @return mixed
     */
    public function actionDelete()
    {
        $model = $this->findModel(Yii::$app->request->getBodyParams('id'));
        if ($model->remove() && !$model->getErrors()) {

            return true;
        }
        return $model->getErrors();
    }

    public function actionDeleteOneFile()
    {
        if ($model = File::findOne(Yii::$app->request->getBodyParams('id'))) {
            if ($model->remove() && !$model->getErrors()) {
                return unlink($model->fileDir);
            }
            return $model->getErrors();
        } else {
            throw new NotFoundHttpException('No such file or directory');
        }
    }

    /**
     * Finds the Discipline model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Discipline the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Discipline::findOne($id)) !== null) {
            if ($model->deleted == Discipline::NOT_DELETED) {
                return $model;
            } else {
                throw new NotFoundHttpException('The record was archived.');
            }
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}