<?php
return [
    'Error. Bad request.' => 'Помилка. Запит невірний.',
    "Letter don't sent" => 'Листа не відправлено',
    "User not found" => 'Користувач не знайдений',
    "Invalid login or password" => 'Невірний логін чи пароль',
    "Record was added before" => 'Запис був доданий раніше',
    "Image not uploaded" => 'Зображення не завантажено',
    'Changes not saved' => 'Зміни не були збережені',
];