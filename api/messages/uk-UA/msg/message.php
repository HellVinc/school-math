<?php
return [
    'Check your email for further instructions' => 'Перевірте свою електронну пошту для отримання подальших інструкцій',
    "Letter don't sent." => 'Листа не надіслано',
    'id' => 'Номер',
    'send_from' => 'Відправник',
    'send_to' => 'Отримувач',
    'text' => 'Текст повідомлення',
    'status' => 'Статус',
];