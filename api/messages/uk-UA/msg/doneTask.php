<?php
return [
    "ID" => 'Номер',
    'User ID' => '',
    'Result' => 'Результат',
    'answer' => 'Відповідь',
    'Status' => 'Перевірено',
    'Date create' => 'Дата створення',
    'Date update' => 'Дата редагування',
    'Deleted' => 'Видалено'
];