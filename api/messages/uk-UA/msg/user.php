<?php
return [
    'ID' => "ID",
    'Role' =>  "Роль",
    'Role ID' => "Роль",
    'Confirm' =>  "Підтвердження",
    'Balance' => "Баланс",
    'Email' =>  "Почта",
    'Phone' =>  "Телефон",
    'Password' =>  "Пароль",
    'First Name' =>  "Ім'я",
    'Second Name' =>  "По батькові",
    'Last Name' => "Прізвище",
    'Photo' =>  "Фото",
    'School' => "Школа",
    'City' =>  "Місто",
    'Class' =>  "Клас",
    'Teacher' => "Вчитель",
    'Discipline' =>  "Предмети",
    'My Classes' => "Мої класи"
];