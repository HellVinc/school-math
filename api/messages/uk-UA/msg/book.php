<?php
return [
    "ID" => 'Номер',
    'Name' => 'Назва',
    'Author' => 'Автор',
    'Class' => 'Клас',
    'Year' => 'Рік',
    'Edition' => 'Видавництво',
    'Notes' => 'Нотатки',
    'Deleted' => 'Видалено'
];