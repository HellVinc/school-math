<?php
return [
    "ID" => 'Номер',
    'User' => 'Користувач',
    'User ID' => '',
    'Discipline' => 'Предмет',
    'Discipline ID' => '',
    'Teacher' => 'Вчитель',
    'Deleted' => 'Видалено'
];