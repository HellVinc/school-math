<?php
return [
    "ID" => 'Номер',
    'Task ID' => '',
    'Text' => 'Текст',
    'Result' => 'Результат',
    'Number' => 'Номер',
    'Type' => 'Тип',
    'Created By' => 'Ким створено',
    'Created At' => 'Коли створено',
    'Deleted' => 'Видалено'
];