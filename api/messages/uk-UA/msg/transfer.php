<?php
return [
    "ID" => 'Номер',
    'User ID' => '',
    'Summa' => 'Сума',
    'Type' => 'Тип',
    'Data' => 'Дата',
    'Deleted' => 'Видалено',
    'The sum exceeds admissible' => "Сума перевищує допустиму"
];