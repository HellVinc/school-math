<?php
return [
    "ID" => 'Номер',
    'Creator ID' => '',
    'Class' => 'Клас',
    'Level ID' => '',
    'Theme ID' => '',
    'Type' => 'Тип',
    'Number' => 'Номер',
    'Page' => 'Старінка',
    'Text' => 'Текст',
    'Result' => 'Результат',
    'Explanation' => 'Пояснення',
    'Deleted' => 'Видалено'
];