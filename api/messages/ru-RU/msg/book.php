<?php
return [
    "ID" => 'Номер',
    'Name' => 'Назва',
    'Author' => 'Автор',
    'Year' => 'Год',
    'Edition' => 'Издательство',
    'Notes' => 'Заметки',
    'Deleted' => 'Удален'
];