<?php
return [
    "ID" => 'Номер',
    'Creator ID' => '',
    'Class' => 'Класс',
    'Level ID' => '',
    'Theme ID' => '',
    'Type' => 'Тип',
    'Number' => 'Номер',
    'Page' => 'Страница',
    'Text' => 'Текст',
    'Result' => 'Результат',
    'Explanation' => 'Обьяснение',
    'Deleted' => 'Удален'
];