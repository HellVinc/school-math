<?php
return [
    "ID" => 'Номер',
    'User ID' => '',
    'Result' => 'Результат',
    'Status' => 'Проверено',
    'Date create' => 'Дата создания',
    'Date update' => 'Дата изменения',
    'Deleted' => 'Удален'
];