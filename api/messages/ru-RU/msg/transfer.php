<?php
return [
    "ID" => 'Номер',
    'User ID' => '',
    'Summa' => 'Сумма',
    'Type' => 'Тип',
    'Data' => 'Дата',
    'Deleted' => 'Удален',
    'The sum exceeds admissible' => "Сумма превышает допустимую"
];