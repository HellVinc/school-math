<?php
namespace console\controllers;
use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll(); //удаляем старые данные


        //Включаем наш обработчик
        $rule = new UserRoleRule();
        $auth->add($rule);

        //Добавляем роли
        $admin = $auth->createRole('admin');
        $admin->description = 'Admin';
        $admin->ruleName = $rule->name;
        $auth->add($admin);

        //Добавляем роли
        $manager = $auth->createRole('manager');
        $manager->description = 'Manager';
        $manager->ruleName = $rule->name;
        $auth->add($manager);

        //Добавляем роли
        $curator = $auth->createRole('curator');
        $curator->description = 'Curator';
        $curator->ruleName = $rule->name;
        $auth->add($curator);

        //Добавляем роли
        $tutor = $auth->createRole('tutor');
        $tutor->description = 'Tutor';
        $tutor->ruleName = $rule->name;
        $auth->add($tutor);

        //Добавляем роли
        $teacher = $auth->createRole('teacher');
        $teacher->description = 'Teacher';
        $teacher->ruleName = $rule->name;
        $auth->add($teacher);

        //Добавляем роли
        $learner = $auth->createRole('learner');
        $learner->description = 'Learner';
        $learner->ruleName = $rule->name;
        $auth->add($learner);


        $auth->addChild($tutor, $learner);

    }
}
