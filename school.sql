-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Хост: db653899143.db.1and1.com
-- Время создания: Апр 25 2017 г., 14:30
-- Версия сервера: 5.5.54-0+deb7u2-log
-- Версия PHP: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `db653899143`
--
CREATE DATABASE IF NOT EXISTS `db653899143` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `db653899143`;

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE `book` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `class` tinyint(1) NOT NULL,
  `edition` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `notes` text,
  `paid_content` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`id`, `name`, `author`, `class`, `edition`, `year`, `notes`, `paid_content`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(5, 'математика', 'авттор', 5, 'бла', 2009, '11111111111', 0, 5, 19, 1476371373, 1478601850, 0),
(7, 'англійська', 'автор', 5, 'ййй', 2000, 'фівфівфв', 0, 5, NULL, 1463713713, NULL, 0),
(8, 'геометрія', 'ффф', 5, 'вап', 1875, 'івапр', 0, 5, 19, 1463714545, 1478264187, 0),
(9, 'анг', 'авт', 5, 'выыф', 2008, 'ывам', 0, 19, NULL, 1478703343, NULL, 0),
(10, 'Химия', 'Автор такой-то', 6, 'апавпва', 1998, 'авпавп', 0, 19, NULL, 1482492301, NULL, 0),
(11, 'Him', 'Xxxx', 10, 'xxxx', 1998, 'xxxx', 0, 19, NULL, 1482492344, NULL, 0),
(12, 'rgterg', 'gregerg', 7, 'bgfbfgb', 1099, 'gfbfgbgfb', 0, 19, NULL, 1482492462, NULL, 0),
(13, 'ккк', 'ккк', 7, '454', 454, '654654', 0, 19, NULL, 1483715556, NULL, 0),
(14, '43', '435', 7, '534', 534, '534', 0, 19, NULL, 1484666442, NULL, 0),
(15, 'ыфввфы', 'ыфвфывфы', 7, 'авмвамва', 2323, 'мвамвм', 0, 19, NULL, 1485191088, NULL, 0),
(16, '6756756', '65756', 6, '6575675', 657567, '765767', 0, 19, NULL, 1485259570, NULL, 0),
(17, 'вів', 'іваів', 8, '5464', 546456, '5464', 0, 19, NULL, 1485261682, NULL, 0),
(18, '453443543534', '54353453', 7, '435345', 435345, '5435345', 0, 19, NULL, 1485261793, NULL, 0),
(19, 'парпвапва', 'авпвап', 7, 'вапвапав', 54645645, 'вапвапвапв', 0, 19, NULL, 1485261821, NULL, 0),
(20, '65756', '567567', 7, '657567', 65767567, '65756756765', 0, 19, NULL, 1485261847, NULL, 0),
(21, 'Математика', 'О. С. Істер', 6, 'Київ \"Генеза\"', 2014, 'агшщзлормолдорлдти\nсроли тьлти\nрпсачвпмролщорпмлдор', 0, 19, NULL, 1486478018, NULL, 0),
(22, 'ййййй', 'ццццц', 8, 'йцу', 2017, 'іваіва', 0, 19, NULL, 1491829176, NULL, 0),
(23, 'ййй', 'цццц', 6, 'кккк', 2017, 'йцук', 0, 19, NULL, 1491829691, NULL, 0),
(24, 'qweqwe', 'qweqwe', 8, '123123', 2016, 'qweqweqwe', 0, 19, NULL, 1491905991, NULL, 0),
(25, 'qweqwe', 'qweqwe', 11, 'dasdas', 12312, 'qwdffdgfgfrfe', 0, 19, 19, 1491923914, 1492522115, 0),
(26, 'йцуйцуй', 'йцуйцуйц', 6, 'вмів', 2015, 'іваіва', 0, 19, NULL, 1491924184, NULL, 0),
(27, 'іявчса', 'йіпр', 5, 'првуркапмиртоьбпобрьпгрот еакапртоль', 2015, NULL, 0, 19, NULL, 1491994995, NULL, 0),
(28, 'иьтюбь', 'рлор', 8, 'цуівкапиро шоьрти', 2014, 'увкаепролдж апмир тольб притол ьбюп', 0, 19, NULL, 1491995493, NULL, 0),
(29, 'ее', 'еек', 7, 'прапрпа', 4544, 'прпа', 0, 19, NULL, 1492000183, NULL, 0),
(30, 'кееи', 'еиеи', 7, 'рар', 545, 'ра', 0, 19, NULL, 1492000204, NULL, 1),
(31, 'dfsdf', 'fsdfs', 5, 'я', 1234, 'упшгцук', 0, 19, NULL, 1492080572, NULL, 0),
(32, 'йцу', 'йцу', 8, 'чфівчів', 123123, 'йцуйцуйцу', 0, 19, NULL, 1492083595, NULL, 0),
(33, 'xqwexqwex', 'qweqwe', 8, 'qweqwe', 123123, 'qweqwe', 0, 19, NULL, 1492083697, NULL, 0),
(34, 'ewfwefw', 'wefwefwef', 8, 'xqwdxqwdx', 1234, 'xqwdxqwd', 0, 19, NULL, 1492083758, NULL, 0),
(35, 'qwexqw', 'qxewxqwex', 9, 'xsdsdf', 3421, 'sxdfxs', 0, 19, NULL, 1492083837, NULL, 0),
(36, 'qweqwe', 'qweqwe', 7, 'ggg', 1991, 'bbb', 0, 19, NULL, 1492084602, NULL, 1),
(37, 'frfrfrf', 'rfrfxx', 9, 'xerfxer', 2113, 'fxerfxer', 0, 19, NULL, 1492084796, NULL, 0),
(38, 'xrevxerf', 'vxrvxerv', 7, 'xvrvx', 1223, 'xwrvxwrvxr', 0, 19, NULL, 1492084894, NULL, 1),
(39, 'werfvf', 'dvffve', 7, 'vvfv', 1991, 'vvfv', 0, 19, NULL, 1492090491, NULL, 0),
(40, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492091318, NULL, 0),
(41, 'Гарри Поттер', 'Andrey Ivinsky', 7, 'Coworking', 2017, 'Cool', 0, 19, NULL, 1492174015, NULL, 0),
(42, 'Гарри Поттер', 'Andrey Ivinsky', 7, 'Coworking', 2017, 'Cool', 0, 19, NULL, 1492174019, NULL, 0),
(43, 'Гарри Поттер', 'Andrey', 7, 'Coworking', 2017, 'Cool', 0, 19, NULL, 1492174065, NULL, 0),
(44, 'tbgvf', 'trfed', 8, 'gt', 1234, 'qwer', 0, 19, NULL, 1492174204, NULL, 0),
(45, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492174483, NULL, 0),
(46, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492174511, NULL, 0),
(47, 'tbgvf', 'trfed', 8, 'gt', 1234, 'qwer', 0, 19, NULL, 1492174562, NULL, 0),
(48, 'Test2', 'Andrey', 8, 'Cow', 1991, 'qwe', 0, 19, NULL, 1492174918, NULL, 0),
(49, 'ttttt', 'aaaaa', 9, 'cccccc', 1991, '[poiuytre', 0, 19, NULL, 1492175239, NULL, 0),
(50, 'Andrey Book', 'Andrey', 7, 'Home', 2017, 'Book for test', 0, 19, NULL, 1492178711, NULL, 0),
(51, 'wbchnsj', 'yghjmk', 8, 'gbhjnmk', 1991, 'yhujiko', 0, 19, NULL, 1492179197, NULL, 1),
(52, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492425907, NULL, 0),
(53, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492425914, NULL, 0),
(54, 'qwer', 'qweqwe', 12, 'qewrqwe', 1234, NULL, 0, 5, NULL, 1492425929, NULL, 0),
(55, 'Marvel', 'Andrey', 5, 'Valve', 1991, 'POE', 0, 19, NULL, 1492507608, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `book_discipline`
--

CREATE TABLE `book_discipline` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `book_discipline`
--

INSERT INTO `book_discipline` (`id`, `book_id`, `discipline_id`, `created_at`, `updated_at`, `deleted`) VALUES
(6, 5, 11, 1476371373, NULL, 0),
(7, 8, 11, 0, NULL, 0),
(8, 7, 16, 0, NULL, 0),
(9, 9, 16, 1478703344, NULL, 0),
(10, 10, 11, 1482492302, NULL, 0),
(11, 11, 11, 1482492344, NULL, 0),
(12, 12, 11, 1482492462, NULL, 0),
(13, 13, 11, 1483715556, NULL, 0),
(14, 14, 20, 1484666442, NULL, 0),
(15, 15, 14, 1485191089, NULL, 0),
(16, 16, 20, 1485259570, NULL, 1),
(17, 17, 20, 1485261683, NULL, 1),
(18, 18, 20, 1485261793, NULL, 1),
(19, 19, 20, 1485261822, NULL, 1),
(20, 20, 20, 1485261849, NULL, 0),
(21, 21, 39, 1486478019, 1492507649, 0),
(22, 22, 40, 1491829176, NULL, 0),
(23, 23, 39, 1491829691, 1492084580, 1),
(24, 24, 39, 1491905991, 1492074292, 1),
(25, 25, 42, 1491923915, NULL, 0),
(26, 26, 42, 1491924186, 1491995018, 1),
(27, 27, 42, 1491994997, 1491995002, 1),
(28, 28, 42, 1491995494, 1491995503, 1),
(29, 29, 45, 1492000184, NULL, 0),
(30, 30, 45, 1492000204, 1492184700, 1),
(31, 31, 39, 1492080572, 1492083739, 1),
(32, 32, 39, 1492083595, 1492083736, 1),
(33, 33, 39, 1492083697, 1492083734, 1),
(34, 34, 39, 1492083758, 1492083819, 1),
(35, 35, 39, 1492083837, 1492084582, 1),
(36, 36, 39, 1492084602, 1492084762, 1),
(37, 37, 39, 1492084796, 1492084913, 1),
(38, 38, 39, 1492084894, 1492084908, 1),
(39, 47, 39, 1492174564, 1492504746, 1),
(40, 48, 39, 1492174918, NULL, 0),
(41, 49, 39, 1492175239, 1492507525, 1),
(42, 50, 39, 1492178711, 1492185635, 1),
(43, 51, 39, 1492179198, 1492185868, 1),
(44, 55, 39, 1492507608, 1492507621, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `bookmark`
--

CREATE TABLE `bookmark` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `book_id` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `bookmark`
--

INSERT INTO `bookmark` (`id`, `task_id`, `book_id`, `created_by`, `created_at`, `updated_at`, `deleted`) VALUES
(68, 27, 5, 1, 1478188420, 1486045937, 0),
(69, 26, 5, 2, 1478703590, 1486740726, 0),
(70, 27, 5, 119, 1479818061, 1479818650, 0),
(71, 26, 5, 52, 1480437298, 1481201208, 0),
(72, 27, 5, 2, 1482849455, 1488898580, 0),
(73, 28, 5, 2, 1482849457, 1484570577, 0),
(74, 29, 5, 2, 1482849459, 1485516803, 0),
(75, 30, 5, 2, 1482849461, 1484311678, 0),
(76, 32, 5, 2, 1482849462, 1484314098, 0),
(77, 33, 5, 2, 1482849464, 1484570746, 0),
(78, 35, 5, 2, 1483005865, 1484311771, 0),
(79, 37, 5, 2, 1483005961, 1486471510, 0),
(80, 38, 5, 2, 1483006055, NULL, 0),
(81, 39, 5, 2, 1483006064, 1484311762, 0),
(82, 40, 5, 2, 1483006071, 1484584356, 0),
(83, 25, 5, 2, 1484132465, 1492074960, 0),
(84, 27, 5, 52, 1484216878, 1484216884, 0),
(85, 34, 5, 2, 1484311635, NULL, 0),
(86, 47, 14, 2, 1485346321, 1486399636, 0),
(87, 41, 14, 2, 1485351450, 1486740677, 0),
(88, 23, 5, 1, 1485770597, 1492274570, 0),
(89, 48, 14, 2, 1485783850, 1486478370, 0),
(90, 47, 14, 1, 1485857474, 1486049216, 0),
(91, 41, 14, 1, 1485858214, 1486049248, 0),
(92, 49, 14, 1, 1486044822, NULL, 0),
(93, 48, 14, 1, 1486049786, NULL, 0),
(94, 54, 21, 2, 1486659259, 1486745393, 0),
(95, 55, 21, 2, 1486659309, 1486726965, 0),
(96, 58, 21, 2, 1486659317, 1486725382, 0),
(97, 56, 21, 2, 1486725325, 1486745266, 0),
(98, 95, 21, 2, 1487856598, 1487856598, 0),
(99, 96, 21, 2, 1487856609, NULL, 0),
(100, 96, 21, 2, 1487856609, NULL, 0),
(101, 109, 21, 2, 1487856621, 1487856621, 0),
(102, 355, 21, 2, 1487856663, NULL, 0),
(103, 355, 21, 2, 1487856663, NULL, 0),
(104, 383, 21, 2, 1487856738, 1487856738, 0),
(105, 385, 21, 2, 1487856753, 1487856753, 0),
(106, 401, 21, 2, 1487856760, NULL, 0),
(107, 401, 21, 2, 1487856760, NULL, 0),
(108, 402, 21, 2, 1487856766, NULL, 0),
(109, 402, 21, 2, 1487856766, NULL, 0),
(110, 406, 21, 2, 1487856773, 1487856773, 0),
(111, 405, 21, 2, 1487856779, 1487856780, 0),
(112, 391, 21, 2, 1487856809, 1487856809, 0),
(113, 392, 21, 2, 1487856817, NULL, 0),
(114, 392, 21, 2, 1487856817, NULL, 0),
(115, 221, 21, 2, 1487856851, 1492082241, 0),
(116, 203, 21, 2, 1490362825, 1490362826, 0),
(117, 373, 21, 2, 1490362845, 1490362845, 0),
(118, 380, 21, 2, 1490362852, 1490362852, 0),
(119, 368, 21, 2, 1490362863, 1492509060, 0),
(120, 502, 21, 1, 1492166020, 1492508658, 0),
(121, 501, 21, 1, 1492166034, 1492166034, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `checkup`
--

CREATE TABLE `checkup` (
  `id` int(11) NOT NULL,
  `done_task_id` int(11) NOT NULL,
  `text` text,
  `status` bit(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `class_discipline`
--

CREATE TABLE `class_discipline` (
  `id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `class` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `class_discipline`
--

INSERT INTO `class_discipline` (`id`, `discipline_id`, `class`, `created_at`, `deleted`) VALUES
(1, 11, 5, 0, 0),
(2, 13, 5, 0, 0),
(3, 11, 6, 1478259011, 0),
(4, 20, 5, 1478260111, 0),
(5, 20, 6, 1478260111, 0),
(6, 21, 5, 1478260189, 0),
(7, 21, 6, 1478260189, 0),
(10, 24, 6, 1482927446, 0),
(11, 25, 7, 1482927460, 0),
(12, 26, 5, 1482927477, 0),
(13, 27, 6, 1482927817, 0),
(14, 28, 9, 1482927827, 0),
(15, 29, 6, 1482927842, 0),
(16, 30, 6, 1483980328, 0),
(17, 31, 6, 1483981518, 0),
(18, 32, 6, 1484048301, 0),
(19, 33, 5, 1484048329, 0),
(20, 34, 7, 1484055067, 0),
(21, 35, 5, 1484055088, 0),
(22, 36, 6, 1484140258, 0),
(23, 37, 5, 1485262052, 0),
(24, 38, 5, 1485266935, 0),
(25, 39, 5, 1486477838, 0),
(26, 39, 6, 1486477838, 0),
(27, 40, 8, 1491829151, 0),
(28, 41, 5, 1491908989, 0),
(29, 42, 5, 1491909169, 0),
(30, 43, 5, 1491910275, 0),
(31, 43, 6, 1491910275, 0),
(32, 43, 7, 1491910275, 0),
(33, 43, 8, 1491910275, 0),
(34, 44, 9, 1491994516, 0),
(35, 45, 9, 1491998485, 0),
(36, 45, 10, 1491998485, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `comment`
--

CREATE TABLE `comment` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comment`
--

INSERT INTO `comment` (`id`, `task_id`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(1, 23, 0, 1479995807, NULL, 1, NULL, 0),
(3, 27, 0, 1480514559, NULL, 1, NULL, 0),
(8, 27, 0, 1480602275, NULL, 52, NULL, 0),
(17, 27, 0, 1480952600, NULL, 2, NULL, 0),
(19, 25, 0, 1481124284, NULL, 2, NULL, 0),
(20, 26, 0, 1481201220, NULL, 52, NULL, 0),
(24, 26, 0, 1481201660, NULL, 2, NULL, 0),
(25, 28, 0, 1481201715, NULL, 2, NULL, 0),
(26, 40, 0, 1484310400, NULL, 2, NULL, 0),
(27, 37, 0, 1484847984, NULL, 2, NULL, 0),
(28, 41, 0, 1485351468, NULL, 2, NULL, 0),
(29, 47, 0, 1485516843, NULL, 2, NULL, 0),
(30, 48, 0, 1486053135, NULL, 2, NULL, 0),
(31, 221, 0, 1492085886, NULL, 2, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `control`
--

CREATE TABLE `control` (
  `id` int(11) NOT NULL,
  `checkup_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `country`
--

INSERT INTO `country` (`id`, `name`, `deleted`) VALUES
(1, 'Ukraine', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `discipline`
--

CREATE TABLE `discipline` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `discipline`
--

INSERT INTO `discipline` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(11, 'Математика', 5, 19, 1476371373, 1478601766, 1),
(13, 'алгебра', 5, NULL, 1476371373, NULL, 0),
(14, 'тригонометрія', 5, NULL, 1476371373, NULL, 0),
(15, 'фізика', 5, NULL, 1476371373, NULL, 0),
(16, 'англійська', 19, NULL, 1478256514, NULL, 0),
(20, 'test', 5, NULL, 1478260109, NULL, 0),
(21, 'new discipline', 19, NULL, 1478260189, NULL, 1),
(24, 'Физкультура', 19, NULL, 1482927446, NULL, 1),
(25, 'Бег', 19, NULL, 1482927460, NULL, 1),
(26, 'Ріжки', 19, NULL, 1482927477, NULL, 1),
(27, 'что-то', 19, NULL, 1482927817, NULL, 1),
(28, 'енг', 19, NULL, 1482927827, NULL, 1),
(29, 'ук', 19, NULL, 1482927842, NULL, 1),
(30, 'dss', 19, NULL, 1483980328, NULL, 1),
(31, 'rrrаауаіуаіу', 19, 19, 1483981518, 1484054982, 1),
(32, 'Геом', 19, NULL, 1484048301, NULL, 1),
(33, 'у', 19, NULL, 1484048329, NULL, 1),
(34, 'ісіс', 19, NULL, 1484055067, NULL, 1),
(35, 'ічіфч', 19, NULL, 1484055088, NULL, 1),
(36, 'Хімія', 19, NULL, 1484140258, NULL, 0),
(37, '545654', 19, NULL, 1485262052, NULL, 1),
(38, '456456', 19, NULL, 1485266935, NULL, 1),
(39, 'Математика', 19, NULL, 1486477838, NULL, 0),
(40, 'Математика', 19, NULL, 1491829151, NULL, 1),
(41, 'Література', 19, NULL, 1491908989, NULL, 1),
(42, 'Вишивання', 19, 19, 1491909169, 1492522025, 0),
(43, 'Фізра', 19, NULL, 1491910275, NULL, 1),
(44, 'еесапмиртоль', 19, NULL, 1491994516, NULL, 1),
(45, 'бла бла бла', 19, 19, 1491998485, 1492003888, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `done_task`
--

CREATE TABLE `done_task` (
  `id` int(11) NOT NULL,
  `task_id` int(11) DEFAULT NULL,
  `subtask_id` int(11) DEFAULT NULL,
  `result` int(1) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `done_task`
--

INSERT INTO `done_task` (`id`, `task_id`, `subtask_id`, `result`, `answer`, `status`, `created_at`, `updated_at`, `created_by`, `updated_by`, `deleted`) VALUES
(70, 23, NULL, 0, '51', 0, 1478179196, 1485870219, 1, 1, 0),
(71, 27, NULL, 0, 'Привет мир', 0, 1478704758, 1481559524, 2, 2, 0),
(85, 27, NULL, 0, '1', 0, 1480952978, NULL, 1, NULL, 0),
(86, 27, NULL, 0, '1', 0, 1480953226, NULL, 1, NULL, 0),
(87, 27, NULL, 0, '1', 0, 1480953279, NULL, 1, NULL, 0),
(88, 25, NULL, 0, 'jyjyjyuyjyjyjyjyj', 0, 1481124284, 1484836193, 2, 2, 0),
(89, 27, NULL, 0, 'Апоолр', 0, 1481124341, NULL, 52, NULL, 0),
(90, 27, 26, 0, '15', 0, 1483303777, 1486045441, 2, 2, 0),
(91, 27, 45, 0, 'Иоио', 0, 1483303793, NULL, 2, NULL, 0),
(92, 26, NULL, 0, 'Ответ 1', 0, 1484310443, 1484740323, 2, 2, 0),
(93, 27, 47, 0, '444', 0, 1484312998, 1485792456, 2, 1, 0),
(94, 27, 47, 0, 'Результат ', 0, 1484313007, NULL, 2, NULL, 0),
(95, 27, 47, 0, 'Результат ', 0, 1484313011, NULL, 2, NULL, 0),
(96, 27, 51, 0, 'Решилось ', 0, 1484584059, NULL, 2, NULL, 0),
(97, 41, 0, 1, '4', 0, 1485351468, 1485351509, 2, 2, 0),
(98, 48, NULL, 0, 'Тест ', 0, 1486053135, NULL, 2, NULL, 0),
(99, 48, NULL, 0, 'Тест ', 0, 1486053160, NULL, 2, NULL, 0),
(100, 48, NULL, 0, 'Тест ', 0, 1486053188, NULL, 2, NULL, 0),
(101, 47, NULL, 0, '4444', 0, 1486399672, NULL, 2, NULL, 0),
(102, 221, 400, 0, '\\frac{15}{5}', 0, 1492085886, NULL, 2, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `eng_dictionary`
--

CREATE TABLE `eng_dictionary` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `explanation` varchar(255) DEFAULT NULL,
  `intensity` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `eng_dictionary`
--

INSERT INTO `eng_dictionary` (`id`, `value`, `explanation`, `intensity`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted`) VALUES
(1, 'cat', NULL, 0, 5, 1476371373, NULL, NULL, 0),
(2, 'dictionary', NULL, 0, 5, 1476371373, NULL, NULL, 0),
(3, 'life', NULL, 0, 5, 1476371373, NULL, NULL, 0),
(4, 'apple', NULL, 0, 5, 1476371373, NULL, NULL, 0),
(5, 'book', NULL, 0, 5, 1476371373, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE `file` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `table` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file`
--

INSERT INTO `file` (`id`, `parent_id`, `table`, `extension`, `url`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(24, 70, 'done_task', 'jpg', '2581b397.jpg', 1, NULL, 1478179196, NULL, 1),
(26, 23, 'task', 'jpg', '357d28fc.jpg', 5, NULL, 1478179196, NULL, 1),
(28, 70, 'done_task', 'jpeg', '1581b55d.jpeg', 1, NULL, 1478186453, NULL, 1),
(29, 70, 'done_task', 'jpg', '5581b566.jpg', 1, NULL, 1478186607, NULL, 1),
(30, 26, 'task', 'jpg', '15820725.jpg', 19, NULL, 1478521434, NULL, 0),
(31, 26, 'task', 'jpg', '15820725.jpg', 19, NULL, 1478521434, NULL, 0),
(32, 27, 'task', 'jpg', '65820763.jpg', 19, NULL, 1478522424, NULL, 1),
(33, 27, 'task', 'jpg', '15820763.jpg', 19, NULL, 1478522424, NULL, 1),
(34, 28, 'task', 'jpg', '158209dc.jpg', 19, NULL, 1478532548, NULL, 1),
(35, 28, 'task', 'jpg', '258209dc.jpg', 19, NULL, 1478532548, NULL, 1),
(36, 27, 'task', 'jpg', '45820a15.jpg', 19, NULL, 1478533468, NULL, 1),
(37, 27, 'task', 'jpg', '55820a36.jpg', 19, NULL, 1478533996, NULL, 1),
(38, 27, 'task', 'jpg', '55820a44.jpg', 19, NULL, 1478534208, NULL, 1),
(39, 23, 'task', 'jpg', '15821b6a.jpg', 19, NULL, 1478604461, NULL, 1),
(40, 23, 'task', 'jpg', '55821b6a.jpg', 19, NULL, 1478604461, NULL, 1),
(41, 71, 'done_task', 'jpg', '65840377.jpg', 2, NULL, 1480603515, NULL, 1),
(42, 71, 'done_task', 'jpg', '15840377.jpg', 2, NULL, 1480603515, NULL, 1),
(43, 71, 'done_task', 'jpg', '25840377.jpg', 2, NULL, 1480603515, NULL, 1),
(44, 71, 'done_task', 'jpg', '158483c9.jpg', 2, NULL, 1481129106, NULL, 1),
(45, 29, 'task', 'jpg', '1585d091.jpg', 19, NULL, 1482492189, NULL, 1),
(46, 30, 'task', 'jpg', '3585d0bb.jpg', 19, NULL, 1482492849, NULL, 1),
(47, 31, 'task', 'jpg', '5585d0c9.jpg', 19, NULL, 1482493078, NULL, 1),
(48, 32, 'task', 'png', '3585d0ce.png', 19, NULL, 1482493154, NULL, 1),
(49, 33, 'task', 'jpg', '6585d0d3.jpg', 19, NULL, 1482493243, NULL, 1),
(50, 34, 'task', 'png', '1585d0d5.png', 19, NULL, 1482493278, NULL, 1),
(51, 35, 'task', 'jpg', '4585d0e5.jpg', 19, NULL, 1482493528, NULL, 1),
(98, 36, 'task', 'jpg', '55862636.jpg', 19, NULL, 1482842989, NULL, 0),
(99, 37, 'task', 'jpg', '3586266a.jpg', 19, NULL, 1482843820, NULL, 0),
(101, 38, 'task', 'jpg', '158626a2.jpg', 19, NULL, 1482844706, NULL, 0),
(102, 39, 'task', 'png', '658626bd.png', 19, NULL, 1482845144, NULL, 0),
(103, 40, 'task', 'jpg', '358626c6.jpg', 19, NULL, 1482845287, NULL, 1),
(107, 40, 'task', 'png', '358627a1.png', 19, NULL, 1482848796, NULL, 1),
(108, 40, 'task', 'jpg', '358627a6.jpg', 19, NULL, 1482848864, NULL, 1),
(109, 40, 'task', 'png', '158627a7.png', 19, NULL, 1482848881, NULL, 0),
(110, 28, 'task', 'png', '458627e3.png', 19, NULL, 1482849845, NULL, 0),
(111, 28, 'task', 'png', '358627e4.png', 19, NULL, 1482849863, NULL, 0),
(145, 50, 'subtask', 'jpg', '258628f4.jpg', 19, NULL, 1482854211, NULL, 1),
(146, 50, 'subtask', 'jpg', '658628f4.jpg', 19, NULL, 1482854212, NULL, 1),
(147, 50, 'subtask', 'png', '458628fa.png', 19, NULL, 1482854305, NULL, 1),
(148, 50, 'subtask', 'png', '558628fa.png', 19, NULL, 1482854305, NULL, 1),
(149, 50, 'subtask', 'png', '258628fa.png', 19, NULL, 1482854305, NULL, 1),
(150, 50, 'subtask', 'jpg', '658628fc.jpg', 19, NULL, 1482854341, NULL, 1),
(151, 50, 'subtask', 'jpg', '458628fc.jpg', 19, NULL, 1482854341, NULL, 1),
(152, 50, 'subtask', 'jpg', '658628fc.jpg', 19, NULL, 1482854341, NULL, 1),
(153, 50, 'subtask', 'png', '35862903.png', 19, NULL, 1482854452, NULL, 1),
(154, 50, 'subtask', 'png', '35862903.png', 19, NULL, 1482854453, NULL, 1),
(155, 50, 'subtask', 'png', '45862903.png', 19, NULL, 1482854453, NULL, 1),
(156, 50, 'subtask', 'jpg', '4586290f.jpg', 19, NULL, 1482854641, NULL, 1),
(157, 50, 'subtask', 'jpg', '5586290f.jpg', 19, NULL, 1482854641, NULL, 1),
(158, 50, 'subtask', 'jpg', '5586290f.jpg', 19, NULL, 1482854641, NULL, 1),
(159, 50, 'subtask', 'jpg', '65862913.jpg', 19, NULL, 1482854713, NULL, 1),
(160, 50, 'subtask', 'jpg', '25862913.jpg', 19, NULL, 1482854714, NULL, 1),
(161, 50, 'subtask', 'jpg', '65862913.jpg', 19, NULL, 1482854714, NULL, 1),
(162, 50, 'subtask', 'png', '35862916.png', 19, NULL, 1482854767, NULL, 1),
(163, 50, 'subtask', 'png', '25862916.png', 19, NULL, 1482854767, NULL, 1),
(164, 50, 'subtask', 'png', '25862916.png', 19, NULL, 1482854767, NULL, 1),
(165, 50, 'subtask', 'jpg', '25862920.jpg', 19, NULL, 1482854921, NULL, 1),
(166, 50, 'subtask', 'jpg', '55862921.jpg', 19, NULL, 1482854934, NULL, 1),
(167, 50, 'subtask', 'jpg', '15862921.jpg', 19, NULL, 1482854939, NULL, 1),
(168, 50, 'subtask', 'png', '4586292b.png', 19, NULL, 1482855103, NULL, 1),
(169, 50, 'subtask', 'png', '3586292b.png', 19, NULL, 1482855103, NULL, 1),
(170, 50, 'subtask', 'png', '4586292b.png', 19, NULL, 1482855103, NULL, 1),
(171, 50, 'subtask', 'jpg', '5586292d.jpg', 19, NULL, 1482855128, NULL, 1),
(172, 50, 'subtask', 'jpg', '5586292d.jpg', 19, NULL, 1482855129, NULL, 1),
(173, 50, 'subtask', 'jpg', '6586292d.jpg', 19, NULL, 1482855129, NULL, 1),
(174, 50, 'subtask', 'jpg', '25862943.jpg', 19, NULL, 1482855484, NULL, 1),
(175, 50, 'subtask', 'png', '25862946.png', 19, NULL, 1482855529, NULL, 1),
(176, 50, 'subtask', 'png', '35862946.png', 19, NULL, 1482855529, NULL, 1),
(177, 50, 'subtask', 'png', '35862946.png', 19, NULL, 1482855529, NULL, 1),
(178, 50, 'subtask', 'jpg', '5586295d.jpg', 19, NULL, 1482855899, NULL, 1),
(179, 50, 'subtask', 'jpg', '65862961.jpg', 19, NULL, 1482855957, NULL, 1),
(180, 50, 'subtask', 'png', '15862964.png', 19, NULL, 1482856009, NULL, 1),
(181, 50, 'subtask', 'jpg', '6586296e.jpg', 19, NULL, 1482856160, NULL, 1),
(182, 50, 'subtask', 'png', '6586297e.png', 19, NULL, 1482856426, NULL, 1),
(183, 50, 'subtask', 'png', '15862980.png', 19, NULL, 1482856461, NULL, 1),
(184, 50, 'subtask', 'jpg', '2586298b.jpg', 19, NULL, 1482856633, NULL, 1),
(185, 50, 'subtask', 'jpg', '6586298d.jpg', 19, NULL, 1482856669, NULL, 1),
(186, 50, 'subtask', 'png', '258629a2.png', 19, NULL, 1482856998, NULL, 1),
(187, 50, 'subtask', 'png', '358629a6.png', 19, NULL, 1482857067, NULL, 1),
(188, 50, 'subtask', 'jpg', '658629aa.jpg', 19, NULL, 1482857120, NULL, 1),
(189, 51, 'subtask', 'jpg', '65873b3f.jpg', 19, NULL, 1483977712, NULL, 1),
(190, 52, 'subtask', 'jpg', '65874fa9.jpg', 19, NULL, 1484061329, NULL, 1),
(191, 53, 'subtask', 'png', '4587500e.png', 19, NULL, 1484062951, NULL, 1),
(192, 54, 'subtask', 'jpg', '15875018.jpg', 19, NULL, 1484063112, NULL, 1),
(193, 55, 'subtask', 'png', '2587501c.png', 19, NULL, 1484063174, NULL, 1),
(194, 56, 'subtask', 'jpg', '5587501f.jpg', 19, NULL, 1484063222, NULL, 1),
(195, 57, 'subtask', 'png', '25875045.png', 19, NULL, 1484063832, NULL, 1),
(196, 71, 'message', 'm4a', '65875140.m4a', 2, NULL, 1484067843, NULL, 1),
(197, 72, 'message', 'm4a', '25875163.m4a', 2, NULL, 1484068406, NULL, 1),
(198, 88, 'done_task', 'jpg', '5587cd6e.jpg', 2, NULL, 1484576489, NULL, 0),
(199, 88, 'done_task', 'jpg', '5587cd6f.jpg', 2, NULL, 1484576500, NULL, 0),
(200, 88, 'done_task', 'jpg', '1587cd6f.jpg', 2, NULL, 1484576500, NULL, 0),
(201, 88, 'done_task', 'jpg', '3587cd95.jpg', 2, NULL, 1484577118, NULL, 0),
(202, 88, 'done_task', 'jpg', '3587cd99.jpg', 2, NULL, 1484577175, NULL, 0),
(203, 88, 'done_task', 'jpg', '6587cdb1.jpg', 2, NULL, 1484577561, NULL, 0),
(204, 41, 'task', 'jpg', '6587e367.jpg', 19, NULL, 1484666482, NULL, 1),
(205, 88, 'done_task', 'jpg', '55880cc2.jpg', 2, NULL, 1484835881, NULL, 0),
(206, 88, 'done_task', 'jpg', '25880cd6.jpg', 2, NULL, 1484836193, NULL, 0),
(207, 88, 'done_task', 'jpg', '45880cd6.jpg', 2, NULL, 1484836193, NULL, 0),
(208, 58, 'subtask', 'jpg', '1588626e.jpg', 19, NULL, 1485186798, NULL, 1),
(209, 59, 'subtask', 'jpg', '458871d1.jpg', 19, NULL, 1485249817, NULL, 1),
(210, 60, 'subtask', 'jpg', '45887258.jpg', 19, NULL, 1485251979, NULL, 1),
(211, 61, 'subtask', 'png', '5588727b.png', 19, NULL, 1485252530, NULL, 1),
(212, 42, 'task', 'png', '4588727d.png', 19, NULL, 1485252565, NULL, 1),
(213, 62, 'subtask', 'jpg', '15887327.jpg', 19, NULL, 1485255289, NULL, 1),
(214, 43, 'task', 'jpg', '3588732d.jpg', 19, NULL, 1485255379, NULL, 1),
(215, 44, 'task', 'jpg', '4588733c.jpg', 19, NULL, 1485255627, NULL, 1),
(216, 45, 'task', 'png', '35887344.png', 19, NULL, 1485255750, NULL, 1),
(217, 46, 'task', 'jpg', '45887372.jpg', 19, NULL, 1485256495, NULL, 1),
(218, 63, 'subtask', 'png', '35887379.png', 19, NULL, 1485256596, NULL, 1),
(219, 47, 'task', 'png', '3588739d.png', 19, NULL, 1485257177, NULL, 0),
(220, 48, 'task', 'png', '658873f7.png', 19, NULL, 1485258609, NULL, 0),
(221, 49, 'task', 'jpg', '358873fa.jpg', 19, NULL, 1485258661, NULL, 0),
(222, 50, 'task', 'png', '55887417.png', 19, NULL, 1485259129, NULL, 1),
(223, 51, 'task', 'jpg', '4588742e.jpg', 19, NULL, 1485259498, NULL, 1),
(224, 110, 'message', 'm4a', '5588f1e1.m4a', 1, NULL, 1485774363, NULL, 0),
(225, 114, 'message', 'm4a', '15890886.m4a', 1, NULL, 1485867113, NULL, 0),
(226, 120, 'message', 'm4a', '1589212d.m4a', 1, NULL, 1485968088, NULL, 0),
(227, 62, 'subtask', 'jpg', '458a1c3d.jpg', 19, NULL, 1486996439, NULL, 1),
(228, 62, 'subtask', 'jpg', '558a1cc8.jpg', 19, NULL, 1486998666, NULL, 1),
(229, 62, 'subtask', 'png', '658a1cc9.png', 19, NULL, 1486998685, NULL, 1),
(230, 62, 'subtask', 'png', '658a1ccb.png', 19, NULL, 1486998714, NULL, 1),
(231, 62, 'subtask', 'png', '658a1cec.png', 19, NULL, 1486999240, NULL, 1),
(232, 62, 'subtask', 'jpg', '358a1d0b.jpg', 19, NULL, 1486999733, NULL, 1),
(233, 62, 'subtask', 'jpg', '258a1d13.jpg', 19, NULL, 1486999868, NULL, 1),
(234, 62, 'subtask', 'jpg', '558a1d17.jpg', 19, NULL, 1486999921, NULL, 1),
(235, 62, 'subtask', 'png', '358a1d17.png', 19, NULL, 1486999933, NULL, 1),
(236, 62, 'subtask', 'png', '558a1d18.png', 19, NULL, 1486999947, NULL, 1),
(237, 62, 'subtask', 'jpg', '258a1d19.jpg', 19, NULL, 1486999967, NULL, 1),
(238, 62, 'subtask', 'jpg', '358a1d21.jpg', 19, NULL, 1487000080, NULL, 1),
(239, 62, 'subtask', 'jpg', '458a1d21.jpg', 19, NULL, 1487000090, NULL, 1),
(240, 41, 'task', 'jpg', '358a1d2c.jpg', 19, NULL, 1487000258, NULL, 1),
(241, 41, 'task', 'png', '258a1d43.png', 19, NULL, 1487000626, NULL, 1),
(242, 62, 'subtask', 'png', '658a1d89.png', 19, NULL, 1487001752, NULL, 1),
(243, 123, 'subtask', 'jpg', '358a1d96.jpg', 19, NULL, 1487001960, NULL, 0),
(244, 53, 'task', 'png', '158a1d9b.png', 19, NULL, 1487002040, NULL, 1),
(245, 53, 'task', 'jpg', '158a1d9f.jpg', 19, NULL, 1487002109, NULL, 1),
(246, 53, 'task', 'jpg', '258a1da1.jpg', 19, NULL, 1487002140, NULL, 1),
(247, 53, 'task', 'png', '358a1da3.png', 19, NULL, 1487002170, NULL, 1),
(248, 53, 'task', 'jpg', '658a1da5.jpg', 19, NULL, 1487002203, NULL, 1),
(249, 62, 'subtask', 'jpg', '458a1e16.jpg', 19, NULL, 1487004015, NULL, 1),
(250, 62, 'subtask', 'png', '658a1e17.png', 19, NULL, 1487004028, NULL, 0),
(251, 53, 'task', 'jpg', '358a1ebb.jpg', 19, NULL, 1487006653, NULL, 1),
(252, 53, 'task', 'jpg', '458a1ebc.jpg', 19, NULL, 1487006671, NULL, 1),
(253, 53, 'task', 'jpg', '458a1eca.jpg', 19, NULL, 1487006881, NULL, 1),
(254, 53, 'task', 'jpg', '458a1f1e.jpg', 19, NULL, 1487008228, NULL, 1),
(255, 53, 'task', 'jpg', '258a1f22.jpg', 19, NULL, 1487008293, NULL, 1),
(256, 53, 'task', 'png', '558a1f24.png', 19, NULL, 1487008325, NULL, 1),
(257, 53, 'task', 'png', '358a1f3b.png', 19, NULL, 1487008703, NULL, 1),
(258, 53, 'task', 'jpg', '458a1f7a.jpg', 19, NULL, 1487009709, NULL, 1),
(259, 53, 'task', 'jpg', '258a1f7f.jpg', 19, NULL, 1487009784, NULL, 1),
(260, 53, 'task', 'png', '258a1f85.png', 19, NULL, 1487009884, NULL, 1),
(261, 53, 'task', 'jpg', '558a1fda.jpg', 19, NULL, 1487011233, NULL, 1),
(262, 53, 'task', 'png', '358a1fe4.png', 19, NULL, 1487011407, NULL, 1),
(263, 53, 'task', 'png', '558a1ff7.png', 19, NULL, 1487011703, NULL, 1),
(264, 53, 'task', 'jpg', '258a1ff9.jpg', 19, NULL, 1487011738, NULL, 1),
(265, 41, 'task', 'png', '258a2217.png', 19, NULL, 1487020412, NULL, 1),
(266, 41, 'task', 'png', '458a2258.png', 19, NULL, 1487021443, NULL, 1),
(267, 41, 'task', 'png', '358a2258.png', 19, NULL, 1487021443, NULL, 1),
(268, 41, 'task', 'jpg', '658a2259.jpg', 19, NULL, 1487021466, NULL, 1),
(269, 41, 'task', 'jpg', '458a2259.jpg', 19, NULL, 1487021466, NULL, 1),
(270, 41, 'task', 'png', '458a225c.png', 19, NULL, 1487021515, NULL, 1),
(271, 41, 'task', 'jpg', '358a225c.jpg', 19, NULL, 1487021515, NULL, 1),
(272, 41, 'task', 'jpg', '558a225c.jpg', 19, NULL, 1487021515, NULL, 1),
(273, 41, 'task', 'png', '358a225c.png', 19, NULL, 1487021515, NULL, 1),
(274, 41, 'task', 'png', '558a225c.png', 19, NULL, 1487021515, NULL, 1),
(275, 41, 'task', 'png', '458a226e.png', 19, NULL, 1487021803, NULL, 1),
(276, 41, 'task', 'jpg', '358a226e.jpg', 19, NULL, 1487021803, NULL, 1),
(277, 41, 'task', 'jpg', '558a226e.jpg', 19, NULL, 1487021803, NULL, 1),
(278, 41, 'task', 'png', '458a2275.png', 19, NULL, 1487021904, NULL, 1),
(279, 41, 'task', 'jpg', '158a2275.jpg', 19, NULL, 1487021904, NULL, 1),
(280, 41, 'task', 'jpg', '358a2275.jpg', 19, NULL, 1487021904, NULL, 1),
(281, 41, 'task', 'jpg', '658a2281.jpg', 19, NULL, 1487022100, NULL, 1),
(282, 41, 'task', 'png', '158a2281.png', 19, NULL, 1487022100, NULL, 1),
(283, 41, 'task', 'jpg', '658a2286.jpg', 19, NULL, 1487022187, NULL, 1),
(284, 41, 'task', 'jpg', '358a2286.jpg', 19, NULL, 1487022187, NULL, 1),
(285, 41, 'task', 'jpg', '358a2286.jpg', 19, NULL, 1487022187, NULL, 1),
(286, 41, 'task', 'png', '258a2286.png', 19, NULL, 1487022187, NULL, 1),
(287, 41, 'task', 'png', '558a2286.png', 19, NULL, 1487022187, NULL, 1),
(288, 41, 'task', 'jpg', '458a228b.jpg', 19, NULL, 1487022270, NULL, 1),
(289, 41, 'task', 'jpg', '458a228b.jpg', 19, NULL, 1487022270, NULL, 1),
(290, 41, 'task', 'png', '158a228b.png', 19, NULL, 1487022270, NULL, 1),
(291, 41, 'task', 'png', '658a228e.png', 19, NULL, 1487022310, NULL, 1),
(292, 41, 'task', 'jpg', '658a228e.jpg', 19, NULL, 1487022310, NULL, 1),
(293, 41, 'task', 'jpg', '158a228f.jpg', 19, NULL, 1487022334, NULL, 1),
(294, 41, 'task', 'jpg', '358a228f.jpg', 19, NULL, 1487022334, NULL, 1),
(295, 41, 'task', 'jpg', '658a2290.jpg', 19, NULL, 1487022349, NULL, 1),
(296, 41, 'task', 'jpg', '558a2293.jpg', 19, NULL, 1487022392, NULL, 1),
(297, 41, 'task', 'jpg', '658a2293.jpg', 19, NULL, 1487022392, NULL, 1),
(298, 41, 'task', 'png', '458a2293.png', 19, NULL, 1487022392, NULL, 1),
(299, 41, 'task', 'png', '358a2293.png', 19, NULL, 1487022392, NULL, 1),
(300, 41, 'task', 'png', '258a2293.png', 19, NULL, 1487022392, NULL, 0),
(301, 41, 'task', 'jpg', '658a2295.jpg', 19, NULL, 1487022430, NULL, 1),
(302, 41, 'task', 'jpg', '358a2295.jpg', 19, NULL, 1487022430, NULL, 1),
(303, 41, 'task', 'jpg', '258a229b.jpg', 19, NULL, 1487022514, NULL, 1),
(304, 41, 'task', 'jpg', '658a229b.jpg', 19, NULL, 1487022514, NULL, 1),
(305, 41, 'task', 'png', '358a229b.png', 19, NULL, 1487022514, NULL, 1),
(306, 41, 'task', 'jpg', '158a229b.jpg', 19, NULL, 1487022514, NULL, 1),
(307, 41, 'task', 'jpg', '358a22a0.jpg', 19, NULL, 1487022602, NULL, 1),
(308, 41, 'task', 'jpg', '558a22a0.jpg', 19, NULL, 1487022602, NULL, 1),
(309, 41, 'task', 'jpg', '158a22a0.jpg', 19, NULL, 1487022602, NULL, 1),
(310, 41, 'task', 'jpg', '658a22a0.jpg', 19, NULL, 1487022602, NULL, 1),
(311, 41, 'task', 'jpg', '658a22a3.jpg', 19, NULL, 1487022641, NULL, 1),
(312, 41, 'task', 'png', '558a22ab.png', 19, NULL, 1487022779, NULL, 1),
(313, 41, 'task', 'jpg', '258a22ae.jpg', 19, NULL, 1487022820, NULL, 0),
(314, 41, 'task', 'jpg', '658a22b4.jpg', 19, NULL, 1487022917, NULL, 0),
(315, 41, 'task', 'png', '258a22b4.png', 19, NULL, 1487022917, NULL, 0),
(316, 41, 'task', 'png', '458a22b4.png', 19, NULL, 1487022917, NULL, 0),
(317, 41, 'task', 'png', '358a22b4.png', 19, NULL, 1487022917, NULL, 0),
(318, 41, 'task', 'jpg', '158a22b4.jpg', 19, NULL, 1487022917, NULL, 0),
(319, 84, 'task', 'png', '258a2348.png', 19, NULL, 1487025281, NULL, 0),
(320, 84, 'task', 'png', '658a2348.png', 19, NULL, 1487025281, NULL, 0),
(321, 84, 'task', 'jpg', '658a2348.jpg', 19, NULL, 1487025281, NULL, 0),
(322, 85, 'task', 'jpg', '558a234b.jpg', 19, NULL, 1487025340, NULL, 1),
(323, 85, 'task', 'jpg', '458a234b.jpg', 19, NULL, 1487025340, NULL, 1),
(324, 85, 'task', 'png', '658a234b.png', 19, NULL, 1487025340, NULL, 1),
(325, 86, 'task', 'png', '258a2350.png', 19, NULL, 1487025419, NULL, 0),
(326, 86, 'task', 'png', '458a2350.png', 19, NULL, 1487025419, NULL, 0),
(327, 86, 'task', 'jpg', '158a2350.jpg', 19, NULL, 1487025419, NULL, 0),
(328, 87, 'task', 'jpg', '658a2355.jpg', 19, NULL, 1487025500, NULL, 1),
(329, 87, 'task', 'jpg', '558a2355.jpg', 19, NULL, 1487025500, NULL, 1),
(330, 87, 'task', 'png', '558a2355.png', 19, NULL, 1487025500, NULL, 1),
(331, 88, 'task', 'png', '358a23a4.png', 19, NULL, 1487026761, NULL, 0),
(332, 88, 'task', 'jpg', '558a23a4.jpg', 19, NULL, 1487026761, NULL, 0),
(333, 89, 'task', 'png', '558a23a7.png', 19, NULL, 1487026815, NULL, 0),
(334, 89, 'task', 'jpg', '158a23a7.jpg', 19, NULL, 1487026815, NULL, 0),
(335, 90, 'task', 'png', '458a23ad.png', 19, NULL, 1487026909, NULL, 0),
(336, 90, 'task', 'jpg', '358a23ad.jpg', 19, NULL, 1487026909, NULL, 0),
(337, 124, 'subtask', 'png', '358a23af.png', 19, NULL, 1487026941, NULL, 1),
(338, 124, 'subtask', 'png', '358a23af.png', 19, NULL, 1487026941, NULL, 1),
(339, 124, 'subtask', 'jpg', '358a23b0.jpg', 19, NULL, 1487026955, NULL, 1),
(340, 124, 'subtask', 'jpg', '458a23b1.jpg', 19, NULL, 1487026971, NULL, 1),
(341, 124, 'subtask', 'png', '158a23b2.png', 19, NULL, 1487026988, NULL, 0),
(342, 52, 'task', 'png', '658a23bb.png', 19, NULL, 1487027125, NULL, 0),
(343, 52, 'task', 'png', '258a23bb.png', 19, NULL, 1487027125, NULL, 0),
(344, 52, 'task', 'jpg', '358a23bb.jpg', 19, NULL, 1487027125, NULL, 0),
(345, 53, 'task', 'png', '158a23c4.png', 19, NULL, 1487027270, NULL, 0),
(346, 53, 'task', 'jpg', '458a23c4.jpg', 19, NULL, 1487027270, NULL, 0),
(347, 125, 'subtask', 'png', '658a23d1.png', 19, NULL, 1487027480, NULL, 0),
(348, 125, 'subtask', 'png', '258a23d1.png', 19, NULL, 1487027480, NULL, 0),
(349, 126, 'subtask', 'png', '658a23ec.png', 19, NULL, 1487027915, NULL, 0),
(350, 127, 'subtask', 'jpg', '158a2404.jpg', 19, NULL, 1487028303, NULL, 0),
(351, 91, 'task', 'png', '458a2d1a.png', 19, NULL, 1487065508, NULL, 0),
(352, 91, 'task', 'jpg', '658a2d1a.jpg', 19, NULL, 1487065508, NULL, 0),
(353, 115, 'task', 'png', '358a2fcf.png', 19, NULL, 1487076595, NULL, 0),
(354, 125, 'task', 'jpg', '258a310e.jpg', 19, NULL, 1487081709, NULL, 0),
(355, 126, 'task', 'jpg', '458a3113.jpg', 19, NULL, 1487081778, NULL, 0),
(356, 126, 'task', 'jpg', '158a311c.jpg', 19, NULL, 1487081928, NULL, 0),
(357, 126, 'task', 'png', '358a311d.png', 19, NULL, 1487081946, NULL, 0),
(358, 126, 'task', 'png', '658a311d.png', 19, NULL, 1487081946, NULL, 0),
(359, 127, 'task', 'jpg', '158a3129.jpg', 19, NULL, 1487082136, NULL, 0),
(360, 127, 'task', 'png', '558a3129.png', 19, NULL, 1487082136, NULL, 0),
(361, 158, 'task', 'png', '358a3349.png', 19, NULL, 1487090842, NULL, 1),
(362, 294, 'subtask', 'png', '658a4367.png', 19, NULL, 1487156857, NULL, 0),
(363, 295, 'subtask', 'png', '358a4371.png', 19, NULL, 1487157021, NULL, 0),
(364, 323, 'subtask', 'png', '458a43c0.png', 19, NULL, 1487158273, NULL, 1),
(365, 323, 'subtask', 'png', '658a43ce.png', 19, NULL, 1487158510, NULL, 1),
(366, 324, 'subtask', 'png', '658a43d3.png', 19, NULL, 1487158582, NULL, 0),
(367, 325, 'subtask', 'png', '458a43d4.png', 19, NULL, 1487158598, NULL, 0),
(368, 326, 'subtask', 'png', '558a43d6.png', 19, NULL, 1487158628, NULL, 0),
(369, 327, 'subtask', 'png', '658a43d9.png', 19, NULL, 1487158686, NULL, 0),
(370, 345, 'subtask', 'png', '358a445a.png', 19, NULL, 1487160738, NULL, 0),
(371, 346, 'subtask', 'png', '158a445b.png', 19, NULL, 1487160755, NULL, 0),
(372, 347, 'subtask', 'png', '358a445c.png', 19, NULL, 1487160771, NULL, 0),
(373, 374, 'subtask', 'png', '558a44e9.png', 19, NULL, 1487163025, NULL, 0),
(374, 375, 'subtask', 'png', '158a44eb.png', 19, NULL, 1487163071, NULL, 0),
(375, 376, 'subtask', 'png', '458a44ed.png', 19, NULL, 1487163097, NULL, 0),
(376, 377, 'subtask', 'png', '158a44f0.png', 19, NULL, 1487163143, NULL, 0),
(377, 378, 'subtask', 'png', '558a44f4.png', 19, NULL, 1487163206, NULL, 0),
(378, 379, 'subtask', 'png', '358a44f5.png', 19, NULL, 1487163218, NULL, 0),
(379, 206, 'task', 'png', '158a44fb.png', 19, NULL, 1487163313, NULL, 0),
(380, 385, 'subtask', 'png', '358a474a.png', 19, NULL, 1487172783, NULL, 0),
(381, 386, 'subtask', 'png', '558a474c.png', 19, NULL, 1487172803, NULL, 0),
(382, 387, 'subtask', 'png', '558a474e.png', 19, NULL, 1487172835, NULL, 0),
(383, 388, 'subtask', 'png', '658a4750.png', 19, NULL, 1487172873, NULL, 0),
(384, 389, 'subtask', 'png', '158a4755.png', 19, NULL, 1487172948, NULL, 0),
(385, 390, 'subtask', 'png', '158a4756.png', 19, NULL, 1487172964, NULL, 0),
(386, 391, 'subtask', 'png', '158a4756.png', 19, NULL, 1487172975, NULL, 0),
(387, 392, 'subtask', 'png', '558a475a.png', 19, NULL, 1487173033, NULL, 0),
(388, 216, 'task', 'png', '258a4760.png', 19, NULL, 1487173121, NULL, 0),
(389, 217, 'task', 'png', '558a4762.png', 19, NULL, 1487173163, NULL, 0),
(390, 393, 'subtask', 'png', '158a47c2.png', 19, NULL, 1487174703, NULL, 0),
(391, 394, 'subtask', 'png', '258a47ec.png', 19, NULL, 1487175372, NULL, 0),
(392, 395, 'subtask', 'png', '258a47ee.png', 19, NULL, 1487175402, NULL, 0),
(393, 396, 'subtask', 'png', '558a47f9.png', 19, NULL, 1487175572, NULL, 0),
(394, 397, 'subtask', 'png', '458a4813.png', 19, NULL, 1487175987, NULL, 0),
(395, 398, 'subtask', 'png', '658a481d.png', 19, NULL, 1487176148, NULL, 0),
(396, 399, 'subtask', 'png', '258a482d.png', 19, NULL, 1487176414, NULL, 0),
(397, 400, 'subtask', 'png', '458a4844.png', 19, NULL, 1487176769, NULL, 0),
(398, 401, 'subtask', 'png', '258a4846.png', 19, NULL, 1487176801, NULL, 0),
(399, 402, 'subtask', 'png', '558a484d.png', 19, NULL, 1487176919, NULL, 0),
(400, 403, 'subtask', 'png', '558a484f.png', 19, NULL, 1487176958, NULL, 0),
(401, 404, 'subtask', 'png', '558a4851.png', 19, NULL, 1487176976, NULL, 0),
(402, 405, 'subtask', 'png', '558a4852.png', 19, NULL, 1487176997, NULL, 0),
(403, 406, 'subtask', 'png', '458a486f.png', 19, NULL, 1487177456, NULL, 1),
(404, 407, 'subtask', 'png', '658a486f.png', 19, NULL, 1487177471, NULL, 1),
(405, 408, 'subtask', 'png', '458a5845.png', 19, NULL, 1487242329, NULL, 0),
(406, 409, 'subtask', 'png', '458a5849.png', 19, NULL, 1487242394, NULL, 0),
(407, 410, 'subtask', 'png', '358a584a.png', 19, NULL, 1487242412, NULL, 0),
(408, 411, 'subtask', 'png', '358a584c.png', 19, NULL, 1487242443, NULL, 0),
(409, 412, 'subtask', 'png', '658a5854.png', 19, NULL, 1487242574, NULL, 0),
(410, 413, 'subtask', 'png', '358a5856.png', 19, NULL, 1487242595, NULL, 0),
(411, 414, 'subtask', 'png', '458a5859.png', 19, NULL, 1487242654, NULL, 0),
(412, 415, 'subtask', 'png', '258a585b.png', 19, NULL, 1487242677, NULL, 0),
(413, 416, 'subtask', 'png', '558a585f.png', 19, NULL, 1487242746, NULL, 0),
(414, 417, 'subtask', 'png', '358a5860.png', 19, NULL, 1487242763, NULL, 0),
(415, 418, 'subtask', 'png', '258a5862.png', 19, NULL, 1487242787, NULL, 0),
(416, 419, 'subtask', 'png', '458a5863.png', 19, NULL, 1487242814, NULL, 0),
(417, 420, 'subtask', 'png', '158a5865.png', 19, NULL, 1487242836, NULL, 0),
(418, 421, 'subtask', 'png', '658a5866.png', 19, NULL, 1487242852, NULL, 0),
(419, 422, 'subtask', 'png', '658a5868.png', 19, NULL, 1487242889, NULL, 0),
(420, 423, 'subtask', 'png', '158a5869.png', 19, NULL, 1487242909, NULL, 0),
(421, 424, 'subtask', 'png', '258a586f.png', 19, NULL, 1487242996, NULL, 0),
(422, 425, 'subtask', 'png', '358a5870.png', 19, NULL, 1487243013, NULL, 0),
(423, 426, 'subtask', 'png', '158a5871.png', 19, NULL, 1487243027, NULL, 0),
(424, 427, 'subtask', 'png', '258a5872.png', 19, NULL, 1487243044, NULL, 0),
(425, 428, 'subtask', 'png', '558a5873.png', 19, NULL, 1487243062, NULL, 0),
(426, 429, 'subtask', 'png', '258a5874.png', 19, NULL, 1487243077, NULL, 0),
(427, 430, 'subtask', 'png', '558a5875.png', 19, NULL, 1487243092, NULL, 0),
(428, 431, 'subtask', 'png', '158a5876.png', 19, NULL, 1487243110, NULL, 0),
(429, 432, 'subtask', 'png', '358a588a.png', 19, NULL, 1487243432, NULL, 0),
(430, 433, 'subtask', 'png', '658a588b.png', 19, NULL, 1487243448, NULL, 0),
(431, 434, 'subtask', 'png', '158a588c.png', 19, NULL, 1487243462, NULL, 0),
(432, 435, 'subtask', 'png', '158a588d.png', 19, NULL, 1487243482, NULL, 0),
(433, 230, 'task', 'png', '258a5895.png', 19, NULL, 1487243601, NULL, 0),
(434, 231, 'task', 'png', '258a5897.png', 19, NULL, 1487243637, NULL, 0),
(435, 440, 'subtask', 'png', '258a58a2.png', 19, NULL, 1487243816, NULL, 0),
(436, 441, 'subtask', 'png', '658a58a4.png', 19, NULL, 1487243849, NULL, 0),
(437, 234, 'task', 'png', '658a58ad.png', 19, NULL, 1487243990, NULL, 0),
(438, 235, 'task', 'png', '258a58c7.png', 19, NULL, 1487244401, NULL, 0),
(439, 460, 'subtask', 'png', '458a5919.png', 19, NULL, 1487245714, NULL, 0),
(440, 461, 'subtask', 'png', '458a591a.png', 19, NULL, 1487245736, NULL, 0),
(441, 462, 'subtask', 'png', '358a592a.png', 19, NULL, 1487245995, NULL, 0),
(442, 463, 'subtask', 'png', '358a592c.png', 19, NULL, 1487246018, NULL, 0),
(443, 464, 'subtask', 'png', '158a5937.png', 19, NULL, 1487246201, NULL, 0),
(444, 465, 'subtask', 'png', '158a5939.png', 19, NULL, 1487246229, NULL, 0),
(445, 466, 'subtask', 'png', '358a593a.png', 19, NULL, 1487246243, NULL, 0),
(446, 467, 'subtask', 'png', '658a593b.png', 19, NULL, 1487246262, NULL, 0),
(447, 468, 'subtask', 'png', '658a5949.png', 19, NULL, 1487246489, NULL, 1),
(448, 469, 'subtask', 'png', '458a594e.png', 19, NULL, 1487246565, NULL, 0),
(449, 470, 'subtask', 'png', '158a5951.png', 19, NULL, 1487246612, NULL, 0),
(450, 471, 'subtask', 'png', '658a5955.png', 19, NULL, 1487246679, NULL, 0),
(451, 472, 'subtask', 'png', '258a5957.png', 19, NULL, 1487246711, NULL, 0),
(452, 473, 'subtask', 'png', '558a5960.png', 19, NULL, 1487246848, NULL, 0),
(453, 474, 'subtask', 'png', '358a5963.png', 19, NULL, 1487246901, NULL, 1),
(454, 475, 'subtask', 'png', '158a5966.png', 19, NULL, 1487246955, NULL, 0),
(455, 476, 'subtask', 'png', '158a5987.png', 19, NULL, 1487247485, NULL, 0),
(456, 477, 'subtask', 'png', '358a59f5.png', 19, NULL, 1487249240, NULL, 0),
(457, 478, 'subtask', 'png', '558a59f7.png', 19, NULL, 1487249276, NULL, 0),
(458, 479, 'subtask', 'png', '458a59f9.png', 19, NULL, 1487249306, NULL, 0),
(459, 480, 'subtask', 'png', '658a59fb.png', 19, NULL, 1487249332, NULL, 0),
(460, 481, 'subtask', 'png', '258a5a03.png', 19, NULL, 1487249456, NULL, 0),
(461, 482, 'subtask', 'png', '158a5a04.png', 19, NULL, 1487249482, NULL, 0),
(462, 483, 'subtask', 'png', '558a5a06.png', 19, NULL, 1487249518, NULL, 0),
(463, 484, 'subtask', 'png', '258a5a08.png', 19, NULL, 1487249539, NULL, 0),
(464, 485, 'subtask', 'png', '658a5a09.png', 19, NULL, 1487249560, NULL, 0),
(465, 489, 'subtask', 'png', '558a5bff.png', 19, NULL, 1487257591, NULL, 0),
(466, 490, 'subtask', 'png', '258a5c01.png', 19, NULL, 1487257621, NULL, 0),
(467, 491, 'subtask', 'png', '458a5c03.png', 19, NULL, 1487257650, NULL, 0),
(468, 492, 'subtask', 'png', '558a5c04.png', 19, NULL, 1487257666, NULL, 0),
(469, 501, 'subtask', 'png', '258a5c1e.png', 19, NULL, 1487258083, NULL, 0),
(470, 502, 'subtask', 'png', '458a5c1f.png', 19, NULL, 1487258111, NULL, 0),
(471, 503, 'subtask', 'png', '358a5c21.png', 19, NULL, 1487258135, NULL, 0),
(472, 504, 'subtask', 'png', '158a5c2c.png', 19, NULL, 1487258306, NULL, 0),
(473, 505, 'subtask', 'png', '458a5c31.png', 19, NULL, 1487258392, NULL, 0),
(474, 506, 'subtask', 'png', '458a5c32.png', 19, NULL, 1487258411, NULL, 0),
(475, 507, 'subtask', 'png', '458a5c33.png', 19, NULL, 1487258426, NULL, 0),
(476, 508, 'subtask', 'png', '358a5c35.png', 19, NULL, 1487258452, NULL, 0),
(477, 509, 'subtask', 'png', '458a5c43.png', 19, NULL, 1487258680, NULL, 0),
(478, 510, 'subtask', 'png', '358a5c45.png', 19, NULL, 1487258715, NULL, 0),
(479, 511, 'subtask', 'png', '558a5c46.png', 19, NULL, 1487258733, NULL, 0),
(480, 512, 'subtask', 'png', '258a5c48.png', 19, NULL, 1487258755, NULL, 0),
(481, 513, 'subtask', 'png', '658a5c49.png', 19, NULL, 1487258778, NULL, 0),
(482, 514, 'subtask', 'png', '158a5c4e.png', 19, NULL, 1487258849, NULL, 0),
(483, 515, 'subtask', 'png', '158a5c55.png', 19, NULL, 1487258962, NULL, 0),
(484, 516, 'subtask', 'png', '458a5c5d.png', 19, NULL, 1487259088, NULL, 0),
(485, 517, 'subtask', 'png', '158a5ce3.png', 19, NULL, 1487261241, NULL, 0),
(486, 518, 'subtask', 'png', '158a5ce4.png', 19, NULL, 1487261257, NULL, 0),
(487, 519, 'subtask', 'png', '358a5ce5.png', 19, NULL, 1487261273, NULL, 0),
(488, 520, 'subtask', 'png', '358a5ce7.png', 19, NULL, 1487261297, NULL, 0),
(489, 521, 'subtask', 'png', '258a5ce9.png', 19, NULL, 1487261330, NULL, 0),
(490, 522, 'subtask', 'png', '358a5cea.png', 19, NULL, 1487261353, NULL, 0),
(491, 523, 'subtask', 'png', '658a5cec.png', 19, NULL, 1487261377, NULL, 0),
(492, 524, 'subtask', 'png', '158a5ced.png', 19, NULL, 1487261404, NULL, 0),
(493, 525, 'subtask', 'png', '258a5cf9.png', 19, NULL, 1487261598, NULL, 0),
(494, 526, 'subtask', 'png', '658a5cfa.png', 19, NULL, 1487261613, NULL, 0),
(495, 527, 'subtask', 'png', '158a5cfc.png', 19, NULL, 1487261647, NULL, 0),
(496, 528, 'subtask', 'png', '458a5cfd.png', 19, NULL, 1487261661, NULL, 0),
(497, 529, 'subtask', 'png', '258a5d07.png', 19, NULL, 1487261810, NULL, 0),
(498, 530, 'subtask', 'png', '258a5d08.png', 19, NULL, 1487261831, NULL, 0),
(499, 531, 'subtask', 'png', '658a5d09.png', 19, NULL, 1487261849, NULL, 0),
(500, 532, 'subtask', 'png', '458a5d0a.png', 19, NULL, 1487261869, NULL, 0),
(501, 533, 'subtask', 'png', '358a5d0e.png', 19, NULL, 1487261929, NULL, 0),
(502, 534, 'subtask', 'png', '358a5d0f.png', 19, NULL, 1487261951, NULL, 0),
(503, 535, 'subtask', 'png', '258a5d17.png', 19, NULL, 1487262065, NULL, 0),
(504, 536, 'subtask', 'png', '158a5d18.png', 19, NULL, 1487262082, NULL, 0),
(505, 537, 'subtask', 'png', '458a5d19.png', 19, NULL, 1487262099, NULL, 0),
(506, 538, 'subtask', 'png', '558a5d1b.png', 19, NULL, 1487262133, NULL, 0),
(507, 539, 'subtask', 'png', '258a5d20.png', 19, NULL, 1487262216, NULL, 0),
(508, 540, 'subtask', 'png', '158a5d22.png', 19, NULL, 1487262240, NULL, 0),
(509, 541, 'subtask', 'png', '358a5d22.png', 19, NULL, 1487262255, NULL, 0),
(510, 542, 'subtask', 'png', '258a5d27.png', 19, NULL, 1487262325, NULL, 1),
(511, 543, 'subtask', 'png', '158a5d29.png', 19, NULL, 1487262362, NULL, 0),
(512, 544, 'subtask', 'png', '158a5d2a.png', 19, NULL, 1487262378, NULL, 0),
(513, 545, 'subtask', 'png', '358a5d2b.png', 19, NULL, 1487262392, NULL, 0),
(514, 266, 'task', 'png', '558a5d32.png', 19, NULL, 1487262496, NULL, 0),
(515, 267, 'task', 'png', '558a5d35.png', 19, NULL, 1487262557, NULL, 0),
(516, 268, 'task', 'png', '558a5d40.png', 19, NULL, 1487262726, NULL, 0),
(517, 269, 'task', 'png', '358a5d4f.png', 19, NULL, 1487262966, NULL, 0),
(518, 271, 'task', 'png', '558a5d61.png', 19, NULL, 1487263249, NULL, 0),
(519, 546, 'subtask', 'png', '658a5d65.png', 19, NULL, 1487263313, NULL, 0),
(520, 547, 'subtask', 'png', '658a5d66.png', 19, NULL, 1487263337, NULL, 0),
(521, 548, 'subtask', 'png', '658a5d6b.png', 19, NULL, 1487263409, NULL, 0),
(522, 549, 'subtask', 'png', '358a5d6c.png', 19, NULL, 1487263431, NULL, 0),
(523, 550, 'subtask', 'png', '358aaf64.png', 19, NULL, 1487599178, NULL, 0),
(524, 551, 'subtask', 'png', '358aaf68.png', 19, NULL, 1487599246, NULL, 0),
(525, 552, 'subtask', 'png', '458aaf82.png', 19, NULL, 1487599652, NULL, 0),
(526, 553, 'subtask', 'png', '658aaf84.png', 19, NULL, 1487599681, NULL, 0),
(527, 554, 'subtask', 'png', '558aaf88.png', 19, NULL, 1487599756, NULL, 0),
(528, 555, 'subtask', 'png', '458aaf8c.png', 19, NULL, 1487599813, NULL, 0),
(529, 556, 'subtask', 'png', '458aaf9c.png', 19, NULL, 1487600064, NULL, 0),
(530, 557, 'subtask', 'png', '258aaf9d.png', 19, NULL, 1487600095, NULL, 0),
(531, 558, 'subtask', 'png', '158aaf9f.png', 19, NULL, 1487600127, NULL, 0),
(532, 559, 'subtask', 'png', '458aafa1.png', 19, NULL, 1487600154, NULL, 0),
(533, 560, 'subtask', 'png', '658aafa4.png', 19, NULL, 1487600205, NULL, 0),
(534, 561, 'subtask', 'png', '458aafa6.png', 19, NULL, 1487600237, NULL, 0),
(535, 562, 'subtask', 'png', '458ab089.png', 19, NULL, 1487603864, NULL, 0),
(536, 563, 'subtask', 'png', '258ab08a.png', 19, NULL, 1487603880, NULL, 0),
(537, 564, 'subtask', 'png', '558ab08b.png', 19, NULL, 1487603902, NULL, 0),
(538, 565, 'subtask', 'png', '558ab08c.png', 19, NULL, 1487603918, NULL, 0),
(539, 566, 'subtask', 'png', '458ab08e.png', 19, NULL, 1487603938, NULL, 0),
(540, 567, 'subtask', 'png', '658ab090.png', 19, NULL, 1487603971, NULL, 0),
(541, 568, 'subtask', 'png', '458ab091.png', 19, NULL, 1487603986, NULL, 0),
(542, 569, 'subtask', 'png', '558ab092.png', 19, NULL, 1487604006, NULL, 0),
(543, 570, 'subtask', 'png', '458ab096.png', 19, NULL, 1487604079, NULL, 0),
(544, 571, 'subtask', 'png', '158ab097.png', 19, NULL, 1487604093, NULL, 0),
(545, 572, 'subtask', 'png', '658ab098.png', 19, NULL, 1487604109, NULL, 0),
(546, 574, 'subtask', 'png', '558ab09f.png', 19, NULL, 1487604209, NULL, 0),
(547, 575, 'subtask', 'png', '358ab0a0.png', 19, NULL, 1487604235, NULL, 0),
(548, 576, 'subtask', 'png', '158ab0a3.png', 19, NULL, 1487604275, NULL, 0),
(549, 577, 'subtask', 'png', '358ab0a4.png', 19, NULL, 1487604298, NULL, 0),
(550, 578, 'subtask', 'png', '258ab0bd.png', 19, NULL, 1487604695, NULL, 0),
(551, 579, 'subtask', 'png', '658ab0be.png', 19, NULL, 1487604719, NULL, 0),
(552, 580, 'subtask', 'png', '158ab0c0.png', 19, NULL, 1487604741, NULL, 0),
(553, 581, 'subtask', 'png', '258ab0c1.png', 19, NULL, 1487604766, NULL, 0),
(554, 582, 'subtask', 'png', '658ab0c3.png', 19, NULL, 1487604792, NULL, 0),
(555, 583, 'subtask', 'png', '558ab0c4.png', 19, NULL, 1487604813, NULL, 0),
(556, 584, 'subtask', 'png', '658ab0c4.png', 19, NULL, 1487604814, NULL, 0),
(557, 585, 'subtask', 'png', '558ab0c7.png', 19, NULL, 1487604851, NULL, 0),
(558, 586, 'subtask', 'png', '658ab0cb.png', 19, NULL, 1487604922, NULL, 0),
(559, 587, 'subtask', 'png', '458ab0de.png', 19, NULL, 1487605220, NULL, 0),
(560, 588, 'subtask', 'png', '158ab0df.png', 19, NULL, 1487605246, NULL, 0),
(561, 589, 'subtask', 'png', '358ab0e1.png', 19, NULL, 1487605264, NULL, 0),
(562, 590, 'subtask', 'png', '658ab0e2.png', 19, NULL, 1487605285, NULL, 0),
(563, 591, 'subtask', 'png', '158ab0e4.png', 19, NULL, 1487605315, NULL, 0),
(564, 592, 'subtask', 'png', '158ab0e5.png', 19, NULL, 1487605335, NULL, 0),
(565, 593, 'subtask', 'png', '358ab0e6.png', 19, NULL, 1487605356, NULL, 0),
(566, 594, 'subtask', 'png', '258ab0ea.png', 19, NULL, 1487605414, NULL, 0),
(567, 595, 'subtask', 'png', '158ab0f4.png', 19, NULL, 1487605580, NULL, 0),
(568, 596, 'subtask', 'png', '558ab0f6.png', 19, NULL, 1487605603, NULL, 0),
(569, 597, 'subtask', 'png', '158ab0ff.png', 19, NULL, 1487605746, NULL, 0),
(570, 598, 'subtask', 'png', '458ab100.png', 19, NULL, 1487605775, NULL, 0),
(571, 599, 'subtask', 'png', '658ab109.png', 19, NULL, 1487605904, NULL, 0),
(572, 600, 'subtask', 'png', '458ab10b.png', 19, NULL, 1487605942, NULL, 0),
(573, 601, 'subtask', 'png', '158ab110.png', 19, NULL, 1487606029, NULL, 0),
(574, 602, 'subtask', 'png', '558ab112.png', 19, NULL, 1487606052, NULL, 0),
(575, 603, 'subtask', 'png', '358ab122.png', 19, NULL, 1487606319, NULL, 0),
(576, 604, 'subtask', 'png', '658ab124.png', 19, NULL, 1487606342, NULL, 0),
(577, 605, 'subtask', 'png', '158ab126.png', 19, NULL, 1487606378, NULL, 0),
(578, 606, 'subtask', 'png', '358ab136.png', 19, NULL, 1487606633, NULL, 0),
(579, 607, 'subtask', 'png', '358ab137.png', 19, NULL, 1487606653, NULL, 0),
(580, 608, 'subtask', 'png', '458ab139.png', 19, NULL, 1487606672, NULL, 0),
(581, 609, 'subtask', 'png', '358ab13f.png', 19, NULL, 1487606773, NULL, 0),
(582, 610, 'subtask', 'png', '558ab140.png', 19, NULL, 1487606793, NULL, 0),
(583, 611, 'subtask', 'png', '158ab141.png', 19, NULL, 1487606811, NULL, 0),
(584, 612, 'subtask', 'png', '358ab146.png', 19, NULL, 1487606883, NULL, 0),
(585, 613, 'subtask', 'png', '458ab146.png', 19, NULL, 1487606885, NULL, 0),
(586, 614, 'subtask', 'png', '458ab148.png', 19, NULL, 1487606912, NULL, 0),
(587, 615, 'subtask', 'png', '158ab14d.png', 19, NULL, 1487606992, NULL, 0),
(588, 616, 'subtask', 'png', '358ab14e.png', 19, NULL, 1487607013, NULL, 0),
(589, 617, 'subtask', 'png', '358ab154.png', 19, NULL, 1487607105, NULL, 0),
(590, 618, 'subtask', 'png', '158ab156.png', 19, NULL, 1487607137, NULL, 0),
(591, 619, 'subtask', 'png', '558ab160.png', 19, NULL, 1487607298, NULL, 0),
(592, 620, 'subtask', 'png', '458ab161.png', 19, NULL, 1487607318, NULL, 0),
(593, 621, 'subtask', 'png', '658ab16b.png', 19, NULL, 1487607474, NULL, 0),
(594, 622, 'subtask', 'png', '258ab16c.png', 19, NULL, 1487607496, NULL, 0),
(595, 623, 'subtask', 'png', '358ab16d.png', 19, NULL, 1487607515, NULL, 0),
(596, 624, 'subtask', 'png', '658ab16f.png', 19, NULL, 1487607551, NULL, 0),
(597, 625, 'subtask', 'png', '158ab174.png', 19, NULL, 1487607627, NULL, 0),
(598, 626, 'subtask', 'png', '358ab182.png', 19, NULL, 1487607855, NULL, 0),
(599, 627, 'subtask', 'png', '458ab184.png', 19, NULL, 1487607881, NULL, 0),
(600, 628, 'subtask', 'png', '558ab186.png', 19, NULL, 1487607908, NULL, 0),
(601, 635, 'subtask', 'png', '158ab1a4.png', 19, NULL, 1487608394, NULL, 0),
(602, 636, 'subtask', 'png', '558ab1a6.png', 19, NULL, 1487608419, NULL, 0),
(603, 309, 'task', 'png', '558ab1a9.png', 19, NULL, 1487608479, NULL, 0),
(604, 638, 'subtask', 'png', '558ab1b9.png', 19, NULL, 1487608734, NULL, 0),
(605, 639, 'subtask', 'png', '258ac101.png', 19, NULL, 1487671320, NULL, 0),
(606, 640, 'subtask', 'png', '258ac102.png', 19, NULL, 1487671343, NULL, 0),
(607, 641, 'subtask', 'png', '558ac104.png', 19, NULL, 1487671368, NULL, 0),
(608, 642, 'subtask', 'png', '658ac105.png', 19, NULL, 1487671388, NULL, 0),
(609, 643, 'subtask', 'png', '558ac107.png', 19, NULL, 1487671418, NULL, 0),
(610, 644, 'subtask', 'png', '658ac10a.png', 19, NULL, 1487671467, NULL, 0),
(611, 645, 'subtask', 'png', '658ac10c.png', 19, NULL, 1487671493, NULL, 0),
(612, 646, 'subtask', 'png', '158ac10d.png', 19, NULL, 1487671515, NULL, 0),
(613, 647, 'subtask', 'png', '558ac111.png', 19, NULL, 1487671580, NULL, 0),
(614, 648, 'subtask', 'png', '558ac112.png', 19, NULL, 1487671596, NULL, 0),
(615, 649, 'subtask', 'png', '158ac114.png', 19, NULL, 1487671618, NULL, 0),
(616, 650, 'subtask', 'png', '358ac115.png', 19, NULL, 1487671635, NULL, 0),
(617, 651, 'subtask', 'png', '158ac118.png', 19, NULL, 1487671691, NULL, 0),
(618, 652, 'subtask', 'png', '158ac11a.png', 19, NULL, 1487671717, NULL, 0),
(619, 653, 'subtask', 'png', '458ac11c.png', 19, NULL, 1487671748, NULL, 0),
(620, 654, 'subtask', 'png', '558ac11d.png', 19, NULL, 1487671763, NULL, 0),
(621, 655, 'subtask', 'png', '258ac120.png', 19, NULL, 1487671820, NULL, 0),
(622, 656, 'subtask', 'png', '658ac122.png', 19, NULL, 1487671842, NULL, 0),
(623, 657, 'subtask', 'png', '658ac123.png', 19, NULL, 1487671866, NULL, 0),
(624, 658, 'subtask', 'png', '458ac124.png', 19, NULL, 1487671884, NULL, 0),
(625, 659, 'subtask', 'png', '258ac125.png', 19, NULL, 1487671903, NULL, 0),
(626, 660, 'subtask', 'png', '658ac127.png', 19, NULL, 1487671923, NULL, 0),
(627, 661, 'subtask', 'png', '358ac128.png', 19, NULL, 1487671948, NULL, 0),
(628, 662, 'subtask', 'png', '158ac12b.png', 19, NULL, 1487671984, NULL, 0),
(629, 663, 'subtask', 'png', '658ac12f.png', 19, NULL, 1487672057, NULL, 0),
(630, 664, 'subtask', 'png', '658ac130.png', 19, NULL, 1487672071, NULL, 0),
(631, 665, 'subtask', 'png', '458ac136.png', 19, NULL, 1487672167, NULL, 0),
(632, 666, 'subtask', 'png', '658ac139.png', 19, NULL, 1487672210, NULL, 0),
(633, 667, 'subtask', 'png', '358ac13a.png', 19, NULL, 1487672232, NULL, 0),
(634, 668, 'subtask', 'png', '158ac13b.png', 19, NULL, 1487672250, NULL, 0),
(635, 669, 'subtask', 'png', '558ac13c.png', 19, NULL, 1487672265, NULL, 0),
(636, 670, 'subtask', 'png', '658ac13d.png', 19, NULL, 1487672280, NULL, 0),
(637, 671, 'subtask', 'png', '258ac14e.png', 19, NULL, 1487672550, NULL, 0),
(638, 672, 'subtask', 'png', '458ac14f.png', 19, NULL, 1487672572, NULL, 0),
(639, 673, 'subtask', 'png', '158ac150.png', 19, NULL, 1487672590, NULL, 0),
(640, 674, 'subtask', 'png', '558ac152.png', 19, NULL, 1487672614, NULL, 0),
(641, 675, 'subtask', 'png', '158ac1ed.png', 19, NULL, 1487675088, NULL, 0),
(642, 676, 'subtask', 'png', '158ac1ee.png', 19, NULL, 1487675109, NULL, 0),
(643, 677, 'subtask', 'png', '358ac1f0.png', 19, NULL, 1487675151, NULL, 0),
(644, 678, 'subtask', 'png', '458ac1f2.png', 19, NULL, 1487675173, NULL, 0),
(645, 324, 'task', 'png', '458ac1f7.png', 19, NULL, 1487675257, NULL, 0),
(646, 325, 'task', 'png', '558ac1fb.png', 19, NULL, 1487675323, NULL, 0),
(647, 679, 'subtask', 'png', '158ac201.png', 19, NULL, 1487675421, NULL, 0),
(648, 680, 'subtask', 'png', '358ac203.png', 19, NULL, 1487675441, NULL, 0),
(649, 681, 'subtask', 'png', '558ac203.png', 19, NULL, 1487675442, NULL, 1),
(650, 682, 'subtask', 'png', '158ac204.png', 19, NULL, 1487675465, NULL, 0),
(651, 683, 'subtask', 'png', '458ac205.png', 19, NULL, 1487675479, NULL, 0),
(652, 684, 'subtask', 'png', '258ac206.png', 19, NULL, 1487675494, NULL, 0),
(653, 685, 'subtask', 'png', '658ac20a.png', 19, NULL, 1487675560, NULL, 0),
(654, 686, 'subtask', 'png', '358ac20b.png', 19, NULL, 1487675574, NULL, 0),
(655, 687, 'subtask', 'png', '558ac20c.png', 19, NULL, 1487675595, NULL, 0),
(656, 688, 'subtask', 'png', '458ac214.png', 19, NULL, 1487675723, NULL, 0),
(657, 689, 'subtask', 'png', '358ac215.png', 19, NULL, 1487675740, NULL, 0),
(658, 690, 'subtask', 'png', '158ac217.png', 19, NULL, 1487675773, NULL, 0),
(659, 691, 'subtask', 'png', '258ac219.png', 19, NULL, 1487675796, NULL, 0),
(660, 692, 'subtask', 'png', '658ac21a.png', 19, NULL, 1487675816, NULL, 0),
(661, 693, 'subtask', 'png', '458ac21b.png', 19, NULL, 1487675834, NULL, 0),
(662, 694, 'subtask', 'png', '558ac21d.png', 19, NULL, 1487675860, NULL, 0),
(663, 695, 'subtask', 'png', '158ac21e.png', 19, NULL, 1487675876, NULL, 0),
(664, 696, 'subtask', 'png', '258ac227.png', 19, NULL, 1487676026, NULL, 0),
(665, 697, 'subtask', 'png', '558ac229.png', 19, NULL, 1487676059, NULL, 0),
(666, 698, 'subtask', 'png', '658ac236.png', 19, NULL, 1487676259, NULL, 0),
(667, 699, 'subtask', 'png', '258ac237.png', 19, NULL, 1487676281, NULL, 0),
(668, 700, 'subtask', 'png', '558ac26b.png', 19, NULL, 1487677117, NULL, 0),
(669, 701, 'subtask', 'png', '458ac26d.png', 19, NULL, 1487677144, NULL, 0),
(670, 702, 'subtask', 'png', '258ac275.png', 19, NULL, 1487677273, NULL, 0),
(671, 703, 'subtask', 'png', '258ac276.png', 19, NULL, 1487677289, NULL, 0),
(672, 704, 'subtask', 'png', '658ac277.png', 19, NULL, 1487677306, NULL, 0),
(673, 705, 'subtask', 'png', '458ac279.png', 19, NULL, 1487677330, NULL, 0),
(674, 706, 'subtask', 'png', '258ac27a.png', 19, NULL, 1487677351, NULL, 0),
(675, 707, 'subtask', 'png', '458ac27c.png', 19, NULL, 1487677378, NULL, 0),
(676, 708, 'subtask', 'png', '458ac281.png', 19, NULL, 1487677467, NULL, 1),
(677, 709, 'subtask', 'png', '358ac283.png', 19, NULL, 1487677502, NULL, 0),
(678, 710, 'subtask', 'png', '558ac285.png', 19, NULL, 1487677529, NULL, 0),
(679, 711, 'subtask', 'png', '458ac286.png', 19, NULL, 1487677545, NULL, 0),
(680, 712, 'subtask', 'png', '258ac28d.png', 19, NULL, 1487677655, NULL, 0),
(681, 713, 'subtask', 'png', '558ac28e.png', 19, NULL, 1487677674, NULL, 0),
(682, 714, 'subtask', 'png', '558ac291.png', 19, NULL, 1487677716, NULL, 0),
(683, 715, 'subtask', 'png', '458ac29b.png', 19, NULL, 1487677884, NULL, 0),
(684, 716, 'subtask', 'png', '558ac29e.png', 19, NULL, 1487677929, NULL, 0),
(685, 717, 'subtask', 'png', '658ac2a3.png', 19, NULL, 1487678003, NULL, 0),
(686, 718, 'subtask', 'png', '558ac2a4.png', 19, NULL, 1487678026, NULL, 0),
(687, 719, 'subtask', 'png', '158ac2aa.png', 19, NULL, 1487678113, NULL, 0),
(688, 720, 'subtask', 'png', '658ac2ac.png', 19, NULL, 1487678146, NULL, 0),
(689, 721, 'subtask', 'png', '558ac2b6.png', 19, NULL, 1487678307, NULL, 0),
(690, 722, 'subtask', 'png', '558ac2b7.png', 19, NULL, 1487678325, NULL, 0),
(691, 723, 'subtask', 'png', '358ac2dd.png', 19, NULL, 1487678933, NULL, 0),
(692, 724, 'subtask', 'png', '658ac2df.png', 19, NULL, 1487678962, NULL, 0),
(693, 725, 'subtask', 'png', '158ac2f1.png', 19, NULL, 1487679249, NULL, 0),
(694, 726, 'subtask', 'png', '158ac2f2.png', 19, NULL, 1487679272, NULL, 0),
(695, 729, 'subtask', 'png', '658ac5c3.png', 19, NULL, 1487690813, NULL, 0),
(696, 730, 'subtask', 'png', '358ac5cd.png', 19, NULL, 1487690961, NULL, 0),
(697, 731, 'subtask', 'png', '258ac5ce.png', 19, NULL, 1487690979, NULL, 0),
(698, 732, 'subtask', 'png', '558ac5dd.png', 19, NULL, 1487691228, NULL, 0),
(699, 733, 'subtask', 'png', '158ac60e.png', 19, NULL, 1487692001, NULL, 0),
(700, 734, 'subtask', 'png', '658ac60f.png', 19, NULL, 1487692025, NULL, 0),
(701, 735, 'subtask', 'png', '658ac610.png', 19, NULL, 1487692046, NULL, 0),
(702, 736, 'subtask', 'png', '658ac612.png', 19, NULL, 1487692065, NULL, 0),
(703, 737, 'subtask', 'png', '558ac617.png', 19, NULL, 1487692149, NULL, 0),
(704, 738, 'subtask', 'png', '358ac618.png', 19, NULL, 1487692167, NULL, 0),
(705, 739, 'subtask', 'png', '258ac61a.png', 19, NULL, 1487692192, NULL, 0),
(706, 740, 'subtask', 'png', '358ac61b.png', 19, NULL, 1487692211, NULL, 0),
(707, 741, 'subtask', 'png', '358ac61c.png', 19, NULL, 1487692238, NULL, 0),
(708, 742, 'subtask', 'png', '258ac61e.png', 19, NULL, 1487692260, NULL, 0),
(709, 743, 'subtask', 'png', '658ac62e.png', 19, NULL, 1487692525, NULL, 0),
(710, 744, 'subtask', 'png', '258ac62f.png', 19, NULL, 1487692543, NULL, 0),
(711, 745, 'subtask', 'png', '158ac633.png', 19, NULL, 1487692605, NULL, 0),
(712, 746, 'subtask', 'png', '258ac635.png', 19, NULL, 1487692625, NULL, 0),
(713, 747, 'subtask', 'png', '658ac638.png', 19, NULL, 1487692684, NULL, 0),
(714, 748, 'subtask', 'png', '458ac639.png', 19, NULL, 1487692700, NULL, 0),
(715, 749, 'subtask', 'png', '358ac63a.png', 19, NULL, 1487692713, NULL, 0),
(716, 750, 'subtask', 'png', '658ac63c.png', 19, NULL, 1487692740, NULL, 0),
(717, 751, 'subtask', 'png', '158ac6b6.png', 19, NULL, 1487694690, NULL, 0),
(718, 752, 'subtask', 'png', '558ac6b7.png', 19, NULL, 1487694706, NULL, 0),
(719, 753, 'subtask', 'png', '558ac6b8.png', 19, NULL, 1487694726, NULL, 0),
(720, 754, 'subtask', 'png', '658ac6b9.png', 19, NULL, 1487694744, NULL, 0),
(721, 764, 'subtask', 'png', '258ac6e1.png', 19, NULL, 1487695387, NULL, 0),
(722, 765, 'subtask', 'png', '658ac6e2.png', 19, NULL, 1487695407, NULL, 0),
(723, 766, 'subtask', 'png', '458ac6e3.png', 19, NULL, 1487695423, NULL, 0),
(724, 767, 'subtask', 'png', '458ac6e7.png', 19, NULL, 1487695473, NULL, 0),
(725, 768, 'subtask', 'png', '158ad7ce.png', 19, NULL, 1487764711, NULL, 0),
(726, 769, 'subtask', 'png', '158ad7d0.png', 19, NULL, 1487764738, NULL, 0),
(727, 770, 'subtask', 'png', '458ad7d6.png', 19, NULL, 1487764845, NULL, 0),
(728, 771, 'subtask', 'png', '158ad7d8.png', 19, NULL, 1487764864, NULL, 0),
(729, 366, 'task', 'png', '358ad815.png', 19, NULL, 1487765844, NULL, 0),
(730, 783, 'subtask', 'png', '358aeb63.png', 19, NULL, 1487844921, NULL, 0),
(731, 784, 'subtask', 'png', '358aeb65.png', 19, NULL, 1487844948, NULL, 0),
(732, 785, 'subtask', 'png', '458aeb6d.png', 19, NULL, 1487845084, NULL, 0),
(733, 786, 'subtask', 'png', '558aeb73.png', 19, NULL, 1487845168, NULL, 0),
(734, 787, 'subtask', 'png', '258aeb7d.png', 19, NULL, 1487845333, NULL, 0),
(735, 788, 'subtask', 'png', '658aeb7e.png', 19, NULL, 1487845352, NULL, 0),
(736, 789, 'subtask', 'png', '658aeb7f.png', 19, NULL, 1487845372, NULL, 0),
(737, 790, 'subtask', 'png', '358aeb81.png', 19, NULL, 1487845405, NULL, 0),
(738, 799, 'subtask', 'png', '258aebb2.png', 19, NULL, 1487846185, NULL, 0),
(739, 800, 'subtask', 'png', '558aebb4.png', 19, NULL, 1487846216, NULL, 0),
(740, 801, 'subtask', 'png', '458aebb5.png', 19, NULL, 1487846238, NULL, 0),
(741, 802, 'subtask', 'png', '558aebb7.png', 19, NULL, 1487846266, NULL, 0),
(742, 803, 'subtask', 'png', '158aebbd.png', 19, NULL, 1487846353, NULL, 0),
(743, 804, 'subtask', 'png', '558aebbe.png', 19, NULL, 1487846373, NULL, 0),
(744, 805, 'subtask', 'png', '358aebc0.png', 19, NULL, 1487846404, NULL, 0),
(745, 806, 'subtask', 'png', '458aebc2.png', 19, NULL, 1487846435, NULL, 0),
(746, 807, 'subtask', 'png', '658aebd1.png', 19, NULL, 1487846682, NULL, 0),
(747, 808, 'subtask', 'png', '658aebd2.png', 19, NULL, 1487846702, NULL, 0),
(748, 809, 'subtask', 'png', '258aebd3.png', 19, NULL, 1487846719, NULL, 0),
(749, 810, 'subtask', 'png', '358aebd5.png', 19, NULL, 1487846741, NULL, 0),
(750, 811, 'subtask', 'png', '258aec16.png', 19, NULL, 1487847789, NULL, 0),
(751, 812, 'subtask', 'png', '558aec1b.png', 19, NULL, 1487847865, NULL, 0),
(752, 813, 'subtask', 'png', '558aec1d.png', 19, NULL, 1487847894, NULL, 0),
(753, 814, 'subtask', 'png', '358aec1e.png', 19, NULL, 1487847908, NULL, 0),
(754, 815, 'subtask', 'png', '658aec44.png', 19, NULL, 1487848512, NULL, 1),
(755, 816, 'subtask', 'png', '358aec46.png', 19, NULL, 1487848545, NULL, 1),
(756, 817, 'subtask', 'png', '458aec47.png', 19, NULL, 1487848573, NULL, 1),
(757, 818, 'subtask', 'png', '458aec4d.png', 19, NULL, 1487848663, NULL, 0),
(758, 819, 'subtask', 'png', '158aec4f.png', 19, NULL, 1487848689, NULL, 0),
(759, 820, 'subtask', 'png', '458aec4f.png', 19, NULL, 1487848703, NULL, 0),
(760, 821, 'subtask', 'png', '358aec52.png', 19, NULL, 1487848744, NULL, 0),
(761, 822, 'subtask', 'png', '458aec53.png', 19, NULL, 1487848761, NULL, 0),
(762, 823, 'subtask', 'png', '558aec53.png', 19, NULL, 1487848763, NULL, 0),
(763, 824, 'subtask', 'png', '358aec54.png', 19, NULL, 1487848778, NULL, 0),
(764, 825, 'subtask', 'png', '658aec55.png', 19, NULL, 1487848797, NULL, 0),
(765, 826, 'subtask', 'png', '258aec57.png', 19, NULL, 1487848817, NULL, 0),
(766, 827, 'subtask', 'png', '158aec61.png', 19, NULL, 1487848991, NULL, 0),
(767, 828, 'subtask', 'png', '458aec63.png', 19, NULL, 1487849010, NULL, 0),
(768, 829, 'subtask', 'png', '258aec65.png', 19, NULL, 1487849042, NULL, 0),
(769, 830, 'subtask', 'png', '658aec67.png', 19, NULL, 1487849073, NULL, 0),
(770, 831, 'subtask', 'png', '358aec68.png', 19, NULL, 1487849093, NULL, 0),
(771, 832, 'subtask', 'png', '458aec69.png', 19, NULL, 1487849108, NULL, 0),
(772, 833, 'subtask', 'png', '158aec6a.png', 19, NULL, 1487849123, NULL, 0),
(773, 834, 'subtask', 'png', '258aec6b.png', 19, NULL, 1487849143, NULL, 0),
(774, 835, 'subtask', 'png', '258aec6d.png', 19, NULL, 1487849173, NULL, 0),
(775, 836, 'subtask', 'png', '258aec6f.png', 19, NULL, 1487849205, NULL, 0),
(776, 837, 'subtask', 'png', '158aec71.png', 19, NULL, 1487849234, NULL, 0),
(777, 838, 'subtask', 'png', '658aec73.png', 19, NULL, 1487849264, NULL, 0),
(778, 839, 'subtask', 'png', '258aec7c.png', 19, NULL, 1487849419, NULL, 0),
(779, 840, 'subtask', 'png', '158aec7d.png', 19, NULL, 1487849434, NULL, 0),
(780, 841, 'subtask', 'png', '658aec7e.png', 19, NULL, 1487849449, NULL, 0),
(781, 842, 'subtask', 'png', '358aec7f.png', 19, NULL, 1487849468, NULL, 0),
(782, 843, 'subtask', 'png', '158aec85.png', 19, NULL, 1487849566, NULL, 0);
INSERT INTO `file` (`id`, `parent_id`, `table`, `extension`, `url`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(783, 844, 'subtask', 'png', '458aec87.png', 19, NULL, 1487849599, NULL, 0),
(784, 845, 'subtask', 'png', '658aec89.png', 19, NULL, 1487849620, NULL, 0),
(785, 846, 'subtask', 'png', '458aec8a.png', 19, NULL, 1487849637, NULL, 0),
(786, 847, 'subtask', 'png', '458aec8e.png', 19, NULL, 1487849706, NULL, 0),
(787, 848, 'subtask', 'png', '158aec92.png', 19, NULL, 1487849768, NULL, 0),
(788, 849, 'subtask', 'png', '258aec94.png', 19, NULL, 1487849802, NULL, 1),
(789, 850, 'subtask', 'png', '358aec97.png', 19, NULL, 1487849844, NULL, 0),
(790, 851, 'subtask', 'png', '458aec9a.png', 19, NULL, 1487849893, NULL, 0),
(791, 852, 'subtask', 'png', '158aec9f.png', 19, NULL, 1487849969, NULL, 0),
(792, 853, 'subtask', 'png', '458aeca0.png', 19, NULL, 1487849993, NULL, 0),
(793, 854, 'subtask', 'png', '158aeca2.png', 19, NULL, 1487850028, NULL, 0),
(794, 855, 'subtask', 'png', '258aeca4.png', 19, NULL, 1487850063, NULL, 0),
(795, 856, 'subtask', 'png', '558aeca6.png', 19, NULL, 1487850084, NULL, 0),
(796, 857, 'subtask', 'png', '458aeca7.png', 19, NULL, 1487850105, NULL, 0),
(797, 858, 'subtask', 'png', '258aecab.png', 19, NULL, 1487850160, NULL, 0),
(798, 859, 'subtask', 'png', '358aecac.png', 19, NULL, 1487850178, NULL, 0),
(799, 860, 'subtask', 'png', '658aecb3.png', 19, NULL, 1487850295, NULL, 0),
(800, 861, 'subtask', 'png', '258aecb4.png', 19, NULL, 1487850311, NULL, 0),
(801, 862, 'subtask', 'png', '258aecb5.png', 19, NULL, 1487850330, NULL, 0),
(802, 863, 'subtask', 'png', '458aecb6.png', 19, NULL, 1487850346, NULL, 0),
(803, 864, 'subtask', 'png', '558aecc3.png', 19, NULL, 1487850552, NULL, 0),
(804, 865, 'subtask', 'png', '158aecc9.png', 19, NULL, 1487850652, NULL, 0),
(805, 866, 'subtask', 'png', '558aecca.png', 19, NULL, 1487850671, NULL, 0),
(806, 867, 'subtask', 'png', '658aeccc.png', 19, NULL, 1487850691, NULL, 0),
(807, 868, 'subtask', 'png', '258aeccd.png', 19, NULL, 1487850707, NULL, 0),
(808, 869, 'subtask', 'png', '458aecce.png', 19, NULL, 1487850726, NULL, 0),
(809, 870, 'subtask', 'png', '558aeccf.png', 19, NULL, 1487850746, NULL, 0),
(810, 871, 'subtask', 'png', '558aecd0.png', 19, NULL, 1487850763, NULL, 0),
(811, 872, 'subtask', 'png', '258aecdb.png', 19, NULL, 1487850933, NULL, 0),
(812, 873, 'subtask', 'png', '658aecdc.png', 19, NULL, 1487850952, NULL, 0),
(813, 874, 'subtask', 'png', '458aecdd.png', 19, NULL, 1487850973, NULL, 0),
(814, 875, 'subtask', 'png', '358aece0.png', 19, NULL, 1487851009, NULL, 0),
(815, 876, 'subtask', 'png', '458aece2.png', 19, NULL, 1487851040, NULL, 0),
(816, 877, 'subtask', 'png', '558aece3.png', 19, NULL, 1487851067, NULL, 0),
(817, 878, 'subtask', 'png', '558aece5.png', 19, NULL, 1487851088, NULL, 0),
(818, 879, 'subtask', 'png', '358aece6.png', 19, NULL, 1487851117, NULL, 0),
(819, 880, 'subtask', 'png', '358aecf1.png', 19, NULL, 1487851293, NULL, 0),
(820, 881, 'subtask', 'png', '158aecfb.png', 19, NULL, 1487851455, NULL, 0),
(821, 882, 'subtask', 'png', '358aecfd.png', 19, NULL, 1487851482, NULL, 0),
(822, 883, 'subtask', 'png', '458aed04.png', 19, NULL, 1487851591, NULL, 0),
(823, 884, 'subtask', 'png', '658aed05.png', 19, NULL, 1487851609, NULL, 0),
(824, 885, 'subtask', 'png', '558aed0b.png', 19, NULL, 1487851704, NULL, 0),
(825, 886, 'subtask', 'png', '258aed0c.png', 19, NULL, 1487851723, NULL, 0),
(826, 887, 'subtask', 'png', '658aed0f.png', 19, NULL, 1487851765, NULL, 0),
(827, 888, 'subtask', 'png', '258aed11.png', 19, NULL, 1487851794, NULL, 0),
(828, 889, 'subtask', 'png', '658aed17.png', 19, NULL, 1487851898, NULL, 0),
(829, 890, 'subtask', 'png', '558aed19.png', 19, NULL, 1487851923, NULL, 0),
(830, 891, 'subtask', 'png', '358aed1a.png', 19, NULL, 1487851943, NULL, 0),
(831, 892, 'subtask', 'png', '258aed1c.png', 19, NULL, 1487851969, NULL, 0),
(832, 893, 'subtask', 'png', '358aed1c.png', 19, NULL, 1487851972, NULL, 0),
(833, 894, 'subtask', 'png', '358aed1c.png', 19, NULL, 1487851972, NULL, 0),
(834, 895, 'subtask', 'png', '558aed24.png', 19, NULL, 1487852105, NULL, 0),
(835, 896, 'subtask', 'png', '658aed25.png', 19, NULL, 1487852123, NULL, 0),
(836, 897, 'subtask', 'png', '358aed29.png', 19, NULL, 1487852180, NULL, 0),
(837, 898, 'subtask', 'png', '158aed2a.png', 19, NULL, 1487852207, NULL, 0),
(838, 899, 'subtask', 'png', '358aed33.png', 19, NULL, 1487852338, NULL, 0),
(839, 900, 'subtask', 'png', '358aed35.png', 19, NULL, 1487852368, NULL, 0),
(840, 901, 'subtask', 'png', '658aed36.png', 19, NULL, 1487852393, NULL, 1),
(841, 902, 'subtask', 'png', '558aed39.png', 19, NULL, 1487852439, NULL, 0),
(842, 903, 'subtask', 'png', '158aed3b.png', 19, NULL, 1487852464, NULL, 0),
(843, 904, 'subtask', 'png', '358aed4c.png', 19, NULL, 1487852742, NULL, 0),
(844, 905, 'subtask', 'png', '158aeda8.png', 19, NULL, 1487854215, NULL, 0),
(845, 906, 'subtask', 'png', '158aedaa.png', 19, NULL, 1487854255, NULL, 0),
(846, 907, 'subtask', 'png', '158aedc9.png', 19, NULL, 1487854745, NULL, 0),
(847, 908, 'subtask', 'png', '258aedcb.png', 19, NULL, 1487854770, NULL, 0),
(848, 909, 'subtask', 'png', '358aedcc.png', 19, NULL, 1487854788, NULL, 0),
(849, 405, 'task', 'png', '358aedd9.png', 19, NULL, 1487854992, NULL, 0),
(850, 910, 'subtask', 'png', '458aedec.png', 19, NULL, 1487855306, NULL, 0),
(851, 911, 'subtask', 'png', '558aedf6.png', 19, NULL, 1487855457, NULL, 0),
(852, 912, 'subtask', 'png', '458aedf7.png', 19, NULL, 1487855479, NULL, 0),
(853, 913, 'subtask', 'png', '458aedf8.png', 19, NULL, 1487855502, NULL, 0),
(854, 914, 'subtask', 'png', '158aee30.png', 19, NULL, 1487856395, NULL, 0),
(855, 915, 'subtask', 'png', '558aee33.png', 19, NULL, 1487856439, NULL, 0),
(856, 916, 'subtask', 'png', '358aee36.png', 19, NULL, 1487856480, NULL, 0),
(857, 917, 'subtask', 'png', '458aee37.png', 19, NULL, 1487856505, NULL, 0),
(858, 918, 'subtask', 'png', '358aee5f.png', 19, NULL, 1487857136, NULL, 0),
(859, 919, 'subtask', 'png', '258aee61.png', 19, NULL, 1487857173, NULL, 0),
(860, 920, 'subtask', 'png', '658aee63.png', 19, NULL, 1487857203, NULL, 0),
(861, 921, 'subtask', 'png', '158aee6d.png', 19, NULL, 1487857361, NULL, 0),
(862, 922, 'subtask', 'png', '458aee6e.png', 19, NULL, 1487857388, NULL, 0),
(863, 923, 'subtask', 'png', '158aee75.png', 19, NULL, 1487857494, NULL, 0),
(864, 924, 'subtask', 'png', '358aee7d.png', 19, NULL, 1487857618, NULL, 0),
(865, 925, 'subtask', 'png', '358aee7f.png', 19, NULL, 1487857658, NULL, 0),
(866, 412, 'task', 'png', '358aee89.png', 19, NULL, 1487857810, NULL, 0),
(867, 413, 'task', 'png', '458aee8f.png', 19, NULL, 1487857905, NULL, 0),
(868, 415, 'task', 'png', '358aee97.png', 19, NULL, 1487858040, NULL, 0),
(869, 416, 'task', 'png', '358aee9a.png', 19, NULL, 1487858083, NULL, 0),
(870, 926, 'subtask', 'png', '258aeea5.png', 19, NULL, 1487858266, NULL, 0),
(871, 927, 'subtask', 'png', '258aeea6.png', 19, NULL, 1487858287, NULL, 0),
(872, 928, 'subtask', 'png', '158aeea9.png', 19, NULL, 1487858322, NULL, 0),
(873, 936, 'subtask', 'png', '258aefbd.png', 19, NULL, 1487862736, NULL, 0),
(874, 937, 'subtask', 'png', '358aefbe.png', 19, NULL, 1487862761, NULL, 0),
(875, 938, 'subtask', 'png', '458aefbf.png', 19, NULL, 1487862781, NULL, 0),
(876, 939, 'subtask', 'png', '558aefc2.png', 19, NULL, 1487862824, NULL, 1),
(877, 940, 'subtask', 'png', '658aefc6.png', 19, NULL, 1487862889, NULL, 0),
(878, 953, 'subtask', 'png', '658af02c.png', 19, NULL, 1487864513, NULL, 0),
(879, 954, 'subtask', 'png', '358af02d.png', 19, NULL, 1487864543, NULL, 0),
(880, 955, 'subtask', 'png', '158af02f.png', 19, NULL, 1487864566, NULL, 0),
(881, 956, 'subtask', 'png', '658af030.png', 19, NULL, 1487864591, NULL, 0),
(882, 963, 'subtask', 'png', '658af053.png', 19, NULL, 1487865137, NULL, 0),
(883, 964, 'subtask', 'png', '258af058.png', 19, NULL, 1487865225, NULL, 0),
(884, 965, 'subtask', 'png', '358af06d.png', 19, NULL, 1487865557, NULL, 0),
(885, 966, 'subtask', 'png', '558af06e.png', 19, NULL, 1487865577, NULL, 0),
(886, 967, 'subtask', 'png', '158af070.png', 19, NULL, 1487865606, NULL, 0),
(887, 968, 'subtask', 'png', '358af073.png', 19, NULL, 1487865654, NULL, 0),
(888, 969, 'subtask', 'png', '658af076.png', 19, NULL, 1487865706, NULL, 0),
(889, 970, 'subtask', 'png', '658af07a.png', 19, NULL, 1487865760, NULL, 0),
(890, 971, 'subtask', 'png', '158af09a.png', 19, NULL, 1487866276, NULL, 0),
(891, 972, 'subtask', 'png', '458af09b.png', 19, NULL, 1487866294, NULL, 0),
(892, 450, 'task', 'png', '258af0ae.png', 19, NULL, 1487866601, NULL, 0),
(893, 978, 'subtask', 'png', '458af0be.png', 19, NULL, 1487866859, NULL, 0),
(894, 979, 'subtask', 'png', '258af0c0.png', 19, NULL, 1487866882, NULL, 0),
(895, 980, 'subtask', 'png', '358af0d1.png', 19, NULL, 1487867154, NULL, 0),
(896, 981, 'subtask', 'png', '558af0d3.png', 19, NULL, 1487867189, NULL, 0),
(897, 982, 'subtask', 'png', '558af0d4.png', 19, NULL, 1487867213, NULL, 0),
(898, 983, 'subtask', 'png', '358af0d6.png', 19, NULL, 1487867240, NULL, 0),
(899, 990, 'subtask', 'png', '458af0e1.png', 19, NULL, 1487867417, NULL, 0),
(900, 991, 'subtask', 'png', '158af0e3.png', 19, NULL, 1487867442, NULL, 0),
(901, 992, 'subtask', 'png', '658af0e4.png', 19, NULL, 1487867466, NULL, 0),
(902, 993, 'subtask', 'png', '558af0e5.png', 19, NULL, 1487867483, NULL, 0),
(903, 994, 'subtask', 'png', '458af0e6.png', 19, NULL, 1487867500, NULL, 0),
(904, 995, 'subtask', 'png', '258af0e7.png', 19, NULL, 1487867513, NULL, 0),
(905, 996, 'subtask', 'png', '158b02d5.png', 19, NULL, 1487940958, NULL, 0),
(906, 997, 'subtask', 'png', '358b02d7.png', 19, NULL, 1487940981, NULL, 0),
(907, 998, 'subtask', 'png', '258b02d9.png', 19, NULL, 1487941018, NULL, 0),
(908, 999, 'subtask', 'png', '558b02dd.png', 19, NULL, 1487941086, NULL, 0),
(909, 1000, 'subtask', 'png', '158b02fd.png', 19, NULL, 1487941592, NULL, 0),
(910, 1001, 'subtask', 'png', '458b04ec.png', 19, NULL, 1487949508, NULL, 0),
(911, 1002, 'subtask', 'png', '558b0540.png', 19, NULL, 1487950850, NULL, 0),
(912, 1003, 'subtask', 'png', '158b0542.png', 19, NULL, 1487950888, NULL, 0),
(913, 1004, 'subtask', 'png', '258b0556.png', 19, NULL, 1487951206, NULL, 0),
(914, 1005, 'subtask', 'png', '558b0557.png', 19, NULL, 1487951222, NULL, 0),
(915, 1006, 'subtask', 'png', '158b0558.png', 19, NULL, 1487951237, NULL, 0),
(916, 1007, 'subtask', 'png', '658b055a.png', 19, NULL, 1487951268, NULL, 0),
(917, 1008, 'subtask', 'png', '458b055c.png', 19, NULL, 1487951309, NULL, 0),
(918, 1009, 'subtask', 'png', '458b055e.png', 19, NULL, 1487951330, NULL, 0),
(919, 1010, 'subtask', 'png', '458b0565.png', 19, NULL, 1487951444, NULL, 0),
(920, 1011, 'subtask', 'png', '158b0566.png', 19, NULL, 1487951461, NULL, 0),
(921, 1012, 'subtask', 'png', '258b0567.png', 19, NULL, 1487951477, NULL, 0),
(922, 1013, 'subtask', 'png', '358b0568.png', 19, NULL, 1487951493, NULL, 0),
(923, 1014, 'subtask', 'png', '658b056a.png', 19, NULL, 1487951527, NULL, 0),
(924, 1015, 'subtask', 'png', '158b056d.png', 19, NULL, 1487951569, NULL, 0),
(925, 1016, 'subtask', 'png', '358b0572.png', 19, NULL, 1487951656, NULL, 0),
(926, 1017, 'subtask', 'png', '558b0575.png', 19, NULL, 1487951711, NULL, 0),
(927, 1018, 'subtask', 'png', '458b0602.png', 19, NULL, 1487953965, NULL, 0),
(928, 1019, 'subtask', 'png', '458b0604.png', 19, NULL, 1487953991, NULL, 0),
(929, 1020, 'subtask', 'png', '158b0607.png', 19, NULL, 1487954039, NULL, 0),
(930, 1021, 'subtask', 'png', '358b0608.png', 19, NULL, 1487954053, NULL, 0),
(931, 1022, 'subtask', 'png', '458b0609.png', 19, NULL, 1487954068, NULL, 0),
(932, 1025, 'subtask', 'png', '558b0632.png', 19, NULL, 1487954731, NULL, 0),
(933, 1026, 'subtask', 'png', '558b0636.png', 19, NULL, 1487954788, NULL, 0),
(934, 1027, 'subtask', 'png', '258b063e.png', 19, NULL, 1487954917, NULL, 0),
(935, 1028, 'subtask', 'png', '158b0643.png', 19, NULL, 1487954998, NULL, 0),
(936, 1029, 'subtask', 'png', '358b064a.png', 19, NULL, 1487955115, NULL, 0),
(937, 1030, 'subtask', 'png', '158b064c.png', 19, NULL, 1487955137, NULL, 0),
(938, 1031, 'subtask', 'png', '658b064f.png', 19, NULL, 1487955196, NULL, 0),
(939, 1032, 'subtask', 'png', '358b0654.png', 19, NULL, 1487955273, NULL, 0),
(940, 1033, 'subtask', 'png', '458b0656.png', 19, NULL, 1487955309, NULL, 0),
(941, 1034, 'subtask', 'png', '658b065f.png', 19, NULL, 1487955443, NULL, 0),
(942, 1035, 'subtask', 'png', '358c7df6.png', 19, NULL, 1489493861, NULL, 0),
(943, 1036, 'subtask', 'png', '358c7df7.png', 19, NULL, 1489493883, NULL, 0),
(944, 122, 'task', 'png', '358c7e49.png', 19, NULL, 1489495187, NULL, 0),
(945, 1037, 'subtask', 'png', '158c7e9e.png', 19, NULL, 1489496557, NULL, 0),
(946, 1038, 'subtask', 'png', '458c81c1.png', 19, NULL, 1489509406, NULL, 0),
(947, 1039, 'subtask', 'png', '558c81c3.png', 19, NULL, 1489509430, NULL, 0),
(948, 1040, 'subtask', 'png', '258c81c7.png', 19, NULL, 1489509493, NULL, 0),
(949, 1041, 'subtask', 'png', '258c81cd.png', 19, NULL, 1489509591, NULL, 0),
(950, 1042, 'subtask', 'png', '358c81cf.png', 19, NULL, 1489509626, NULL, 1),
(951, 1043, 'subtask', 'png', '258c81d1.png', 19, NULL, 1489509653, NULL, 0),
(952, 1044, 'subtask', 'png', '658caa4f.png', 19, NULL, 1489675517, NULL, 1),
(953, 1045, 'subtask', 'png', '558caa51.png', 19, NULL, 1489675546, NULL, 1),
(954, 1046, 'subtask', 'png', '358caa57.png', 19, NULL, 1489675634, NULL, 0),
(955, 1047, 'subtask', 'png', '158caa59.png', 19, NULL, 1489675668, NULL, 0),
(956, 1048, 'subtask', 'png', '258caa5a.png', 19, NULL, 1489675689, NULL, 0),
(957, 1049, 'subtask', 'png', '358caa5c.png', 19, NULL, 1489675726, NULL, 0),
(958, 1050, 'subtask', 'png', '458caa5e.png', 19, NULL, 1489675754, NULL, 0),
(959, 1051, 'subtask', 'png', '158caa65.png', 19, NULL, 1489675861, NULL, 0),
(960, 1052, 'subtask', 'png', '458caa6b.png', 19, NULL, 1489675962, NULL, 0),
(961, 1053, 'subtask', 'png', '658caa6c.png', 19, NULL, 1489675979, NULL, 0),
(962, 1054, 'subtask', 'png', '158caa6d.png', 19, NULL, 1489675999, NULL, 0),
(963, 1055, 'subtask', 'png', '558caa70.png', 19, NULL, 1489676036, NULL, 0),
(964, 1056, 'subtask', 'png', '658caa72.png', 19, NULL, 1489676072, NULL, 0),
(965, 1057, 'subtask', 'png', '158caa73.png', 19, NULL, 1489676095, NULL, 0),
(966, 1058, 'subtask', 'png', '158caa78.png', 19, NULL, 1489676168, NULL, 1),
(967, 1059, 'subtask', 'png', '558caa81.png', 19, NULL, 1489676310, NULL, 1),
(968, 1060, 'subtask', 'png', '558caa81.png', 19, NULL, 1489676314, NULL, 1),
(969, 1061, 'subtask', 'png', '558caa83.png', 19, NULL, 1489676341, NULL, 1),
(970, 1062, 'subtask', 'png', '658caa84.png', 19, NULL, 1489676366, NULL, 1),
(971, 1063, 'subtask', 'png', '158caa8c.png', 19, NULL, 1489676481, NULL, 0),
(972, 1064, 'subtask', 'png', '558caa8d.png', 19, NULL, 1489676508, NULL, 0),
(973, 1065, 'subtask', 'png', '458caa8e.png', 19, NULL, 1489676525, NULL, 0),
(974, 1066, 'subtask', 'png', '158caa92.png', 19, NULL, 1489676579, NULL, 0),
(975, 1067, 'subtask', 'png', '358caaa5.png', 19, NULL, 1489676886, NULL, 0),
(976, 1068, 'subtask', 'png', '658caaa8.png', 19, NULL, 1489676940, NULL, 0),
(977, 1069, 'subtask', 'png', '158caaab.png', 19, NULL, 1489676978, NULL, 0),
(978, 1070, 'subtask', 'png', '558caaac.png', 19, NULL, 1489676996, NULL, 0),
(979, 1071, 'subtask', 'png', '558caaae.png', 19, NULL, 1489677025, NULL, 0),
(980, 1072, 'subtask', 'png', '558caab0.png', 19, NULL, 1489677060, NULL, 0),
(981, 1073, 'subtask', 'png', '558caab5.png', 19, NULL, 1489677148, NULL, 0),
(982, 1074, 'subtask', 'png', '558caab7.png', 19, NULL, 1489677171, NULL, 0),
(983, 1075, 'subtask', 'png', '458caab8.png', 19, NULL, 1489677199, NULL, 0),
(984, 1076, 'subtask', 'png', '658caabf.png', 19, NULL, 1489677304, NULL, 0),
(985, 1077, 'subtask', 'png', '158caac7.png', 19, NULL, 1489677435, NULL, 0),
(986, 1078, 'subtask', 'png', '458caacc.png', 19, NULL, 1489677504, NULL, 0),
(987, 1079, 'subtask', 'png', '258caacd.png', 19, NULL, 1489677522, NULL, 0),
(988, 1080, 'subtask', 'png', '658caacf.png', 19, NULL, 1489677566, NULL, 0),
(989, 1081, 'subtask', 'png', '658caada.png', 19, NULL, 1489677734, NULL, 0),
(990, 1082, 'subtask', 'png', '658caadd.png', 19, NULL, 1489677777, NULL, 0),
(991, 1083, 'subtask', 'png', '258caade.png', 19, NULL, 1489677799, NULL, 0),
(992, 1084, 'subtask', 'png', '458caade.png', 19, NULL, 1489677800, NULL, 0),
(993, 1085, 'subtask', 'png', '358caadf.png', 19, NULL, 1489677822, NULL, 0),
(994, 1086, 'subtask', 'png', '458caae6.png', 19, NULL, 1489677920, NULL, 0),
(995, 1087, 'subtask', 'png', '558caaeb.png', 19, NULL, 1489678000, NULL, 0),
(996, 1088, 'subtask', 'png', '358caaec.png', 19, NULL, 1489678018, NULL, 0),
(997, 1089, 'subtask', 'png', '358caaf0.png', 19, NULL, 1489678089, NULL, 0),
(998, 1090, 'subtask', 'png', '658cab17.png', 19, NULL, 1489678719, NULL, 0),
(999, 1091, 'subtask', 'png', '258cab19.png', 19, NULL, 1489678749, NULL, 0),
(1000, 1092, 'subtask', 'png', '558cab1e.png', 19, NULL, 1489678819, NULL, 0),
(1001, 484, 'task', 'jpg', '358eb843.jpg', 19, NULL, 1491829811, NULL, 0),
(1002, 485, 'task', 'jpg', '458eb9e9.jpg', 19, NULL, 1491836560, NULL, 1),
(1003, 1100, 'subtask', 'jpg', '158eba42.jpg', 19, NULL, 1491837992, NULL, 0),
(1004, 487, 'task', 'jpg', '158ecb69.jpg', 19, NULL, 1491908241, NULL, 1),
(1005, 488, 'task', 'jpg', '458ecbac.jpg', 19, NULL, 1491909315, NULL, 1),
(1006, 1103, 'subtask', 'png', '458ee20e.png', 19, NULL, 1492000999, NULL, 0),
(1007, 1103, 'subtask', 'png', '658ee20e.png', 19, NULL, 1492001000, NULL, 0),
(1008, 1103, 'subtask', 'png', '658ee20f.png', 19, NULL, 1492001010, NULL, 0),
(1009, 1103, 'subtask', 'png', '258ee210.png', 19, NULL, 1492001038, NULL, 0),
(1010, 95, 'task', 'jpeg', '158ee24e.jpeg', 19, NULL, 1492002027, NULL, 1),
(1011, 490, 'task', 'jpg', '558ee279.jpg', 19, NULL, 1492002715, NULL, 1),
(1012, 487, 'task', 'jpg', '158ee281.jpg', 19, NULL, 1492002842, NULL, 1),
(1013, 487, 'task', 'jpg', '558ee283.jpg', 19, NULL, 1492002872, NULL, 1),
(1014, 487, 'task', 'jpg', '458ee288.jpg', 19, NULL, 1492002951, NULL, 0),
(1015, 487, 'task', 'jpeg', '258ee28c.jpeg', 19, NULL, 1492003011, NULL, 0),
(1016, 487, 'task', 'jpg', '558ee28c.jpg', 19, NULL, 1492003011, NULL, 0),
(1017, 487, 'task', 'jpg', '658ee28c.jpg', 19, NULL, 1492003011, NULL, 0),
(1018, 485, 'task', 'jpg', '558ee2f7.jpg', 19, NULL, 1492004735, NULL, 1),
(1019, 485, 'task', 'jpg', '358ee2f7.jpg', 19, NULL, 1492004735, NULL, 1),
(1020, 485, 'task', 'jpeg', '458ee2f7.jpeg', 19, NULL, 1492004735, NULL, 1),
(1021, 491, 'task', 'jpg', '558ee3fb.jpg', 19, NULL, 1492008891, NULL, 0),
(1022, 491, 'task', 'jpg', '158ee3fb.jpg', 19, NULL, 1492008891, NULL, 0),
(1023, 492, 'task', 'jpg', '658ef58f.jpg', 19, NULL, 1492080888, NULL, 0),
(1024, 496, 'task', 'jpg', '658f096b.jpg', 19, NULL, 1492162228, NULL, 1),
(1025, 46, 'book', 'jpg', '158f0c6a.jpg', 5, NULL, 1492174511, NULL, 0),
(1026, 54, 'book', 'jpg', '358f49cc.jpg', 5, NULL, 1492425929, NULL, 0),
(1027, 504, 'task', 'jpg', '158f5e09.jpg', 19, NULL, 1492508818, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `level`
--

CREATE TABLE `level` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `level`
--

INSERT INTO `level` (`id`, `name`, `deleted`) VALUES
(1, 'Рівень 1', 0),
(2, 'Рівень 2', 0),
(3, 'Рівень 3', 0),
(4, 'Рівень 4', 0),
(5, 'Рівень 5', 0),
(6, 'Рівень 6', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `comment_id` int(11) NOT NULL,
  `send_from` int(11) NOT NULL,
  `send_to` int(11) DEFAULT NULL,
  `type` tinyint(1) DEFAULT NULL,
  `text` text,
  `created_at` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `message`
--

INSERT INTO `message` (`id`, `comment_id`, `send_from`, `send_to`, `type`, `text`, `created_at`, `status`, `deleted`) VALUES
(1, 1, 1, NULL, 0, 'sdfghjk', 1479995807, 2, 0),
(3, 3, 1, NULL, 0, 'test test test test test ', 1479995808, 2, 0),
(5, 3, 1, NULL, 0, 'ggregrge', 1479995809, 2, 0),
(6, 3, 1, NULL, 0, 'ergerger', 1479995810, 2, 0),
(7, 3, 1, NULL, 0, 'rttt', 1479995811, 2, 0),
(8, 3, 1, NULL, 0, 'пукпукпку', 1479995812, 2, 0),
(9, 3, 1, NULL, 0, 'gergreg', 1479995813, 2, 0),
(10, 1, 1, NULL, 0, 'rhrthtrht', 1479995814, 2, 0),
(11, 1, 1, NULL, 0, 'rhrttr', 1479995815, 2, 0),
(12, 1, 1, NULL, 0, 'kukuykyukut', 1479995816, 2, 0),
(13, 1, 1, NULL, 0, 'gfdgdf', 0, 2, 0),
(14, 1, 1, NULL, 0, '1212', 0, 2, 0),
(15, 3, 52, NULL, 0, 'екеукеукеук', 0, 2, 0),
(16, 3, 52, NULL, 0, 'текст', 0, 2, 0),
(17, 8, 52, NULL, 0, 'тетететет', 0, 2, 0),
(21, 3, 1, NULL, 1, 'Ви відправили відповідь 0', 1479995823, 2, 0),
(22, 3, 1, NULL, 1, 'Ви відправили відповідь 1', 1479995824, 2, 0),
(23, 3, 1, NULL, 1, 'Ви відправили відповідь 2', 1479995825, 2, 0),
(26, 17, 2, NULL, 1, 'Ви відправили відповідь 0', 1479995835, 2, 0),
(30, 17, 2, NULL, 1, 'Ви відправили відповідь 0', 1479995857, 2, 0),
(35, 17, 2, NULL, 1, 'Ви відправили відповідь 0', 1479995867, 2, 0),
(36, 17, 2, NULL, 1, 'Ви відправили відповідь 2322er5g5reg415erg6er54g6re4g6464 654 564er654v6er4v4re6 4er56 4re654vre654 re4 64er6', 1479995877, 2, 0),
(37, 17, 2, NULL, 1, 'Ви відправили відповідь undefined', 1479995888, 2, 0),
(38, 17, 2, NULL, 1, 'Ви відправили відповідь ', 1479995899, 2, 0),
(39, 17, 2, 17, 0, 'feer', 1481031187, 2, 0),
(40, 17, 2, 17, 0, 'test22', 1481031556, 2, 0),
(41, 17, 2, 17, 0, 'test11', 1481031627, 2, 0),
(42, 3, 2, 3, 0, 'tetetetete', 1481031638, 2, 0),
(43, 17, 2, 17, 0, 'ffewffwe', 1481031845, 2, 0),
(44, 17, 2, 17, 0, 'gfgfdgd', 1481032061, 2, 0),
(45, 17, 2, NULL, 1, 'Ви відправили відповідь Lorem Ipsum - це текст-\"риба\", що використовується в друкарстві та дизайні. Lorem Ipsum є, фактично, стандартною \"рибою\" аж з XVI сторіччя, коли н', 1481041579, 2, 0),
(46, 17, 2, NULL, 1, 'Ви відправили відповідь \r\nПоорррргр', 1481118116, 2, 0),
(47, 19, 2, NULL, 1, 'Ви відправили відповідь Ррр', 1481124284, 2, 0),
(48, 19, 2, 19, 0, 'Оо', 1481124297, 2, 0),
(49, 8, 52, 8, 0, 'Ппрорлооо', 1481124333, 2, 0),
(50, 8, 52, NULL, 1, 'Ви відправили відповідь Апоолр', 1481124341, 2, 0),
(51, 8, 2, 8, 0, 'Оорррооирро', 1481124390, 2, 0),
(52, 17, 2, NULL, 1, 'Ви відправили відповідь \r\nПоорррргр', 1481129106, 2, 0),
(53, 20, 52, NULL, 0, 'Роумінг ', 1481201220, 2, 0),
(54, 20, 2, 20, 0, 'Птички ', 1481201261, 2, 0),
(55, 24, 2, NULL, 0, '123', 1481201660, 2, 0),
(56, 25, 2, NULL, NULL, 'Привіт ', 1481201715, 1, 0),
(57, 17, 2, 17, 0, NULL, 1481290143, 2, 0),
(58, 17, 2, 17, 0, NULL, 1481291723, 2, 0),
(59, 17, 2, 17, 0, NULL, 1481552768, 2, 0),
(60, 17, 2, NULL, 1, 'Ви відправили відповідь Привет мир', 1481559524, 2, 0),
(61, 24, 2, 24, 0, 'Пиоппи', 1481575099, 2, 0),
(62, 17, 2, 17, 0, NULL, 1481628772, 2, 0),
(63, 17, 2, 17, 0, NULL, 1481634783, 2, 0),
(64, 17, 2, 17, 0, 'Gghgg', 1481634788, 2, 0),
(65, 17, 2, 17, 0, 'Gghgg', 1481634796, 2, 0),
(66, 17, 2, 17, 0, 'Gghgg', 1481634888, 2, 0),
(67, 17, 2, 17, 0, NULL, 1481734517, 2, 0),
(68, 17, 2, 17, 0, 'Did', 1482762770, 2, 0),
(69, 17, 2, NULL, 1, 'Ви відправили відповідь 55', 1483303777, 2, 0),
(70, 17, 2, NULL, 1, 'Ви відправили відповідь Иоио', 1483303793, 2, 0),
(71, 24, 2, 24, 0, NULL, 1484067843, 2, 0),
(72, 17, 2, 8, 0, ' ', 1484068406, 2, 0),
(73, 26, 2, NULL, NULL, 'Тест', 1484310400, 1, 0),
(74, 24, 2, NULL, 1, 'Ви відправили відповідь Ответ 1', 1484310443, 2, 0),
(75, 17, 2, 17, 0, '    ', 1484312323, 2, 0),
(76, 17, 2, 3, 0, ' ', 1484312580, 2, 0),
(77, 17, 2, 3, 0, ' ', 1484312585, 2, 0),
(78, 17, 2, 3, 0, ' ', 1484312594, 2, 0),
(79, 17, 2, NULL, 1, 'Ви відправили відповідь Результат ', 1484312998, 2, 0),
(80, 17, 2, NULL, 1, 'Ви відправили відповідь Результат ', 1484313007, 2, 0),
(81, 17, 2, NULL, 1, 'Ви відправили відповідь Результат ', 1484313011, 2, 0),
(82, 24, 2, 20, 0, 'Собака ', 1484314617, 2, 0),
(83, 24, 2, 20, 0, 'Собака ', 1484314646, 2, 0),
(84, 24, 2, 20, 0, 'Собака ', 1484314662, 2, 0),
(85, 24, 2, 20, 0, 'Собака ', 1484314733, 2, 0),
(86, 17, 2, 17, 0, 'Почав', 1484322604, 2, 0),
(87, 17, 2, 17, 0, 'Тест', 1484562367, 2, 0),
(88, 17, 2, 17, 0, 'Роорр', 1484564020, 2, 0),
(89, 17, 2, 17, 0, 'То рос', 1484564078, 2, 0),
(90, 17, 2, 3, 0, 'Полт', 1484565624, 2, 0),
(91, 17, 2, 17, 0, 'Оощ', 1484566512, 2, 0),
(92, 19, 2, NULL, 1, 'Ви відправили відповідь Ррр', 1484576488, 2, 0),
(93, 19, 2, NULL, 1, 'Ви відправили відповідь Ррр', 1484576500, 2, 0),
(94, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484577022, 2, 0),
(95, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484577118, 2, 0),
(96, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484577175, 2, 0),
(97, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484577402, 2, 0),
(98, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484577561, 2, 0),
(99, 19, 2, 19, 0, 'Ответ', 1484582850, 2, 0),
(100, 17, 2, NULL, 1, 'Ви відправили відповідь Решилось ', 1484584059, 2, 0),
(101, 26, 2, 26, NULL, 'Два', 1484584370, 1, 0),
(102, 24, 2, NULL, 1, 'Ви відправили відповідь Ответ 1', 1484740323, 2, 0),
(103, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484835881, 2, 0),
(104, 19, 2, NULL, 1, 'Ви відправили відповідь jyjyjyuyjyjyjyjyj', 1484836192, 2, 0),
(105, 27, 2, NULL, 0, 'Тест', 1484847984, 2, 0),
(106, 28, 2, NULL, 1, 'Ви відправили відповідь 5', 1485351468, 2, 0),
(107, 28, 2, NULL, 1, 'Ви відправили відповідь 4', 1485351509, 2, 0),
(108, 29, 2, NULL, 0, '456', 1485516843, 2, 0),
(109, 1, 1, NULL, 1, 'Ви відправили відповідь Пливуть', 1485770608, 2, 0),
(110, 1, 1, 1, 0, NULL, 1485774363, 2, 0),
(111, 1, 1, NULL, 1, 'Ви відправили відповідь ', 1485791037, 2, 0),
(112, 3, 1, NULL, 1, 'Ви відправили відповідь 15', 1485792445, 2, 0),
(113, 3, 1, NULL, 1, 'Ви відправили відповідь 444', 1485792456, 2, 0),
(114, 1, 1, 1, 0, NULL, 1485867112, 2, 0),
(115, 1, 1, NULL, 1, 'Ви відправили відповідь Аротсполтмпр', 1485867193, 2, 0),
(116, 1, 1, NULL, 1, 'Ви відправили відповідь 51', 1485870219, 2, 0),
(117, 1, 1, 1, 0, 'Привет', 1485877243, 2, 0),
(118, 1, 1, 1, 0, 'Привет', 1485877265, 2, 0),
(119, 1, 1, 1, 0, 'Привет', 1485882215, 2, 0),
(120, 1, 1, 1, 0, NULL, 1485968088, 2, 0),
(121, 17, 2, NULL, 1, 'Ви відправили відповідь 15', 1486045441, 2, 0),
(122, 30, 2, NULL, 1, 'Ви відправили відповідь Тест ', 1486053135, 2, 0),
(123, 30, 2, NULL, 1, 'Ви відправили відповідь Тест ', 1486053160, 2, 0),
(124, 30, 2, NULL, 1, 'Ви відправили відповідь Тест ', 1486053188, 2, 0),
(125, 29, 2, 29, 0, 'Овлвлутвдадтв\n', 1486399649, 2, 0),
(126, 29, 2, NULL, 1, 'Ви відправили відповідь 4444', 1486399672, 2, 0),
(127, 31, 2, NULL, 1, 'Ви відправили відповідь \\frac{15}{5}', 1492085886, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `my_book`
--

CREATE TABLE `my_book` (
  `id` int(11) NOT NULL,
  `book_discipline_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `my_book`
--

INSERT INTO `my_book` (`id`, `book_discipline_id`, `created_at`, `deleted`, `user_id`) VALUES
(3, 6, 1480435027, 0, 5),
(4, 6, 1480437283, 0, 52),
(6, 9, 1481129252, 0, 5),
(7, 14, 1485164403, 0, 5),
(8, 15, 1485345285, 0, 6),
(30, 15, 1485868281, 0, 1),
(31, 20, 1485874731, 0, 1),
(32, 9, 1485878314, 0, 1),
(33, 14, 1485943211, 0, 2),
(34, 14, 1485943222, 0, 2),
(35, 15, 1486380585, 0, 2),
(36, 21, 1486659251, 0, 2),
(37, 21, 1492166012, 0, 1),
(38, 40, 1492508727, 0, 1),
(39, 29, 1492508758, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `my_class`
--

CREATE TABLE `my_class` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `number` tinyint(2) NOT NULL,
  `letter` varchar(5) DEFAULT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `my_class`
--

INSERT INTO `my_class` (`id`, `user_id`, `number`, `letter`, `type`, `created_at`, `deleted`) VALUES
(1, 1, 11, 'л', 1, 1485773988, 0),
(2, 7, 10, 'A', 1, 0, 0),
(4, 99, 10, 'D', 1, 1478612881, 0),
(5, 100, 8, 'A', 1, 1478612924, 0),
(6, 101, 3, NULL, 1, 1478612988, 0),
(7, 102, 7, 'B', 1, 1478612990, 0),
(8, 103, 6, 'B', 1, 1478612992, 0),
(9, 104, 10, 'B', 1, 1478612994, 0),
(10, 105, 10, 'A', 1, 1478612997, 0),
(11, 106, 8, 'B', 1, 1478612999, 0),
(12, 107, 6, 'B', 1, 1478613001, 0),
(13, 108, 5, 'A', 1, 1478613004, 0),
(14, 109, 6, 'A', 1, 1478613006, 0),
(15, 110, 7, 'A', 1, 1478613008, 0),
(16, 52, 10, 'A', 1, 0, 0),
(17, 111, 6, NULL, 1, 1478706826, 0),
(19, 111, 2, NULL, 0, 1478708536, 1),
(20, 111, 6, NULL, 0, 1478708536, 1),
(21, 111, 9, NULL, 0, 1478708536, 1),
(22, 111, 11, NULL, 0, 1478708536, 1),
(23, 111, 1, NULL, 0, 1478708639, 1),
(24, 111, 8, NULL, 0, 1478708639, 1),
(25, 111, 10, NULL, 0, 1478708639, 0),
(27, 111, 5, NULL, 0, 1478709703, 1),
(69, 2, 5, 'лол', 1, 1479821092, 0),
(70, 2, 1, NULL, 0, 1478787490, 0),
(71, 2, 2, NULL, 0, 1478787490, 1),
(72, 2, 3, NULL, 0, 1478787490, 1),
(73, 2, 4, NULL, 0, 1478787490, 0),
(74, 2, 5, NULL, 0, 1478787490, 0),
(77, 2, 10, NULL, 0, 1478787706, 0),
(78, 2, 11, NULL, 0, 1478787706, 0),
(79, 119, 4, 'А', 1, 1479815569, 0),
(80, 120, 1, 'R', 1, 1479825041, 0),
(81, 121, 1, 'SSS', 1, 1479825244, 0),
(82, 121, 9, NULL, 0, 1479825244, 0),
(83, 121, 10, NULL, 0, 1479825244, 0),
(84, 121, 11, NULL, 0, 1479825244, 0),
(85, 121, 8, NULL, 0, 1479825281, 0),
(86, 115, 10, 'A', 0, 0, 0),
(87, 2, 6, NULL, 0, 1484314988, 0),
(88, 2, 7, NULL, 0, 1484314988, 0),
(89, 2, 8, NULL, 0, 1484314988, 0),
(90, 2, 9, NULL, 0, 1484314988, 0),
(91, 2, 6, NULL, 0, 1484314988, 0),
(92, 2, 7, NULL, 0, 1484314988, 0),
(93, 2, 8, NULL, 0, 1484314988, 0),
(94, 2, 9, NULL, 0, 1484314988, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `my_discipline`
--

CREATE TABLE `my_discipline` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `my_discipline`
--

INSERT INTO `my_discipline` (`id`, `user_id`, `discipline_id`, `teacher`, `created_at`, `deleted`) VALUES
(2, 144, 13, NULL, 1484909624, 0),
(3, 148, 14, NULL, 1484914323, 0),
(4, 149, 14, NULL, 1484914376, 0),
(5, 150, 13, NULL, 1484914765, 0),
(6, 151, 14, NULL, 1484916707, 0),
(7, 155, 14, NULL, 1484917745, 0),
(8, 162, 14, NULL, 1484921181, 0),
(9, 163, 14, NULL, 1484921229, 0),
(10, 164, 14, NULL, 1484921546, 0),
(11, 165, 14, NULL, 1484921730, 0),
(12, 166, 14, NULL, 1484921783, 0),
(13, 167, 14, NULL, 1484921869, 0),
(14, 168, 14, NULL, 1484921906, 0),
(15, 169, 13, NULL, 1484921998, 0),
(16, 170, 14, NULL, 1485263690, 0),
(17, 170, 15, NULL, 1485263690, 0),
(18, 170, 16, NULL, 1485263690, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `my_pupil`
--

CREATE TABLE `my_pupil` (
  `id` int(11) NOT NULL,
  `pupil_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `my_pupil`
--

INSERT INTO `my_pupil` (`id`, `pupil_id`, `user_id`, `type`, `created_at`, `deleted`) VALUES
(16, 117, 2, 1, 1486028176, 0),
(17, 116, 2, 1, 1486028295, 0),
(18, 1, 2, 1, 1486031091, 0),
(19, 7, 2, 1, 1493027323, 0),
(20, 52, 99, 1, 1493027336, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `region`
--

CREATE TABLE `region` (
  `id` int(11) NOT NULL,
  `country_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `region`
--

INSERT INTO `region` (`id`, `country_id`, `name`, `created_at`, `deleted`) VALUES
(8, 1, 'Полтавская область', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `name`, `deleted`) VALUES
(1, 'learner', 0),
(2, 'tutor', 0),
(3, 'teacher', 0),
(4, 'curator', 0),
(5, 'manager', 0),
(6, 'admin', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `school`
--

CREATE TABLE `school` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `sity_id` int(11) NOT NULL,
  `street` varchar(255) DEFAULT NULL,
  `house` varchar(255) DEFAULT NULL,
  `phone` double(11,0) DEFAULT NULL,
  `email` varchar(55) DEFAULT NULL,
  `director` varchar(255) DEFAULT NULL,
  `notes` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `school`
--

INSERT INTO `school` (`id`, `name`, `sity_id`, `street`, `house`, `phone`, `email`, `director`, `notes`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 'school 1', 1, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, 0, NULL, 0),
(2, 'school 2', 1, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, 0, NULL, 0),
(3, 'school 3', 3, NULL, NULL, NULL, NULL, NULL, NULL, 5, NULL, 0, NULL, 0),
(4, 'Школа', 2, 'Улица', '23', NULL, NULL, NULL, NULL, 5, NULL, 0, NULL, 0),
(5, 'New School', 1, 'Street', '22', NULL, NULL, NULL, NULL, 5, NULL, 0, NULL, 0),
(6, 'School name', 1, 'Street name', '1', 3453452345, 'mail@mail.com', 'Director', 'Notes', 19, NULL, 1477927267, NULL, 0),
(7, 'Школа №69', 1, 'Жовтнева', '20', 4545454544, 'school69@email.com', 'Петров Іван Дмитрович', 'тестовий запис', 19, NULL, 1484051277, NULL, 0),
(8, 'Школа №69', 1, 'Жовтнева', '20', 4545454544, 'school69@email.com', 'Петров Іван Дмитрович', 'тестовий запис', 19, NULL, 1484051289, NULL, 0),
(9, 'ПолтаваШкола', 1, 'Зигіна', '4', 5646546546, 'Rer@mail.com', 'Вася', '12345', 19, NULL, 1484752447, NULL, 0),
(10, 'Класна школа', 1, 'Харківська', '45', 5656456456, 'eeee@e.com', 'Дима', '64566пре', 19, NULL, 1484753580, NULL, 1),
(11, 'Что', 1, 'цу', 'цу', 4543543534, 'Eee@e.com', 'Петро', 'ыфссыфсы', 19, NULL, 1484754833, NULL, 1),
(12, 'школа сумма', 2, 'Хз', '45', 4546454545, '54453645@mail.com', 'sdadas', 'asdasdasd', 19, NULL, 1484915729, NULL, 0),
(13, 'ЧТО', 1, 'авва', 'ыв', 4353454354, '435435345@mail.ru', 'ewfefes', 'sdsdf', 19, NULL, 1485186362, NULL, 1),
(14, 'ЧТО', 1, 'аіі', 'віаіваів', 5464566456, '343534@meta.ua', 'esfe', 'efrf', 19, NULL, 1485186470, NULL, 1),
(15, 'ЧТО1', 1, 'выасывс', 'высывсыв', 4354354353, '4534534534@meta.ua', 'rdgdf', 'gdfgdfgdf', 19, NULL, 1485187092, NULL, 1),
(16, '5464564', 1, '43534', '4354', 4354543543, '43534534534@meta.com', 'ggdfgdfgdfg', 'gfdgdfggdf', 19, NULL, 1485262498, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `section`
--

CREATE TABLE `section` (
  `id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `section`
--

INSERT INTO `section` (`id`, `book_id`, `name`, `deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(8, 5, 'роздел 1', 1, 5, NULL, 1476371373, NULL),
(9, 5, 'SECTION', 0, 19, 19, 1478269920, 1478601894),
(10, 5, 'SECTION', 1, 19, NULL, 1478269922, NULL),
(11, 5, 'Section 1', 0, 19, NULL, 1478270917, NULL),
(12, 5, 'Section 2', 0, 19, 19, 1478270925, 1478270952),
(13, 5, 'Section 3', 0, 19, NULL, 1478270930, NULL),
(14, 5, 'cvxf', 1, 19, NULL, 1482085739, NULL),
(15, 5, 'Section 6', 0, 19, NULL, 1482492250, NULL),
(16, 7, '235', 0, 19, 19, 1484052859, 1484052865),
(17, 5, 'Секція 74', 0, 19, 19, 1484053449, 1484053458),
(18, 5, 'sfs', 1, 19, NULL, 1484304522, NULL),
(19, 5, 'dcd', 1, 19, NULL, 1484304530, NULL),
(20, 14, '43543', 0, 19, NULL, 1484666452, NULL),
(21, 14, 'пкк', 1, 19, NULL, 1485258439, NULL),
(22, 14, 'ццкецу', 1, 19, NULL, 1485258580, NULL),
(23, 14, '4545', 1, 19, NULL, 1485259538, NULL),
(24, 20, '55t4t', 0, 19, 19, 1485812513, 1485812516),
(25, 20, '456456', 0, 19, NULL, 1485812521, NULL),
(26, 21, 'Розділ : Подільність', 0, 19, 19, 1486478086, 1492001032),
(27, 21, 'Розділ 2. Звичайні дроби', 0, 19, NULL, 1487172629, NULL),
(28, 21, 'Розділ 3. Вишка', 1, 19, NULL, 1491829004, NULL),
(29, 21, '3юрв', 1, 19, NULL, 1491829601, NULL),
(30, 23, 'йцуйцу', 0, 19, NULL, 1491829717, NULL),
(31, 23, 'шагврпва', 1, 19, NULL, 1491829742, NULL),
(32, 23, 'uysgfuyagsufgaysufgsyudgfuysdgfuysdgfuysdgfuysgdfyugsduyfgsuydfgsyudgfusdf', 1, 19, NULL, 1491906240, NULL),
(33, 23, 'ergidfjsd fsuidhf siudhf siudhf sdf siodf sidfjsijdf sijdf iosjd foisjd foisjdf osijdf osidjf osidjf osidjf osidjf osijd foisjd foisjdfoisjd ofijsd ofijsdoifjsodif', 1, 19, NULL, 1491908944, NULL),
(34, 23, 'іагнівпа', 1, 19, NULL, 1491909825, NULL),
(35, 23, 'іваіва', 1, 19, NULL, 1491909842, NULL),
(36, 23, '123', 1, 19, NULL, 1491910097, NULL),
(37, 23, 'приувэт', 0, 19, 19, 1491910182, 1492074388),
(38, 25, 'QQQQ', 0, 19, 19, 1491924216, 1492522224),
(39, 21, 'fghbjnm,', 1, 19, NULL, 1491924326, NULL),
(40, 26, 'ячииьтчамт', 0, 19, NULL, 1491924357, NULL),
(41, 26, 'ааааааааааа', 0, 19, NULL, 1491924373, NULL),
(42, 21, 'ikjmghbvc', 1, 19, NULL, 1491924398, NULL),
(43, 23, 'qweqweqwe', 0, 19, NULL, 1492076836, NULL),
(44, 48, 'Дроби', 1, 19, NULL, 1492507708, NULL),
(45, 48, 'Рацио', 0, 19, 19, 1492516043, 1492625726),
(46, 48, 'Основное свойство дроби: сокращение дробей', 0, 19, NULL, 1492516057, NULL),
(47, 48, 'Сложение и вычитание дробей с разными знаменателями: 8 класс', 0, 19, NULL, 1492516064, NULL),
(48, 48, 'Умножение и деление дробей: сокращение дробей + полезные советы', 0, 19, NULL, 1492516076, NULL),
(49, 48, 'Возведение дроби в степень: отрицательная, буквенная, со степенью', 0, 19, NULL, 1492516083, NULL),
(50, 48, 'Преобразование рациональных выражений: способы преобразований и примеры', 0, 19, NULL, 1492516091, NULL),
(51, 48, 'Функция y = k/x: ее график и свойства при k<0 и k>0', 0, 19, NULL, 1492516100, NULL),
(52, 48, 'Рациональные числа: определение, сумма, разность, умножение, деление', 0, 19, NULL, 1492516109, NULL),
(53, 48, 'Иррациональные числа: понятие и особенности', 0, 19, NULL, 1492516118, NULL),
(54, 48, 'Иррациональные числа: понятие и особенности', 0, 19, NULL, 1492516132, NULL),
(55, 48, 'Квадратные корни: арифметический квадратный корень', 0, 19, NULL, 1492516138, NULL),
(56, 48, 'Уравнение x^2 = a: все виды решений и график', 0, 19, NULL, 1492516144, NULL),
(57, 21, 'еакпрол', 0, 19, NULL, 1492525719, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `sity`
--

CREATE TABLE `sity` (
  `id` int(11) NOT NULL,
  `region_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sity`
--

INSERT INTO `sity` (`id`, `region_id`, `name`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 8, 'Полтава', 0, 1486045915, 0),
(2, 8, 'Суми', 0, 1484915729, 0),
(3, 8, 'Харьков', 0, 1486381350, 0),
(4, 8, 'Киев', 0, 1484577706, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `transfer_id` int(11) NOT NULL,
  `my_pupil_id` int(11) DEFAULT NULL,
  `code` varchar(16) NOT NULL,
  `date_from` int(11) NOT NULL,
  `date_to` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscription`
--

INSERT INTO `subscription` (`id`, `user_id`, `transfer_id`, `my_pupil_id`, `code`, `date_from`, `date_to`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(1, 1, 2, 18, '3214321', 12342, 2147483647, 1, NULL, 0, 1, 0),
(2, 7, 2, 19, '2314321', 123412, 2147483647, 7, NULL, 0, 7, 0),
(3, 52, 2, 20, '32414321', 43214321, 2147483647, 52, NULL, 0, 52, 0),
(6, 115, 2, NULL, '', 0, 1234432432, 115, NULL, 0, NULL, 1),
(7, 116, 2, 17, '', 0, 2147483647, 116, NULL, 0, NULL, 0),
(8, 117, 2, 16, '', 0, 543254325, 117, NULL, 0, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `subtask`
--

CREATE TABLE `subtask` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `text` text,
  `result` varchar(255) NOT NULL,
  `number` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `deleted` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subtask`
--

INSERT INTO `subtask` (`id`, `task_id`, `text`, `result`, `number`, `created_by`, `created_at`, `updated_at`, `updated_by`, `deleted`) VALUES
(1, 27, 'some text', 'some result', 1, 0, 0, 1482750551, 19, 1),
(2, 27, 'qwerty', 'qwertry', 2, 0, 0, 1482749252, 19, 1),
(3, 27, 'ytrewq', 'ytrewq', 3, 0, 0, 1482749299, 19, 1),
(4, 27, '444444', '444444', 444444, 19, 1482502350, 1482750943, 19, 1),
(5, 27, '5555', '5555', 5555, 19, 1482502839, 1482751311, 19, 1),
(6, 27, '11111111111', '1111111111111111', 1111111111, 19, 1482503112, 1482748922, 19, 1),
(7, 27, '22222222222222', '222222222', 2147483647, 19, 1482503518, 1482752881, 19, 1),
(8, 28, '1234', '1234', 1234, 19, 1482507192, 1482751773, 19, 1),
(9, 27, 'fdgfdgdfg', '657567', 665765, 19, 1482749412, 1482749419, 19, 1),
(10, 27, '555', '555', 555555, 19, 1482749475, 1482749484, 19, 1),
(11, 27, '55555555', '555', 555, 19, 1482749650, 1482749660, 19, 1),
(12, 27, '5555', '555', 555, 19, 1482750337, 1482750350, 19, 1),
(13, 27, '6666', '6666', 666, 19, 1482750868, 1482750880, 19, 1),
(14, 27, '66666666666666666', '54555', 555555, 19, 1482752910, 1482753004, 19, 1),
(15, 27, '555555555555', '55555555', 55555555, 19, 1482752951, 1482753331, 19, 1),
(16, 27, '7777', '777', 7777, 19, 1482752973, 1482753904, 19, 1),
(17, 27, '6666666666', '66666666', 666666, 19, 1482752999, 1482754358, 19, 1),
(18, 27, '656456', '546456', 565646, 19, 1482754413, 1482755535, 19, 1),
(19, 27, '5454', '43535', 43535545, 19, 1482754439, 1482754449, 19, 1),
(20, 27, '333', '333', 3333333, 19, 1482754600, 1482754614, 19, 1),
(21, 27, '54534543', '54534', 45345435, 19, 1482754963, 1482754989, 19, 1),
(22, 27, '5555', '555', 555, 19, 1482755612, 1482755621, 19, 1),
(23, 27, '555', '555', 5555, 19, 1482755665, 1482755687, 19, 1),
(24, 27, '666', '666', 666, 19, 1482755728, 1482755775, 19, 1),
(25, 27, '555', '555', 555, 19, 1482755866, 1482755879, 19, 1),
(26, 27, '55', '555', 555, 19, 1482756227, 1482756227, 19, 0),
(27, 27, '4545', '453', 43543, 19, 1482756249, 1482756313, 19, 1),
(28, 27, '5555', '5555', 555, 19, 1482756279, 1482756297, 19, 1),
(29, 27, '4444', '4444', 4444, 19, 1482756788, 1482756801, 19, 1),
(30, 27, '54353', '543534', 4354554, 19, 1482756846, 1482756872, 19, 1),
(31, 27, '6546', '4564', 5464, 19, 1482757501, 1482757509, 19, 1),
(32, 27, '65464', '546', 45645, 19, 1482757548, 1482758128, 19, 1),
(33, 27, '654645', '546456', 54654, 19, 1482757653, 1482760235, 19, 1),
(34, 27, '444', '444', 444, 19, 1482757933, 1482760068, 19, 1),
(35, 27, '444', '444', 444, 19, 1482757934, 1482758887, 19, 1),
(36, 27, '777', '777', 77, 19, 1482757973, 1482758814, 19, 1),
(37, 27, '555', '555', 555, 19, 1482758008, 1482758358, 19, 1),
(38, 27, '65464', '4656', 546456, 19, 1482758034, 1482758166, 19, 1),
(39, 27, '4', '4', 4, 19, 1482758701, 1482758715, 19, 1),
(40, 27, '546456', '54645', 654645, 19, 1482760021, 1482760028, 19, 1),
(41, 27, '4', '4', 4, 19, 1482760130, 1482760134, 19, 1),
(42, 27, '55', '55', 55, 19, 1482760212, 1482760217, 19, 1),
(43, 27, '67', '67', 6, 19, 1482760285, 1482760303, 19, 1),
(44, 27, '657', '6575', 566, 19, 1482760337, 1482760351, 19, 1),
(45, 27, '4', '4', 45, 19, 1482760516, 1482760516, 19, 0),
(46, 27, '55', '55', 55, 19, 1482761393, 1482761459, 19, 1),
(47, 27, '444', '44', 44, 19, 1482761485, 1482761485, 19, 0),
(48, 27, '7', '7', 7, 19, 1482761507, 1482761545, 19, 1),
(49, 27, '7', '7', 7, 19, 1482761539, 1482764620, 19, 1),
(50, 27, '7', '765778675675567', 7, 19, 1482844630, 1482857238, 19, 1),
(51, 27, '54353543', '45354', 45, 19, 1483977712, 1483977712, 19, 0),
(52, 27, '45445', '4534', 454, 19, 1484061329, 1484062605, 19, 1),
(53, 27, '546456', '54645', 54645, 19, 1484062951, 1484063093, 19, 1),
(54, 27, '435345', '435345', 3434, 19, 1484063112, 1484063126, 19, 1),
(55, 27, '45345', '543534', 4353, 19, 1484063174, 1484063182, 19, 1),
(56, 27, '444', '444', 444, 19, 1484063222, 1484063243, 19, 1),
(57, 27, '11', '111', 111, 19, 1484063832, 1484063843, 19, 1),
(58, 41, 'efsfsf', 'fdsfsdfsdfs', 1, 19, 1485186798, 1485187028, 19, 1),
(59, 27, 'сумма 2+2', '4', 0, 19, 1485257041, 1485257041, 19, 0),
(60, 41, 'сумма 2+2', '4', 5, 19, 1485258322, 1485258322, 19, 0),
(61, 41, '2', '', 1, 19, 1486569167, 1486569167, 19, 0),
(62, 41, '4', '', 4, 19, 1486569688, 1487004028, 19, 0),
(63, 54, '2 і 8', '', 1, 19, 1486569897, 1487067511, 19, 1),
(64, 54, '3 і 5', '', 2, 19, 1486569978, 1487067513, 19, 1),
(65, 54, '14 і 7', '', 3, 19, 1486570002, 1487067516, 19, 1),
(66, 54, '5 і 18', '', 4, 19, 1486570017, 1487067518, 19, 1),
(67, 54, '5 і 18;', '', 4, 19, 1486570046, 1486636398, 19, 1),
(68, 54, '10 і 50', '', 5, 19, 1486570068, 1487067520, 19, 1),
(69, 54, '1 і 2012', '', 6, 19, 1486636605, 1487067522, 19, 1),
(70, 55, '25 і 400', '', 1, 19, 1486640142, 1486640484, 19, 1),
(71, 55, '25 і 400', '', 1, 19, 1486640569, 1487067554, 19, 1),
(72, 55, '13 і 1613', '', 2, 19, 1486640691, 1487067557, 19, 1),
(73, 55, '123 і 3321', '', 3, 19, 1486640723, 1487067559, 19, 1),
(74, 56, '3 і 112', '', 1, 19, 1486641255, 1486641255, 19, 0),
(75, 56, '42 і 1050', '', 2, 19, 1486641313, 1486641313, 19, 0),
(76, 56, '37 і 1645', '', 3, 19, 1486641333, 1486641333, 19, 0),
(77, 58, '12 і 3', '', 1, 19, 1486641507, 1487068789, 19, 1),
(78, 58, '17 і 9', '', 2, 19, 1486641551, 1487068793, 19, 1),
(79, 58, '18 і 1', '', 3, 19, 1486641570, 1487068795, 19, 1),
(80, 58, '23 і 23', '', 4, 19, 1486641595, 1487068797, 19, 1),
(81, 60, 'На 3', '42, 1002, 8109', 1, 19, 1486736943, 1486736943, 19, 0),
(82, 60, 'На 9', '8109', 2, 19, 1486737143, 1486737143, 19, 0),
(83, 62, 'На 3', '9, 9, 18, -, -, 18', 1, 19, 1486737809, 1486737809, 19, 0),
(84, 62, 'На 9', '9, 9, 18, -, -, 18', 2, 19, 1486737930, 1486737930, 19, 0),
(85, 63, 'діляться на 3', '180, 2109, 4590, 111 102', 1, 19, 1486738185, 1486738185, 19, 0),
(86, 63, 'діляться на 9', '180, 4590', 2, 19, 1486738328, 1486738328, 19, 0),
(87, 63, 'діляться на 2 і на 3', '180, 4590, 111 102', 3, 19, 1486738486, 1486738486, 19, 0),
(88, 63, 'не діляться на 3', '541, 7891', 4, 19, 1486738738, 1486738738, 19, 0),
(89, 63, 'діляться на 3, але не діляться на 9.', '2109, 111 102', 5, 19, 1486738864, 1486738864, 19, 0),
(90, 64, 'діляться на 3', '582, 450, 3105', 1, 19, 1486739146, 1486739146, 19, 0),
(91, 64, 'діляться на 9', '450, 3105', 2, 19, 1486739387, 1486739387, 19, 0),
(92, 64, 'діляться на 3 і на 2', '582, 450', 3, 19, 1486739460, 1486739460, 19, 0),
(93, 64, 'діляться на 9 і на 5', '450, 3105', 4, 19, 1486739572, 1486739572, 19, 0),
(94, 65, '1, 2, 3', 'Так', 1, 19, 1486739798, 1486739798, 19, 0),
(95, 65, '2, 3, 5', 'Ні', 2, 19, 1486739837, 1486739837, 19, 0),
(96, 65, '1, 0, 8', 'Так', 3, 19, 1486739875, 1486739875, 19, 0),
(97, 66, '4, 6, 0, 9', 'Ні', 1, 19, 1486740072, 1486740072, 19, 0),
(98, 66, '9, 1, 2, 6', 'Ні', 2, 19, 1486740160, 1486740160, 19, 0),
(99, 67, '2*00', '7', 1, 19, 1486740540, 1486740540, 19, 0),
(100, 67, '*375', '3', 2, 19, 1486740637, 1486740637, 19, 0),
(101, 67, '8*19', '0', 3, 19, 1486740667, 1486740667, 19, 0),
(102, 68, '28*1', '1, 4, 7', 1, 19, 1486740952, 1486740952, 19, 0),
(103, 68, '4*5', '0, 3, 6, 9', 2, 19, 1486741022, 1486741022, 19, 0),
(104, 68, '1111*', '2, 5, 8', 3, 19, 1486741132, 1486741132, 19, 0),
(105, 70, 'e', '234', 2, 19, 1486741457, 1486748025, 19, 0),
(106, 72, '5, 8', '-, -', 1, 19, 1486742612, 1486742612, 19, 0),
(107, 72, '3, 6', '36, 36', 2, 19, 1486742644, 1486742644, 19, 0),
(108, 72, '1, 8', '18, 18', 3, 19, 1486742662, 1486742662, 19, 0),
(109, 73, '1)	5*7* ділилося на 3 і на 10', '0, 3, 6, 9', 1, 19, 1486742835, 1486742835, 19, 0),
(110, 73, '10 0** ділилося на 9 і на 10', '8', 2, 19, 1486742886, 1486742886, 19, 0),
(111, 73, '*0 00* ділилося на 5 і на 9', '4 і 5, 9 і 0', 3, 19, 1486742981, 1486742981, 19, 0),
(112, 73, '71** ділилося на 3, на 5 і на 2', '1 і 0', 4, 19, 1486743037, 1486743037, 19, 0),
(113, 74, '2*7* ділилося на 3 і на 5', '1 і 5, 0 і 0', 1, 19, 1486743161, 1486743161, 19, 0),
(114, 74, '20* 06* ділилося на 2 і на 9', '1 і 0, 8 і 2, 6 і 4, 4 і 6, 8 і 0', 2, 19, 1486743373, 1486743373, 19, 0),
(115, 75, 'на 2 і на 3;', '1002', 1, 19, 1486743686, 1486743741, 19, 0),
(116, 75, 'на 5 і на 9', '1035', 2, 19, 1486744024, 1486744024, 19, 0),
(117, 75, 'на 3 і на 10', '1020', 3, 19, 1486744050, 1486744050, 19, 0),
(118, 75, 'на 2, на 3 і на 5', '1020', 4, 19, 1486744084, 1486744084, 19, 0),
(119, 80, '5 год', '150', 1, 19, 1486744858, 1486744858, 19, 0),
(120, 80, '6 год', '180', 2, 19, 1486744874, 1486744874, 19, 0),
(121, 80, '11 год', '330', 3, 19, 1486744894, 1486744894, 19, 0),
(122, 80, '14 год', '60', 4, 19, 1486744935, 1486744935, 19, 0),
(123, 53, '44', '45', 4, 19, 1487001960, 1487001960, 19, 0),
(124, 90, '6', '6', 6, 19, 1487026941, 1487026988, 19, 0),
(125, 53, '6', '6', 6, 19, 1487027480, 1487027480, 19, 0),
(126, 53, '3', '', 3, 19, 1487027915, 1487027915, 19, 0),
(127, 53, '675', '', 65, 19, 1487028303, 1487028303, 19, 0),
(128, 92, '25 і 400', '', 1, 19, 1487068637, 1487068664, 19, 0),
(129, 92, '13 і 1613', '', 2, 19, 1487068681, 1487068681, 19, 0),
(130, 92, '123 і 3321', '', 3, 19, 1487068709, 1487068709, 19, 0),
(131, 93, '810 і 5', '', 1, 19, 1487069363, 1487069363, 19, 0),
(132, 93, '1036 і 45', '', 2, 19, 1487069382, 1487069382, 19, 0),
(133, 93, '4144 і 37', '', 3, 19, 1487069423, 1487069423, 19, 0),
(134, 94, '189 і 3', '', 1, 19, 1487069510, 1487069510, 19, 0),
(135, 94, '1051 і 6', '', 2, 19, 1487069527, 1487069527, 19, 0),
(136, 94, '3000 і 24', '', 3, 19, 1487069552, 1487069552, 19, 0),
(137, 96, '25 і 400', '', 1, 19, 1487069806, 1487069806, 19, 0),
(138, 96, '13 і 1613', '', 2, 19, 1487069832, 1487069832, 19, 0),
(139, 96, '123 і 3321', '', 3, 19, 1487069848, 1487069848, 19, 0),
(140, 97, '12', '', 1, 19, 1487070075, 1487070075, 19, 0),
(141, 97, '19', '', 2, 19, 1487070092, 1487070092, 19, 0),
(142, 97, '27', '', 3, 19, 1487070108, 1487070108, 19, 0),
(143, 97, '36', '', 4, 19, 1487070138, 1487070138, 19, 0),
(144, 98, '15', '', 1, 19, 1487070189, 1487070189, 19, 0),
(145, 98, '23', '', 2, 19, 1487070203, 1487070203, 19, 0),
(146, 98, '28', '', 3, 19, 1487070214, 1487070214, 19, 0),
(147, 98, '40', '', 4, 19, 1487070228, 1487070228, 19, 0),
(148, 99, '8', '', 1, 19, 1487070292, 1487070292, 19, 0),
(149, 99, '10', '', 2, 19, 1487070303, 1487070303, 19, 0),
(150, 99, '19', '', 3, 19, 1487070331, 1487070331, 19, 0),
(151, 100, '6', '', 1, 19, 1487070395, 1487070395, 19, 0),
(152, 100, '11', '', 2, 19, 1487070406, 1487070406, 19, 0),
(153, 100, '23', '', 3, 19, 1487070420, 1487070420, 19, 0),
(154, 102, 'по 25 коп.', '', 1, 19, 1487070553, 1487070553, 19, 0),
(155, 102, 'по 50 коп.', '', 2, 19, 1487070566, 1487070566, 19, 0),
(156, 103, 'в 2 кошики', '', 1, 19, 1487070831, 1487070831, 19, 0),
(157, 103, 'в 3 кошики', '', 2, 19, 1487070863, 1487070863, 19, 0),
(158, 103, 'в 5 кошиків', '', 3, 19, 1487070879, 1487070879, 19, 0),
(159, 106, '8 і 12', '', 1, 19, 1487071305, 1487071305, 19, 0),
(160, 106, '20 і 30', '', 2, 19, 1487071327, 1487071327, 19, 0),
(161, 106, '13 і 26', '', 3, 19, 1487071344, 1487071344, 19, 0),
(162, 106, '7 і 15', '', 4, 19, 1487071354, 1487071354, 19, 0),
(163, 107, '4 і 9', '', 1, 19, 1487071578, 1487071578, 19, 0),
(164, 107, '15 і 10', '', 2, 19, 1487071594, 1487071594, 19, 0),
(165, 108, '2 і 5', '', 1, 19, 1487072176, 1487072176, 19, 0),
(166, 108, '3 і 6', '', 2, 19, 1487072190, 1487072190, 19, 0),
(167, 108, '9 і 12', '', 3, 19, 1487072205, 1487072205, 19, 0),
(168, 109, '3 і 7', '', 1, 19, 1487072441, 1487072441, 19, 0),
(169, 109, '8 і 12', '', 2, 19, 1487072453, 1487072453, 19, 0),
(170, 111, 'кратні числу 3', '', 1, 19, 1487076155, 1487076155, 19, 0),
(171, 111, 'є дільниками числа 36', '', 2, 19, 1487076170, 1487076170, 19, 0),
(172, 113, 'найбільше чотирицифрове число, що кратне числу 115', '', 1, 19, 1487076453, 1487076453, 19, 0),
(173, 113, 'найменше п’ятицифрове число, що кратне числу 12', '', 2, 19, 1487076470, 1487076470, 19, 0),
(174, 117, '17,89 до одиниць', '', 1, 19, 1487077946, 1487077946, 19, 0),
(175, 117, '15,135 до десятих', '', 2, 19, 1487077967, 1487077967, 19, 0),
(176, 117, '18,475 до сотих', '', 3, 19, 1487077993, 1487077993, 19, 0),
(177, 117, '189,145 до десятків', '', 4, 19, 1487078006, 1487078006, 19, 0),
(178, 119, 'на 2', '', 1, 19, 1487078957, 1487078957, 19, 0),
(179, 119, 'на 5', '', 2, 19, 1487079392, 1487079392, 19, 0),
(180, 119, 'на 10', '', 3, 19, 1487080422, 1487080422, 19, 0),
(181, 120, 'парними ?', '', 1, 19, 1487081209, 1487081209, 19, 0),
(182, 120, 'непарними ?', '', 2, 19, 1487081233, 1487081233, 19, 0),
(183, 121, 'на 2', '', 1, 19, 1487081849, 1487081849, 19, 0),
(184, 121, 'на 5', '', 2, 19, 1487081862, 1487081862, 19, 0),
(185, 121, 'на 10', '', 3, 19, 1487081883, 1487081883, 19, 0),
(186, 128, 'на 2', '', 1, 19, 1487084086, 1487084086, 19, 0),
(187, 128, 'на 5', '', 2, 19, 1487084105, 1487084105, 19, 0),
(188, 128, 'на 10', '', 3, 19, 1487084124, 1487084124, 19, 0),
(189, 129, 'на 2', '', 1, 19, 1487084191, 1487084191, 19, 0),
(190, 129, 'на 5', '', 2, 19, 1487084203, 1487084203, 19, 0),
(191, 129, 'на 10', '', 3, 19, 1487084225, 1487084225, 19, 0),
(192, 130, 'на 2', '', 1, 19, 1487084285, 1487084285, 19, 0),
(193, 130, 'на 5', '', 2, 19, 1487084295, 1487084295, 19, 0),
(194, 130, 'на 10', '', 3, 19, 1487084310, 1487084310, 19, 0),
(195, 132, 'на 2', '', 1, 19, 1487084447, 1487084447, 19, 0),
(196, 132, 'на 5', '', 2, 19, 1487084459, 1487084459, 19, 0),
(197, 132, 'на 10', '', 3, 19, 1487084468, 1487084468, 19, 0),
(198, 133, 'було парним', '', 1, 19, 1487084545, 1487084545, 19, 0),
(199, 133, 'було непарним', '', 2, 19, 1487084559, 1487084559, 19, 0),
(200, 133, 'було непарним', '', 2, 19, 1487084565, 1487084565, 19, 0),
(201, 133, 'ділилося на 5', '', 3, 19, 1487084575, 1487084575, 19, 0),
(202, 133, 'ділилося на 10', '', 4, 19, 1487084590, 1487084590, 19, 0),
(203, 136, 'на 2', '', 1, 19, 1487085023, 1487085023, 19, 0),
(204, 136, 'на 5', '', 2, 19, 1487085032, 1487085032, 19, 0),
(205, 136, 'на 10', '', 3, 19, 1487085047, 1487085047, 19, 0),
(206, 137, 'число, що ділиться на 10', '', 1, 19, 1487085168, 1487085168, 19, 0),
(207, 137, 'парне число', '', 2, 19, 1487085189, 1487085189, 19, 0),
(208, 137, 'парне число', '', 2, 19, 1487085192, 1487085192, 19, 0),
(209, 137, 'число, яке кратне числу 5', '', 3, 19, 1487085204, 1487085204, 19, 0),
(210, 137, 'непарне число', '', 4, 19, 1487085225, 1487085225, 19, 0),
(211, 138, 'чотирицифрове число, що кратне числу 2', '', 1, 19, 1487085460, 1487085460, 19, 0),
(212, 138, 'п’ятицифрове число, що кратне числу 5', '', 2, 19, 1487085485, 1487085485, 19, 0),
(213, 138, 'шестицифрове число, що кратне числу 10', '', 3, 19, 1487085537, 1487085537, 19, 0),
(214, 141, 'більше за 6,7, але менше від 6,9', '', 1, 19, 1487085698, 1487085698, 19, 0),
(215, 141, 'менше від 13 5/9 , але більше за 13', '', 2, 19, 1487085744, 1487085744, 19, 0),
(216, 143, 'менші від 37, але більші за 20', '', 1, 19, 1487086067, 1487086067, 19, 0),
(217, 143, 'більші за 78, але менші від 110', '', 2, 19, 1487086084, 1487086084, 19, 0),
(218, 144, 'прості', '', 1, 19, 1487086222, 1487086222, 19, 0),
(219, 144, 'складені', '', 2, 19, 1487086235, 1487086235, 19, 0),
(220, 145, 'прості', '', 1, 19, 1487086353, 1487086353, 19, 0),
(221, 145, 'складені', '', 2, 19, 1487086362, 1487086362, 19, 0),
(222, 146, '7152', '', 1, 19, 1487086684, 1487086684, 19, 0),
(223, 146, '60 003', '', 2, 19, 1487086696, 1487086696, 19, 0),
(224, 146, '11 115', '', 3, 19, 1487086712, 1487086712, 19, 0),
(225, 146, '3819', '', 4, 19, 1487086734, 1487086734, 19, 0),
(226, 147, '80 001', '', 1, 19, 1487086816, 1487086816, 19, 0),
(227, 147, '7315', '', 2, 19, 1487086829, 1487086829, 19, 0),
(228, 147, '12 340', '', 3, 19, 1487086844, 1487086844, 19, 0),
(229, 147, '738', '', 4, 19, 1487086855, 1487086855, 19, 0),
(230, 149, '273*', '', 1, 19, 1487087224, 1487087224, 19, 0),
(231, 149, '5*39', '', 2, 19, 1487087240, 1487087240, 19, 0),
(232, 150, '987*', '', 1, 19, 1487087361, 1487087361, 19, 0),
(233, 150, '5*41', '', 2, 19, 1487087373, 1487087373, 19, 0),
(234, 151, 'усі прості числа, більші за 7 і менші від 20', '', 1, 19, 1487087439, 1487087439, 19, 0),
(235, 151, 'усі складені числа, більші за 50 і менші від 66', '', 2, 19, 1487087455, 1487087455, 19, 0),
(236, 152, 'усі прості числа х, при яких нерівність 37 < х < 60 є правильною', '', 1, 19, 1487088347, 1487088347, 19, 0),
(237, 152, '2)	усі складені числа у, при яких нерівність 4 < у < 21 є правильною', '', 2, 19, 1487088359, 1487088359, 19, 0),
(238, 153, '13 • 1', '', 1, 19, 1487088696, 1487088696, 19, 0),
(239, 153, '15 • 1', '', 2, 19, 1487088711, 1487088711, 19, 0),
(240, 153, '7 • 11', '', 3, 19, 1487089097, 1487089097, 19, 0),
(241, 153, '1 • 2 • 67', '', 4, 19, 1487089110, 1487089110, 19, 0),
(242, 154, '4, 6, 8', '', 1, 19, 1487089246, 1487089246, 19, 0),
(243, 154, '1, 3, 5', '', 2, 19, 1487089259, 1487089259, 19, 0),
(244, 154, '0, 2, 5', '', 3, 19, 1487089271, 1487089271, 19, 0),
(245, 155, '0, 4, 5', '', 1, 19, 1487089363, 1487089363, 19, 0),
(246, 155, '1, 2, 0', '', 2, 19, 1487089375, 1487089375, 19, 0),
(247, 156, 'п’ятнадцяти одиниць', '', 1, 19, 1487089556, 1487089556, 19, 0),
(248, 156, 'дві тисячі чотирнадцяти п’ятірок', '', 2, 19, 1487089572, 1487089572, 19, 0),
(249, 156, 'усіх десяти цифр', '', 3, 19, 1487089583, 1487089583, 19, 0),
(250, 157, '117/2', '', 1, 19, 1487089687, 1487089687, 19, 0),
(251, 157, '163/17', '', 2, 19, 1487089702, 1487089702, 19, 0),
(252, 157, '900/25', '', 3, 19, 1487089719, 1487089719, 19, 0),
(253, 157, '1235/63', '', 4, 19, 1487090468, 1487090468, 19, 0),
(254, 159, '1 + 3 + 5 + 7 + ... + 97 + 99', '', 1, 19, 1487090956, 1487090956, 19, 0),
(255, 159, '2 + 4 + 6 + ... + 98 + 100 − 1 − 3 − 5−- ... − 97 – 99', '', 2, 19, 1487090988, 1487090988, 19, 0),
(256, 160, '3 • 5', '', 1, 19, 1487154117, 1487154117, 19, 0),
(257, 160, '1 • 5', '', 2, 19, 1487154160, 1487154160, 19, 0),
(258, 160, '1 • 5', '', 2, 19, 1487154167, 1487154167, 19, 0),
(259, 160, '2 • 7 • 12', '', 3, 19, 1487154190, 1487154190, 19, 0),
(260, 160, '4 • 3 • 5', '', 4, 19, 1487154214, 1487154214, 19, 0),
(261, 161, '8', '', 1, 19, 1487154292, 1487154292, 19, 0),
(262, 161, '10', '', 2, 19, 1487154317, 1487154317, 19, 0),
(263, 161, '12', '', 3, 19, 1487154344, 1487154344, 19, 0),
(264, 161, '14', '', 4, 19, 1487154371, 1487154371, 19, 0),
(265, 161, '17', '', 5, 19, 1487154396, 1487154396, 19, 0),
(266, 161, '18', '', 6, 19, 1487154462, 1487154462, 19, 0),
(267, 161, '20', '', 7, 19, 1487154504, 1487154504, 19, 0),
(268, 161, '25', '', 8, 19, 1487154547, 1487154547, 19, 0),
(269, 161, '27', '', 9, 19, 1487154575, 1487154575, 19, 0),
(270, 161, '31', '', 10, 19, 1487154585, 1487154585, 19, 0),
(271, 162, '56', '', 1, 19, 1487155417, 1487155417, 19, 0),
(272, 162, '130', '', 2, 19, 1487155430, 1487155430, 19, 0),
(273, 162, '60', '', 3, 19, 1487155451, 1487155451, 19, 0),
(274, 162, '96', '', 4, 19, 1487155462, 1487155462, 19, 0),
(275, 162, '250', '', 5, 19, 1487155477, 1487155477, 19, 0),
(276, 162, '315', '', 6, 19, 1487155496, 1487155496, 19, 0),
(277, 162, '561', '', 7, 19, 1487155507, 1487155507, 19, 0),
(278, 162, '175', '', 8, 19, 1487155602, 1487155602, 19, 0),
(279, 162, '2240', '', 9, 19, 1487155633, 1487155633, 19, 0),
(280, 162, '1782', '', 10, 19, 1487155650, 1487155650, 19, 0),
(281, 163, '48', '', 1, 19, 1487155759, 1487155759, 19, 0),
(282, 163, '105', '', 2, 19, 1487155825, 1487155825, 19, 0),
(283, 163, '88', '', 3, 19, 1487155841, 1487155841, 19, 0),
(284, 163, '660', '', 4, 19, 1487155854, 1487155854, 19, 0),
(285, 163, '600', '', 5, 19, 1487155863, 1487155863, 19, 0),
(286, 163, '3003', '', 6, 19, 1487155872, 1487155872, 19, 0),
(287, 163, '2772', '', 7, 19, 1487155881, 1487155881, 19, 0),
(288, 163, '4900', '', 8, 19, 1487155893, 1487155893, 19, 0),
(289, 164, '2', '', 1, 19, 1487155985, 1487155985, 19, 0),
(290, 164, '6', '', 2, 19, 1487155999, 1487155999, 19, 0),
(291, 164, '51', '', 3, 19, 1487156013, 1487156013, 19, 0),
(292, 165, 'числа 2 • 3 • 7 • 19 на число 2 • 7 • 19', '', 1, 19, 1487156096, 1487156096, 19, 0),
(293, 165, 'числа 2 • 2 • 3 • 3 • 5 • 7 на число 15', '', 2, 19, 1487156108, 1487156108, 19, 0),
(294, 171, '.', '', 1, 19, 1487156857, 1487156857, 19, 0),
(295, 171, '.', '', 2, 19, 1487157020, 1487157020, 19, 0),
(296, 172, 'Використовуючи таблицю простих чисел (до 1000), знайди всі пари простих чисел, у кожній з яких друге число більше за перше на 2. (Такі пари простих чи¬сел називають числами-близнюками).', '', 1, 19, 1487157102, 1487157102, 19, 0),
(297, 172, 'Використовуючи калькулятор, комп’ютер або інфор¬мацію в Інтернеті, спробуй знайти ще кілька таких пар чисел, кожне з яких більше за 1000.', '', 2, 19, 1487157131, 1487157131, 19, 0),
(298, 173, '30 і 25', '', 1, 19, 1487157335, 1487157335, 19, 0),
(299, 173, '48 і 15', '', 2, 19, 1487157347, 1487157347, 19, 0),
(300, 174, '2 і 4', '', 1, 19, 1487157435, 1487157435, 19, 0),
(301, 174, '6 і 15', '', 2, 19, 1487157449, 1487157449, 19, 0),
(302, 174, '8 і 18', '', 3, 19, 1487157460, 1487157460, 19, 0),
(303, 175, 'а = 2 • 3 • 5 • 11 і b = 2 • 3 • 11 • 13;', '', 1, 19, 1487157570, 1487157570, 19, 0),
(304, 175, 'а = 3 • 5 • 5 • 7 і b = 5 • 5 • 19', '', 2, 19, 1487157593, 1487157593, 19, 0),
(305, 176, 'с = 3 • 3 • 5 • 11 і d = 3 • 5 • 11 • 13', '', 1, 19, 1487157678, 1487157678, 19, 0),
(306, 176, 'с = 2 • 3 • 5 • 5 і d= 3 • 5 • 7', '', 2, 19, 1487157703, 1487157703, 19, 0),
(307, 177, '6 і 9', '', 1, 19, 1487157758, 1487157758, 19, 0),
(308, 177, '7 і 8', '', 2, 19, 1487157772, 1487157772, 19, 0),
(309, 177, '9 і 12', '', 3, 19, 1487157787, 1487157787, 19, 0),
(310, 177, '4 і 11', '', 4, 19, 1487157796, 1487157796, 19, 0),
(311, 179, '78 і 195', '', 1, 19, 1487157894, 1487157894, 19, 0),
(312, 179, '35 і 18', '', 2, 19, 1487157906, 1487157906, 19, 0),
(313, 179, '210 і 120', '', 3, 19, 1487157932, 1487157932, 19, 0),
(314, 179, '735 і 70', '', 4, 19, 1487157946, 1487157946, 19, 0),
(315, 179, '4,  24 і 32', '', 5, 19, 1487157965, 1487157965, 19, 0),
(316, 179, '36,  54 і 72', '', 6, 19, 1487157982, 1487157982, 19, 0),
(317, 180, '24 і 40', '', 1, 19, 1487158029, 1487158029, 19, 0),
(318, 180, '70 і 110', '', 2, 19, 1487158039, 1487158039, 19, 0),
(319, 180, '49 і 48', '', 3, 19, 1487158052, 1487158052, 19, 0),
(320, 180, '231 і 273', '', 4, 19, 1487158062, 1487158062, 19, 0),
(321, 180, '5,  25 і 45', '', 5, 19, 1487158079, 1487158079, 19, 0),
(322, 180, '150, 375 і 600', '', 6, 19, 1487158097, 1487158097, 19, 0),
(323, 181, '.', '', 1, 19, 1487158273, 1487158547, 19, 1),
(324, 181, '.', '', 1, 19, 1487158582, 1487158582, 19, 0),
(325, 181, '.', '', 2, 19, 1487158598, 1487158598, 19, 0),
(326, 181, '.', '', 3, 19, 1487158628, 1487158628, 19, 0),
(327, 181, '.', '', 4, 19, 1487158686, 1487158686, 19, 0),
(328, 182, 'числа 35 і 72 є взаємно простими', '', 1, 19, 1487158753, 1487158753, 19, 0),
(329, 182, 'числа 209 і 171 не є взаємно простими', '', 2, 19, 1487158770, 1487158770, 19, 0),
(330, 183, 'числа 299 і 184 не є взаємно простими', '', 1, 19, 1487158876, 1487158876, 19, 0),
(331, 183, 'числа 26 і 45 є взаємно простими', '', 2, 19, 1487158892, 1487158892, 19, 0),
(332, 184, '3 і 100', '', 1, 19, 1487158997, 1487158997, 19, 0),
(333, 184, '35 і 133', '', 2, 19, 1487159014, 1487159014, 19, 0),
(334, 184, '143 і 209', '', 3, 19, 1487159025, 1487159025, 19, 0),
(335, 184, '2010 і 2012', '', 4, 19, 1487159037, 1487159037, 19, 0),
(336, 185, '7 і 48', '', 1, 19, 1487159709, 1487159709, 19, 0),
(337, 185, '21 і 161', '', 2, 19, 1487159724, 1487159724, 19, 0),
(338, 185, '66 і 455', '', 3, 19, 1487159737, 1487159737, 19, 0),
(339, 185, '2005 і 3005', '', 4, 19, 1487159751, 1487159751, 19, 0),
(340, 189, 'По скільки дисків кожного жанру отримає кожний магазин?', '', 1, 19, 1487160024, 1487160024, 19, 0),
(341, 192, 'Яку найбільшу кількість таких букетів можна скласти?', '', 1, 19, 1487160207, 1487160207, 19, 0),
(342, 192, 'По скільки троянд кожного кольору буде в кожному букеті?', '', 2, 19, 1487160249, 1487160249, 19, 0),
(343, 195, 'Скільки грошей буде на рахунку вкладника через рік ?', '', 1, 19, 1487160450, 1487160541, 19, 0),
(344, 195, 'Скільки грошей буде на рахунку вкладника через два роки ?', '', 2, 19, 1487160476, 1487160476, 19, 0),
(345, 196, '.', '', 1, 19, 1487160738, 1487160738, 19, 0),
(346, 196, '.', '', 2, 19, 1487160755, 1487160755, 19, 0),
(347, 196, '.', '', 3, 19, 1487160771, 1487160771, 19, 0),
(348, 197, 'число 56 спільним кратним чисел 2 і 7', '', 1, 19, 1487161063, 1487161063, 19, 0),
(349, 197, 'число 48 спільним кратним чисел 5 і 6', '', 2, 19, 1487161124, 1487161124, 19, 0),
(350, 198, '5 і 2', '', 1, 19, 1487161641, 1487161641, 19, 0),
(351, 198, '4 і 8', '', 2, 19, 1487161655, 1487161655, 19, 0),
(352, 198, '3 і 7', '', 3, 19, 1487161667, 1487161667, 19, 0),
(353, 198, '3 і 7', '', 3, 19, 1487161668, 1487161668, 19, 0),
(354, 199, 'а = 2 • 3 • 3 • 11 і b = 2 • 3 • 7', '', 1, 19, 1487161747, 1487161747, 19, 0),
(355, 199, 'а = 2 • 2 • 3 • 5 • 7 і b = 2 • 3 • 3 • 3 • 5', '', 2, 19, 1487161777, 1487161777, 19, 0),
(356, 200, 'm = 3 • 5 • 7 і n = 2 • 3 • 5 • 13', '', 1, 19, 1487161873, 1487161873, 19, 0),
(357, 200, 'm = 3 • 3 • 5 • 5 • 11 і n = 3 • 5 • 7 • 11', '', 2, 19, 1487161918, 1487161918, 19, 0),
(358, 201, '7 і 9', '', 1, 19, 1487161969, 1487161969, 19, 0),
(359, 201, '8 і 39', '', 2, 19, 1487161978, 1487161978, 19, 0),
(360, 201, '25 і 42', '', 3, 19, 1487161995, 1487161995, 19, 0),
(361, 201, '7 і 9', '', 1, 19, 1487162068, 1487162076, 19, 1),
(362, 202, '15 і 18', '', 1, 19, 1487162174, 1487162174, 19, 0),
(363, 202, '16 і 24', '', 2, 19, 1487162188, 1487162188, 19, 0),
(364, 202, '48 і 72', '', 3, 19, 1487162205, 1487162205, 19, 0),
(365, 202, '350 і 420', '', 4, 19, 1487162216, 1487162216, 19, 0),
(366, 202, '12, 18 і 36', '', 5, 19, 1487162235, 1487162235, 19, 0),
(367, 202, '280, 360 і 840', '', 6, 19, 1487162249, 1487162249, 19, 0),
(368, 203, '12 і 10', '', 1, 19, 1487162738, 1487162738, 19, 0),
(369, 203, '16 і 21', '', 2, 19, 1487162763, 1487162763, 19, 0),
(370, 203, '110 і 160', '', 3, 19, 1487162775, 1487162775, 19, 0),
(371, 203, '540 і 306', '', 4, 19, 1487162803, 1487162803, 19, 0),
(372, 203, '15, 25 і 75', '', 5, 19, 1487162813, 1487162813, 19, 0),
(373, 203, '270, 324 і 540', '', 6, 19, 1487162830, 1487162830, 19, 0),
(374, 204, '.', '', 1, 19, 1487163025, 1487163025, 19, 0),
(375, 204, '.', '', 2, 19, 1487163071, 1487163071, 19, 0),
(376, 204, '.', '', 3, 19, 1487163097, 1487163097, 19, 0),
(377, 204, '.', '', 4, 19, 1487163143, 1487163143, 19, 0),
(378, 205, '.', '', 1, 19, 1487163206, 1487163206, 19, 0),
(379, 205, '.', '', 2, 19, 1487163218, 1487163218, 19, 0),
(380, 211, '(x − 37,15) • 5,1 = 245,82', '', 1, 19, 1487163904, 1487163904, 19, 0),
(381, 211, '(37,5 + x) : 1,2 = 43,5', '', 2, 19, 1487163926, 1487163926, 19, 0),
(382, 213, 'а = 18; b = 12;', '', 1, 19, 1487164075, 1487164075, 19, 0),
(383, 213, 'а = 15; b = 17;', '', 2, 19, 1487164087, 1487164087, 19, 0),
(384, 213, 'а = 9; b = 27.', '', 3, 19, 1487164103, 1487164103, 19, 0),
(385, 214, '.', '', 1, 19, 1487172783, 1487172783, 19, 0),
(386, 214, '.', '', 2, 19, 1487172803, 1487172803, 19, 0),
(387, 214, '.', '', 3, 19, 1487172835, 1487172835, 19, 0),
(388, 214, '.', '', 4, 19, 1487172851, 1487172873, 19, 0),
(389, 215, '.', '', 1, 19, 1487172948, 1487172948, 19, 0),
(390, 215, '.', '', 2, 19, 1487172964, 1487172964, 19, 0),
(391, 215, '.', '', 3, 19, 1487172975, 1487172975, 19, 0),
(392, 215, '.', '', 4, 19, 1487173033, 1487173033, 19, 0),
(393, 218, '.', '', 1, 19, 1487174703, 1487174703, 19, 0),
(394, 218, '.', '', 2, 19, 1487175372, 1487175372, 19, 0),
(395, 218, '.', '', 2, 19, 1487175402, 1487175402, 19, 0),
(396, 219, '.', '', 1, 19, 1487175572, 1487175572, 19, 0),
(397, 219, '.', '', 2, 19, 1487175987, 1487175987, 19, 0),
(398, 220, '.', '', 1, 19, 1487176148, 1487176148, 19, 0),
(399, 220, '.', '', 2, 19, 1487176414, 1487176414, 19, 0),
(400, 221, '.', '', 1, 19, 1487176769, 1487176769, 19, 0),
(401, 221, '.', '', 2, 19, 1487176801, 1487176801, 19, 0),
(402, 222, '.', '', 1, 19, 1487176918, 1487176918, 19, 0),
(403, 222, '.', '', 2, 19, 1487176958, 1487176958, 19, 0),
(404, 222, '.', '', 3, 19, 1487176976, 1487176976, 19, 0),
(405, 222, '.', '', 4, 19, 1487176997, 1487176997, 19, 0),
(406, 222, '.', '', 1, 19, 1487177456, 1487177486, 19, 1),
(407, 222, '.', '', 2, 19, 1487177471, 1487177559, 19, 1),
(408, 225, '.', '', 1, 19, 1487242329, 1487242329, 19, 0),
(409, 225, '.', '', 2, 19, 1487242394, 1487242394, 19, 0),
(410, 225, '.', '', 3, 19, 1487242412, 1487242412, 19, 0),
(411, 225, '.', '', 4, 19, 1487242443, 1487242443, 19, 0),
(412, 226, '.', '', 1, 19, 1487242574, 1487242574, 19, 0),
(413, 226, '.', '', 2, 19, 1487242595, 1487242595, 19, 0),
(414, 226, '.', '', 3, 19, 1487242654, 1487242654, 19, 0),
(415, 226, '.', '', 4, 19, 1487242677, 1487242677, 19, 0),
(416, 227, '.', '', 1, 19, 1487242746, 1487242746, 19, 0),
(417, 227, '.', '', 2, 19, 1487242763, 1487242763, 19, 0),
(418, 227, '.', '', 3, 19, 1487242787, 1487242787, 19, 0),
(419, 227, '.', '', 4, 19, 1487242814, 1487242814, 19, 0),
(420, 227, '.', '', 5, 19, 1487242836, 1487242836, 19, 0),
(421, 227, '.', '', 6, 19, 1487242852, 1487242852, 19, 0),
(422, 227, '.', '', 7, 19, 1487242889, 1487242889, 19, 0),
(423, 227, '.', '', 8, 19, 1487242909, 1487242909, 19, 0),
(424, 228, '.', '', 1, 19, 1487242996, 1487242996, 19, 0),
(425, 228, '.', '', 2, 19, 1487243013, 1487243013, 19, 0),
(426, 228, '.', '', 3, 19, 1487243027, 1487243027, 19, 0),
(427, 228, '.', '', 4, 19, 1487243044, 1487243044, 19, 0),
(428, 228, '.', '', 5, 19, 1487243062, 1487243062, 19, 0),
(429, 228, '.', '', 6, 19, 1487243077, 1487243077, 19, 0),
(430, 228, '.', '', 7, 19, 1487243092, 1487243092, 19, 0),
(431, 228, '.', '', 8, 19, 1487243110, 1487243110, 19, 0),
(432, 229, '.', '', 1, 19, 1487243432, 1487243432, 19, 0),
(433, 229, '.', '', 2, 19, 1487243448, 1487243448, 19, 0),
(434, 229, '.', '', 3, 19, 1487243462, 1487243462, 19, 0),
(435, 229, '.', '', 4, 19, 1487243482, 1487243482, 19, 0),
(436, 232, '5%', '', 1, 19, 1487243687, 1487243687, 19, 0),
(437, 232, '20%', '', 2, 19, 1487243700, 1487243700, 19, 0),
(438, 232, '38%', '', 3, 19, 1487243710, 1487243710, 19, 0),
(439, 232, '60%', '', 4, 19, 1487243736, 1487243736, 19, 0),
(440, 233, '.', '', 1, 19, 1487243816, 1487243816, 19, 0),
(441, 233, '.', '', 2, 19, 1487243849, 1487243849, 19, 0),
(442, 236, '0,4', '', 1, 19, 1487244640, 1487244640, 19, 0),
(443, 236, '0,22', '', 2, 19, 1487244658, 1487244658, 19, 0),
(444, 236, '0,75', '', 3, 19, 1487244747, 1487244747, 19, 0),
(445, 236, '0,31', '', 4, 19, 1487244766, 1487244766, 19, 0),
(446, 236, '0,125', '', 5, 19, 1487244777, 1487244777, 19, 0),
(447, 236, '0,734', '', 6, 19, 1487244801, 1487244801, 19, 0),
(448, 237, '0,2', '', 1, 19, 1487244887, 1487244887, 19, 0),
(449, 237, '0,18', '', 2, 19, 1487244899, 1487244899, 19, 0),
(450, 237, '0,25', '', 3, 19, 1487244948, 1487244948, 19, 0),
(451, 237, '0,47', '', 4, 19, 1487244960, 1487244960, 19, 0),
(452, 237, '0,375', '', 5, 19, 1487244976, 1487244976, 19, 0),
(453, 237, '0,832', '', 6, 19, 1487244988, 1487244988, 19, 0),
(454, 238, 'яку частину кілограма складають: 40 г, 120 г, 750 г;', '', 1, 19, 1487245087, 1487245134, 19, 0),
(455, 238, 'яку частину години складають: 5 хв, 12 хв, 45 хв;', '', 2, 19, 1487245102, 1487245102, 19, 0),
(456, 238, 'яку частину розгорнутого кута становить кут, градусна міра якого: 18°, 45°, 120°?', '', 3, 19, 1487245121, 1487245156, 19, 0),
(457, 239, 'яку частину метра складають: 25 см, 12 см, 80 см;', '', 1, 19, 1487245260, 1487245260, 19, 0),
(458, 239, 'яку частину хвилини складають: 10 с, 42 с, 40 с;', '', 2, 19, 1487245284, 1487245284, 19, 0),
(459, 239, 'яку частину прямого кута складає кут, градусна міра якого: 5°, 12°, 27°?', '', 3, 19, 1487245296, 1487245308, 19, 0),
(460, 240, '.', '', 1, 19, 1487245714, 1487245714, 19, 0),
(461, 240, '.', '', 2, 19, 1487245736, 1487245736, 19, 0),
(462, 240, '.', '', 3, 19, 1487245995, 1487245995, 19, 0),
(463, 240, '.', '', 4, 19, 1487246018, 1487246018, 19, 0),
(464, 242, '.', '', 1, 19, 1487246201, 1487246201, 19, 0),
(465, 242, '.', '', 2, 19, 1487246229, 1487246229, 19, 0),
(466, 242, '.', '', 3, 19, 1487246243, 1487246243, 19, 0),
(467, 242, '.', '', 4, 19, 1487246262, 1487246262, 19, 0),
(468, 243, '.', '', 1, 19, 1487246489, 1487246534, 19, 1),
(469, 243, '.', '', 1, 19, 1487246565, 1487246565, 19, 0),
(470, 243, '.', '', 2, 19, 1487246612, 1487246612, 19, 0),
(471, 243, '.', '', 3, 19, 1487246679, 1487246679, 19, 0),
(472, 243, '.', '', 4, 19, 1487246711, 1487246711, 19, 0),
(473, 244, '.', '', 1, 19, 1487246848, 1487246848, 19, 0),
(474, 244, '.', '', 2, 19, 1487246901, 1487246924, 19, 1),
(475, 244, '.', '', 2, 19, 1487246955, 1487246955, 19, 0),
(476, 244, '.', '', 3, 19, 1487247485, 1487247485, 19, 0),
(477, 247, '.', '', 1, 19, 1487249240, 1487249240, 19, 0),
(478, 247, '.', '', 2, 19, 1487249276, 1487249276, 19, 0),
(479, 247, '.', '', 3, 19, 1487249306, 1487249306, 19, 0),
(480, 247, '.', '', 4, 19, 1487249332, 1487249332, 19, 0),
(481, 248, '.', '', 1, 19, 1487249456, 1487249456, 19, 0),
(482, 248, '.', '', 2, 19, 1487249482, 1487249482, 19, 0),
(483, 248, '.', '', 3, 19, 1487249518, 1487249518, 19, 0),
(484, 248, '.', '', 4, 19, 1487249539, 1487249539, 19, 0),
(485, 248, '.', '', 5, 19, 1487249560, 1487249560, 19, 0),
(486, 249, 'НСК (24; 36)', '', 1, 19, 1487249611, 1487249611, 19, 0),
(487, 249, 'НСК (80; 120)', '', 2, 19, 1487249635, 1487249635, 19, 0),
(488, 249, 'НСК (42; 91)', '', 3, 19, 1487249654, 1487249654, 19, 0),
(489, 253, '.', '', 1, 19, 1487257591, 1487257591, 19, 0),
(490, 253, '.', '', 2, 19, 1487257621, 1487257621, 19, 0),
(491, 253, '.', '', 3, 19, 1487257650, 1487257650, 19, 0),
(492, 253, '.', '', 4, 19, 1487257666, 1487257666, 19, 0),
(493, 254, '14', '', 1, 19, 1487257820, 1487257820, 19, 0),
(494, 254, '21', '', 2, 19, 1487257828, 1487257828, 19, 0),
(495, 254, '70', '', 3, 19, 1487257848, 1487257848, 19, 0),
(496, 254, '700', '', 4, 19, 1487257859, 1487257859, 19, 0),
(497, 255, '10', '', 1, 19, 1487257940, 1487257940, 19, 0),
(498, 255, '15', '', 2, 19, 1487257949, 1487257949, 19, 0),
(499, 255, '50', '', 3, 19, 1487257956, 1487257956, 19, 0),
(500, 255, '500', '', 4, 19, 1487257969, 1487257969, 19, 0),
(501, 256, '.', '', 1, 19, 1487258083, 1487258083, 19, 0),
(502, 256, '.', '', 2, 19, 1487258111, 1487258111, 19, 0),
(503, 256, '.', '', 3, 19, 1487258135, 1487258135, 19, 0),
(504, 256, '.', '', 1, 19, 1487258306, 1487258306, 19, 0),
(505, 257, '.', '', 1, 19, 1487258392, 1487258392, 19, 0),
(506, 257, '.', '', 2, 19, 1487258411, 1487258411, 19, 0),
(507, 257, '.', '', 3, 19, 1487258426, 1487258426, 19, 0),
(508, 257, '.', '', 4, 19, 1487258452, 1487258452, 19, 0),
(509, 258, '.', '', 1, 19, 1487258680, 1487258680, 19, 0),
(510, 258, '.', '', 2, 19, 1487258715, 1487258715, 19, 0),
(511, 258, '.', '', 3, 19, 1487258733, 1487258733, 19, 0),
(512, 258, '.', '', 4, 19, 1487258755, 1487258755, 19, 0),
(513, 258, '.', '', 5, 19, 1487258778, 1487258778, 19, 0),
(514, 258, '.', '', 6, 19, 1487258849, 1487258849, 19, 0),
(515, 258, '.', '', 7, 19, 1487258962, 1487258962, 19, 0),
(516, 258, '.', '', 8, 19, 1487259088, 1487259088, 19, 0),
(517, 259, '.', '', 1, 19, 1487261241, 1487261241, 19, 0),
(518, 259, '.', '', 2, 19, 1487261256, 1487261256, 19, 0),
(519, 259, '.', '', 3, 19, 1487261273, 1487261273, 19, 0),
(520, 259, '.', '', 4, 19, 1487261297, 1487261297, 19, 0),
(521, 259, '.', '', 5, 19, 1487261330, 1487261330, 19, 0),
(522, 259, '.', '', 6, 19, 1487261353, 1487261353, 19, 0),
(523, 259, '.', '', 7, 19, 1487261377, 1487261377, 19, 0),
(524, 259, '.', '', 8, 19, 1487261404, 1487261404, 19, 0),
(525, 260, '.', '', 1, 19, 1487261598, 1487261598, 19, 0),
(526, 260, '.', '', 2, 19, 1487261613, 1487261613, 19, 0),
(527, 260, '.', '', 3, 19, 1487261647, 1487261647, 19, 0),
(528, 260, '.', '', 4, 19, 1487261661, 1487261661, 19, 0),
(529, 261, '.', '', 1, 19, 1487261810, 1487261810, 19, 0),
(530, 261, '.', '', 2, 19, 1487261831, 1487261831, 19, 0),
(531, 261, '.', '', 3, 19, 1487261849, 1487261849, 19, 0),
(532, 261, '.', '', 4, 19, 1487261869, 1487261869, 19, 0),
(533, 262, '.', '', 1, 19, 1487261929, 1487261929, 19, 0),
(534, 262, '.', '', 2, 19, 1487261951, 1487261951, 19, 0),
(535, 263, '.', '', 1, 19, 1487262065, 1487262065, 19, 0),
(536, 263, '.', '', 2, 19, 1487262082, 1487262082, 19, 0),
(537, 263, '.', '', 3, 19, 1487262099, 1487262099, 19, 0),
(538, 263, '.', '', 4, 19, 1487262133, 1487262133, 19, 0),
(539, 264, '.', '', 1, 19, 1487262216, 1487262216, 19, 0),
(540, 264, '.', '', 2, 19, 1487262240, 1487262240, 19, 0),
(541, 264, '.', '', 3, 19, 1487262255, 1487262255, 19, 0),
(542, 265, '.', '', 1, 19, 1487262325, 1487262339, 19, 1),
(543, 265, '.', '', 1, 19, 1487262362, 1487262362, 19, 0),
(544, 265, '.', '', 2, 19, 1487262378, 1487262378, 19, 0),
(545, 265, '.', '', 3, 19, 1487262392, 1487262392, 19, 0),
(546, 272, '.', '', 1, 19, 1487263313, 1487263313, 19, 0),
(547, 272, '.', '', 2, 19, 1487263337, 1487263337, 19, 0),
(548, 273, '.', '', 1, 19, 1487263409, 1487263409, 19, 0),
(549, 273, '.', '', 2, 19, 1487263431, 1487263431, 19, 0),
(550, 275, '.', '', 1, 19, 1487599178, 1487599178, 19, 0),
(551, 275, '.', '', 2, 19, 1487599246, 1487599246, 19, 0),
(552, 276, '.', '', 1, 19, 1487599652, 1487599652, 19, 0),
(553, 276, '.', '', 2, 19, 1487599681, 1487599681, 19, 0),
(554, 277, '.', '', 1, 19, 1487599756, 1487599756, 19, 0),
(555, 277, '.', '', 2, 19, 1487599813, 1487599813, 19, 0),
(556, 279, '.', '', 1, 19, 1487600064, 1487600064, 19, 0),
(557, 279, '.', '', 2, 19, 1487600095, 1487600095, 19, 0),
(558, 279, '.', '', 3, 19, 1487600127, 1487600127, 19, 0),
(559, 279, '.', '', 4, 19, 1487600154, 1487600154, 19, 0),
(560, 279, '.', '', 5, 19, 1487600205, 1487600205, 19, 0),
(561, 279, '.', '', 6, 19, 1487600237, 1487600237, 19, 0),
(562, 283, '.', '', 1, 19, 1487603864, 1487603864, 19, 0),
(563, 283, '.', '', 2, 19, 1487603880, 1487603880, 19, 0),
(564, 283, '.', '', 3, 19, 1487603902, 1487603902, 19, 0),
(565, 283, '.', '', 4, 19, 1487603918, 1487603918, 19, 0),
(566, 283, '.', '', 5, 19, 1487603938, 1487603938, 19, 0),
(567, 283, '.', '', 6, 19, 1487603971, 1487603971, 19, 0),
(568, 283, '.', '', 7, 19, 1487603986, 1487603986, 19, 0),
(569, 283, '.', '', 8, 19, 1487604006, 1487604006, 19, 0),
(570, 284, '.', '', 1, 19, 1487604079, 1487604079, 19, 0),
(571, 284, '.', '', 2, 19, 1487604093, 1487604093, 19, 0),
(572, 284, '.', '', 3, 19, 1487604109, 1487604109, 19, 0),
(573, 284, '.', '', 4, 19, 1487604158, 1487604158, 19, 0),
(574, 284, '.', '', 5, 19, 1487604209, 1487604209, 19, 0),
(575, 284, '.', '', 6, 19, 1487604235, 1487604235, 19, 0),
(576, 284, '.', '', 7, 19, 1487604275, 1487604275, 19, 0),
(577, 284, '.', '', 8, 19, 1487604298, 1487604298, 19, 0),
(578, 285, '.', '', 1, 19, 1487604695, 1487604695, 19, 0),
(579, 285, '.', '', 2, 19, 1487604719, 1487604719, 19, 0),
(580, 285, '.', '', 3, 19, 1487604741, 1487604741, 19, 0),
(581, 285, '.', '', 4, 19, 1487604766, 1487604766, 19, 0),
(582, 285, '.', '', 5, 19, 1487604792, 1487604792, 19, 0),
(583, 285, '.', '', 6, 19, 1487604813, 1487604813, 19, 0),
(584, 285, '.', '', 6, 19, 1487604814, 1487604814, 19, 0),
(585, 285, '.', '', 7, 19, 1487604851, 1487604851, 19, 0),
(586, 285, '.', '', 8, 19, 1487604922, 1487604922, 19, 0),
(587, 288, '.', '', 1, 19, 1487605220, 1487605220, 19, 0),
(588, 288, '.', '', 2, 19, 1487605246, 1487605246, 19, 0),
(589, 288, '.', '', 3, 19, 1487605264, 1487605264, 19, 0),
(590, 288, '.', '', 4, 19, 1487605285, 1487605285, 19, 0),
(591, 288, '.', '', 5, 19, 1487605315, 1487605315, 19, 0),
(592, 288, '.', '', 6, 19, 1487605335, 1487605335, 19, 0),
(593, 288, '.', '', 7, 19, 1487605356, 1487605356, 19, 0),
(594, 288, '.', '', 8, 19, 1487605414, 1487605414, 19, 0),
(595, 289, '.', '', 1, 19, 1487605579, 1487605579, 19, 0),
(596, 289, '.', '', 2, 19, 1487605603, 1487605603, 19, 0),
(597, 289, '.', '', 3, 19, 1487605746, 1487605746, 19, 0),
(598, 289, '.', '', 4, 19, 1487605775, 1487605775, 19, 0),
(599, 289, '.', '', 5, 19, 1487605904, 1487605904, 19, 0),
(600, 289, '.', '', 6, 19, 1487605942, 1487605942, 19, 0),
(601, 289, '.', '', 7, 19, 1487606029, 1487606029, 19, 0),
(602, 289, '.', '', 8, 19, 1487606052, 1487606052, 19, 0),
(603, 292, '.', '', 1, 19, 1487606319, 1487606319, 19, 0),
(604, 292, '.', '', 2, 19, 1487606342, 1487606342, 19, 0),
(605, 292, '.', '', 3, 19, 1487606378, 1487606378, 19, 0),
(606, 293, '.', '', 1, 19, 1487606633, 1487606633, 19, 0),
(607, 293, '.', '', 2, 19, 1487606653, 1487606653, 19, 0),
(608, 293, '.', '', 3, 19, 1487606672, 1487606672, 19, 0),
(609, 295, '.', '', 1, 19, 1487606773, 1487606773, 19, 0),
(610, 295, '.', '', 2, 19, 1487606793, 1487606793, 19, 0),
(611, 295, '.', '', 3, 19, 1487606811, 1487606811, 19, 0),
(612, 296, '.', '', 1, 19, 1487606883, 1487606883, 19, 0),
(613, 296, '.', '', 1, 19, 1487606885, 1487606885, 19, 0),
(614, 296, '.', '', 2, 19, 1487606912, 1487606912, 19, 0),
(615, 297, '.', '', 1, 19, 1487606992, 1487606992, 19, 0),
(616, 297, '.', '', 2, 19, 1487607013, 1487607013, 19, 0),
(617, 298, '.', '', 1, 19, 1487607105, 1487607105, 19, 0),
(618, 298, '.', '', 2, 19, 1487607137, 1487607137, 19, 0),
(619, 301, '.', '', 1, 19, 1487607298, 1487607298, 19, 0),
(620, 301, '.', '', 2, 19, 1487607318, 1487607318, 19, 0),
(621, 302, '.', '', 1, 19, 1487607474, 1487607474, 19, 0),
(622, 302, '.', '', 2, 19, 1487607496, 1487607496, 19, 0),
(623, 302, '.', '', 3, 19, 1487607515, 1487607515, 19, 0),
(624, 302, '.', '', 4, 19, 1487607551, 1487607551, 19, 0),
(625, 303, '.', '', 1, 19, 1487607627, 1487607627, 19, 0),
(626, 303, '.', '', 2, 19, 1487607855, 1487607855, 19, 0),
(627, 303, '.', '', 3, 19, 1487607881, 1487607881, 19, 0),
(628, 303, '.', '', 4, 19, 1487607908, 1487607908, 19, 0),
(629, 305, 'Яку частину банки варення з’їдає кожен з них за хвилину?', '', 1, 19, 1487608038, 1487608038, 19, 0),
(630, 305, 'Яку частину банки варення вони з’їдять разом за: 1 хв; 2 хв; 3 хв?', '', 2, 19, 1487608063, 1487608063, 19, 0),
(631, 306, 'Чи наповниться більше чверті басейну за дві хвилини одночасної роботи обох труб?', '', 1, 19, 1487608155, 1487608155, 19, 0),
(632, 306, 'Яка частина басейну залишиться незаповненою через дві хвилини одночасної роботи двох труб?', '', 2, 19, 1487608172, 1487608172, 19, 0),
(633, 307, 'Яку частину всього шляху становила відстань між потягами через годину піс¬ля їх виходу?', '', 1, 19, 1487608273, 1487608273, 19, 0),
(634, 307, 'Чи відбулася зустріч потягів, якщо вони були у дорозі 2 год?', '', 2, 19, 1487608326, 1487608326, 19, 0),
(635, 308, '.', '', 1, 19, 1487608394, 1487608394, 19, 0),
(636, 308, '.', '', 2, 19, 1487608419, 1487608419, 19, 0),
(637, 312, 'Розмісти у рядках вказані прізвища відомих математиків: Піфагор, Ньютон, Остроградський, Коші, Кравчук, Вороний, Смогоржевський та прочитай у виділеному стовпчику назву геометричної фігури.', '', 1, 19, 1487608685, 1487608685, 19, 0),
(638, 312, 'Здогадайся, у яких рядках записано прізвища українських математиків. Перевір свою здогадку за допомогою Інтернету або книжок. Ознайомся з біографіями цих математиків.', '', 2, 19, 1487608734, 1487608734, 19, 0),
(639, 313, '.', '', 1, 19, 1487671320, 1487671320, 19, 0),
(640, 313, '.', '', 2, 19, 1487671343, 1487671343, 19, 0),
(641, 313, '.', '', 3, 19, 1487671368, 1487671368, 19, 0),
(642, 313, '.', '', 4, 19, 1487671388, 1487671388, 19, 0),
(643, 313, '.', '', 5, 19, 1487671418, 1487671418, 19, 0),
(644, 313, '.', '', 6, 19, 1487671467, 1487671467, 19, 0),
(645, 313, '.', '', 7, 19, 1487671493, 1487671493, 19, 0),
(646, 313, '.', '', 8, 19, 1487671515, 1487671515, 19, 0),
(647, 314, '.', '', 1, 19, 1487671580, 1487671580, 19, 0),
(648, 314, '.', '', 2, 19, 1487671596, 1487671596, 19, 0),
(649, 314, '.', '', 3, 19, 1487671618, 1487671618, 19, 0),
(650, 314, '.', '', 4, 19, 1487671635, 1487671635, 19, 0),
(651, 315, '.', '', 1, 19, 1487671691, 1487671691, 19, 0),
(652, 315, '.', '', 2, 19, 1487671717, 1487671717, 19, 0),
(653, 315, '.', '', 3, 19, 1487671748, 1487671748, 19, 0),
(654, 315, '.', '', 4, 19, 1487671763, 1487671763, 19, 0),
(655, 316, '.', '', 1, 19, 1487671820, 1487671820, 19, 0),
(656, 316, '.', '', 2, 19, 1487671842, 1487671842, 19, 0),
(657, 316, '.', '', 3, 19, 1487671866, 1487671866, 19, 0),
(658, 316, '.', '', 4, 19, 1487671884, 1487671884, 19, 0),
(659, 316, '.', '', 5, 19, 1487671903, 1487671903, 19, 0),
(660, 316, '.', '', 6, 19, 1487671923, 1487671923, 19, 0),
(661, 316, '.', '', 7, 19, 1487671948, 1487671948, 19, 0),
(662, 316, '.', '', 8, 19, 1487671984, 1487671984, 19, 0),
(663, 317, '.', '', 1, 19, 1487672057, 1487672057, 19, 0),
(664, 317, '.', '', 2, 19, 1487672071, 1487672071, 19, 0),
(665, 317, '.', '', 3, 19, 1487672167, 1487672167, 19, 0),
(666, 317, '.', '', 4, 19, 1487672210, 1487672210, 19, 0),
(667, 317, '.', '', 5, 19, 1487672232, 1487672232, 19, 0),
(668, 317, '.', '', 6, 19, 1487672250, 1487672250, 19, 0),
(669, 317, '.', '', 7, 19, 1487672265, 1487672265, 19, 0),
(670, 317, '.', '', 8, 19, 1487672280, 1487672280, 19, 0),
(671, 321, '.', '', 1, 19, 1487672550, 1487672550, 19, 0),
(672, 321, '.', '', 2, 19, 1487672572, 1487672572, 19, 0),
(673, 321, '.', '', 3, 19, 1487672590, 1487672590, 19, 0),
(674, 321, '.', '', 4, 19, 1487672614, 1487672614, 19, 0),
(675, 323, '.', '', 1, 19, 1487675088, 1487675088, 19, 0),
(676, 323, '.', '', 2, 19, 1487675109, 1487675109, 19, 0),
(677, 323, '.', '', 3, 19, 1487675151, 1487675151, 19, 0),
(678, 323, '.', '', 4, 19, 1487675173, 1487675173, 19, 0),
(679, 326, '.', '', 1, 19, 1487675421, 1487675421, 19, 0),
(680, 326, '.', '', 2, 19, 1487675441, 1487675441, 19, 0),
(681, 326, '.', '', 2, 19, 1487675442, 1487675532, 19, 1),
(682, 326, '.', '', 3, 19, 1487675465, 1487675465, 19, 0),
(683, 326, '.', '', 4, 19, 1487675479, 1487675479, 19, 0),
(684, 326, '.', '', 5, 19, 1487675494, 1487675494, 19, 0),
(685, 326, '.', '', 6, 19, 1487675560, 1487675560, 19, 0),
(686, 326, '.', '', 7, 19, 1487675574, 1487675574, 19, 0),
(687, 326, '.', '', 8, 19, 1487675595, 1487675595, 19, 0),
(688, 327, '.', '', 1, 19, 1487675723, 1487675723, 19, 0),
(689, 327, '.', '', 2, 19, 1487675740, 1487675740, 19, 0),
(690, 327, '.', '', 3, 19, 1487675773, 1487675773, 19, 0),
(691, 327, '.', '', 4, 19, 1487675796, 1487675796, 19, 0),
(692, 327, '.', '', 5, 19, 1487675816, 1487675816, 19, 0),
(693, 327, '.', '', 6, 19, 1487675834, 1487675834, 19, 0),
(694, 327, '.', '', 7, 19, 1487675860, 1487675860, 19, 0),
(695, 327, '.', '', 8, 19, 1487675876, 1487675876, 19, 0),
(696, 329, '.', '', 1, 19, 1487676026, 1487676026, 19, 0),
(697, 329, '.', '', 2, 19, 1487676059, 1487676059, 19, 0),
(698, 330, '.', '', 1, 19, 1487676259, 1487676259, 19, 0),
(699, 330, '.', '', 2, 19, 1487676281, 1487676281, 19, 0),
(700, 336, '.', '', 1, 19, 1487677117, 1487677117, 19, 0),
(701, 336, '.', '', 2, 19, 1487677144, 1487677144, 19, 0),
(702, 337, '.', '', 1, 19, 1487677273, 1487677273, 19, 0),
(703, 337, '.', '', 2, 19, 1487677289, 1487677289, 19, 0),
(704, 337, '.', '', 3, 19, 1487677306, 1487677306, 19, 0),
(705, 337, '.', '', 4, 19, 1487677330, 1487677330, 19, 0),
(706, 337, '.', '', 5, 19, 1487677351, 1487677351, 19, 0),
(707, 337, '.', '', 6, 19, 1487677378, 1487677378, 19, 0),
(708, 338, '.', '', 2, 19, 1487677467, 1487677510, 19, 1),
(709, 338, '.', '', 1, 19, 1487677502, 1487677502, 19, 0),
(710, 338, '.', '', 2, 19, 1487677529, 1487677529, 19, 0),
(711, 338, '.', '', 3, 19, 1487677545, 1487677545, 19, 0),
(712, 338, '.', '', 4, 19, 1487677655, 1487677655, 19, 0),
(713, 338, '.', '', 5, 19, 1487677674, 1487677674, 19, 0),
(714, 338, '.', '', 6, 19, 1487677716, 1487677716, 19, 0),
(715, 339, '.', '', 1, 19, 1487677884, 1487677884, 19, 0),
(716, 339, '.', '', 2, 19, 1487677929, 1487677929, 19, 0),
(717, 340, '.', '', 1, 19, 1487678003, 1487678003, 19, 0),
(718, 340, '.', '', 2, 19, 1487678026, 1487678026, 19, 0),
(719, 341, '.', '', 1, 19, 1487678113, 1487678113, 19, 0),
(720, 341, '.', '', 2, 19, 1487678146, 1487678146, 19, 0),
(721, 342, '.', '', 1, 19, 1487678307, 1487678307, 19, 0),
(722, 342, '.', '', 2, 19, 1487678324, 1487678324, 19, 0),
(723, 347, '.', '', 1, 19, 1487678933, 1487678933, 19, 0),
(724, 347, '.', '', 2, 19, 1487678962, 1487678962, 19, 0),
(725, 348, '.', '', 1, 19, 1487679249, 1487679249, 19, 0),
(726, 348, '.', '', 2, 19, 1487679272, 1487679272, 19, 0),
(727, 351, 'У ящику, що знаходиться у темній кімнаті, лежать 12 білих і 12 чорних шкарпеток. Яку найменшу кількість шкарпеток треба взяти навмання, щоб серед них обов’язково була пара шкарпеток одного кольору?', '', 1, 19, 1487679463, 1487679463, 19, 0),
(728, 351, 'У коробці, що знаходиться у темній кімнаті, лежать 12 пар коричневих і 12 пар чорних рукавичок. Яку найменшу кількість рукавичок треба взяти навмання, щоб cеред них обов’язково була пара рукавичок одного кольору?', '', 2, 19, 1487679489, 1487679489, 19, 0),
(729, 353, '.', '', 1, 19, 1487690813, 1487690813, 19, 0),
(730, 353, '.', '', 2, 19, 1487690961, 1487690961, 19, 0),
(731, 353, '.', '', 3, 19, 1487690978, 1487690978, 19, 0),
(732, 353, '.', '', 4, 19, 1487691228, 1487691228, 19, 0),
(733, 354, '.', '', 1, 19, 1487692001, 1487692001, 19, 0),
(734, 354, '.', '', 2, 19, 1487692025, 1487692025, 19, 0),
(735, 354, '.', '', 3, 19, 1487692046, 1487692046, 19, 0),
(736, 354, '.', '', 4, 19, 1487692065, 1487692065, 19, 0),
(737, 355, '.', '', 1, 19, 1487692149, 1487692149, 19, 0),
(738, 355, '.', '', 2, 19, 1487692167, 1487692167, 19, 0),
(739, 355, '.', '', 3, 19, 1487692192, 1487692192, 19, 0),
(740, 355, '.', '', 4, 19, 1487692211, 1487692211, 19, 0),
(741, 355, '.', '', 5, 19, 1487692238, 1487692238, 19, 0),
(742, 355, '.', '', 6, 19, 1487692260, 1487692260, 19, 0),
(743, 356, '.', '', 1, 19, 1487692525, 1487692525, 19, 0),
(744, 356, '.', '', 2, 19, 1487692543, 1487692543, 19, 0),
(745, 356, '.', '', 3, 19, 1487692605, 1487692605, 19, 0),
(746, 356, '.', '', 4, 19, 1487692625, 1487692625, 19, 0),
(747, 357, '.', '', 1, 19, 1487692684, 1487692684, 19, 0),
(748, 357, '.', '', 2, 19, 1487692700, 1487692700, 19, 0),
(749, 357, '.', '', 3, 19, 1487692713, 1487692713, 19, 0),
(750, 357, '.', '', 4, 19, 1487692739, 1487692739, 19, 0),
(751, 358, '.', '', 1, 19, 1487694690, 1487694690, 19, 0),
(752, 358, '.', '', 2, 19, 1487694706, 1487694706, 19, 0),
(753, 358, '.', '', 3, 19, 1487694726, 1487694726, 19, 0),
(754, 358, '.', '', 4, 19, 1487694744, 1487694744, 19, 0),
(755, 359, '5 :	99', '', 1, 19, 1487694872, 1487694872, 19, 0),
(756, 359, '19 : 11', '', 2, 19, 1487694887, 1487694887, 19, 0),
(757, 359, '43 : 12', '', 3, 19, 1487694903, 1487694903, 19, 0),
(758, 359, '12,5	: 27', '', 4, 19, 1487694938, 1487694938, 19, 0),
(759, 359, '12,5	: 27', '', 4, 19, 1487694939, 1487694939, 19, 0),
(760, 360, '10 : 9', '', 1, 19, 1487695055, 1487695055, 19, 0),
(761, 360, '7 : 15', '', 2, 19, 1487695068, 1487695068, 19, 0),
(762, 360, '44 : 6', '', 3, 19, 1487695095, 1487695095, 19, 0),
(763, 360, '25,4	: 11', '', 4, 19, 1487695110, 1487695110, 19, 0),
(764, 361, '.', '', 1, 19, 1487695387, 1487695387, 19, 0),
(765, 361, '.', '', 2, 19, 1487695407, 1487695407, 19, 0),
(766, 361, '.', '', 3, 19, 1487695423, 1487695423, 19, 0),
(767, 361, '.', '', 4, 19, 1487695473, 1487695473, 19, 0),
(768, 362, '.', '', 1, 19, 1487764711, 1487764711, 19, 0),
(769, 362, '.', '', 2, 19, 1487764738, 1487764738, 19, 0);
INSERT INTO `subtask` (`id`, `task_id`, `text`, `result`, `number`, `created_by`, `created_at`, `updated_at`, `updated_by`, `deleted`) VALUES
(770, 362, '.', '', 3, 19, 1487764845, 1487764845, 19, 0),
(771, 362, '.', '', 4, 19, 1487764864, 1487764864, 19, 0),
(772, 364, 'до одиниць: 2,73; 3,052; 7,5789;', '', 1, 19, 1487765563, 1487765563, 19, 0),
(773, 364, 'до десятих: 11,82; 0,4859; 11,2342;', '', 2, 19, 1487765581, 1487765581, 19, 0),
(774, 364, 'до сотих: 0,451; 12,499; 1,574.', '', 3, 19, 1487765595, 1487765595, 19, 0),
(775, 365, 'Чи є взаємно простими числа 2012 і 2015?', '', 1, 19, 1487765755, 1487765755, 19, 0),
(776, 365, 'Назви три числа, кожне з яких є взаємно простим з числом 2012 і з числом 2015.', '', 2, 19, 1487765778, 1487765778, 19, 0),
(777, 367, 'десятих;', '', 1, 19, 1487844145, 1487844145, 19, 0),
(778, 367, 'сотих;', '', 2, 19, 1487844156, 1487844156, 19, 0),
(779, 367, 'тисячних', '', 3, 19, 1487844167, 1487844167, 19, 0),
(780, 368, 'десятих', '', 1, 19, 1487844239, 1487844239, 19, 0),
(781, 368, 'сотих', '', 2, 19, 1487844275, 1487844275, 19, 0),
(782, 368, 'тисячних', '', 3, 19, 1487844283, 1487844283, 19, 0),
(783, 369, '.', '', 1, 19, 1487844921, 1487844921, 19, 0),
(784, 369, '.', '', 2, 19, 1487844948, 1487844948, 19, 0),
(785, 369, '.', '', 3, 19, 1487845084, 1487845084, 19, 0),
(786, 369, '.', '', 4, 19, 1487845168, 1487845168, 19, 0),
(787, 370, '.', '', 1, 19, 1487845333, 1487845333, 19, 0),
(788, 370, '.', '', 2, 19, 1487845352, 1487845352, 19, 0),
(789, 370, '.', '', 3, 19, 1487845372, 1487845372, 19, 0),
(790, 370, '.', '', 4, 19, 1487845405, 1487845405, 19, 0),
(791, 373, '13х = 7', '', 1, 19, 1487845598, 1487845598, 19, 0),
(792, 373, '8 : х = 125', '', 2, 19, 1487845612, 1487845612, 19, 0),
(793, 373, '56х = 103', '', 3, 19, 1487845626, 1487845626, 19, 0),
(794, 373, '17 : х = 6', '', 4, 19, 1487845639, 1487845639, 19, 0),
(795, 374, '19х = 25', '', 1, 19, 1487845703, 1487845703, 19, 0),
(796, 374, '7 : х = 57', '', 2, 19, 1487845713, 1487845713, 19, 0),
(797, 378, '2,(76) + 4,(5) −3,(1)', '', 1, 19, 1487846014, 1487846014, 19, 0),
(798, 378, '4,0(3) − 2,(7) − 0,(24)', '', 2, 19, 1487846027, 1487846027, 19, 0),
(799, 379, '.', '', 1, 19, 1487846185, 1487846185, 19, 0),
(800, 379, '.', '', 2, 19, 1487846216, 1487846216, 19, 0),
(801, 379, '.', '', 3, 19, 1487846238, 1487846238, 19, 0),
(802, 379, '.', '', 4, 19, 1487846266, 1487846266, 19, 0),
(803, 380, '.', '', 1, 19, 1487846353, 1487846353, 19, 0),
(804, 380, '.', '', 2, 19, 1487846373, 1487846373, 19, 0),
(805, 380, '.', '', 3, 19, 1487846404, 1487846404, 19, 0),
(806, 380, '.', '', 4, 19, 1487846435, 1487846435, 19, 0),
(807, 382, '.', '', 1, 19, 1487846682, 1487846682, 19, 0),
(808, 382, '.', '', 2, 19, 1487846702, 1487846702, 19, 0),
(809, 382, '.', '', 3, 19, 1487846719, 1487846719, 19, 0),
(810, 382, '.', '', 4, 19, 1487846741, 1487846741, 19, 0),
(811, 383, '.', '', 1, 19, 1487847789, 1487847789, 19, 0),
(812, 383, '.', '', 2, 19, 1487847865, 1487847865, 19, 0),
(813, 383, '.', '', 3, 19, 1487847894, 1487847894, 19, 0),
(814, 383, '.', '', 4, 19, 1487847908, 1487847908, 19, 0),
(815, 383, '.', '', 4, 19, 1487848512, 1487848594, 19, 1),
(816, 383, '.', '', 5, 19, 1487848545, 1487848598, 19, 1),
(817, 383, '.', '', 2, 19, 1487848573, 1487848602, 19, 1),
(818, 383, '.', '', 5, 19, 1487848663, 1487848663, 19, 0),
(819, 383, '.', '', 6, 19, 1487848689, 1487848689, 19, 0),
(820, 383, '.', '', 7, 19, 1487848703, 1487848703, 19, 0),
(821, 383, '.', '', 8, 19, 1487848744, 1487848744, 19, 0),
(822, 383, '.', '', 9, 19, 1487848761, 1487848761, 19, 0),
(823, 383, '.', '', 9, 19, 1487848763, 1487848763, 19, 0),
(824, 383, '.', '', 10, 19, 1487848778, 1487848778, 19, 0),
(825, 383, '.', '', 11, 19, 1487848797, 1487848797, 19, 0),
(826, 383, '.', '', 12, 19, 1487848817, 1487848817, 19, 0),
(827, 384, '.', '', 1, 19, 1487848991, 1487848991, 19, 0),
(828, 384, '.', '', 2, 19, 1487849010, 1487849010, 19, 0),
(829, 384, '.', '', 3, 19, 1487849042, 1487849042, 19, 0),
(830, 384, '.', '', 4, 19, 1487849073, 1487849073, 19, 0),
(831, 384, '.', '', 5, 19, 1487849093, 1487849093, 19, 0),
(832, 384, '.', '', 6, 19, 1487849108, 1487849108, 19, 0),
(833, 384, '.', '', 7, 19, 1487849123, 1487849123, 19, 0),
(834, 384, '.', '', 8, 19, 1487849143, 1487849143, 19, 0),
(835, 384, '.', '', 9, 19, 1487849173, 1487849173, 19, 0),
(836, 384, '.', '', 10, 19, 1487849205, 1487849205, 19, 0),
(837, 384, '.', '', 11, 19, 1487849234, 1487849234, 19, 0),
(838, 384, '.', '', 12, 19, 1487849264, 1487849264, 19, 0),
(839, 386, '.', '', 1, 19, 1487849419, 1487849419, 19, 0),
(840, 386, '.', '', 2, 19, 1487849434, 1487849434, 19, 0),
(841, 386, '.', '', 3, 19, 1487849449, 1487849449, 19, 0),
(842, 386, '.', '', 4, 19, 1487849468, 1487849468, 19, 0),
(843, 387, '.', '', 1, 19, 1487849566, 1487849566, 19, 0),
(844, 387, '.', '', 2, 19, 1487849599, 1487849599, 19, 0),
(845, 387, '.', '', 3, 19, 1487849620, 1487849620, 19, 0),
(846, 387, '.', '', 4, 19, 1487849637, 1487849637, 19, 0),
(847, 388, '.', '', 1, 19, 1487849706, 1487849706, 19, 0),
(848, 388, '.', '', 2, 19, 1487849768, 1487849768, 19, 0),
(849, 388, '.', '', 3, 19, 1487849802, 1487849817, 19, 1),
(850, 388, '.', '', 3, 19, 1487849844, 1487849844, 19, 0),
(851, 388, '.', '', 4, 19, 1487849893, 1487849893, 19, 0),
(852, 389, '.', '', 1, 19, 1487849969, 1487849969, 19, 0),
(853, 389, '.', '', 2, 19, 1487849993, 1487849993, 19, 0),
(854, 389, '.', '', 3, 19, 1487850028, 1487850028, 19, 0),
(855, 389, '.', '', 4, 19, 1487850063, 1487850063, 19, 0),
(856, 389, '.', '', 5, 19, 1487850084, 1487850084, 19, 0),
(857, 389, '.', '', 6, 19, 1487850105, 1487850105, 19, 0),
(858, 389, '.', '', 7, 19, 1487850160, 1487850160, 19, 0),
(859, 389, '.', '', 8, 19, 1487850178, 1487850178, 19, 0),
(860, 390, '.', '', 1, 19, 1487850295, 1487850295, 19, 0),
(861, 390, '.', '', 2, 19, 1487850311, 1487850311, 19, 0),
(862, 390, '.', '', 3, 19, 1487850330, 1487850330, 19, 0),
(863, 390, '.', '', 4, 19, 1487850346, 1487850346, 19, 0),
(864, 393, '.', '', 1, 19, 1487850552, 1487850552, 19, 0),
(865, 393, '.', '', 2, 19, 1487850652, 1487850652, 19, 0),
(866, 393, '.', '', 3, 19, 1487850671, 1487850671, 19, 0),
(867, 393, '.', '', 4, 19, 1487850691, 1487850691, 19, 0),
(868, 393, '.', '', 5, 19, 1487850707, 1487850707, 19, 0),
(869, 393, '.', '', 6, 19, 1487850726, 1487850726, 19, 0),
(870, 393, '.', '', 7, 19, 1487850746, 1487850746, 19, 0),
(871, 393, '.', '', 8, 19, 1487850763, 1487850763, 19, 0),
(872, 394, '.', '', 1, 19, 1487850933, 1487850933, 19, 0),
(873, 394, '.', '', 2, 19, 1487850952, 1487850952, 19, 0),
(874, 394, '.', '', 3, 19, 1487850973, 1487850973, 19, 0),
(875, 394, '.', '', 4, 19, 1487851009, 1487851009, 19, 0),
(876, 394, '.', '', 5, 19, 1487851040, 1487851040, 19, 0),
(877, 394, '.', '', 6, 19, 1487851067, 1487851067, 19, 0),
(878, 394, '.', '', 7, 19, 1487851088, 1487851088, 19, 0),
(879, 394, '.', '', 8, 19, 1487851117, 1487851117, 19, 0),
(880, 395, '.', '', 1, 19, 1487851293, 1487851293, 19, 0),
(881, 395, '.', '', 2, 19, 1487851455, 1487851455, 19, 0),
(882, 395, '.', '', 3, 19, 1487851482, 1487851482, 19, 0),
(883, 396, '.', '', 1, 19, 1487851591, 1487851591, 19, 0),
(884, 396, '.', '', 2, 19, 1487851609, 1487851609, 19, 0),
(885, 397, '.', '', 1, 19, 1487851704, 1487851704, 19, 0),
(886, 397, '.', '', 2, 19, 1487851723, 1487851723, 19, 0),
(887, 397, '.', '', 3, 19, 1487851765, 1487851765, 19, 0),
(888, 397, '.', '', 4, 19, 1487851794, 1487851794, 19, 0),
(889, 398, '.', '', 1, 19, 1487851898, 1487851898, 19, 0),
(890, 398, '.', '', 2, 19, 1487851923, 1487851923, 19, 0),
(891, 398, '.', '', 3, 19, 1487851942, 1487851942, 19, 0),
(892, 398, '.', '', 4, 19, 1487851969, 1487851969, 19, 0),
(893, 398, '.', '', 4, 19, 1487851972, 1487851972, 19, 0),
(894, 398, '.', '', 4, 19, 1487851972, 1487851972, 19, 0),
(895, 399, '.', '', 1, 19, 1487852105, 1487852105, 19, 0),
(896, 399, '.', '', 2, 19, 1487852123, 1487852123, 19, 0),
(897, 399, '.', '', 3, 19, 1487852180, 1487852180, 19, 0),
(898, 399, '.', '', 4, 19, 1487852207, 1487852207, 19, 0),
(899, 400, '.', '', 1, 19, 1487852338, 1487852338, 19, 0),
(900, 400, '.', '', 2, 19, 1487852368, 1487852368, 19, 0),
(901, 400, '.', '', 4, 19, 1487852393, 1487852402, 19, 1),
(902, 400, '.', '', 3, 19, 1487852439, 1487852439, 19, 0),
(903, 400, '.', '', 4, 19, 1487852464, 1487852464, 19, 0),
(904, 403, '.', '', 1, 19, 1487852742, 1487852796, 19, 0),
(905, 403, '.', '', 2, 19, 1487854215, 1487854215, 19, 0),
(906, 403, '.', '', 3, 19, 1487854255, 1487854255, 19, 0),
(907, 404, '.', '', 1, 19, 1487854745, 1487854745, 19, 0),
(908, 404, '.', '', 2, 19, 1487854770, 1487854770, 19, 0),
(909, 404, '.', '', 3, 19, 1487854788, 1487854788, 19, 0),
(910, 407, '.', '', 1, 19, 1487855306, 1487855306, 19, 0),
(911, 407, '.', '', 2, 19, 1487855457, 1487855457, 19, 0),
(912, 407, '.', '', 3, 19, 1487855479, 1487855479, 19, 0),
(913, 407, '.', '', 4, 19, 1487855502, 1487855502, 19, 0),
(914, 408, '.', '', 1, 19, 1487856395, 1487856395, 19, 0),
(915, 408, '.', '', 2, 19, 1487856439, 1487856439, 19, 0),
(916, 408, '.', '', 3, 19, 1487856480, 1487856480, 19, 0),
(917, 408, '.', '', 4, 19, 1487856505, 1487856505, 19, 0),
(918, 409, '.', '', 1, 19, 1487857136, 1487857136, 19, 0),
(919, 409, '.', '', 2, 19, 1487857173, 1487857173, 19, 0),
(920, 409, '.', '', 3, 19, 1487857203, 1487857203, 19, 0),
(921, 410, '.', '', 1, 19, 1487857361, 1487857361, 19, 0),
(922, 410, '.', '', 2, 19, 1487857388, 1487857388, 19, 0),
(923, 411, '.', '', 1, 19, 1487857494, 1487857494, 19, 0),
(924, 411, '.', '', 2, 19, 1487857618, 1487857618, 19, 0),
(925, 411, '.', '', 3, 19, 1487857658, 1487857658, 19, 0),
(926, 417, '.', '', 1, 19, 1487858266, 1487858266, 19, 0),
(927, 417, '.', '', 2, 19, 1487858287, 1487858287, 19, 0),
(928, 417, '.', '', 3, 19, 1487858322, 1487858322, 19, 0),
(929, 418, '5 %', '', 1, 19, 1487858441, 1487858441, 19, 0),
(930, 418, '7 %', '', 2, 19, 1487858452, 1487858452, 19, 0),
(931, 418, '14 %', '', 3, 19, 1487858470, 1487858470, 19, 0),
(932, 418, '20 %', '', 4, 19, 1487858486, 1487858486, 19, 0),
(933, 418, '52 %', '', 5, 19, 1487858507, 1487858507, 19, 0),
(934, 418, '100 %', '', 6, 19, 1487858526, 1487858526, 19, 0),
(935, 418, '120 %', '', 7, 19, 1487858559, 1487858559, 19, 0),
(936, 421, '.', '', 1, 19, 1487862736, 1487862736, 19, 0),
(937, 421, '.', '', 2, 19, 1487862761, 1487862761, 19, 0),
(938, 421, '.', '', 3, 19, 1487862781, 1487862781, 19, 0),
(939, 421, '.', '', 4, 19, 1487862824, 1487862853, 19, 1),
(940, 421, '.', '', 4, 19, 1487862889, 1487862889, 19, 0),
(941, 422, '1/6 від 24;', '', 1, 19, 1487863068, 1487863068, 19, 0),
(942, 422, '3/4  від 12;', '', 2, 19, 1487863134, 1487863134, 19, 0),
(943, 422, '0,7 від 40', '', 3, 19, 1487863154, 1487863154, 19, 0),
(944, 422, '0,32 від 10', '', 4, 19, 1487863164, 1487863164, 19, 0),
(945, 422, '10%	 від	27', '', 5, 19, 1487863181, 1487863181, 19, 0),
(946, 422, '20% від 30', '', 6, 19, 1487863276, 1487863276, 19, 0),
(947, 423, '1/10 від 180', '', 1, 19, 1487863882, 1487863882, 19, 0),
(948, 423, '2/3 від  15', '', 2, 19, 1487863899, 1487863899, 19, 0),
(949, 423, '0,5 від 10', '', 3, 19, 1487863929, 1487863929, 19, 0),
(950, 423, '0,47 від 100', '', 4, 19, 1487863985, 1487863985, 19, 0),
(951, 423, '50 %  від	2', '', 5, 19, 1487863997, 1487863997, 19, 0),
(952, 423, '30% від 40', '', 6, 19, 1487864012, 1487864012, 19, 0),
(953, 429, '.', '', 1, 19, 1487864513, 1487864513, 19, 0),
(954, 429, '.', '', 2, 19, 1487864543, 1487864543, 19, 0),
(955, 429, '.', '', 3, 19, 1487864566, 1487864566, 19, 0),
(956, 429, '.', '', 4, 19, 1487864591, 1487864591, 19, 0),
(957, 430, '1/5', '', 1, 19, 1487864822, 1487864822, 19, 0),
(958, 430, '5/8', '', 2, 19, 1487864837, 1487864837, 19, 0),
(959, 430, '4/7', '', 3, 19, 1487864881, 1487864881, 19, 0),
(960, 430, '0,215', '', 4, 19, 1487864980, 1487864980, 19, 0),
(961, 430, '2,7', '', 5, 19, 1487864992, 1487864992, 19, 0),
(962, 430, '4,19', '', 6, 19, 1487865004, 1487865004, 19, 0),
(963, 431, '.', '', 1, 19, 1487865137, 1487865137, 19, 0),
(964, 431, '.', '', 2, 19, 1487865225, 1487865225, 19, 0),
(965, 435, '.', '', 1, 19, 1487865557, 1487865557, 19, 0),
(966, 435, '.', '', 2, 19, 1487865577, 1487865577, 19, 0),
(967, 435, '.', '', 3, 19, 1487865606, 1487865606, 19, 0),
(968, 436, '.', '', 1, 19, 1487865654, 1487865654, 19, 0),
(969, 436, '.', '', 2, 19, 1487865706, 1487865706, 19, 0),
(970, 436, '.', '', 3, 19, 1487865760, 1487865760, 19, 0),
(971, 447, '.', '', 1, 19, 1487866276, 1487866276, 19, 0),
(972, 447, '.', '', 2, 19, 1487866294, 1487866294, 19, 0),
(973, 448, '3х = 1', '', 1, 19, 1487866410, 1487866410, 19, 0),
(974, 448, 'х • 13 = 1', '', 2, 19, 1487866421, 1487866421, 19, 0),
(975, 448, '0,2х = 1', '', 3, 19, 1487866447, 1487866447, 19, 0),
(976, 451, 'Яке число є оберненим до одиниці?', '', 1, 19, 1487866772, 1487866772, 19, 0),
(977, 451, 'Чи існує число, обернене числу нуль?', '', 2, 19, 1487866789, 1487866789, 19, 0),
(978, 452, '.', '', 1, 19, 1487866859, 1487866859, 19, 0),
(979, 452, '.', '', 2, 19, 1487866882, 1487866882, 19, 0),
(980, 452, '.', '', 3, 19, 1487867154, 1487867154, 19, 0),
(981, 452, '.', '', 4, 19, 1487867189, 1487867189, 19, 0),
(982, 452, '.', '', 5, 19, 1487867213, 1487867213, 19, 0),
(983, 452, '.', '', 6, 19, 1487867240, 1487867240, 19, 0),
(984, 453, '0,1', '', 1, 19, 1487867284, 1487867284, 19, 0),
(985, 453, '0,13', '', 2, 19, 1487867293, 1487867293, 19, 0),
(986, 453, '0,02', '', 3, 19, 1487867308, 1487867308, 19, 0),
(987, 453, '0,25', '', 4, 19, 1487867320, 1487867320, 19, 0),
(988, 453, '0,36', '', 5, 19, 1487867330, 1487867330, 19, 0),
(989, 453, '0,45', '', 6, 19, 1487867343, 1487867343, 19, 0),
(990, 454, '.', '', 1, 19, 1487867416, 1487867416, 19, 0),
(991, 454, '.', '', 2, 19, 1487867442, 1487867442, 19, 0),
(992, 454, '.', '', 3, 19, 1487867466, 1487867466, 19, 0),
(993, 454, '.', '', 4, 19, 1487867483, 1487867483, 19, 0),
(994, 454, '.', '', 5, 19, 1487867500, 1487867500, 19, 0),
(995, 454, '.', '', 6, 19, 1487867513, 1487867513, 19, 0),
(996, 455, '.', '', 1, 19, 1487940958, 1487940958, 19, 0),
(997, 455, '.', '', 2, 19, 1487940981, 1487940981, 19, 0),
(998, 455, '.', '', 3, 19, 1487941018, 1487941018, 19, 0),
(999, 455, '.', '', 4, 19, 1487941086, 1487941086, 19, 0),
(1000, 456, '.', '', 1, 19, 1487941592, 1487941592, 19, 0),
(1001, 456, '.', '', 2, 19, 1487949508, 1487949508, 19, 0),
(1002, 456, '.', '', 3, 19, 1487950850, 1487950850, 19, 0),
(1003, 456, '.', '', 4, 19, 1487950888, 1487950888, 19, 0),
(1004, 457, '.', '', 1, 19, 1487951206, 1487951206, 19, 0),
(1005, 457, '.', '', 2, 19, 1487951222, 1487951222, 19, 0),
(1006, 457, '.', '', 3, 19, 1487951237, 1487951237, 19, 0),
(1007, 457, '.', '', 4, 19, 1487951268, 1487951268, 19, 0),
(1008, 457, '.', '', 5, 19, 1487951309, 1487951309, 19, 0),
(1009, 457, '.', '', 6, 19, 1487951330, 1487951330, 19, 0),
(1010, 458, '.', '', 1, 19, 1487951444, 1487951444, 19, 0),
(1011, 458, '.', '', 2, 19, 1487951461, 1487951461, 19, 0),
(1012, 458, '.', '', 3, 19, 1487951477, 1487951477, 19, 0),
(1013, 458, '.', '', 4, 19, 1487951493, 1487951493, 19, 0),
(1014, 458, '.', '', 5, 19, 1487951527, 1487951527, 19, 0),
(1015, 458, '.', '', 6, 19, 1487951569, 1487951569, 19, 0),
(1016, 459, '.', '', 1, 19, 1487951656, 1487951656, 19, 0),
(1017, 459, '.', '', 2, 19, 1487951711, 1487951711, 19, 0),
(1018, 460, '.', '', 1, 19, 1487953965, 1487953965, 19, 0),
(1019, 460, '.', '', 2, 19, 1487953991, 1487953991, 19, 0),
(1020, 461, '.', '', 1, 19, 1487954039, 1487954039, 19, 0),
(1021, 461, '.', '', 2, 19, 1487954053, 1487954053, 19, 0),
(1022, 461, '.', '', 3, 19, 1487954068, 1487954068, 19, 0),
(1023, 462, 'до кожного числа існує йому обернене;', '', 1, 19, 1487954564, 1487954564, 19, 0),
(1024, 462, 'існують числа, обернені до самих себе', '', 2, 19, 1487954588, 1487954588, 19, 0),
(1025, 463, '.', '', 1, 19, 1487954731, 1487954731, 19, 0),
(1026, 463, '.', '', 2, 19, 1487954788, 1487954788, 19, 0),
(1027, 464, '.', '', 1, 19, 1487954917, 1487954917, 19, 0),
(1028, 464, '.', '', 2, 19, 1487954998, 1487954998, 19, 0),
(1029, 465, '.', '', 1, 19, 1487955115, 1487955115, 19, 0),
(1030, 465, '.', '', 2, 19, 1487955137, 1487955137, 19, 0),
(1031, 465, '.', '', 3, 19, 1487955196, 1487955196, 19, 0),
(1032, 465, '.', '', 4, 19, 1487955273, 1487955273, 19, 0),
(1033, 465, '.', '', 5, 19, 1487955309, 1487955309, 19, 0),
(1034, 465, '.', '', 6, 19, 1487955443, 1487955443, 19, 0),
(1035, 122, '22', '', 22, 19, 1489493861, 1489494073, 19, 0),
(1036, 122, '33', '', 33, 19, 1489493883, 1489493934, 19, 0),
(1037, 122, '777', '', 777, 19, 1489496557, 1489496557, 19, 0),
(1038, 466, '.', '', 1, 19, 1489509405, 1489509405, 19, 0),
(1039, 466, '.', '', 2, 19, 1489509430, 1489509430, 19, 0),
(1040, 466, '.', '', 3, 19, 1489509493, 1489509493, 19, 0),
(1041, 467, '.', '', 1, 19, 1489509591, 1489509591, 19, 0),
(1042, 467, '.', '', 2, 19, 1489509626, 1489509636, 19, 1),
(1043, 467, '.', '', 2, 19, 1489509653, 1489509653, 19, 0),
(1044, 469, '.', '', 1, 19, 1489675517, 1489675609, 19, 1),
(1045, 469, '.', '', 2, 19, 1489675546, 1489675612, 19, 1),
(1046, 469, '.', '', 1, 19, 1489675634, 1489675634, 19, 0),
(1047, 469, '.', '', 2, 19, 1489675668, 1489675668, 19, 0),
(1048, 469, '.', '', 3, 19, 1489675689, 1489675689, 19, 0),
(1049, 469, '.', '', 4, 19, 1489675726, 1489675726, 19, 0),
(1050, 469, '.', '', 5, 19, 1489675754, 1489675754, 19, 0),
(1051, 469, '.', '', 6, 19, 1489675861, 1489675861, 19, 0),
(1052, 470, '.', '', 1, 19, 1489675962, 1489675962, 19, 0),
(1053, 470, '.', '', 2, 19, 1489675979, 1489675979, 19, 0),
(1054, 470, '.', '', 3, 19, 1489675999, 1489675999, 19, 0),
(1055, 470, '.', '', 4, 19, 1489676036, 1489676036, 19, 0),
(1056, 470, '.', '', 5, 19, 1489676072, 1489676072, 19, 0),
(1057, 470, '.', '', 6, 19, 1489676094, 1489676094, 19, 0),
(1058, 470, '.', '', 7, 19, 1489676168, 1489676378, 19, 1),
(1059, 470, '.', '', 8, 19, 1489676310, 1489676420, 19, 1),
(1060, 470, '.', '', 8, 19, 1489676314, 1489676433, 19, 1),
(1061, 470, '.', '', 9, 19, 1489676341, 1489676437, 19, 1),
(1062, 470, '.', '', 10, 19, 1489676366, 1489676439, 19, 1),
(1063, 470, '.', '', 7, 19, 1489676481, 1489676481, 19, 0),
(1064, 470, '.', '', 8, 19, 1489676508, 1489676508, 19, 0),
(1065, 470, '.', '', 9, 19, 1489676525, 1489676525, 19, 0),
(1066, 470, '.', '', 10, 19, 1489676579, 1489676579, 19, 0),
(1067, 471, '.', '', 1, 19, 1489676886, 1489676886, 19, 0),
(1068, 471, '.', '', 2, 19, 1489676940, 1489676940, 19, 0),
(1069, 471, '.', '', 3, 19, 1489676978, 1489676978, 19, 0),
(1070, 471, '.', '', 4, 19, 1489676996, 1489676996, 19, 0),
(1071, 471, '.', '', 5, 19, 1489677025, 1489677025, 19, 0),
(1072, 471, '.', '', 6, 19, 1489677060, 1489677060, 19, 0),
(1073, 471, '.', '', 7, 19, 1489677148, 1489677148, 19, 0),
(1074, 471, '.', '', 8, 19, 1489677171, 1489677171, 19, 0),
(1075, 471, '.', '', 9, 19, 1489677199, 1489677199, 19, 0),
(1076, 471, '.', '', 10, 19, 1489677304, 1489677304, 19, 0),
(1077, 472, '.', '', 1, 19, 1489677435, 1489677435, 19, 0),
(1078, 472, '.', '', 2, 19, 1489677504, 1489677504, 19, 0),
(1079, 472, '.', '', 3, 19, 1489677522, 1489677522, 19, 0),
(1080, 472, '.', '', 4, 19, 1489677566, 1489677566, 19, 0),
(1081, 473, '.', '', 1, 19, 1489677734, 1489677734, 19, 0),
(1082, 473, '.', '', 2, 19, 1489677777, 1489677777, 19, 0),
(1083, 473, '.', '', 3, 19, 1489677799, 1489677799, 19, 0),
(1084, 473, '.', '', 3, 19, 1489677800, 1489677800, 19, 0),
(1085, 473, '.', '', 4, 19, 1489677822, 1489677822, 19, 0),
(1086, 474, '.', '', 1, 19, 1489677920, 1489677920, 19, 0),
(1087, 474, '.', '', 2, 19, 1489678000, 1489678000, 19, 0),
(1088, 474, '.', '', 3, 19, 1489678018, 1489678018, 19, 0),
(1089, 474, '.', '', 4, 19, 1489678089, 1489678089, 19, 0),
(1090, 475, '.', '', 1, 19, 1489678719, 1489678719, 19, 0),
(1091, 475, '.', '', 2, 19, 1489678749, 1489678749, 19, 0),
(1092, 475, '.', '', 3, 19, 1489678819, 1489678819, 19, 0),
(1093, 481, '3/4', '', 1, 19, 1489679085, 1489679085, 19, 0),
(1094, 481, '4 1/2', '', 2, 19, 1489679104, 1489679104, 19, 0),
(1095, 481, '18', '', 3, 19, 1489679112, 1489679112, 19, 0),
(1096, 481, '3 3/8', '', 4, 19, 1489679123, 1489679123, 19, 0),
(1097, 482, '12/35', '', 1, 19, 1489679219, 1489679219, 19, 0),
(1098, 482, '1 2/25', '', 2, 19, 1489679234, 1489679234, 19, 0),
(1099, 482, '2 2/35', '', 3, 19, 1489679262, 1489679262, 19, 0),
(1100, 485, 'ы', 'вам', 12, 19, 1491837992, 1491837992, 19, 0),
(1101, 487, 'qweqwe', 'qwexqwe', 123123, 19, 1491992491, 1491992491, 19, 0),
(1102, 489, 'уцываприольбдю', '', 1, 19, 1492000921, 1492000936, 19, 1),
(1103, 489, 'вчсапмиртольд', '', 4512, 19, 1492000957, 1492001038, 19, 0),
(1104, 514, 'qwedddd', '3', 1, 19, 1493026256, 1493026256, 19, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE `task` (
  `id` int(11) NOT NULL,
  `class` int(11) DEFAULT NULL,
  `level_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `result` varchar(255) DEFAULT NULL,
  `explanation` text,
  `type` tinyint(11) NOT NULL,
  `number` int(11) DEFAULT NULL,
  `sheet` int(11) DEFAULT NULL,
  `push_status` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`id`, `class`, `level_id`, `theme_id`, `text`, `result`, `explanation`, `type`, `number`, `sheet`, `push_status`, `created_by`, `created_at`, `updated_at`, `updated_by`, `deleted`) VALUES
(23, 5, 1, 14, 'задание 1', '1', 'якесь пояснення1', 1, 10, 10, 1, 5, 1345547456, 1478604821, 19, 0),
(25, 5, 1, 14, 'завдання 2', '2', '223', 1, 11, 11, 1, 5, 1234234243, NULL, NULL, 1),
(26, 5, 1, 14, '222', 'qwedgfhj', 'qwesdf', 1, 1, 1, 1, 19, 1478521434, 1478532081, 19, 0),
(27, 5, 1, 15, 'we', 'we', 'we', 1, 1, 1, 0, 19, 1478522424, 1478534208, 19, 0),
(28, 5, 1, 15, 'new', 'new', 'new11', 1, 1, 1, 0, 19, 1478532544, 1482849863, 19, 0),
(29, 7, 1, 15, 'ааыаыа ауауыаыу', 'кееку', 'куеке', 1, 4, 546546, 0, 19, 1482492189, NULL, NULL, 0),
(30, 6, 1, 15, 'чсмчсмчсм', 'с ссч', 'мсчмчмсчмчсм', 1, 465454, 2147483647, 0, 19, 1482492849, NULL, NULL, 0),
(31, 7, 1, 15, 'авмвамв', 'мавмвамвм', 'ввв', 2, 453454354, 4353453, 0, 19, 1482493078, NULL, NULL, 0),
(32, 5, 1, 15, 'кпв', 'мтпмтпмт', 'птпт', 1, 5645, 45345, 0, 19, 1482493154, NULL, NULL, 0),
(33, 6, 1, 15, 'вмывмвымв', '564ав', 'счмсчм', 2, 45345, 566546, 0, 19, 1482493243, NULL, NULL, 0),
(34, 6, 1, 15, 'ваыавыаы', 'вмымыв', 'выыв', 1, 5445, 64645, 0, 19, 1482493278, NULL, NULL, 0),
(35, 6, 1, 15, 'вымывмвы', 'вывм', 'вымвмыв', 2, 343, 435345, 0, 19, 1482493528, NULL, NULL, 0),
(36, 6, 1, 15, '4', '4', '4', 1, 4, 4, 0, 19, 1482842989, NULL, NULL, 0),
(37, 7, 1, 15, '6', '6', '6', 1, 6, 6, 0, 19, 1482843820, NULL, NULL, 0),
(38, 6, 1, 15, '6', '6', '6', 1, 6, 6, 0, 19, 1482844706, NULL, NULL, 0),
(39, 5, 1, 15, '5', '5', '5', 1, 5, 5, 0, 19, 1482845144, NULL, NULL, 0),
(40, 5, 1, 15, '5', '5', '5', 2, 5, 5, 0, 19, 1482845287, 1482848881, 19, 0),
(41, 7, 1, 20, '3453', '345', '34543', 1, 435, 534, 0, 19, 1484666482, 1487022917, 19, 0),
(42, 5, 1, 20, '4', '4', '444', 1, 4, 4, 0, 19, 1485252565, NULL, NULL, 1),
(43, 5, 1, 20, '456456', '645645', '546456', 1, 6456, 564, 0, 19, 1485255379, NULL, NULL, 1),
(44, 6, 1, 20, '4353', '54353', '543534', 1, 435, 45, 0, 19, 1485255627, NULL, NULL, 1),
(45, 6, 1, 20, '4', '4', '4', 1, 4, 5, 0, 19, 1485255750, NULL, NULL, 1),
(46, 6, 1, 20, '43534', '4ц53', '43534', 1, 4353, 435435, 0, 19, 1485256495, NULL, NULL, 1),
(47, 5, 1, 20, '6546', '64', '456', 1, 464, 564, 0, 19, 1485257177, NULL, NULL, 0),
(48, 6, 1, 25, '65465', '45645', '5464', 2, 54, 456, 0, 19, 1485258609, NULL, NULL, 0),
(49, 6, 1, 25, '87978', '8797', '7897', 1, 9879, 87978978, 0, 19, 1485258661, NULL, NULL, 0),
(50, 6, 1, 25, '99999', '999999999999', '99999999999', 1, 999, 99999, 0, 19, 1485259129, NULL, NULL, 1),
(51, 5, 1, 25, '6546', '4556', '54645', 2, 546456, 645, 0, 19, 1485259498, NULL, NULL, 1),
(52, 7, 1, 20, '4', NULL, NULL, 1, 4, 4, 0, 19, 1486485495, 1487027125, 19, 0),
(53, 7, 1, 20, '46', NULL, NULL, 1, 43, 4, 0, 19, 1486485594, 1487027270, 19, 0),
(54, 6, 1, 27, 'Назви ті пари чисел, у яких перше число є дільником другого: 1)2 і 8;  2)3 і 5;  3)14 і 7;  4) 5 і 18;  5)10 і 50;  6)1 і 2012.', NULL, NULL, 1, 1, 7, 0, 19, 1486486002, 1487067504, 19, 1),
(55, 6, 1, 27, 'Перевір, чи є перше число дільником другого: 1) 25 і 400;  2) 13 і 1613;  3) 123 і 3321.', NULL, NULL, 1, 2, 7, 0, 19, 1486637720, 1487067605, 19, 1),
(56, 6, 1, 27, 'Перевір, чи є перше число дільником другого:', NULL, NULL, 1, 3, 7, 0, 19, 1486640832, NULL, NULL, 0),
(57, 6, 1, 27, 'Перевір, чи є перше число дільником другого:', NULL, NULL, 1, 3, 7, 0, 19, 1486640875, NULL, NULL, 1),
(58, 6, 1, 27, 'Назви пари чисел, у яких перше число є кратним другому: 1) 12 і 3;  2) 17 і 9;  3) 18 і 1;  4) 23 і 23.', NULL, NULL, 1, 4, 7, 0, 19, 1486641430, 1487069090, 19, 0),
(59, 6, 1, 27, 'Перевір, чи є перше число кратним другому:', NULL, NULL, 1, 5, 7, 0, 19, 1486651152, NULL, NULL, 1),
(60, 6, 1, 30, 'Які із чисел 42, 217, 35, 1002, 8109 діляться', '', NULL, 1, 47, 12, 0, 19, 1486736826, 1486737040, 19, 0),
(61, 6, 1, 30, 'Заповни в зошиті таку таблицю', '', NULL, 1, 48, 13, 0, 19, 1486737290, 1486737435, 19, 0),
(62, 6, 1, 30, 'Знайди суму цифр кожного із чисел: 135, 207, 396, 1086, 12 002, 576. Які з них діляться', '', NULL, 1, 49, 13, 0, 19, 1486737523, 1486737576, 19, 0),
(63, 6, 2, 30, 'Із чисел 180, 2109, 541, 4590, 111 102, 7891 випиши ті, які:', '', NULL, 1, 50, 13, 0, 19, 1486738006, 1486738038, 19, 0),
(64, 6, 2, 30, 'Із чисел 582, 509, 450, 3105, 2017 випиши ті, які', '', NULL, 1, 51, 13, 0, 19, 1486738964, NULL, NULL, 0),
(65, 6, 2, 30, 'Чи можна скласти трицифрове число, яке не містить однакових цифр і ділиться на 3, із цифр', '', NULL, 1, 52, 13, 0, 19, 1486739622, NULL, NULL, 0),
(66, 6, 2, 30, 'Чи можна з даних цифр скласти чотирицифрове число, що не містить однакових цифр, яке ділиться на 9', '', NULL, 1, 53, 13, 0, 19, 1486739931, NULL, NULL, 0),
(67, 6, 2, 30, 'Яку цифру потрібно підставити замість зірочки, щоб отримати число, яке ділиться на 9', '', NULL, 1, 54, 13, 0, 19, 1486740445, NULL, NULL, 0),
(68, 6, 2, 30, 'Яку цифру потрібно підставити замість зірочки, щоб отримати число, яке ділиться на 3', '', NULL, 1, 55, 13, 0, 19, 1486740721, NULL, NULL, 0),
(69, 6, 3, 30, 'Запиши значення х, які кратні числу 3, при яких нерівність 45 < х < 57 буде правильною', '48, 51, 54', NULL, 1, 56, 13, 0, 19, 1486741214, 1486741349, 19, 0),
(70, 5, 2, 27, '3', '3', '3', 2, 3, 3, 0, 19, 1486741249, 1486741560, 19, 1),
(71, 6, 3, 30, 'Запиши значення у, які кратні числу 3, але не кратні числу 9, при яких нерівність 116 < у < 145 буде правильною', '120, 123, 129, 132, 138, 141', NULL, 1, 57, 13, 0, 19, 1486741961, NULL, NULL, 0),
(72, 6, 3, 30, 'З даних цифр утвори, якщо це можливо, одне трицифрове число, яке ділиться на 3, і одне трицифрове число, яке ділиться на 9 (цифри в числі можуть повторюватися)', '', NULL, 1, 58, 13, 0, 19, 1486742457, 1486742689, 19, 0),
(73, 6, 3, 30, 'Підстав замість зірочок такі цифри, щоб число:', '', NULL, 1, 59, 14, 0, 19, 1486742756, NULL, NULL, 0),
(74, 6, 4, 30, 'Підстав замість зірочок такі цифри, щоб число', '', NULL, 1, 60, 14, 0, 19, 1486743087, NULL, NULL, 0),
(75, 6, 4, 30, 'Запиши найменше чотирицифрове число, яке ділиться:', '', NULL, 1, 61, 14, 0, 19, 1486743646, NULL, NULL, 0),
(76, 6, 1, 27, 'Назви ті пари чисел, у яких перше число є дільником другого:1) 2 і 8;	2)	3 і 5; 3)	14 і 7;    4) 5 і 18;	5)	10 і 50;	6)	1 і 2012.', '', NULL, 1, 1, 7, 0, 19, 1486744577, NULL, NULL, 1),
(77, 6, 5, 30, 'Підстав замість зірочок такі цифри, щоб число 1*2* ділилося на 15. (Знайди всі можливі розв’язки.)', '0 і 0, 1 і 5,', NULL, 1, 62, 14, 0, 19, 1486744632, NULL, NULL, 0),
(78, 6, 2, 30, 'Учень прочитав 150 сторінок, що становить 5\\7 книжки. Скільки сторінок у книжці?', '210', NULL, 1, 63, 14, 0, 19, 1486744707, NULL, NULL, 0),
(79, 6, 3, 30, 'Знайди градусну міру кута між стрілками го¬динника, коли вони показують:', '', NULL, 1, 64, 14, 0, 19, 1486744784, NULL, NULL, 0),
(80, 6, 3, 30, 'Знайди градусну міру кута між стрілками годинника, коли вони показують:', '', NULL, 1, 64, 14, 0, 19, 1486744784, 1486744824, 19, 0),
(82, 6, 6, 30, 'На столі лежать 25 сірників. Двоє гравців по черзі беруть 1 або 2 сірники. Переможе той, хто візьме останній сірник. Як має діяти перший гравець, щоб виграти?', '', NULL, 1, 65, 14, 0, 19, 1486744994, NULL, NULL, 0),
(83, 6, 1, 27, 'Перевір, чи є перше число дільником другого: 1) 25 і 400; 2) 13 і 1613; 3) 123 і 3321.', '', NULL, 1, 2, 7, 0, 19, 1486745364, NULL, NULL, 1),
(84, 7, 1, 20, '546', NULL, NULL, 1, 546, 5, 0, 19, 1487025281, NULL, NULL, 0),
(85, 7, 2, 20, '2', '2', NULL, 1, 2, 2, 0, 19, 1487025340, NULL, NULL, 1),
(86, 7, 1, 20, '2', '2', NULL, 1, 2, 2, 0, 19, 1487025419, NULL, NULL, 0),
(87, 7, 2, 20, '4', NULL, NULL, 2, 4, 4, 0, 19, 1487025500, NULL, NULL, 1),
(88, 7, 2, 20, '4', NULL, NULL, 1, 4, 4, 0, 19, 1487026761, NULL, NULL, 0),
(89, 7, 2, 20, '7', NULL, NULL, 1, 7, 7, 0, 19, 1487026815, NULL, NULL, 0),
(90, 7, 2, 20, '9', NULL, NULL, 1, 9, 9, 0, 19, 1487026909, NULL, NULL, 0),
(91, 7, 2, 20, '4', NULL, NULL, 2, 4, 4, 0, 19, 1487065508, NULL, NULL, 0),
(92, 6, 1, 27, 'Перевір, чи є перше число дільником другого:', NULL, NULL, 1, 2, 7, 0, 19, 1487068602, NULL, NULL, 1),
(93, 6, 1, 27, 'Перевір, чи є перше число кратним другому:', NULL, NULL, 1, 5, 7, 0, 19, 1487069340, NULL, NULL, 0),
(94, 6, 1, 27, 'Перевір, чи є перше число кратним другому:', NULL, NULL, 1, 6, 7, 0, 19, 1487069481, NULL, NULL, 0),
(95, 6, 1, 27, 'Назви ті пари чисел, у яких перше число є дільником другого: 1) 2 і 8;  2) 3 і 5;  3) 14 і 7;  4) 5 і 18;  5) 10 і 50;  6) 1 і 2012.', NULL, NULL, 1, 1, 7, 0, 19, 1487069693, 1492002027, 19, 0),
(96, 6, 1, 27, 'Перевір, чи є перше число дільником другого:', NULL, NULL, 1, 2, 7, 0, 19, 1487069776, NULL, NULL, 0),
(97, 6, 2, 27, 'Запиши всі дільники числа:', NULL, NULL, 1, 7, 7, 0, 19, 1487070050, NULL, NULL, 0),
(98, 6, 2, 27, 'Запиши всі дільники числа:', NULL, NULL, 1, 8, 7, 0, 19, 1487070170, NULL, NULL, 0),
(99, 6, 2, 27, 'Запиши чотири числа, кратні числу:', NULL, NULL, 1, 9, 7, 0, 19, 1487070272, NULL, NULL, 0),
(100, 6, 2, 27, 'Запиши чотири числа, кратні числу:', NULL, NULL, 1, 10, 7, 0, 19, 1487070362, NULL, NULL, 0),
(101, 6, 2, 27, 'Треба поділити порівну між кількома дітьми 24 цукерки. Скільки може бути дітей?', NULL, NULL, 1, 11, 7, 0, 19, 1487070472, NULL, NULL, 0),
(102, 6, 2, 27, 'Чи можна дати здачу 2 грн 25 коп. монетами:', NULL, NULL, 1, 12, 7, 0, 19, 1487070524, NULL, NULL, 0),
(103, 6, 2, 27, 'Чи можна 65 огірків розкласти порівну:', NULL, NULL, 1, 13, 7, 0, 19, 1487070661, NULL, NULL, 0),
(104, 6, 3, 27, 'Запиши всі двоцифрові числа, які кратні числу 17.', NULL, NULL, 1, 14, 7, 0, 19, 1487070982, NULL, NULL, 0),
(105, 6, 3, 27, 'Запиши всі двоцифрові числа, які кратні числу 13.', NULL, NULL, 1, 15, 8, 0, 19, 1487071104, 1487071460, 19, 0),
(106, 6, 3, 27, 'Вкажи яке-небудь число, що є дільником чисел:', NULL, NULL, 1, 16, 8, 0, 19, 1487071239, 1487071498, 19, 0),
(107, 6, 3, 27, 'Вкажи яке-небудь число, що є дільником чисел:', NULL, NULL, 1, 17, 8, 0, 19, 1487071439, 1487071563, 19, 0),
(108, 6, 3, 27, 'Вкажи яке-небудь число, що є кратне числам:', NULL, NULL, 1, 18, 8, 0, 19, 1487072148, NULL, NULL, 0),
(109, 6, 3, 27, 'Вкажи яке-небудь число, що є кратне числам:', NULL, NULL, 1, 19, 8, 0, 19, 1487072362, NULL, NULL, 0),
(110, 6, 3, 27, 'Запиши значення х, які кратні числу 5, при яких подвійна нерівність 23 < х < 36 буде правильною.', NULL, NULL, 1, 20, 8, 0, 19, 1487072568, NULL, NULL, 0),
(111, 6, 3, 27, 'Запиши значення b, при яких подвійна нерівність 4 < b < 17 буде правильною і які:', NULL, NULL, 1, 22, 8, 0, 19, 1487072611, 1487076285, 19, 0),
(112, 6, 3, 27, 'Запиши значення у, що є дільниками числа 30, при яких подвійна нерівність 2 < у < 14 буде правильною.', NULL, NULL, 1, 21, 8, 0, 19, 1487076365, 1487076386, 19, 0),
(113, 6, 4, 27, 'Знайди:', NULL, NULL, 1, 23, 8, 0, 19, 1487076418, NULL, NULL, 0),
(114, 6, 4, 27, 'Яка найменша кількість горіхів повинна бути в кошику, щоб їх можна було розкласти на купки або по 6, або по 8, або по 9 горіхів у кожній?', NULL, NULL, 1, 24, 8, 0, 19, 1487076512, NULL, NULL, 0),
(115, 6, 4, 27, 'На координатному промені позначено число b (мал. 1). Познач на такому промені у зошиті три числа, які кратні числу b.', NULL, NULL, 1, 25, 8, 0, 19, 1487076560, 1487076595, 19, 0),
(116, 6, 2, 27, 'Знайди периметр і площу квадрата, сторона якого дорівнює 2,4 см. Вирази площу цього квадрата у мм^2.', NULL, NULL, 1, 26, 8, 0, 19, 1487077092, NULL, NULL, 0),
(117, 6, 2, 27, 'Округли:', NULL, NULL, 1, 27, 8, 0, 19, 1487077903, NULL, NULL, 0),
(118, 6, 6, 27, 'Доведи, що два натуральних числа а і b мають таку властивість: або а, або b, або а + b, або а - b ділиться на 3.', NULL, NULL, 1, 28, 8, 0, 19, 1487078062, 1487078119, 19, 0),
(119, 6, 1, 29, '(Усно) Серед чисел 275, 96, 107, 95, 100, 512, 715, 2100, 109 назви ті, що діляться :', NULL, NULL, 1, 29, 10, 0, 19, 1487078881, NULL, NULL, 0),
(120, 6, 1, 29, '(Усно) Які із чисел 1002, 913, 714, 7008, 411, 1005, 676 є :', NULL, NULL, 1, 30, 10, 0, 19, 1487080659, 1487081781, 19, 0),
(121, 6, 1, 29, 'Які із чисел 6538, 7780, 9835, 10 391, 15 932, 18 060, 44 445 діляться:', NULL, NULL, 1, 31, 10, 0, 19, 1487081350, NULL, NULL, 0),
(122, 7, 2, 20, '1', '1', NULL, 2, 1, 1, 0, 19, 1487081477, 1489495187, 19, 0),
(123, 7, 2, 20, '555', '555', NULL, 2, 555, 555, 0, 19, 1487081537, NULL, NULL, 0),
(124, 7, 1, 20, '999', '999', NULL, 1, 999, 999, 0, 19, 1487081576, NULL, NULL, 0),
(125, 7, 2, 20, '234', '234', NULL, 1, 234, 234, 0, 19, 1487081709, NULL, NULL, 0),
(126, 7, 2, 20, '987', '987', NULL, 1, 987, 987, 0, 19, 1487081778, 1487081946, 19, 0),
(127, 7, 1, 20, '135', '135', NULL, 1, 135, 135, 0, 19, 1487082111, 1487082135, 19, 0),
(128, 6, 1, 29, 'Які із чисел 5866, 5075, 8160, 13 382, 15 047, 405 185, 80 407, 72 310 діляться:', NULL, NULL, 1, 32, 10, 0, 19, 1487084048, NULL, NULL, 0),
(129, 6, 2, 29, 'Запиши по три чотирицифрових числа, які діляться:', NULL, NULL, 1, 33, 10, 0, 19, 1487084172, NULL, NULL, 0),
(130, 6, 2, 29, 'Запиши по два п’ятицифрових числа, які діляться:', NULL, NULL, 1, 34, 10, 0, 19, 1487084261, NULL, NULL, 0),
(131, 6, 2, 29, '(Усно) Наприкінці уроку учні здали зошити для контрольних робіт і зошити для вправ, усього 51 зошит. Чи всі учні здали обидва зошити?', NULL, NULL, 1, 35, 10, 0, 19, 1487084367, NULL, NULL, 0),
(132, 6, 2, 29, 'Допиши праворуч до числа 37 таку цифру, щоб це число ділилося:', NULL, NULL, 1, 36, 10, 0, 19, 1487084420, NULL, NULL, 0),
(133, 6, 2, 29, 'Замість зірочки постав таку цифру, щоб число 519*  :', NULL, NULL, 1, 37, 10, 0, 19, 1487084522, NULL, NULL, 0),
(134, 6, 3, 29, 'Запиши значення х, при яких нерівність 413 < x < 424 буде правильною і які кратні числу 2.', NULL, NULL, 1, 38, 10, 0, 19, 1487084697, 1487084750, 19, 0),
(135, 6, 3, 29, 'Запиши значення b, при яких нерівність 182 < b < 223 буде правильною і які кратні числу 10.', NULL, NULL, 1, 39, 11, 0, 19, 1487084797, NULL, NULL, 0),
(136, 6, 3, 29, 'Із цифр 0, 1, 5 і 8 склади по три чотирицифрові числа, які діляться (цифри в запису числа не повторюються):', NULL, '', 1, 40, 11, 0, 19, 1487084884, 1487085005, 19, 0),
(137, 6, 3, 29, 'Чи можна, використовуючи лише цифри 1 і 2, записати:', NULL, NULL, 1, 41, 11, 0, 19, 1487085132, NULL, NULL, 0),
(138, 6, 3, 29, 'Використовуючи кожну цифру один раз, запиши найбільше:', NULL, NULL, 1, 42, 11, 0, 19, 1487085425, NULL, NULL, 0),
(139, 6, 4, 29, 'Із цифр 2, 0, 5 і 7 утвори всі можливі чотирицифрові парні числа. (Цифри в запису числа не повторюються.)', NULL, NULL, 1, 43, 11, 0, 19, 1487085598, NULL, NULL, 0),
(140, 6, 2, 29, 'Знайди об’єм і площу поверхні куба, ребро якого дорівнює 1,2 см.', NULL, NULL, 1, 44, 11, 0, 19, 1487085632, NULL, NULL, 0),
(141, 6, 3, 29, 'Запиши три числа, кожне з яких:', NULL, NULL, 1, 45, 11, 0, 19, 1487085670, NULL, NULL, 0),
(142, 6, 6, 29, 'Перевір, що кожне із чисел 6, 28, 496 дорівнює сумі всіх його дільників, не враховуючи самого числа. (Такі числа називають досконалими.)', NULL, NULL, 1, 46, 11, 0, 19, 1487085819, NULL, NULL, 0),
(143, 6, 1, 31, '(Усно) Використовуючи таблицю простих чисел, назви прості числа, які:', NULL, NULL, 1, 66, 16, 0, 19, 1487086022, NULL, NULL, 0),
(144, 6, 1, 31, 'Перевір, користуючись таблицею простих чисел, які із чисел 197, 203, 239, 489, 563, 839, 871 :', NULL, NULL, 1, 67, 16, 0, 19, 1487086210, NULL, NULL, 0),
(145, 6, 1, 31, 'Визнач, використовуючи таблицю простих чисел, які із чисел 113, 137, 171, 251, 293, 403, 439, 501, 701 :', NULL, NULL, 1, 68, 16, 0, 19, 1487086339, NULL, NULL, 0),
(146, 6, 2, 31, 'Доведи, що є складеним число:', NULL, NULL, 1, 69, 16, 0, 19, 1487086420, NULL, NULL, 0),
(147, 6, 2, 31, 'Доведи, що є складеним число:', NULL, NULL, 1, 70, 16, 0, 19, 1487086793, NULL, NULL, 0),
(148, 6, 2, 31, 'Запиши всі дільники числа 24. Підкресли ті з них, які є простими числами.', NULL, NULL, 1, 71, 16, 0, 19, 1487086961, NULL, NULL, 0),
(149, 6, 2, 31, 'Запиши замість зірочки таку цифру, щоб було складеним число:', NULL, NULL, 1, 72, 16, 0, 19, 1487087213, NULL, NULL, 0),
(150, 6, 2, 31, 'Запиши замість зірочки таку цифру, щоб було складеним число:', NULL, NULL, 1, 73, 16, 0, 19, 1487087329, 1487087342, 19, 0),
(151, 6, 3, 31, 'Не використовуючи таблицю простих чисел, запиши:', NULL, NULL, 1, 74, 17, 0, 19, 1487087412, NULL, NULL, 0),
(152, 6, 3, 31, 'Не використовуючи таблицю простих чисел, знайди:', NULL, NULL, 1, 75, 17, 0, 19, 1487087520, NULL, NULL, 0),
(153, 6, 3, 31, 'Простим чи складеним числом є добуток:', NULL, NULL, 1, 76, 17, 0, 19, 1487088657, NULL, NULL, 0),
(154, 6, 4, 31, 'Чи можна записати просте трицифрове число, ви¬користавши лише один раз кожну із цифр:', NULL, NULL, 1, 77, 17, 0, 19, 1487089228, NULL, NULL, 0),
(155, 6, 4, 31, 'Чи можна записати просте трицифрове число, використавши лише один раз кожну із цифр:', NULL, NULL, 1, 78, 17, 0, 19, 1487089313, NULL, NULL, 0),
(156, 6, 4, 31, 'Простим чи складеним є число, записане за допомогою:', NULL, NULL, 1, 79, 17, 0, 19, 1487089430, NULL, NULL, 0),
(157, 6, 2, 31, 'Виділи цілу та дробову частини числа:', NULL, NULL, 1, 80, 17, 0, 19, 1487089651, NULL, NULL, 0),
(158, 6, 3, 31, 'Знайди пропущені числа та прочитай прізвище видатного письменника, який народився в Україні.', NULL, NULL, 1, 81, 17, 0, 19, 1487090842, 1487090872, 19, 0),
(159, 6, 6, 31, 'Знайди зручний спосіб для обчислення значення виразу:', NULL, NULL, 1, 82, 17, 0, 19, 1487090910, NULL, NULL, 0),
(160, 6, 1, 32, '(Усно) Чи є розкладом на прості множники добуток:', NULL, NULL, 1, 83, 19, 0, 19, 1487154055, NULL, NULL, 0),
(161, 6, 1, 32, '(Усно) Розклади на прості множники число:', NULL, NULL, 1, 84, 19, 0, 19, 1487154268, NULL, NULL, 0),
(162, 6, 2, 32, 'Розклади на прості множники число:', NULL, NULL, 1, 85, 19, 0, 19, 1487154626, NULL, NULL, 0),
(163, 6, 2, 32, 'Розклади на прості множники число:', NULL, NULL, 1, 86, 19, 0, 19, 1487155740, NULL, NULL, 0),
(164, 6, 3, 32, 'Чи ділиться число 2 • 2 • 2 • 3 • 17 на (у разі позитивної відповіді знайди частку від ділення) :', NULL, NULL, 1, 87, 19, 0, 19, 1487155926, 1487155970, 19, 0),
(165, 6, 3, 32, 'Знайди частку від ділення:', NULL, NULL, 1, 88, 20, 0, 19, 1487156052, NULL, NULL, 0),
(166, 6, 3, 32, 'У деяку кількість кошиків, яких менше ніж 20, розклали порівну 85 яблук. Скільки всього було кошиків і скільки яблук містив кожний кошик?', NULL, NULL, 1, 89, 20, 0, 19, 1487156227, NULL, NULL, 0),
(167, 6, 4, 32, 'Розклади на прості множники число 990 та знайди всі його дільники.', NULL, NULL, 1, 90, 20, 0, 19, 1487156286, NULL, NULL, 0),
(168, 6, 4, 32, 'Розклади на прості множники число 700 та знайди всі його дільники.', NULL, NULL, 1, 91, 20, 0, 19, 1487156326, NULL, NULL, 0),
(169, 6, 5, 32, 'Заміни зірочки цифрами, щоб рівність була правильною: 7** = 5 • 7 • 11 • *', NULL, NULL, 1, 92, 20, 0, 19, 1487156399, 1487156636, 19, 0),
(170, 6, 2, 32, 'Площа Італії разом з островами становить приблизно 309 500 км^2, а площа України на 95 % за неї більша. Знайди площу України. Порівняй отриманий результат з точними даними про площу України.', NULL, NULL, 1, 93, 20, 0, 19, 1487156467, NULL, NULL, 0),
(171, 6, 3, 32, 'Виконай дії:', NULL, NULL, 1, 94, 20, 0, 19, 1487156621, NULL, NULL, 0),
(172, 6, 6, 32, '.', NULL, NULL, 1, 95, 20, 0, 19, 1487157051, NULL, NULL, 0),
(173, 6, 1, 33, '(Усно) Чи є число 5 спільним дільником чисел:', NULL, NULL, 1, 96, 23, 0, 19, 1487157306, NULL, NULL, 0),
(174, 6, 1, 33, '(Усно) Знайди спільні дільники та найбільший спільний дільник чисел:', NULL, NULL, 1, 97, 23, 0, 19, 1487157411, NULL, NULL, 0),
(175, 6, 1, 33, 'Знайди найбільший спільний дільник чисел a і b, якщо:', NULL, NULL, 1, 98, 23, 0, 19, 1487157509, NULL, NULL, 0),
(176, 6, 1, 33, 'Знайди найбільший спільний дільник чисел с і d, якщо:', NULL, NULL, 1, 99, 23, 0, 19, 1487157642, NULL, NULL, 0),
(177, 6, 1, 33, '(Усно) Чи є взаємно простими числа:', NULL, NULL, 1, 100, 23, 0, 19, 1487157737, NULL, NULL, 0),
(178, 6, 2, 33, '(Усно) Серед чисел 2, 7, 14 і 20 знайди всі пари взаємно простих чисел.', NULL, NULL, 1, 101, 23, 0, 19, 1487157837, NULL, NULL, 0),
(179, 6, 2, 33, 'Знайди найбільший спільний дільник чисел:', NULL, NULL, 1, 102, 23, 0, 19, 1487157862, NULL, NULL, 0),
(180, 6, 2, 33, 'Знайди найбільший спільний дільник чисел:', NULL, NULL, 1, 103, 23, 0, 19, 1487158006, NULL, NULL, 0),
(181, 6, 2, 33, 'Знайди найбільший спільний дільник чисельника і знаменника дробу:', NULL, NULL, 1, 104, 23, 0, 19, 1487158251, NULL, NULL, 0),
(182, 6, 2, 33, 'Доведи, що:', NULL, NULL, 1, 105, 23, 0, 19, 1487158727, NULL, NULL, 0),
(183, 6, 2, 33, 'Доведи, що:', NULL, NULL, 1, 106, 23, 0, 19, 1487158846, NULL, NULL, 0),
(184, 6, 2, 33, 'Чи є взаємно простими числа:', NULL, NULL, 1, 107, 24, 0, 19, 1487158979, 1487159684, 19, 0),
(185, 6, 2, 33, 'Чи є взаємно простими числа:', NULL, NULL, 1, 108, 24, 0, 19, 1487159636, NULL, NULL, 0),
(186, 6, 3, 33, 'Запиши всі правильні дроби зі знаменником 18, у яких чисельник і знаменник — взаємно прості числа.', NULL, NULL, 1, 109, 24, 0, 19, 1487159781, NULL, NULL, 0),
(187, 6, 3, 33, 'Запиши всі неправильні дроби із чисельником 20, у яких чисельник і знаменник — взаємно прості числа.', NULL, NULL, 1, 110, 24, 0, 19, 1487159819, NULL, NULL, 0),
(188, 6, 3, 33, 'Яку найбільшу кількість однакових подарунків можна скласти, маючи 60 цукерок і 45 яблук, так, щоб використати всі цукерки і яблука та щоб кожен подарунок містив і цукерки, і яблука.', NULL, NULL, 1, 111, 24, 0, 19, 1487159917, NULL, NULL, 0),
(189, 6, 3, 33, 'У яку найбільшу кількість магазинів можна порівну розподілити для продажу 108 DVD-дисків з мультфільмами і 120 дисків із фільмами про тварин?', NULL, NULL, 1, 112, 24, 0, 19, 1487160002, NULL, NULL, 0),
(190, 6, 4, 33, 'У шостих класах 24 хлопчики і 36 дівчаток. Учнів поділили на групи для привітання ветеранів так, щоб в усіх групах була однакова кількість дівчаток і однакова кількість хлопчиків. Скількох ветеранів привітали, якщо їх більше за 7?', NULL, NULL, 1, 113, 24, 0, 19, 1487160063, NULL, NULL, 0),
(191, 6, 4, 33, 'В одному потязі 252 купейних місця, а в іншому — 396 купейних місць. По скільки купейних вагонів у кожному потязі, якщо в усіх купейних вагонах кількість місць є однаковою і їх більше за 20?', NULL, NULL, 1, 114, 24, 0, 19, 1487160147, NULL, NULL, 0),
(192, 6, 4, 33, 'Із 210 білих, 150 жовтих і 90 червоних троянд необхідно скласти однакові букети так, щоб у кожному букеті були троянди всіх трьох кольорів.', NULL, NULL, 1, 115, 24, 0, 19, 1487160176, NULL, NULL, 0),
(193, 6, 2, 33, 'Шлях від А до В завдовжки 360 км автомобіль проїхав за 4 год, а повернувся назад — за 5 год. Яка середня швидкість автомобіля за весь час руху?', NULL, NULL, 1, 116, 24, 0, 19, 1487160302, NULL, NULL, 0),
(194, 6, 3, 33, 'Автомобіль їхав 2 год зі швидкістю 72,4 км/год і 3 год зі швидкістю 71,6 км/год. Знайди середню швидкість автомобіля за весь час руху', NULL, NULL, 1, 117, 24, 0, 19, 1487160375, NULL, NULL, 0),
(195, 6, 3, 33, 'Вкладник поклав до банку 8000 грн під 15 % річних.', NULL, NULL, 1, 118, 25, 0, 19, 1487160425, NULL, NULL, 0),
(196, 6, 6, 33, 'Знайди останню цифру числа:', NULL, NULL, 1, 119, 25, 0, 19, 1487160603, NULL, NULL, 0),
(197, 6, 1, 34, '(Усно) Чи є:', NULL, NULL, 1, 120, 27, 0, 19, 1487161042, NULL, NULL, 0),
(198, 6, 1, 34, 'Назви кілька спільних кратних чисел:', NULL, NULL, 1, 121, 27, 0, 19, 1487161556, NULL, NULL, 0),
(199, 6, 1, 34, 'Знайди найменше спільне кратне чисел а і b, якщо:', NULL, NULL, 1, 122, 27, 0, 19, 1487161731, NULL, NULL, 0),
(200, 6, 1, 34, 'Знайди найменше спільне кратне чисел m і n, якщо:', NULL, NULL, 1, 123, 27, 0, 19, 1487161845, NULL, NULL, 0),
(201, 6, 2, 34, 'Доведи, що дані числа є взаємно простими, та знайди їх найменше спільне кратне:', NULL, NULL, 1, 124, 27, 0, 19, 1487161946, NULL, NULL, 0),
(202, 6, 2, 34, 'Знайди найменше спільне кратне чисел:', NULL, NULL, 1, 125, 27, 0, 19, 1487162140, NULL, NULL, 0),
(203, 6, 2, 34, 'Знайди найменше спільне кратне чисел:', NULL, NULL, 1, 126, 27, 0, 19, 1487162284, NULL, NULL, 0),
(204, 6, 2, 34, 'Знайди найменше спільне кратне знаменників дробів:', NULL, NULL, 1, 127, 27, 0, 19, 1487163003, NULL, NULL, 0),
(205, 6, 2, 34, 'Знайди найменше спільне кратне знаменників дробів :', NULL, NULL, 1, 128, 27, 0, 19, 1487163184, NULL, NULL, 0),
(206, 6, 3, 34, 'Довжина кроку батька 75 см, довжина кроку сина 50 см. Яку найменшу однакову відстань вони мають пройти, щоб кількість кроків кожного довірнювала цілому числу?', NULL, NULL, 1, 129, 27, 0, 19, 1487163295, 1487163313, 19, 0),
(207, 6, 3, 34, 'Від пункту А вздовж дороги встановлено стовпи через кожні 40 м. Ці стовпи вирішили замінити іншими і встановили їх на відстані 55 м один від одного. Знайди відстань від пункту А до найближчого стовпа, який буде встановлено на місці старого.', NULL, NULL, 1, 130, 28, 0, 19, 1487163398, NULL, NULL, 0),
(208, 6, 4, 34, 'Бабуся має трьох онуків. Сергій відвідує бабусю кожні 4 дні, Іван — кожні 5 днів, Петро — кожні 6 днів. Хлопці зустрілися у бабусі 1 січня невисокосного року. Якого числа вони зустрінуться у бабусі наступного разу?', NULL, NULL, 1, 131, 28, 0, 19, 1487163457, NULL, NULL, 0),
(209, 6, 4, 34, 'Три теплоходи здійснюють регулярні рейси з Одеси. Один з них повертається через 10 діб, другий — через 12 діб, третій — через 18 діб. Теплоходи зустрілися в одеському порту в понеділок. Через скільки діб і в який день тижня вони зустрінуться в цьому порту знову?', NULL, NULL, 1, 132, 28, 0, 19, 1487163771, NULL, NULL, 0),
(210, 6, 4, 34, 'Одну жінку запитали, скільки курчат вона привезла на базар. Вона відповіла, що їх більше ніж 115, але менше ніж 145 і при цьому їх можна розділити по 4, по 6 і по 10. Скільки курчат привезла жінка на базар?', NULL, NULL, 1, 133, 28, 0, 19, 1487163830, NULL, NULL, 0),
(211, 6, 3, 34, 'Розв’яжи рівняння:', NULL, NULL, 1, 134, 28, 0, 19, 1487163869, NULL, NULL, 0),
(212, 6, 4, 34, 'Зустрілися шестеро друзів і потисли один одному руки. Скільки всього було здійснено рукостискань?', NULL, NULL, 1, 135, 28, 0, 19, 1487163990, NULL, NULL, 0),
(213, 6, 6, 34, 'Можна довести, що для будь-яких натуральних чисел а і b виконується рівність НСД (а; b) • НСК (a; b) = а • b. Перевір виконання цієї рівності для таких пар чисел:', NULL, NULL, 1, 136, 28, 0, 19, 1487164045, NULL, NULL, 0),
(214, 6, 1, 35, '(Усно) Обґрунтуй рівність:', NULL, NULL, 1, 137, 32, 0, 19, 1487172754, NULL, NULL, 0),
(215, 6, 1, 35, '(Усно) Чи правильна рівність:', NULL, NULL, 1, 138, 33, 0, 19, 1487172926, NULL, NULL, 0),
(216, 6, 1, 35, 'Запиши три дроби, які дорівнюють', NULL, NULL, 1, 139, 33, 0, 19, 1487173121, NULL, NULL, 0),
(217, 6, 1, 35, 'Запиши два дроби, які дорівнюють', NULL, NULL, 1, 140, 33, 0, 19, 1487173163, NULL, NULL, 0),
(218, 6, 1, 35, 'Помнож чисельник і знаменник кожного дробу на 3 та запиши відповідні рівності :', NULL, NULL, 1, 141, 33, 0, 19, 1487174675, NULL, NULL, 0),
(219, 6, 1, 35, 'Помнож чисельник і знаменник кожного дробу на 2 та запиши відповідні рівності :', NULL, NULL, 1, 142, 33, 0, 19, 1487175538, NULL, NULL, 0),
(220, 6, 1, 35, 'Поділи чисельник і знаменник кожного дробу на 2 та запиши відповідні рівності:', NULL, NULL, 1, 143, 33, 0, 19, 1487176098, NULL, NULL, 0),
(221, 6, 1, 35, 'Поділи чисельник і знаменник кожного дробу на 3 та запиши відповідні рівності:', NULL, NULL, 1, 144, 33, 0, 19, 1487176623, NULL, NULL, 0),
(222, 6, 1, 35, '(Усно) Скороти дріб:', NULL, NULL, 1, 145, 33, 0, 19, 1487176868, NULL, NULL, 0),
(223, 6, 1, 35, 'Запиши три дроби, що дорівнюють дробу 16/24, знаменники яких менші від знаменника даного дробу.', NULL, NULL, 1, 146, 33, 0, 19, 1487177890, NULL, NULL, 0),
(224, 6, 2, 35, 'Запиши три дроби, що дорівнюють дробу 18/48, знаменники яких менші від знаменника даного дробу.', NULL, NULL, 1, 147, 33, 0, 19, 1487240557, NULL, NULL, 0),
(225, 6, 2, 35, 'Заміни дробом, знаменник якого дорівнює 36, кожний з дробів:', NULL, NULL, 1, 148, 33, 0, 19, 1487242303, NULL, NULL, 0),
(226, 6, 2, 35, 'Заміни дробом, знаменник якого дорівнює 24, кожний з дробів:', NULL, NULL, 1, 149, 33, 0, 19, 1487242541, NULL, NULL, 0),
(227, 6, 2, 35, 'Скороти дріб:', NULL, NULL, 1, 150, 33, 0, 19, 1487242717, NULL, NULL, 0),
(228, 6, 2, 35, 'Скороти дріб:', NULL, NULL, 1, 151, 34, 0, 19, 1487242963, NULL, NULL, 0),
(229, 6, 2, 35, 'Знайди НСД чисельника і знаменника кожного з дробів та скороти дроби:', NULL, NULL, 1, 152, 34, 0, 19, 1487243410, NULL, NULL, 0),
(230, 6, 2, 35, 'Випиши з поданих нижче дробів ті, які можна скоротити, і скороти їх:', NULL, NULL, 1, 153, 34, 0, 19, 1487243601, NULL, NULL, 0),
(231, 6, 2, 35, 'Випиши дроби, які можна скоротити, і скороти їх:', NULL, NULL, 1, 154, 34, 0, 19, 1487243637, NULL, NULL, 0),
(232, 6, 2, 35, 'Запиши відсотки звичайним нескоротним дробом:', NULL, NULL, 1, 155, 34, 0, 19, 1487243662, NULL, NULL, 0),
(233, 6, 2, 35, 'Віднови запис:', NULL, NULL, 1, 156, 34, 0, 19, 1487243770, NULL, NULL, 0),
(234, 6, 2, 35, 'Накресли координатний промінь, узявши за одиничний відрізок 24 клітинки зошита. Познач на промені точки з даними координатами. Які з цих точок зображуються на координатному промені однією і тією самою точкою? Запиши відповідні рівності.', NULL, NULL, 1, 157, 34, 0, 19, 1487243990, 1487244218, 19, 0),
(235, 6, 2, 35, 'Накресли координатний промінь, узявши за одиничний відрізок 18 клітинок. Познач на промені точки з  даними координатами. Які з цих точок зображуються на координатному промені однією і тією самою точкою? Запиши відповідні рівності.', NULL, NULL, 1, 158, 34, 0, 19, 1487244401, 1487244553, 19, 0),
(236, 6, 3, 35, 'Запиши десятковий дріб у вигляді звичайного і результат, якщо можливо, скороти:', NULL, NULL, 1, 159, 35, 0, 19, 1487244609, NULL, NULL, 0),
(237, 6, 3, 35, 'Запиши десятковий дріб у вигляді звичайного і результат, якщо можливо, скороти:', NULL, NULL, 1, 160, 35, 0, 19, 1487244863, NULL, NULL, 0),
(238, 6, 3, 35, 'Дай відповідь у вигляді нескоротного дробу:', NULL, NULL, 1, 161, 35, 0, 19, 1487245053, NULL, NULL, 0),
(239, 6, 3, 35, 'Дай відповідь у вигляді нескоротного дробу:', NULL, NULL, 1, 162, 35, 0, 19, 1487245232, NULL, NULL, 0),
(240, 6, 3, 35, 'Виконай дії і результат скороти:', NULL, NULL, 1, 163, 35, 0, 19, 1487245345, NULL, NULL, 0),
(241, 6, 3, 35, '(Усно) Спочатку дріб скоротили на 2, потім — на 3, а потім — на 7 й отримали нескоротний дріб. На яке число можна було одразу скоротити дріб?', NULL, NULL, 1, 164, 35, 0, 19, 1487246110, NULL, NULL, 0),
(242, 6, 3, 35, 'Використовуючи основну властивість дробу, знайди х:', NULL, NULL, 1, 165, 35, 0, 19, 1487246171, NULL, NULL, 0),
(243, 6, 3, 35, 'Використовуючи основну властивість дробу, знайди у:', NULL, NULL, 1, 166, 35, 0, 19, 1487246464, NULL, NULL, 0),
(244, 6, 4, 35, 'Розв’яжи рівняння:', NULL, NULL, 1, 167, 35, 0, 19, 1487246776, NULL, NULL, 0),
(245, 6, 4, 35, 'Дріб a/54 скоротили на 9 і отримали 5/b. Знайди значення а і b.', NULL, NULL, 1, 168, 36, 0, 19, 1487248987, NULL, NULL, 0),
(246, 6, 4, 35, 'Дріб 18/x скоротили на 2 і отримали y/13. Знайди значення х і у.', NULL, NULL, 1, 169, 36, 0, 19, 1487249145, NULL, NULL, 0),
(247, 6, 5, 35, 'Скороти дріб (буквами позначено натуральні числа):', NULL, NULL, 1, 170, 36, 0, 19, 1487249206, NULL, NULL, 0),
(248, 6, 1, 35, 'Порівняй дроби :', NULL, NULL, 1, 171, 36, 0, 19, 1487249385, NULL, NULL, 0),
(249, 6, 2, 35, 'Знайди:', NULL, NULL, 1, 172, 36, 0, 19, 1487249590, NULL, NULL, 0),
(250, 6, 3, 35, 'Сторони прямокутника дорівнюють 8 см і 12 см. Знайди площу квадрата, периметр якого на 4 см більший за периметр прямокутника.', NULL, NULL, 1, 173, 36, 0, 19, 1487249687, NULL, NULL, 0),
(251, 6, 4, 35, 'Маленька коробка вміщує 12 олівців, а велика — 18 олівців. Відомо, що Ірина може розкласти всі свої олівці як у маленькі коробки, так і у великі. Скільки олівців у Ірини, якщо їх більше ніж 57, але менше ніж 80?', NULL, NULL, 1, 174, 36, 0, 19, 1487249794, NULL, NULL, 0),
(252, 6, 6, 35, '24 серпня 1991 року позачерговою сесією Верхов¬ної Ради УРСР було прийнято Акт проголошення неза¬лежності України. Скільки днів пройшло з цієї дати до сьогоднішнього дня?', NULL, NULL, 1, 175, 36, 0, 19, 1487249830, NULL, NULL, 0),
(253, 6, 1, 36, '(Усно) Зведи до знаменника 12 дріб:', NULL, NULL, 1, 176, 39, 0, 19, 1487257088, NULL, NULL, 0),
(254, 6, 1, 36, 'Зведи 2/7 дріб до знаменника:', NULL, NULL, 1, 177, 39, 0, 19, 1487257795, NULL, NULL, 0),
(255, 6, 1, 36, 'Зведи дріб 3/5 до знаменника:', NULL, NULL, 1, 178, 39, 0, 19, 1487257926, NULL, NULL, 0),
(256, 6, 1, 36, 'Назви найменший спільний знаменник дробів:', NULL, NULL, 1, 179, 39, 0, 19, 1487258013, NULL, NULL, 0),
(257, 6, 1, 36, '(Усно) Порівняй:', NULL, NULL, 1, 180, 39, 0, 19, 1487258362, NULL, NULL, 0),
(258, 6, 2, 36, 'Зведи до найменшого спільного знаменника дроби:', NULL, NULL, 1, 181, 39, 0, 19, 1487258651, NULL, NULL, 0),
(259, 6, 2, 36, 'Зведи до найменшого спільного знаменника дроби:', NULL, NULL, 1, 182, 39, 0, 19, 1487261216, NULL, NULL, 0),
(260, 6, 2, 36, 'Порівняй дроби:', NULL, NULL, 1, 183, 39, 0, 19, 1487261515, NULL, NULL, 0),
(261, 6, 2, 36, 'Порівняй дроби:', NULL, NULL, 1, 184, 39, 0, 19, 1487261746, NULL, NULL, 0),
(262, 6, 2, 36, 'Що більше:', NULL, NULL, 1, 185, 39, 0, 19, 1487261907, NULL, NULL, 0),
(263, 6, 2, 36, 'Що менше:', NULL, NULL, 1, 186, 39, 0, 19, 1487262030, NULL, NULL, 0),
(264, 6, 2, 36, 'Зведи до найменшого спільного знаменника дроби:', NULL, NULL, 1, 187, 40, 0, 19, 1487262180, NULL, NULL, 0),
(265, 6, 2, 36, 'Зведи до найменшого спільного знаменника дроби:', NULL, NULL, 1, 188, 40, 0, 19, 1487262297, NULL, NULL, 0),
(266, 6, 2, 36, 'Розмісти в порядку зростання дроби і прочитай прізвище видатного українського письменника:', NULL, NULL, 1, 189, 40, 0, 19, 1487262470, 1487262496, 19, 0),
(267, 6, 2, 36, 'Розмісти в порядку зростання дроби та прочитай назву першої столиці України:', NULL, NULL, 1, 190, 40, 0, 19, 1487262557, NULL, NULL, 0),
(268, 6, 2, 36, 'Накресли координатний промінь, узявши за одиничний відрізок 16 клітинок. Познач на промені точки, яким відповідають  дані числа. Запиши ці числа в порядку спадання.', NULL, NULL, 1, 191, 40, 0, 19, 1487262726, NULL, NULL, 0),
(269, 6, 3, 36, 'Знайди найменше спільне кратне знаменників дробів, і розкладанням їх на прості множники, а потім зведи ці дроби до найменшого спільного знаменника.', NULL, NULL, 1, 192, 40, 0, 19, 1487262965, NULL, NULL, 0),
(270, 6, 3, 36, 'Знайди найменше спільне кратне знаменників дробів, розкладанням їх на прості множники, а потім зведи ці дроби до найменшого спільного знаменника.', NULL, NULL, 1, 193, 40, 0, 19, 1487263128, NULL, NULL, 0),
(271, 6, 3, 36, 'Розмісти в порядку спадання:', NULL, NULL, 1, 194, 40, 0, 19, 1487263249, NULL, NULL, 0),
(272, 6, 3, 36, 'Порівняй:', NULL, NULL, 1, 195, 40, 0, 19, 1487263279, NULL, NULL, 0),
(273, 6, 3, 36, 'Порівняй:', NULL, NULL, 1, 196, 40, 0, 19, 1487263385, NULL, NULL, 0),
(274, 6, 3, 36, 'Один робітник виготовляє 54 однакові деталі за 7 год, а другий — 23 такі самі деталі за 3 год. Який робітник виготовляє більше деталей за годину?', NULL, NULL, 1, 197, 41, 0, 19, 1487599012, 1487599044, 19, 0),
(275, 6, 3, 36, 'Знайди всі натуральні значення х, при яких є правильною нерівність:', NULL, NULL, 1, 198, 41, 0, 19, 1487599108, NULL, NULL, 0),
(276, 6, 3, 36, 'Знайди всі натуральні значення у, при яких є правильною нерівність:', NULL, NULL, 1, 199, 41, 0, 19, 1487599609, NULL, NULL, 0),
(277, 6, 4, 36, 'Знайди будь-яких два дроби, кожний з яких:', NULL, NULL, 1, 200, 41, 0, 19, 1487599728, NULL, NULL, 0),
(278, 6, 4, 36, 'Знайди будь-яких два дроби, кожний з яких більший за 1/5, але менший від 1/3.', NULL, NULL, 1, 201, 41, 0, 19, 1487599881, NULL, NULL, 0),
(279, 6, 2, 36, 'Виконай дії:', NULL, NULL, 1, 202, 41, 0, 19, 1487600017, NULL, NULL, 0),
(280, 6, 3, 36, 'З двох міст одночасно назустріч один одному вийшли два потяги. Швидкість одного 58 км/год, а швидкість другого становить 90 % від швидкості першого. Потяги зустрілися через 2,5 год. Знайди відстань між містами.', NULL, NULL, 1, 203, 41, 0, 19, 1487600293, NULL, NULL, 0),
(281, 6, 4, 36, 'При якому найменшому трицифровому натуральному значенні х значення виразу 157 + х є кратним числу 10?', NULL, NULL, 1, 204, 41, 0, 19, 1487600378, NULL, NULL, 0),
(282, 6, 6, 36, 'Чи можна з прямокутних паралелепіпедів, лінійні виміри яких дорівнюють 1 см, 2 см і 3 см, скласти куб, ребро якого дорівнює 5 см?', NULL, NULL, 1, 205, 41, 0, 19, 1487600444, NULL, NULL, 0),
(283, 6, 1, 37, '(Усно) Знайди значення виразу:', NULL, NULL, 1, 206, 43, 0, 19, 1487603836, NULL, NULL, 0),
(284, 6, 1, 37, 'Обчисли:', NULL, NULL, 1, 207, 43, 0, 19, 1487604054, NULL, NULL, 0),
(285, 6, 1, 37, 'Обчисли:', NULL, NULL, 1, 208, 43, 0, 19, 1487604615, NULL, NULL, 0),
(286, 6, 1, 37, 'Магазин першого дня продав 1/3, а другого −  1/2 всіх фруктів, які надійшли з бази. Яку частину фруктів продав магазин за два дні?', NULL, NULL, 1, 209, 43, 0, 19, 1487604970, NULL, NULL, 0),
(287, 6, 1, 37, 'Оля за перший день прочитала 2/5, а за другий — 3/10 всієї книжки. Яку частину книжки Оля прочитала за два дні?', NULL, NULL, 1, 210, 43, 0, 19, 1487605164, NULL, NULL, 0),
(288, 6, 2, 37, 'Обчисли:', NULL, NULL, 1, 211, 44, 0, 19, 1487605200, NULL, NULL, 0),
(289, 6, 2, 37, 'Знайди значення виразу:', NULL, NULL, 1, 212, 44, 0, 19, 1487605541, NULL, NULL, 0),
(290, 6, 2, 37, 'Першого дня до школи завезли 5/9 від замовленої кількості підручників, а другого — на  1/6 менше. Яку частину завезли другого дня? Чи завезли всі підручники за ці два дні?', NULL, NULL, 1, 213, 44, 0, 19, 1487606139, NULL, NULL, 0),
(291, 6, 2, 37, 'Сергій виконав домашнє завдання з математики за 7/15 год, а на виконання домашнього завдання з української мови витратив на 1/12 год менше. Скільки часу витратив Сергій на виконання завдань з цих двох предметів разом?', NULL, NULL, 1, 214, 44, 0, 19, 1487606207, NULL, NULL, 0),
(292, 6, 2, 37, 'Обчисли:', NULL, NULL, 1, 215, 44, 0, 19, 1487606256, NULL, NULL, 0),
(293, 6, 2, 37, 'Обчисли:', NULL, NULL, 1, 216, 44, 0, 19, 1487606604, NULL, NULL, 0),
(294, 6, 2, 37, 'В одному глечику 12/25 л молока, а в другому — 9/20 лмолока. В якому глечику молока більше і на скільки?', NULL, NULL, 1, 217, 44, 0, 19, 1487606710, NULL, NULL, 0),
(295, 6, 2, 37, 'Обчисли:', NULL, NULL, 1, 218, 44, 0, 19, 1487606745, NULL, NULL, 0),
(296, 6, 2, 37, 'Обчисли:', NULL, NULL, 1, 219, 44, 0, 19, 1487606836, NULL, NULL, 0),
(297, 6, 2, 37, 'Знайди значення виразу:', NULL, NULL, 1, 220, 44, 0, 19, 1487606960, NULL, NULL, 0),
(298, 6, 2, 37, 'Знайди значення виразу:', NULL, NULL, 1, 221, 45, 0, 19, 1487607082, NULL, NULL, 0),
(299, 6, 2, 37, 'За першу годину автомобіль проїхав 1/3 усієї відстані, яку мав проїхати, за другу годину — 2/5 усієї відстані, а за третю — решту шляху. Яку частину всієї відстані проїхав автомобіль за третю годину?', NULL, NULL, 1, 222, 45, 0, 19, 1487607184, NULL, NULL, 0),
(300, 6, 2, 37, 'На сніданок Вінні-Пух з’їв 2/9 л меду, на обід − 7/15 л, а на вечерю — решту. Скільки літрів меду з’їв Вінні-Пух на вечерю?', NULL, NULL, 1, 223, 45, 0, 19, 1487607226, NULL, NULL, 0),
(301, 6, 2, 37, 'Розв’яжи рівняння:', NULL, NULL, 1, 224, 46, 0, 19, 1487607265, NULL, NULL, 0),
(302, 6, 3, 37, 'Обчисли значення виразу, використовуючи властивості додавання і віднімання:', NULL, NULL, 1, 225, 45, 0, 19, 1487607409, NULL, NULL, 0),
(303, 6, 3, 37, 'Обчисли значення виразу, використовуючи властивості додавання і віднімання:', NULL, NULL, 1, 226, 45, 0, 19, 1487607594, NULL, NULL, 0),
(304, 6, 3, 37, 'Периметр трикутника дорівнює 19/20 м. Одна з його сторін дорівнює 2/5 м, а друга — менша від першої на 1/10 м. Знайди довжину третьої сторони трикутника.', NULL, NULL, 1, 227, 45, 0, 19, 1487607946, NULL, NULL, 0),
(305, 6, 3, 37, 'Малюк з’їдає банку варення за 12 хв, а Карлсон — за 8 хв.', NULL, NULL, 1, 228, 45, 0, 19, 1487607994, NULL, NULL, 0),
(306, 6, 4, 37, 'Через одну трубу басейн наповнюється за 18 хв, а через другу — за 24 хв.', NULL, NULL, 1, 229, 46, 0, 19, 1487608126, NULL, NULL, 0),
(307, 6, 4, 37, 'З двох міст одночасно назустріч один одному вийшли два потяги. Відстань між цими містами перший потяг долає за 3 год, а другий — за 4 год.', NULL, NULL, 1, 230, 46, 0, 19, 1487608235, 1487608317, 19, 0),
(308, 6, 4, 37, 'Як зміниться різниця, якщо:', NULL, NULL, 1, 231, 46, 0, 19, 1487608367, NULL, NULL, 0),
(309, 6, 5, 37, 'Обчисли значення виразу:', NULL, NULL, 1, 232, 46, 0, 19, 1487608450, 1487608479, 19, 0),
(310, 6, 3, 37, 'Периметр рівнобедреного трикутника 12,8 см. Довжина однієї сторони становить 3/8 периметра, а дві інші сторони рівні між собою. Знайди довжини сторін трикутника.', NULL, NULL, 1, 233, 46, 0, 19, 1487608561, NULL, NULL, 0),
(311, 6, 4, 37, 'У числі 20*06* заміни зірочки цифрами так, щоб це число ділилося на 5 і на 9 і не ділилося на 10.', NULL, NULL, 1, 234, 46, 0, 19, 1487608606, NULL, NULL, 0),
(312, 6, 6, 37, '.', NULL, NULL, 1, 235, 46, 0, 19, 1487608644, NULL, NULL, 0),
(313, 6, 1, 38, '(Усно) Обчисли:', NULL, NULL, 1, 236, 48, 0, 19, 1487671278, NULL, NULL, 0),
(314, 6, 2, 38, 'Виконай додавання:', NULL, NULL, 1, 237, 49, 0, 19, 1487671547, NULL, NULL, 0),
(315, 6, 2, 38, 'Виконай додавання:', NULL, NULL, 1, 238, 49, 0, 19, 1487671664, NULL, NULL, 0),
(316, 6, 2, 38, 'Виконай віднімання:', NULL, NULL, 1, 239, 49, 0, 19, 1487671794, NULL, NULL, 0),
(317, 6, 2, 38, 'Виконай віднімання:', NULL, NULL, 1, 240, 49, 0, 19, 1487672026, NULL, NULL, 0),
(318, 6, 2, 38, 'Стрічку розрізали на дві частини завдовжки 4 1/2 м і 3 1/4 м. Якою була довжина стрічки? На скільки довжина однієї частини виявилася більшою за довжину другої?', NULL, NULL, 1, 241, 49, 0, 19, 1487672368, NULL, NULL, 0),
(319, 6, 2, 38, 'Туристи за першу годину пройшли 4 3/10 км, а за другу — на 1 1/5 км менше. Скільки кілометрів подолали туристи за дві години?', NULL, NULL, 1, 242, 49, 0, 19, 1487672410, NULL, NULL, 0),
(320, 6, 2, 38, 'На одному автомобілі 4 7/10 т вантажу, а на іншому — на 1 3/5 т менше. Скільки тонн вантажу на обох автомобілях разом?', NULL, NULL, 1, 243, 49, 0, 19, 1487672443, NULL, NULL, 0),
(321, 6, 2, 38, 'Знайди значення виразу:', NULL, NULL, 1, 244, 49, 0, 19, 1487672483, NULL, NULL, 0),
(322, 6, 2, 38, 'Знайди значення виразу:', NULL, NULL, 1, 244, 49, 0, 19, 1487672484, NULL, NULL, 1),
(323, 6, 2, 38, 'Обчисли:', NULL, NULL, 1, 245, 49, 0, 19, 1487675064, NULL, NULL, 0),
(324, 6, 2, 38, 'Обчисли', NULL, NULL, 1, 246, 50, 0, 19, 1487675257, NULL, NULL, 0),
(325, 6, 2, 38, 'Знайди значення суми', NULL, NULL, 1, 247, 50, 0, 19, 1487675323, NULL, NULL, 0),
(326, 6, 2, 38, 'Виконай віднімання:', NULL, NULL, 1, 248, 50, 0, 19, 1487675396, NULL, NULL, 0),
(327, 6, 2, 38, 'Обчисли:', NULL, NULL, 1, 249, 50, 0, 19, 1487675640, NULL, NULL, 0),
(328, 6, 2, 38, 'Власна швидкість човна дорівнює 13 5/12  км/год, а швидкість течії — 1 3/4 км/год. Знайди швидкість човна за течією і проти течії.', NULL, NULL, 1, 250, 50, 0, 19, 1487675947, NULL, NULL, 0),
(329, 6, 2, 38, 'Розв’яжи рівняння:', NULL, NULL, 1, 251, 50, 0, 19, 1487675990, NULL, NULL, 0),
(330, 6, 2, 38, 'Розв’яжи рівняння:', NULL, NULL, 1, 252, 50, 0, 19, 1487676087, NULL, NULL, 0),
(331, 6, 2, 38, 'Кіт Матроскін вранці надоїв від корови 7 1/2 л молока, в обід — на 4/5 л менше ніж вранці, а ввечері — 6 1/10 л. Яким виявився денний надій молока?', NULL, NULL, 1, 253, 50, 0, 19, 1487676413, NULL, NULL, 0),
(332, 6, 2, 38, 'У двох мішках разом 18 3/4 кг борошна, причому в першому 8 5/8 кг. У якому мішку більше борошна і на скільки?', NULL, NULL, 1, 254, 50, 0, 19, 1487676456, NULL, NULL, 0),
(333, 6, 2, 38, 'Мотоцикліст проїхав за дві години 82 3/5 км, причому за першу годину 41 7/10 км. За яку годину мотоцикліст проїхав більшу відстань і на скільки?', NULL, NULL, 1, 255, 50, 0, 19, 1487676517, NULL, NULL, 0),
(334, 6, 3, 38, 'Мавпочка Абу придбала три ящики бананів. У першому ящику 7 1/4 кг бананів, у другому — на 3/5 кг більше, ніж у першому, а в третьому − на 1 1/2 кг менше, ніж у другому. Скільки кілограмів бананів придбала мавпочка Абу?', NULL, NULL, 1, 256, 51, 0, 19, 1487676593, NULL, NULL, 0),
(335, 6, 3, 38, 'Одна сторона	трикутника  дорівнює 11 3/5 дм,  друга - на 2 17/20 дм коротша від першої, а третя — на 1 1/2 дм довша за другу Знайди периметр трикутника.', NULL, NULL, 1, 257, 51, 0, 19, 1487676648, NULL, NULL, 0),
(336, 6, 3, 38, 'Обчисли значення виразу, використовуючи властивості додавання:', NULL, NULL, 1, 258, 51, 0, 19, 1487676686, NULL, NULL, 0),
(337, 6, 3, 38, 'Знайди значення виразу:', NULL, NULL, 1, 259, 51, 0, 19, 1487677215, NULL, NULL, 0),
(338, 6, 3, 38, 'Знайди значення виразу:', NULL, NULL, 1, 260, 51, 0, 19, 1487677423, NULL, NULL, 0),
(339, 6, 3, 38, 'Розв\'яжи рівняння:', NULL, NULL, 1, 261, 51, 0, 19, 1487677829, NULL, NULL, 0),
(340, 6, 3, 38, 'Розв’яжи рівняння:', NULL, NULL, 1, 262, 51, 0, 19, 1487677965, NULL, NULL, 0),
(341, 6, 3, 38, 'Перетвори десяткові дроби у звичайні й обчисли:', NULL, NULL, 1, 263, 52, 0, 19, 1487678081, NULL, NULL, 0),
(342, 6, 3, 38, 'Перетвори десяткові дроби у звичайні й обчисли:', NULL, NULL, 1, 264, 52, 0, 19, 1487678226, NULL, NULL, 0),
(343, 6, 3, 38, 'Одна сторона прямокутника дорівнює 10 5/8 дм, а друга — на 1 3/4 дм довша. Знайди периметр прямокутника.', NULL, NULL, 1, 265, 52, 0, 19, 1487678404, NULL, NULL, 0),
(344, 6, 3, 38, 'В одному мішку було 12 2/5 кг борошна. Коли із цього мішка пересипали в другий мішок 1 7/10 кг, то борошна в обох мішках стало порівну. Скільки кілограмів борошна було в другому мішку спочатку?', NULL, NULL, 1, 266, 52, 0, 19, 1487678454, NULL, NULL, 0),
(345, 6, 4, 38, 'За 3 год велосипедист проїхав 35 1/10   км. За перші 2 год він проїхав 24 2/25 км, а за останні 2 год — 23 4/5 км. Скільки кілометрів долав велосипедист щогодини?', NULL, NULL, 1, 267, 52, 0, 19, 1487678604, NULL, NULL, 0),
(346, 6, 4, 38, 'Сашко, Дмитро та Сергій разом зібрали 9 1/2 кг суниць. Сашко і Дмитро разом зібрали 5 7/8 кг суниць, а Дмитро і Сергій — 5 3/10 кг суниць. По скільки кілограмів суниць зібрав кожен із хлопців?', NULL, NULL, 1, 268, 52, 0, 19, 1487678674, NULL, NULL, 0),
(347, 6, 4, 38, 'Як зміниться сума двох чисел, якщо:', NULL, NULL, 1, 269, 52, 0, 19, 1487678744, NULL, NULL, 0),
(348, 6, 3, 38, 'Знайди значення виразу:', NULL, NULL, 1, 270, 52, 0, 19, 1487679215, NULL, NULL, 0),
(349, 6, 4, 38, 'Яке число треба підставити замість а, щоб коренем рівняння (х + 5) • а = 75 було число 20?', NULL, NULL, 1, 271, 53, 0, 19, 1487679315, NULL, NULL, 0),
(350, 6, 4, 38, 'Число а при діленні на 5 дає в остачі 2. Яку остачу при діленні на 5 дає число b, якщо сума а + b ділиться на 5?', NULL, NULL, 1, 272, 53, 0, 19, 1487679354, NULL, NULL, 0),
(351, 6, 6, 38, '.', NULL, NULL, 1, 273, 53, 0, 19, 1487679413, NULL, NULL, 0),
(352, 6, 1, 39, 'Прочитай: 0,5; 1,13; 0,(7); 3,1(2); 4,2(37).', NULL, NULL, 1, 274, 56, 0, 19, 1487689182, 1487690294, 19, 0),
(353, 6, 1, 39, 'Перевір, що:', NULL, NULL, 1, 275, 56, 0, 19, 1487690784, NULL, NULL, 0),
(354, 6, 1, 39, 'Перевір, що:', NULL, NULL, 1, 276, 56, 0, 19, 1487691972, NULL, NULL, 0),
(355, 6, 2, 39, 'Перетвори у десятковий дріб дане число (у випадку отримання нескінченного дробу ділення припини після визначення періоду):', NULL, NULL, 1, 277, 56, 0, 19, 1487692120, NULL, NULL, 0),
(356, 6, 2, 39, 'Перетвори у десятковий  дріб дане число (у випадку отримання нескінченного дробу ділення припини після визначення періоду):', NULL, NULL, 1, 278, 56, 0, 19, 1487692439, NULL, NULL, 0),
(357, 6, 2, 39, 'Перетвори звичайний дріб у десятковий і обчисли:', NULL, NULL, 1, 279, 56, 0, 19, 1487692664, NULL, NULL, 0),
(358, 6, 2, 39, 'Перетвори звичайний дріб у десятковий і обчисли:', NULL, NULL, 1, 280, 57, 0, 19, 1487692787, NULL, NULL, 0),
(359, 6, 3, 39, 'Запиши у вигляді нескінченного десяткового періодичного дробу частку:', NULL, NULL, 1, 281, 57, 0, 19, 1487694840, NULL, NULL, 0),
(360, 6, 3, 39, 'Запиши у вигляді нескінченного десяткового періодичного дробу частку:', NULL, NULL, 1, 282, 57, 0, 19, 1487694996, NULL, NULL, 0),
(361, 6, 3, 39, 'Порівняй дроби, записавши попередньо звичайні дроби у вигляді десяткових дробів:', NULL, NULL, 1, 283, 57, 0, 19, 1487695251, NULL, NULL, 0),
(362, 6, 3, 39, 'Порівняй дроби, записавши попередньо звичайні дроби у вигляді десяткових дробів:', NULL, NULL, 1, 284, 57, 0, 19, 1487764537, NULL, NULL, 0),
(363, 6, 4, 39, 'Червону стрічку, довжина якої 25 м, розрізали на 7 однакових частин, а зелену стрічку, довжина якої 39 м, розрізали на 11 однакових частин. Довжина якої з отриманих частин більша: червоної чи зеленої?', NULL, NULL, 1, 285, 57, 0, 19, 1487765064, NULL, NULL, 0),
(364, 6, 2, 39, 'Округли десяткові дроби:', NULL, NULL, 1, 286, 57, 0, 19, 1487765538, NULL, NULL, 0),
(365, 6, 3, 39, '.', NULL, NULL, 1, 287, 57, 0, 19, 1487765727, NULL, NULL, 0),
(366, 6, 6, 39, 'Чи можна число 1 подати у вигляді суми дробів, де a, b, c, d — непарні натуральні числа?', NULL, NULL, 1, 288, 57, 0, 19, 1487765844, 1487765860, 19, 0),
(367, 6, 1, 40, 'Відомо, що 3 1/6=3,1666… і 47/55=0,854545… . Знайди десяткове наближення кожного з цих звичайних дробів, округлене до:', NULL, NULL, 1, 289, 59, 0, 19, 1487844120, NULL, NULL, 0),
(368, 6, 1, 40, 'Відомо, що 7 6/11=7,545454… . Знайди десяткове наближення цього звичайного дробу, округлене до:', NULL, NULL, 1, 290, 59, 0, 19, 1487844225, NULL, NULL, 0),
(369, 6, 2, 40, 'Знайди десяткове наближення дробу, округлене до сотих:', NULL, NULL, 1, 291, 59, 0, 19, 1487844313, NULL, NULL, 0),
(370, 6, 2, 40, 'Знайди десяткове наближення дробу, округлене до десятих:', NULL, NULL, 1, 292, 59, 0, 19, 1487845306, NULL, NULL, 0),
(371, 6, 2, 40, 'Велосипедист проїхав 43 км за 3 год. Знайди його швидкість. (Відповідь округли до тисячних км/год).', NULL, NULL, 1, 293, 59, 0, 19, 1487845469, NULL, NULL, 0),
(372, 6, 2, 40, 'Поїзд проїхав 307 км за 6 год. Знайди його швидкість. (Відповідь округли до сотих км/год.)', NULL, NULL, 1, 294, 59, 0, 19, 1487845509, NULL, NULL, 0),
(373, 6, 2, 40, 'Знайди корінь рівняння і округли його до сотих:', NULL, NULL, 1, 295, 59, 0, 19, 1487845571, NULL, NULL, 0),
(374, 6, 2, 40, 'Знайди корінь рівняння і округли його до сотих:', NULL, NULL, 1, 296, 59, 0, 19, 1487845683, NULL, NULL, 0),
(375, 6, 3, 40, 'Площа прямокутної ділянки землі 2730 м^2, а довжина однієї зі сторін 55 м. Знайди довжину іншої сторони. (Відповідь округли до сотих метра.)', NULL, NULL, 1, 297, 59, 0, 19, 1487845767, NULL, NULL, 0),
(376, 6, 3, 40, 'Обчисли висоту прямокутного паралелепіпеда, якщо його об’єм дорівнює 14 дм^3, довжина — 3 дм, а ширина — 2,5 дм. (Відповідь округли до десятих дециметра.)', NULL, NULL, 1, 298, 59, 0, 19, 1487845856, NULL, NULL, 0);
INSERT INTO `task` (`id`, `class`, `level_id`, `theme_id`, `text`, `result`, `explanation`, `type`, `number`, `sheet`, `push_status`, `created_by`, `created_at`, `updated_at`, `updated_by`, `deleted`) VALUES
(377, 6, 3, 40, 'Велосипедист за першу годину проїхав 12 1/5км, за другу — 13 3/4 км, а за третю — 14 1/2 км. Знайди середню швидкість руху велосипедиста. (Відповідь округли до десятих км/год.)', NULL, NULL, 1, 299, 59, 0, 19, 1487845896, 1487845933, 19, 0),
(378, 6, 4, 40, 'Округли періодичні дроби до сотих і обчисли:', NULL, NULL, 1, 300, 60, 0, 19, 1487845961, NULL, NULL, 0),
(379, 6, 2, 40, 'Скороти дріб:', NULL, NULL, 1, 301, 60, 0, 19, 1487846129, NULL, NULL, 0),
(380, 6, 2, 40, 'Обчисли:', NULL, NULL, 1, 302, 60, 0, 19, 1487846300, NULL, NULL, 0),
(381, 6, 6, 40, 'Чи можна число 64 подати у вигляді суми трьох простих чисел?', NULL, NULL, 1, 303, 60, 0, 19, 1487846506, NULL, NULL, 0),
(382, 6, 1, 41, '(Усно) Обчисли:', NULL, NULL, 1, 304, 63, 0, 19, 1487846649, 1487846811, 19, 0),
(383, 6, 1, 41, 'Обчисли значення виразу:', NULL, NULL, 1, 305, 64, 0, 19, 1487847752, 1487849355, 19, 0),
(384, 6, 1, 41, 'Знайди значення виразу:', NULL, NULL, 1, 306, 64, 0, 19, 1487848959, 1487849319, 19, 0),
(385, 6, 1, 41, 'Знайди периметр і площу квадрата, сторона якого дорівнює 1/10 м.', NULL, NULL, 1, 307, 64, 0, 19, 1487849309, NULL, NULL, 0),
(386, 6, 2, 41, '(Усно) Обчисли, використовуючи переставну та сполучну властивості множення:', NULL, NULL, 1, 308, 64, 0, 19, 1487849390, NULL, NULL, 0),
(387, 6, 2, 41, 'Виконай множення:', NULL, NULL, 1, 309, 64, 0, 19, 1487849531, NULL, NULL, 0),
(388, 6, 2, 41, 'Виконай множення:', NULL, NULL, 1, 310, 64, 0, 19, 1487849684, NULL, NULL, 0),
(389, 6, 2, 41, 'Обчисли:', NULL, NULL, 1, 311, 64, 0, 19, 1487849929, NULL, NULL, 0),
(390, 6, 2, 41, 'Обчисли:', NULL, NULL, 1, 312, 64, 0, 19, 1487850253, NULL, NULL, 0),
(391, 6, 2, 41, 'Який шлях подолає мотоцикліст за 5/12 год, якщо його швидкість 42 км/год?', NULL, NULL, 1, 313, 65, 0, 19, 1487850398, NULL, NULL, 0),
(392, 6, 2, 41, '1 кг печива коштує 25 1/5 — гри. Скільки коштує 1/4 кг, 1/2 кг, 3/4 кг, 7/10 кг цього печива?', NULL, NULL, 1, 314, 65, 0, 19, 1487850448, NULL, NULL, 0),
(393, 6, 2, 41, 'Виконай множення:', NULL, NULL, 1, 315, 65, 0, 19, 1487850518, NULL, NULL, 0),
(394, 6, 2, 41, 'Виконай множення:', NULL, NULL, 1, 316, 65, 0, 19, 1487850897, NULL, NULL, 0),
(395, 6, 2, 41, 'Знайди об’єм прямокутного паралелепіпеда, якщо його виміри дорівнюють:', NULL, NULL, 1, 317, 65, 0, 19, 1487851249, NULL, NULL, 0),
(396, 6, 2, 41, 'Обчисли площу прямокутника, якщо його сторони дорівнюють:', NULL, NULL, 1, 318, 65, 0, 19, 1487851527, NULL, NULL, 0),
(397, 6, 2, 41, 'Обчисли:', NULL, NULL, 1, 319, 65, 0, 19, 1487851645, NULL, NULL, 0),
(398, 6, 2, 41, 'Обчисли:', NULL, NULL, 1, 320, 65, 0, 19, 1487851840, NULL, NULL, 0),
(399, 6, 2, 41, 'Перетвори десяткові дроби у звичайні, а потім обчисли:', NULL, NULL, 1, 321, 65, 0, 19, 1487852073, NULL, NULL, 0),
(400, 6, 2, 41, '(Усно) Обчисли, застосовуючи розподільний закон множення:', NULL, NULL, 1, 322, 65, 0, 19, 1487852301, NULL, NULL, 0),
(401, 6, 2, 41, 'Знайти масу металевої деталі, об’єм якої дорівнює 4 1/3 дм^3, якщо маса 1 дм^3 цього металу дорівнює 7 4/5 кг.', NULL, NULL, 1, 323, 66, 0, 19, 1487852551, NULL, NULL, 0),
(402, 6, 2, 41, 'Щоб потрапити в гості до крокодила Гени, Чебурашка пройшов пішки 1 1/4 км та ще проїхав на автобусі 1/3 год зі швидкістю 58 1/2 км/год. Яку відстань подолав Чебурашка?', NULL, NULL, 1, 324, 66, 0, 19, 1487852628, NULL, NULL, 0),
(403, 6, 2, 41, 'Обчисли значення виразу, використовуючи переставну та сполучну властивості множення:', NULL, NULL, 1, 325, 66, 0, 19, 1487852698, NULL, NULL, 0),
(404, 6, 2, 41, 'Обчисли значення виразу, використовуючи переставну та сполучну властивості множення:', NULL, NULL, 1, 326, 66, 0, 19, 1487854683, NULL, NULL, 0),
(405, 6, 3, 41, 'З одного міста одночасно в протилежних напрямах виїхали автомобіль і мікроавтобус. Швидкість автомобіля 72 1/10км/год, а швидкість мікроавтобуса — 61 2/5  км/год. Якою буде відстань між ними через 3 1/3  год?', NULL, NULL, 1, 327, 66, 0, 19, 1487854937, 1487854992, 19, 0),
(406, 6, 3, 41, 'З одного порту одночасно в одному напрямі вирушили катер і теплохід. Катер рухався зі швидкістю 25 2/15  км/год, а теплохід — 32 5/6  км/год. Яка відстань буде між ними через 3 4/7 год?', NULL, NULL, 1, 328, 66, 0, 19, 1487855098, NULL, NULL, 0),
(407, 6, 3, 41, 'Обчисли значення виразу, використовуючи розподільну властивість множення:', NULL, NULL, 1, 329, 66, 0, 19, 1487855258, NULL, NULL, 0),
(408, 6, 3, 41, 'Обчисли значення виразу, використовуючи розподільну властивість множення:', NULL, NULL, 1, 330, 67, 0, 19, 1487856321, NULL, NULL, 0),
(409, 6, 3, 41, 'Знайди значення виразу:', NULL, NULL, 1, 331, 67, 0, 19, 1487857108, NULL, NULL, 0),
(410, 6, 3, 41, 'Знайди значення виразу:', NULL, NULL, 1, 332, 67, 0, 19, 1487857253, NULL, NULL, 0),
(411, 6, 3, 41, 'Розкрий дужки:', NULL, NULL, 1, 333, 67, 0, 19, 1487857425, NULL, NULL, 0),
(412, 6, 3, 41, 'Спрости вираз та обчисли його значення, якщо x=2 1/7.', NULL, NULL, 1, 334, 67, 0, 19, 1487857810, NULL, NULL, 0),
(413, 6, 3, 41, 'Спрости вираз та обчисли його значення, якщо у = 30; 1 1/11.', NULL, NULL, 1, 335, 67, 0, 19, 1487857905, NULL, NULL, 0),
(414, 6, 3, 41, 'Мати купила 2 3/5 кг апельсинів за ціною 16 1/4  грн за кілограм та 4 3/4 кг яблук за ціною 10 2/5 грн. За які фрукти, апельсини чи яблука, мати заплатила більше і на скільки?', NULL, NULL, 1, 336, 67, 0, 19, 1487857972, NULL, NULL, 0),
(415, 6, 4, 41, 'Розв’яжи рівняння:', NULL, NULL, 1, 337, 67, 0, 19, 1487858040, NULL, NULL, 0),
(416, 6, 4, 41, 'Розв’яжи рівняння:', NULL, NULL, 1, 338, 67, 0, 19, 1487858083, NULL, NULL, 0),
(417, 6, 4, 41, 'Не виконуючи множення, порівняй значення виразів:', NULL, NULL, 1, 339, 68, 0, 19, 1487858223, NULL, NULL, 0),
(418, 6, 1, 41, 'Перетвори у звичайні дроби:', NULL, NULL, 1, 340, 68, 0, 19, 1487858386, NULL, NULL, 0),
(419, 6, 2, 41, 'Периметр трикутника дорівнює 7 7/15 — см. Дві сторони трикутника дорівнюють 2 5/6  см та 1 4/5 см. Знайди третю сторону трикутника. Як називають такий трикутник?', NULL, NULL, 1, 341, 68, 0, 19, 1487858628, NULL, NULL, 0),
(420, 6, 6, 41, 'Як за допомогою відер, що вміщують 5 л і 7 л, налити з крану в акваріум 6 л води?', NULL, NULL, 1, 342, 68, 0, 19, 1487858706, NULL, NULL, 0),
(421, 6, 1, 42, '(Усно) Знайди:', NULL, NULL, 1, 343, 69, 0, 19, 1487862660, NULL, NULL, 0),
(422, 6, 1, 42, 'Обчисли:', NULL, NULL, 1, 344, 70, 0, 19, 1487862954, NULL, NULL, 0),
(423, 6, 1, 42, 'Обчисли:', NULL, NULL, 1, 345, 70, 0, 19, 1487863857, NULL, NULL, 0),
(424, 6, 1, 42, 'Петро зібрав 40 грибів, з яких 3/10 — маслюки. Скільки маслюків зібрав Петро?', NULL, NULL, 1, 346, 70, 0, 19, 1487864055, NULL, NULL, 0),
(425, 6, 1, 42, 'Маса бегемота 2000 кг, а маса носорога становить 9/10 маси бегемота. Знайди масу носорога.', NULL, NULL, 1, 347, 70, 0, 19, 1487864109, NULL, NULL, 0),
(426, 6, 2, 42, 'Площа однієї кімнати 21 м^2, а площа другої становить 4/7 від площі першої кімнати. Знайди площу обох кімнат разом.', NULL, NULL, 1, 348, 70, 0, 19, 1487864182, NULL, NULL, 0),
(427, 6, 2, 42, 'Поштарка має доставити адресатам 96 листів, причому 7/12 кількості листів вона доставила до обіду. Скільки листів залишилося поштарці доставити адресатам?', NULL, NULL, 1, 349, 70, 0, 19, 1487864274, NULL, NULL, 0),
(428, 6, 2, 42, 'Дві бригади трактористів зорали 550 га землі, причому перша бригада виконала 6/11 цього обсягу. Скільки гектарів землі зорала друга бригада?', NULL, NULL, 1, 350, 70, 0, 19, 1487864434, NULL, NULL, 0),
(429, 6, 2, 42, 'Запиши звичайним дробом:', NULL, NULL, 1, 351, 70, 0, 19, 1487864462, NULL, NULL, 0),
(430, 6, 2, 42, 'Запиши у вигляді відсотків дріб:', NULL, NULL, 1, 352, 71, 0, 19, 1487864776, NULL, NULL, 0),
(431, 6, 2, 42, 'Запиши:', NULL, NULL, 1, 353, 71, 0, 19, 1487865055, NULL, NULL, 0),
(432, 6, 2, 42, 'Довжина прямокутника 24 см, а ширина становить 75 % довжини. Знайди площу прямокутника.', NULL, NULL, 1, 354, 71, 0, 19, 1487865274, NULL, NULL, 0),
(433, 6, 2, 42, 'Довжина прямокутника 25 см, а ширина становить 3/5 довжини. Обчисли периметр прямокутника.', NULL, NULL, 1, 355, 71, 0, 19, 1487865315, NULL, NULL, 0),
(434, 6, 2, 42, 'Знайко	прочитав 3/4 	книжки,	яка	має	a сторінок. Склади вираз для знаходження кількості сторінок, які прочитав Знайко, та знайди його значення, якщо a = 180.', NULL, NULL, 1, 356, 71, 0, 19, 1487865366, NULL, NULL, 0),
(435, 6, 2, 42, 'Знайди:', NULL, NULL, 1, 357, 71, 0, 19, 1487865477, NULL, NULL, 0),
(436, 6, 2, 42, 'Знайди:', NULL, NULL, 1, 358, 71, 0, 19, 1487865627, NULL, NULL, 0),
(437, 6, 2, 42, 'Морська вода містить 7 % солі. Скільки солі міститься в 340 кг морської води?', NULL, NULL, 1, 359, 71, 0, 19, 1487865813, NULL, NULL, 0),
(438, 6, 2, 42, '360.	Токар мав виготовити за день 200 деталей. Але він перевиконав завдання на 5 %. Скільки деталей виготовив токар?', NULL, NULL, 1, 360, 71, 0, 19, 1487865840, NULL, NULL, 0),
(439, 6, 2, 42, 'На базу привезли 2800 кг картоплі. З них 45 % привезли першого дня, а решту — другого. Скільки кілограмів картоплі привезли першого дня і скільки другого?', NULL, NULL, 1, 361, 71, 0, 19, 1487865881, NULL, NULL, 0),
(440, 6, 2, 42, 'Під час сушіння яблука втрачають 9/10  своєї маси. Скільки кілограмів сушених яблук вийде з 240 кг свіжих?', NULL, NULL, 1, 362, 71, 0, 19, 1487865923, NULL, NULL, 0),
(441, 6, 3, 42, 'За три дні зібрали 532 кг насіння соняшника. У перший день зібрали 2/7, у другий — 5/14 цієї маси. Скільки кілограмів насіння було зібрано в третій день?', NULL, NULL, 1, 363, 72, 0, 19, 1487865958, NULL, NULL, 0),
(442, 6, 3, 42, 'Три кондитери виготовили 270 тортів. Перший виконав 2/9  цієї роботи, другий — 5/18, а третій — решту. По скільки тортів виготовив кожен кондитер?', NULL, NULL, 1, 364, 72, 0, 19, 1487866004, NULL, NULL, 0),
(443, 6, 3, 42, 'На базі було 270 ц картоплі. Спочатку вивезли 4/9 від цієї маси, а потім — 9/30 від того, що залишилося. Скільки центнерів картоплі після цього залишилося на базі?', NULL, NULL, 1, 365, 72, 0, 19, 1487866040, NULL, NULL, 0),
(444, 6, 3, 42, 'Від стрічки довжиною 40 м спочатку відрізали 5/8 її довжини, а потім — 0,45 решти. Скільки метрів стрічки відрізали за другий раз?', NULL, NULL, 1, 366, 72, 0, 19, 1487866089, NULL, NULL, 0),
(445, 6, 3, 42, 'Автомобіль за 3 год проїхав 234 км. За першу годину він проїхав 7/18 цієї відстані, за другу — 0,4 решти. Яку відстань проїхав автомобіль за третю годину?', NULL, NULL, 1, 367, 72, 0, 19, 1487866134, NULL, NULL, 0),
(446, 6, 4, 42, 'Що більше: 18,7 % від 12,7 чи 12,7 % від 18,7?', NULL, NULL, 1, 368, 72, 0, 19, 1487866168, 1487866331, 19, 0),
(447, 6, 4, 42, 'Яке із чисел х або у є більшим (х та у не дорівнюють нулю), якщо:', NULL, NULL, 1, 369, 72, 0, 19, 1487866200, 1487866313, 19, 0),
(448, 6, 2, 42, 'Розв’яжи рівняння і виконай перевірку:', NULL, NULL, 1, 370, 72, 0, 19, 1487866374, NULL, NULL, 0),
(449, 6, 3, 42, 'Використовуючи цифри 0, 1, 2, 3, 5, запиши найбільше та найменше трицифрові числа, кратні числу 3 так, щоб цифри в кожному числі не повторювалися.', NULL, NULL, 1, 371, 72, 0, 19, 1487866492, NULL, NULL, 0),
(450, 6, 6, 42, '372.	Знайди невідомі числа та склади назву столиці європейської держави..', NULL, NULL, 1, 372, 72, 0, 19, 1487866601, NULL, NULL, 0),
(451, 6, 1, 43, '(Усно)', NULL, NULL, 1, 373, 74, 0, 19, 1487866750, NULL, NULL, 0),
(452, 6, 1, 43, 'Знайди число, обернене числу:', NULL, NULL, 1, 374, 74, 0, 19, 1487866826, NULL, NULL, 0),
(453, 6, 1, 43, 'Перетвори десятковий дріб у звичайний і знайди обернене до нього число:', NULL, NULL, 1, 375, 74, 0, 19, 1487867269, NULL, NULL, 0),
(454, 6, 1, 43, 'Знайди число, обернене числу:', NULL, NULL, 1, 376, 74, 0, 19, 1487867390, NULL, NULL, 0),
(455, 6, 2, 43, 'Чи будуть взаємно оберненими числа:', NULL, NULL, 1, 377, 74, 0, 19, 1487940879, NULL, NULL, 0),
(456, 6, 2, 43, 'Чи будуть взаємно оберненими числа:', NULL, NULL, 1, 378, 74, 0, 19, 1487941185, NULL, NULL, 0),
(457, 6, 2, 43, 'Знайди число, обернене до числа:', NULL, NULL, 1, 379, 75, 0, 19, 1487951175, NULL, NULL, 0),
(458, 6, 2, 43, 'Знайди число, обернене до числа:', NULL, NULL, 1, 380, 75, 0, 19, 1487951420, NULL, NULL, 0),
(459, 6, 2, 43, 'Розв’яжи рівняння:', NULL, NULL, 1, 381, 75, 0, 19, 1487951623, NULL, NULL, 0),
(460, 6, 2, 43, 'Розв’яжи рівняння:', NULL, NULL, 1, 382, 75, 0, 19, 1487953040, NULL, NULL, 0),
(461, 6, 2, 43, 'Обчисли:', NULL, NULL, 1, 383, 75, 0, 19, 1487953282, NULL, NULL, 0),
(462, 6, 3, 43, '(Усно) Чи правильно, що:', NULL, NULL, 1, 384, 75, 0, 19, 1487954146, NULL, NULL, 0),
(463, 6, 3, 43, 'Знайди число, обернене:', NULL, NULL, 1, 385, 75, 0, 19, 1487954672, NULL, NULL, 0),
(464, 6, 3, 43, 'Знайди число, обернене:', NULL, NULL, 1, 386, 75, 0, 19, 1487954873, NULL, NULL, 0),
(465, 6, 4, 43, 'Знайди число, обернене числу:', NULL, NULL, 1, 387, 75, 0, 19, 1487955060, NULL, NULL, 0),
(466, 6, 4, 43, 'Спрости вираз:', NULL, NULL, 1, 388, 75, 0, 19, 1489509260, NULL, NULL, 0),
(467, 6, 3, 43, 'Склади задачі за схемами та розв’яжи їх. Стрілка напрямлена до більшого числа.', NULL, NULL, 1, 389, 75, 0, 19, 1489509529, NULL, NULL, 0),
(468, 6, 6, 43, 'Як із шести сірників скласти 4 рівносторонніх трикутники зі стороною, що дорівнює довжині сірника?', NULL, NULL, 1, 390, 76, 0, 19, 1489509723, NULL, NULL, 0),
(469, 6, 1, 44, '(Усно) Обчисли:', NULL, NULL, 1, 391, 77, 0, 19, 1489675460, NULL, NULL, 0),
(470, 6, 1, 44, 'Виконай ділення:', NULL, NULL, 1, 392, 77, 0, 19, 1489675932, NULL, NULL, 0),
(471, 6, 1, 44, 'Виконай ділення:', NULL, NULL, 1, 393, 77, 0, 19, 1489676837, NULL, NULL, 0),
(472, 6, 2, 44, 'Знайди значення частки:', NULL, NULL, 1, 394, 77, 0, 19, 1489677351, NULL, NULL, 0),
(473, 6, 2, 44, 'Знайди значення частки:', NULL, NULL, 1, 395, 78, 0, 19, 1489677642, NULL, NULL, 0),
(474, 6, 2, 44, 'Обчисли:', NULL, NULL, 1, 396, 78, 0, 19, 1489677894, NULL, NULL, 0),
(475, 6, 2, 44, 'Обчисли:', NULL, NULL, 1, 397, 78, 0, 19, 1489678158, NULL, NULL, 0),
(476, 6, 2, 44, 'Обчисли:', NULL, NULL, 1, 397, 78, 0, 19, 1489678189, NULL, NULL, 1),
(477, 6, 2, 44, 'Площа прямокутника 4 4/5 м^2. Знайди його довжину, якщо ширина дорівнює 1 3/5 м.', NULL, NULL, 1, 398, 78, 0, 19, 1489678905, NULL, NULL, 0),
(478, 6, 2, 44, 'Скільки коштує 1 м тканини, якщо 1 1/2 м коштує 45 3/5 грн?', NULL, NULL, 1, 399, 78, 0, 19, 1489678951, NULL, NULL, 0),
(479, 6, 2, 44, 'Велосипедист проїхав 12 км за 4/5 год. Знайди його швидкість.', NULL, NULL, 1, 400, 78, 0, 19, 1489678988, NULL, NULL, 0),
(480, 6, 2, 44, 'З дослідницької ділянки, площа якої 4 4/5 га, зібрали 300 ц пшениці. Скільки центнерів пшениці в середньому зібрали з 1 га?', NULL, NULL, 1, 401, 78, 0, 19, 1489679022, NULL, NULL, 0),
(481, 6, 2, 44, 'Знайди значення виразу a : 9/16, якщо a=', NULL, NULL, 1, 402, 78, 0, 19, 1489679064, NULL, NULL, 0),
(482, 6, 2, 44, 'Знайди значення виразу 3/5 : x, якщо	x=', NULL, NULL, 1, 403, 78, 0, 19, 1489679171, NULL, NULL, 0),
(483, 6, 2, 44, 'Розв’яжи рівняння:', NULL, NULL, 1, 404, 78, 0, 19, 1489679299, NULL, NULL, 0),
(484, 6, 2, 45, 'ываыв', 'фывфыв', 'мыв', 1, 13, 2, 0, 19, 1491829811, NULL, NULL, 0),
(485, 6, 2, 46, 'qweqwe', 'hjdsbfjsd', 'sub gogogogo', 1, 12, 12, 0, 19, 1491836560, 1492005477, 19, 1),
(486, 6, 1, 50, 'екпрпрмприо р', NULL, NULL, 1, 2632, 99, 0, 19, 1491908161, NULL, NULL, 0),
(487, 6, 2, 49, 'qweqwe', 'asd', 'sd', 2, 123, 123, 0, 19, 1491908241, 1492003011, 19, 0),
(488, 6, 2, 53, 'qweqwe', 'qweqwe', 'qweqwe', 1, 1, 123, 0, 19, 1491909315, NULL, NULL, 1),
(489, 6, 2, 34, 'фйывраосыбткцабтьаыпьбт мпт ип', NULL, NULL, 1, 4512, 15, 0, 19, 1492000887, NULL, NULL, 0),
(490, 6, 3, 27, 'sdfwef', NULL, 'qwdqwdqwd', 1, 123, 23, 0, 19, 1492002715, NULL, NULL, 1),
(491, 6, 4, 46, 'xvbxfbz', 'dfbdfb', 'bvbxfbdfb', 1, 53446, 789, 0, 19, 1492008891, NULL, NULL, 0),
(492, 6, 1, 63, 'йцу', 'йцу', 'йцу', 1, 12, 12, 0, 19, 1492080888, NULL, NULL, 0),
(493, 6, 3, 27, '', 'qwexq', 'dhfgbkjdf', 1, 128896, 123, 0, 19, 1492161860, NULL, NULL, 1),
(494, 6, 1, 27, '', '4', 'Тест Андрей', 1, 5, 123, 0, 19, 1492161959, NULL, NULL, 1),
(495, 6, 2, 27, '', '4', 'Андрей', 1, 2, 1, 0, 19, 1492162030, NULL, NULL, 1),
(496, 6, 1, 27, '', '4', 'fint something', 1, 123, 3, 0, 19, 1492162228, NULL, NULL, 1),
(497, 6, 2, 27, '', '100500', 'POE', 1, 9379992, 999, 0, 19, 1492162392, NULL, NULL, 1),
(498, 6, 1, 27, 'POE', '1', 'Nugi', 1, 9379992, 100500, 0, 19, 1492162618, NULL, NULL, 1),
(499, 6, 3, 27, 'Text - Text', 'resolt - resolt', 'explain - expain', 1, 9379992, 12321, 0, 19, 1492162763, NULL, NULL, 1),
(500, 6, 1, 27, 'text - text', 'resolt - resolt', 'explaining - explaining', 1, 123321, 5, 0, 19, 1492162877, NULL, NULL, 1),
(501, 6, 2, 64, 'dgfhgh', '1234', 'dfgmh', 1, 987, 123, 0, 19, 1492165581, NULL, NULL, 0),
(502, 6, 1, 64, '\\sqrt{256\\cdot\\frac{5}{25^4}}', 'asdfgn', 'dfbgnhj', 1, 4686, 1325, 0, 19, 1492165807, NULL, NULL, 0),
(503, 6, 3, 64, '5\\cdot3\\sqrt{4}', 'erbtnyu', 'wrety', 1, 1345, 123, 0, 19, 1492166591, NULL, NULL, 0),
(504, 6, 2, 65, '\\sqrt{4}', '2', 'Знайти відповідь.', 1, 1, 123, 0, 19, 1492508817, NULL, NULL, 0),
(505, 6, 2, 68, '\\sqrt{4}', '2', 'Знайти відповідь', 1, 2, 1, 0, 19, 1492526212, NULL, NULL, 1),
(506, 7, 2, 68, '2^5', 'qwcertv', 'xwervtrhyjtu', 1, 1234, 1234, 0, 19, 1492527694, NULL, NULL, 1),
(507, 5, 5, 68, '\\frac{2^3\\cdot\\pi}{23\\ge4}', 'qecwf', 'ewexf', 1, 123, 123, 0, 19, 1492528816, NULL, NULL, 0),
(508, 9, 2, 68, 'яка\\ буде\\ выдповыдь\\ \\frac{16}{2}', '8', '', 1, 1234, 13, 0, 19, 1492601787, NULL, NULL, 1),
(509, 9, 2, 68, 'яка\\ буде\\ выдповыдь\\ \\frac{16}{2}', '8', 'кцук', 1, 1234, 13, 0, 19, 1492601789, NULL, NULL, 1),
(510, 9, 4, 68, 'Знайти\\ відповідь\\ на\\ поставлену\\ задачу\\ задачу.\\ \\sqrt{4}\\cdot\\frac{2}{2}.', '2', 'Пояснення', 1, 1, 2, 0, 19, 1492602765, NULL, NULL, 1),
(511, 9, 2, 68, '\\sqrt{4}\\ привет\\ 2\\cdot2\\ge3', 'вкрапо', 'мцуіпео', 1, 213, 2, 0, 19, 1492606974, NULL, NULL, 1),
(512, 9, 2, 68, '5^2\\pi', 'ertyui', 'ctjvyuk', 1, 5, 2, 0, 19, 1492614757, NULL, NULL, 0),
(513, 7, 3, 68, '2\\sqrt{2^2}\\ шо\\ будє??', 'йцукеро', 'іваиптроб', 1, 123, 123, 0, 19, 1492614801, NULL, NULL, 1),
(514, 8, 1, 68, '\\sqrt{4}', '4', 'Знайти рішенн!', 1, 1, 5, 0, 19, 1492625821, 1492773729, 19, 0),
(515, 11, 6, 68, '\\pi\\sum_2^x\\sqrt{\\left(x\\cdot y\\right)+1}\\ -\\ \\ Розв`яжи\\ це!', 'хз', 'Жесть', 1, 1, 1, 0, 19, 1492684911, NULL, NULL, 0),
(516, 9, 5, 68, 'fsdfsdf<br></span>wefwiuefhi<br></span>weiufhwiefiwejfoweohfowe<br></span>do;ghdofjgodfjgo<br></span>wceoiwhgoisdm', 'wegrebgnh', 'qfewbg', 1, 123, 123, 0, 19, 1492687343, NULL, NULL, 1),
(517, 7, 2, 68, 'wef<br></span>wefwef<br></span>wefffwefwe<br></span>sdvsds<br></span>bssb<br></span>cv\\ <br></span>2\\sum_x^n4^3', 'має бути', 'Зроби', 1, 4, 32, 0, 19, 1492688846, NULL, NULL, 1),
(518, 7, 1, 68, '\\mathbf{Дискриминант}:\\ X_{1,2}=\\frac{-b\\pm\\sqrt{b^2-4ac}}{2a}', '-', 'Дискриминант.', 1, 1, 1, 0, 19, 1492698368, NULL, NULL, 0),
(519, 9, 2, 68, 'sefsd<br></span>\\mathbf{sdfsdf}', 'qwer', 'ewfh', 1, 123, 123, 0, 19, 1492774235, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `theme`
--

CREATE TABLE `theme` (
  `id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `theme`
--

INSERT INTO `theme` (`id`, `section_id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted`) VALUES
(14, 8, 'Перша', 5, NULL, 1476371373, NULL, 0),
(15, 9, 'Topic', 19, 19, 1478272983, 1478601910, 0),
(16, 9, 'TOPIC 11', 19, 19, 1478273135, 1478515124, 0),
(17, 9, 'Topic 3', 19, NULL, 1482492229, NULL, 0),
(20, 20, '43534', 19, NULL, 1484666458, NULL, 0),
(25, 20, 'віаів', 19, NULL, 1485257905, NULL, 0),
(26, 24, '54654', 19, NULL, 1485812526, NULL, 0),
(27, 26, 'Дільники і кратні натурального числа', 19, 19, 1486478220, 1486478569, 0),
(28, 25, 'в', 19, NULL, 1487020215, NULL, 0),
(29, 26, 'Ознаки подільності на 10, 5 та 2', 19, NULL, 1487078317, NULL, 0),
(30, 26, 'Ознаки подільності на 9  та 3', 19, NULL, 1487078362, NULL, 0),
(31, 26, 'Прості та складені числа', 19, 19, 1487085904, 1487091792, 0),
(32, 26, 'Розкладання чисел на прості множники', 19, 19, 1487091033, 1487091788, 0),
(33, 26, 'Найбільший спільний дільник', 19, 19, 1487091499, 1487157217, 0),
(34, 26, 'Найменше спільне кратне', 19, NULL, 1487160799, NULL, 0),
(35, 27, '8. Основна властивість дробу. Скорочення дробу', 19, NULL, 1487172681, NULL, 0),
(36, 27, '9. Найменший спільник знаменник дробів. Зведення дробів до спільного знаменника. Порівняння дробів', 19, NULL, 1487249880, NULL, 0),
(37, 27, '10. Додавання і віднімання дробів з різними знаменниками', 19, NULL, 1487600615, NULL, 0),
(38, 27, '11. Додавання і віднімання мішаних чисел', 19, NULL, 1487608811, NULL, 0),
(39, 27, '12. Перетворення звичайних дробів у десяткові. Нескінченні періодичні десяткові дроби', 19, NULL, 1487679545, NULL, 0),
(40, 27, '13. Десяткове наближення звичайного дробу', 19, NULL, 1487766068, NULL, 0),
(41, 27, '14. Множення звичайних дробів', 19, NULL, 1487846543, NULL, 0),
(42, 27, '15. Знаходження дробу від числа', 19, 19, 1487861667, 1487861676, 0),
(43, 27, '16.  Взаємно обернені числа', 19, NULL, 1487866629, NULL, 0),
(44, 27, '17. Ділення звичайних дробів', 19, NULL, 1489674975, NULL, 0),
(45, 31, 'йцуйцуйцу', 19, NULL, 1491829762, NULL, 0),
(46, 30, 'qwe', 19, NULL, 1491836513, NULL, 0),
(47, 30, 'suydgfusduyfsudfyusydfgsyudgfysgdfysgdfyugsduyfgsyudgfuysdgf', 19, NULL, 1491906256, NULL, 1),
(48, 30, 'sdfiuhsduifhuwiehfuihwuiefhuwihefiuwhefuihwiuefhwuiehfuwheuifhwiuehfuiwehfuihwueifhwuiehfuiwhefuihweuifhwiuefhiwuehfuiwef', 19, NULL, 1491906288, NULL, 1),
(49, 30, 'uidhfgus gsuidghsuidf sduifhs idfui sduifhs dfudfh s fs dhfius dfu sdiufh siudfh sdfh wf w pfoiwe fw iewigg wiejf wiefj wefijw fw fwehfyughwi', 19, NULL, 1491906808, NULL, 1),
(50, 26, '8. Тест Тест', 19, NULL, 1491908123, NULL, 1),
(51, 26, 'тест', 19, NULL, 1491908420, NULL, 1),
(52, 26, 'вспамри', 19, NULL, 1491908482, NULL, 1),
(53, 33, 'dqwd', 19, NULL, 1491909286, NULL, 1),
(54, 33, 'лвоа', 19, NULL, 1491909456, NULL, 1),
(55, 33, 'ASasASas', 19, NULL, 1491909745, NULL, 1),
(56, 35, 'іваіваіва', 19, NULL, 1491909851, NULL, 1),
(57, 35, 'wiiegfwefuuwhef wuf wgef wuyfg wugf sudf sudf wuhf woefhwehfw efuhw eiufhwiuefhwuief wiuefh wiuefhw efuhwe fiwuehf iwuefhw e', 19, NULL, 1491910004, NULL, 0),
(58, 37, 'йцуйцу', 19, NULL, 1491910190, NULL, 1),
(59, 38, 'укцуеукеук', 19, NULL, 1491924270, NULL, 0),
(60, 39, 'jhngb', 19, NULL, 1491924357, NULL, 0),
(61, 41, 'ааааааааа', 19, NULL, 1491924380, NULL, 0),
(62, 42, 'jnhgbfvcx', 19, NULL, 1491924406, NULL, 1),
(63, 37, 'Theme 1', 19, NULL, 1492076162, NULL, 0),
(64, 29, 'qwefgn', 19, NULL, 1492165522, NULL, 0),
(65, 44, 'Дроби для початку.', 19, NULL, 1492507739, NULL, 0),
(66, 45, 'Тема 1: Примеры целого выражения', 19, 19, 1492516179, 1492516980, 1),
(67, 45, 'Дробные выражения', 19, 19, 1492516187, 1492521575, 1),
(68, 45, 'Рациональная дробь', 19, 19, 1492516200, 1492521615, 0),
(69, 45, 'Основное свойство дроби', 19, 19, 1492516232, 1492521629, 1),
(70, 45, 'Основное свойство дроби', 19, 19, 1492516244, 1492519494, 0),
(71, 45, 'Тема 3: Дроби с разными знаменателями', 19, 19, 1492516265, 1492518381, 1),
(72, 45, 'Алгоритм приведения дробей к общему знаменателю', 19, 19, 1492516277, 1492519508, 0),
(73, 45, 'Общая схему сложения и вычитания дробей с разными знаменателями', 19, NULL, 1492516289, NULL, 1),
(74, 45, 'Отрицательная степень', 19, 19, 1492516308, 1492519513, 0),
(75, 45, 'Буквенная степень123', 19, 19, 1492516319, 1492518246, 1),
(76, 45, 'Умножение и деление дробей со степенями', 19, 19, 1492516330, 1492519485, 0),
(77, 45, 'Возведение дроби со степенью в еще одну степень 1551', 19, 19, 1492516342, 1492521536, 0),
(78, 45, 'Возведение в единицу, квадратный корень', 19, 19, 1492516352, 1492521561, 0),
(79, 45, 'Помните: на ноль делить нельзя!', 19, 19, 1492516363, 1492521596, 1),
(80, 45, 'параграф 3', 19, NULL, 1492516542, NULL, 1),
(81, 45, 'название (тема 3)', 19, NULL, 1492516581, NULL, 1),
(82, 45, 'Валера', 19, NULL, 1492523562, NULL, 1),
(83, 45, 'ПАРАРАФ!!!! 1!!!! БЛИН!', 19, NULL, 1492524046, NULL, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `theory`
--

CREATE TABLE `theory` (
  `id` int(11) NOT NULL,
  `table` varchar(255) NOT NULL,
  `row_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `text` varchar(255) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `transfer`
--

CREATE TABLE `transfer` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `summa` double(11,0) NOT NULL,
  `type` int(11) NOT NULL,
  `data` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transfer`
--

INSERT INTO `transfer` (`id`, `user_id`, `summa`, `type`, `data`, `deleted`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(2, 1, 12, 1, 12312, 0, 1, 1, 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `translate`
--

CREATE TABLE `translate` (
  `id` int(11) NOT NULL,
  `english_id` int(11) NOT NULL,
  `translate` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `translate`
--

INSERT INTO `translate` (`id`, `english_id`, `translate`, `created_by`, `created_at`, `updated_by`, `updated_at`, `deleted`) VALUES
(1, 1, 'кот', 5, 1476371373, NULL, NULL, 0),
(2, 2, 'словарь', 5, 1476371373, NULL, NULL, 0),
(3, 3, 'жизнь', 5, 1476371373, NULL, NULL, 0),
(4, 4, 'яблоко', 5, 1476371373, NULL, NULL, 0),
(5, 5, 'книга', 5, 1476371373, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `school_id` int(11) DEFAULT NULL,
  `sity_id` int(11) DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `first_name` varchar(55) NOT NULL,
  `second_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(55) NOT NULL,
  `confirm` tinyint(1) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `balance` int(11) DEFAULT NULL,
  `push_status` tinyint(1) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  `class` smallint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `school_id`, `sity_id`, `role_id`, `phone`, `email`, `photo`, `first_name`, `second_name`, `last_name`, `confirm`, `auth_key`, `balance`, `push_status`, `updated_at`, `created_at`, `password_hash`, `deleted`, `class`) VALUES
(1, 1, 1, 1, '1111111115', '1@1.c', '', '╚╗╔╝║║♫═╦╦╦╔╗ ╔╝╚╗♫╚╣║║║║╔╣you ╚', 'Ṱ̺̺̕o͞ ̷i̲̬͇̪͙n̝̗͕v̟̜̘̦͟o̶̙̰̠kè͚̮̺̪̹̱̤ ̖t̝͕̳̣̻̪͞h̼͓̲̦̳̘', 'Ṱ̺̺̕o͞ ̷i̲̬͇̪͙n̝̗͕v̟̜̘̦͟o̶̙̰̠kè͚', NULL, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU4i', 40, 0, 1486045915, 1471450288, '$2y$13$izR3bPT56MQjisvDBQ.RgevWmBOirwOYuI6kxuhT2UzEA0LoWQL9y', 0, NULL),
(2, 3, 3, 2, '1234567936', '', '1584ff2d.png', '2', 'ор', '233', NULL, 'ucqN16-chnGJGN0-a1y3IpBudtD__BOA', NULL, 0, 1486381350, 1471450630, '$2y$13$izR3bPT56MQjisvDBQ.RgevWmBOirwOYuI6kxuhT2UzEA0LoWQL9y', 0, NULL),
(3, 1, 1, 3, '1234567936', '', NULL, '3', NULL, '3', NULL, 'pE6X5QJKcRPCSvHFh7PAaWSIiNsA90ZF', NULL, 0, 1471450693, 1471450693, '$2y$13$BIBWYUMYpXsCAqLoScds5equtwegb5u2oeiFxm6f/L5CcMbQjQZj.', 0, NULL),
(4, 1, 1, 4, '1234567936', '4@4.c', NULL, '4', NULL, '4', NULL, 'VHDzWqTBGpdqlsbWrp9M0a618moki--C', 30, 0, 1474369779, 1471450733, '$2y$13$9R3.UnqY.Tizd2RRxLXs9O30Kux2IS8L.3DjCEe7RqIYUm6A5RxWe', 0, NULL),
(5, 3, 3, 5, '1234567936', '', NULL, '5', NULL, '5', NULL, 'ZUCNeFIfLG52YaoOMCYn5Mt6ANNvUDNo', NULL, 0, 1471450830, 1471450830, '$2y$13$5ez6lDL8sspJKDNPc.1iYudXooQF.7R9xOh8s5PkKdNV1Pa8G2sau', 0, NULL),
(6, 1, 1, 6, '1234567936', '', NULL, '6', NULL, '6', NULL, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU45', NULL, 0, 1471450896, 1471450896, '$2y$13$gjioOYB2/f8QiSTsF/WQ/eogJxGeb5VKbYAxTXuHE5sKB/HM1LO6u', 0, NULL),
(7, 4, 2, 1, '1234567808', 'tkalenko.inka@gmail.com', NULL, 'www', NULL, 'www', NULL, 'gFyWAxTFjwvUG6AYiC1H_5hXCEfmZSw9', NULL, 0, 1471943148, 1471942913, '$2y$13$96ijlSxTuPaYlLUqGCpmI.TJ92lA3hnXjKK/58u2yG/tAOtSZDYQO$13$izR3bPT56MQjisvDBQ.RgevWmBOirwOYuI6kxuhT2UzEA0LoWQL9y', 0, NULL),
(19, 1, 1, 5, '5555555555', 'manager@mail.com', '658863ac', 'manager', 'manager', 'manager', NULL, 'UXCxi-ULzubwkdRufzSwBJhz0WfbNcoN', NULL, 0, 1485191957, 1472813525, '$2y$13$Oqsw0pGxqU6ehLIj9kVgU.RqvDuKanf37ylBxgT2TjSybZ2YBvAAC', 0, NULL),
(47, 1, 1, 4, '4444444444', 'curator@mail.com', '157d946b.png', 'Curator', 'Curator', 'Curator', NULL, 'juJehb6MHHI1ZbZdZK1_R-cXERDxABcn', NULL, 0, 1473857215, 1473845680, '$2y$13$nT2MwPhZrMTqcos2PVtOWOLtaJf0ej2T0fwZvyqtPAHyb.giOfX0q', 0, NULL),
(50, 1, 1, 3, '3333333333', 'teacher@mail.com', '257da7d2.png', 'Teacher', 'Teacher', 'Teacher', NULL, 'jdrp4knkgRBoU4Ok9Ong_hQu7XW72pxJ', NULL, 0, 1473938715, 1473936672, '$2y$13$R2ZhRTuumxCsYi1nXTk7Aed5q1bm.CDT2MOGdUWVcwFsn0QSDQVve', 0, NULL),
(52, 1, 1, 1, '1111111111', '', NULL, '5656', '545', '12345454', NULL, 'rDetzRZA-e5h-XnWO4Aa73mveXXU4Y8V', NULL, 0, 1485257149, 1477579612, '$2y$13$piPT3/X80GEBPCxfhKffw.e3zMSXfV2m3DnJGa1ArI0Zv/iXWNZli', 0, NULL),
(95, 3, 3, 2, '6210693167', '', NULL, 'User', 'User', '12345454', NULL, 'mu7szkuYFbqsttlPbms1w3g1JdxwZ65c', NULL, 0, 1478612761, 1478612761, '$2y$13$6oyhbk1aMgOQf581Rtx4VOzvP/Hi4wnDCJE1veKMcou0P12BDf/Dy', 0, NULL),
(97, 3, 3, 2, '7004159492', '', NULL, 'User', 'User', '12345454', NULL, '6pUsRSWybRHIvHY4JhbErqY048N7IYUr', NULL, 0, 1478612804, 1478612804, '$2y$13$g6aZU8cycXaaMOzbU/hZ1eMfyHd0bX.QY9oozLTJRW4DjgtchWOr2', 0, NULL),
(99, 3, 3, 2, '4644980062', '', NULL, 'User', 'User', '12345454', NULL, 'e3zVZ9y25AocHGppaysozNSg7mWgt7pk', NULL, 0, 1478612878, 1478612878, '$2y$13$/aHY0ACa1kDUoqU8tAtrSeHfesrMRK83juSbYOBSjcEfb8k2ECoXi', 0, NULL),
(100, 3, 3, 2, '5796789571', '', NULL, 'User', 'User', '12345454', NULL, '1UcMviWBpWnuBlERPnNrrmKklVP7KSBX', NULL, 0, 1478612920, 1478612920, '$2y$13$7OwpLp31Pi2LV0lh46Y1Neq8f/hOpI2X7Gqp32jEBTz50V.ul3BnS', 0, NULL),
(101, 3, 3, 2, '7659078371', '', NULL, 'User', 'User', '12345454', NULL, 'hXk-1PMsncjOn2iGceij7PthM_ncR33z', NULL, 0, 1478612988, 1478612988, '$2y$13$CV8kDJ7DGseZlXUpOhqmEerJV6NAzmBcPUGet6WOW486mvZCnrJ4K', 0, NULL),
(102, 3, 3, 2, '9008365524', '', NULL, 'User', 'User', '12345454', NULL, 'VsmNSUdaaxwVt4mzupXKfs-fPqHbHyAi', NULL, 0, 1478612990, 1478612990, '$2y$13$fGWPYGrwnumfiM.ySSMsbe1wa7zsBgaTxF0/6YAT5IkP4/s3aDct6', 0, NULL),
(103, 3, 3, 2, '2018355223', '', NULL, 'User', 'User', '12345454', NULL, 'SKKhypsTFGys4i_urcwBcAe4_2pUuAcL', NULL, 0, 1478612992, 1478612992, '$2y$13$rRYXvrmPS3MiTgts6GXQ4eqzLzyUn2Jrk.GL7m4.CGv3fhIXQudMS', 0, NULL),
(104, 3, 3, 2, '7463201588', '', NULL, 'User', 'User', '12345454', NULL, 'y0Xg6YCx70c5mh6vIiFEFPv3ZM27Hd6B', NULL, 0, 1478612994, 1478612994, '$2y$13$iZ60bE/6JqSGiuvYED3f9edpSO.irgQt4S0sAdZtJ4fAx7gGhik6u', 0, NULL),
(105, 3, 3, 2, '7761672861', '', NULL, 'User', 'User', '12345454', NULL, 'QF2MnTRqqXGOZpULXxlVGEP5b3H7BnvP', NULL, 0, 1478612997, 1478612997, '$2y$13$pA1rD7k1C2vhJ8hqh4YPhuoAqf7yBOAmpKIM3NjZVDwnIWXzcdJjq', 0, NULL),
(106, 3, 3, 2, '8116811766', '', NULL, 'User', 'User', '12345454', NULL, 'V6D7YBoTUCjUWgyOl_NmyD11Qz-AKDUG', NULL, 0, 1478612999, 1478612999, '$2y$13$uFlxkPXvlxozk93peNi9bOGHGssfB5GrlRaWe7yPnQ6.foN5yI/wG', 0, NULL),
(107, 3, 3, 2, '1388619875', '', NULL, 'User', 'User', '12345454', NULL, 'kqFgdGR-x-SCwTYWY9ZxyfHiRsFMVfki', NULL, 0, 1478613001, 1478613001, '$2y$13$00Es8cTGFInSlpcPzjWzIOTaPG6vsFjiRB/c2.oBUNo5ekE6n9eB.', 0, NULL),
(108, 3, 3, 2, '2378586379', '', NULL, 'User', 'User', '12345454', NULL, 'jjc9DxJYY8VK8KsIMhap2rMuqjIwcDV6', NULL, 0, 1478613003, 1478613003, '$2y$13$Ot.E0Ehj0HZJnC7yRK5.suF6phNyFkY.7xX/a/jsq7x7doLjWuEXy', 0, NULL),
(109, 3, 3, 2, '1904394297', '', NULL, 'User', 'User', '12345454', NULL, 'WyuqXiHNuGz2I62s02QFDF4O5zMJ-4du', NULL, 0, 1478613006, 1478613006, '$2y$13$YjNF.6mJ8dZO/Folojl6gubnP7.mRAdWoYHotLDOfq5EvtP.ukbkS', 0, NULL),
(110, 3, 3, 2, '4262921001', '', NULL, 'User', 'User', '12345454', NULL, 'mbwVdO8XNm2T8UE_VQmhW_--lwuI99Ao', NULL, 0, 1478613008, 1478613008, '$2y$13$CziqyeBD.4f/jXJKDGJ6kumEbcukghXKfEKw6FJnxdtYur9Oa6Vbu', 0, NULL),
(111, 3, 3, 2, '4229195848', '', '', 'User', 'User', '12345454', NULL, 'zk8yoYl9enHeYG0hJNq9Dm5sPdlar3Hq', NULL, 0, 1478709733, 1478705271, '$2y$13$Qzj7sKBuDzjSh1el6EI2SuSuvKCv05zpxnYiHe6qvlfy9pyT2nA1K', 0, NULL),
(115, 1, 1, 1, '661111111', '', NULL, 'ацуауц', NULL, 'ауауц', NULL, 'EFkHFkSf8mvVaWkRWEsvuFRlGL1uMJI3', NULL, 0, 1479814686, 1479814686, '$2y$13$wHOH0Ji7UXvsQnUaZktzCexwAidBPEUFnLSUB6KoYDL91rVggM/Lu', 0, NULL),
(116, 2, 1, 1, '0951111111', '', NULL, '212121', NULL, '22121', NULL, '5o_WU7JtrvzACSErlf2_oJR_jzfU4t-z', NULL, 0, 1479814875, 1479814875, '$2y$13$ukb/BZypFSYYgrdGeZ3I0OofXnUiuDycrRptBgRvaqUKPeMLKdFOq', 0, NULL),
(117, 5, 1, 1, '0202020202', '', NULL, 'пукпукпук', NULL, 'ааукпук', NULL, '8XUySfhxTEbdSj4H-MiX-wgDsNcxK5Os', NULL, 0, 1479815379, 1479815379, '$2y$13$IHOj4Cf56INU6AAahE/m5.RQOsljl7EC7kYeZfZo6zBATH2pB308q', 0, NULL),
(119, 5, 1, 1, '0303300303', '', NULL, 'екркерке', NULL, 'рекркер', NULL, 'Y5ZvTOCeFIkz-Fn1gWMR3lP4z5oFv58U', NULL, 0, 1479817470, 1479815553, '$2y$13$CnLj9OBLfHSE9v9R0mOdMeoWvFwN8C9rI1.AVU.nA/d2R1K1gfur2', 0, NULL),
(120, 2, 1, 2, '5454654646', '', NULL, 'r43t43t43', NULL, '312321f', NULL, 'SiIaWUpklmsFXmU2v9lWtJhf2SBV6dcB', NULL, 0, 1479825041, 1479825041, '$2y$13$nGFxuDso7XYlBxuMXCZTLOd//wTRQEfMEbmXQFi1f8E8u57K1FV96', 0, NULL),
(121, 4, 1, 2, '4343243423', '', '65834580.jpeg', 'gregerg', NULL, 'gerger', NULL, 'u2HZ23OTLy_B7p9vHhyrvgcmNYRXA-VA', NULL, 0, 1479825413, 1479825239, '$2y$13$ACwV3tltgQIORWqFlx1EeOYExIhXPJt/NqMsZrYou0QwAH7DCkxp6', 0, NULL),
(127, 2, 1, 3, '2222222222', '22222222@mail.com', NULL, 'Петр', 'Петрович', 'Васьков', NULL, 'Yv9XLbbUDXeB4UTIM6ZbcI8zWE_rIJMo', NULL, 0, 1484831971, 1484831971, '$2y$13$TW/Xj/ypwrw0eQ7AbGtH7eyaJw8yzuVKYQll1wxjmryDlILX6mIWe', 0, NULL),
(132, NULL, NULL, 5, '3333333333', '333333333@mail.com', NULL, '111111', '1111111', '11111111', NULL, 'e9ob41wZfHzbgF-0SHaCTFhZYkK-pXs0', NULL, 0, 1484834876, 1484834876, '$2y$13$PVxjwVhfUVmpZYJ2cEfhC.b3fN8al.GAF7bVP1ib9kWACFyqKzGAa', 0, NULL),
(133, NULL, NULL, 5, '2222222222', '222222222@mail.com', NULL, '3r3r23r', '3r32r3', 'r3r3r3r', NULL, 'hGfOypTmozLDvHsXF_mlIne_tGm0mVzr', NULL, 0, 1484840014, 1484840014, '$2y$13$ho6wjaSYAhLKGyn37Kz1GuUZO1qFaEAnX68RToHPrsqgQqX8J.ePy', 0, NULL),
(134, NULL, NULL, 5, '2222222222', '222222222@mail.com', NULL, '222222', 'dsvdc', 'sdvds', NULL, 'ws0JBCFQbjq2rPNPcI5Qv3lMLuyWKge9', NULL, 0, 1484840426, 1484840426, '$2y$13$3xoFLaVVDO6kwmGss8850.KgCANONAZjNLajkJ.Qzw9lwIXQM4p8.', 0, NULL),
(138, NULL, NULL, 5, '2222222222', '222222222@mail.ru', NULL, 'dfsfsd', 'sdcsdcs', 'dsvsd', NULL, 'Dt3rbT3-rOst8FBizSjkeTeUpA1V3rne', NULL, 0, 1484841081, 1484841081, '$2y$13$IM7M8./GjPjR4vl2B2ZZceIXQrrR.d6F/S/JzbOXqg4GjrZKiFflW', 0, NULL),
(144, NULL, NULL, 5, '9999999999', '999999999@mail.com', NULL, '999', '9999', '99999', NULL, 'tRQCwYOi-2IGn4jjd024CrsO5k50Tci6', NULL, 0, 1484909624, 1484909624, '$2y$13$TMmpZ5UVpphgD2B7a5ZtVepeIlkuzkZYRYmpeA/J7pOnQoRGDgLC2', 0, NULL),
(148, 2, 1, 3, '2222222223', '22222223@mail.com', NULL, 'Петр', 'Петрович', 'Васьков', NULL, 'PT-O1attnGmAKKbF9HExhRa4GO8SD0Vy', NULL, 0, 1484914323, 1484914323, '$2y$13$1X9/sNnpgb.UMg0oe/UVMehvbXid5VsOWmboDkADvzAXqrKJsWzRu', 0, NULL),
(149, 2, 1, 3, '2222222223', '22222223@mail.com', NULL, 'Петр', 'Петрович', 'Васьков', NULL, '4zIM6GN83EWZLK7y7vaFRFdlxeyQ0K1s', NULL, 0, 1484914376, 1484914376, '$2y$13$Kzgl87H1o3zYDaG9nNH8oekua.hYMdGZs56xb5XrO3uebmjGcQdC6', 0, NULL),
(150, NULL, NULL, 5, '1231231231', '3213123123@mail.com', '65882004.', 'dffsf', 'sdfsdfsdfds', 'sdfsdfsd', NULL, 'FMzSZF95NAaIT7P3ErR6-QglV4-ne5lN', NULL, 0, 1484914765, 1484914765, '$2y$13$VbN65kBLSZtbL0KQU.Kf2OiPUo0j.1SN2mDMKz7GR0Gnn4SRSliUi', 0, NULL),
(151, 2, 1, 3, '2222222223', '22222223@mail.com', NULL, 'Петр', 'Петрович', 'Васьков', NULL, 'HBOXMp60EysKr12yhBBi_fQiyLmliDpV', NULL, 0, 1484916706, 1484916706, '$2y$13$iKClbsT9ognFpiI/0jqBaubob7UkRVa3v0qhFNGyuPyOueFaJR3X2', 0, NULL),
(155, 2, 1, 3, '2222222223', '22222223@mail.com', NULL, 'Петр', 'Петрович', 'Васьков', NULL, 'Ejcm9GFH9Y8kl8E34cXniYv7MAkAfvV6', NULL, 0, 1484917745, 1484917745, '$2y$13$6tWwwEqvvI0ZAZObJTB7semKG2d0FKazvmRI/KZQ9ljJ2mNijG9B.', 0, NULL),
(162, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, '95gtL65HGBYncw9pwWGic4mduKFC--5A', NULL, 0, 1484921181, 1484921181, '$2y$13$deOF5hgWzzOs.zMf4J7W9em0xUXPHKPr8Va6SDkEMe/tpzFCwz3YG', 0, NULL),
(163, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, '7oBnlFjZf3kVJA1p5kTYYzI-edc1FU2s', NULL, 0, 1484921229, 1484921229, '$2y$13$S8iYAQ09KNk1h5TtFxxe1.DuERdA3AH9dQeaUexVJjofGENeWerDW', 0, NULL),
(164, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, 'SURfYWCxQhCwwFoJQA3g1FLBH_NSd4HL', NULL, 0, 1484921546, 1484921546, '$2y$13$/MtruqCbtjGDGpnUzxytC.Gjsua9a9ptHCstnznj4iUhyHThPHw1a', 0, NULL),
(165, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, '4SoDzNOjsBOic8vHuZ1AgHyf8W8stxAz', NULL, 0, 1484921730, 1484921730, '$2y$13$vul5g/iiLCWF6OcNT7oaU.7mFf85ZNC2xmGpTejE96vhrtqRzD5i2', 0, NULL),
(166, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, 'KkszzdmRACIlo5_81yfjNaXRYJQpe3Se', NULL, 0, 1484921783, 1484921783, '$2y$13$SSt1AD8AJlbJYr8Sv7vo2erErm.DufJCRwLZALXWJBr0Uh7cR62KK', 0, NULL),
(167, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, 'BXkimI_0Mh_VDwH98hUwiCDlIP_oGGy3', NULL, 0, 1484921869, 1484921869, '$2y$13$5K//6L/CKCgq7rOZh6v3/uqx1gck3SMEcvVw9GRdobTFeP6qJH.wq', 0, NULL),
(168, 1, 1, 2, '8888888888', '8888888@vail.com', NULL, 'drgdgdf', 'gfdgdfgfd', 'dfbdf', NULL, 'pstD_X_usVDDhaHtzHzTVkwsd2D-MSwu', NULL, 0, 1484921906, 1484921906, '$2y$13$ZSBt4ZcTYU3pYdLzX5jrredgEpNikQC5iOMPQ8bemRCad2Trt3dJC', 0, NULL),
(169, 1, 1, 2, '7777777777', '7777777@mail.com', '4588242f.jpeg', '5t544646546', '53454534543534', '453543535345', NULL, 'IwIyijJjhMg8BF9UNac6wDOftn65345S', NULL, 0, 1484931839, 1484921998, '$2y$13$esHztP09qWQsdbIMccmznO2dybgQv59nvcKlZ4TWnZEfOS..ReXj6', 0, NULL),
(170, 1, 1, 3, '1111111111', 'email@mail.com', NULL, 'Максим', 'Іванович', 'Антонов', NULL, 'nG94AqA7W245AjXg5C_YA4mi5TyUXYp7', NULL, 0, 1485263690, 1485263690, '$2y$13$gveKlou2I2FY7tTmkKYW5.ua4CR82LVMQhvuCA2NlRPZCPUb1hxhu', 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `user_dictionary`
--

CREATE TABLE `user_dictionary` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `translate_id` int(11) NOT NULL,
  `created_at` int(11) NOT NULL,
  `deleted` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_dictionary`
--

INSERT INTO `user_dictionary` (`id`, `user_id`, `translate_id`, `created_at`, `deleted`) VALUES
(1, 1, 1, 0, 1),
(2, 1, 2, 0, 1),
(4, 1, 3, 1478099527, 1),
(5, 1, 4, 1478104607, 1),
(6, 1, 5, 1478188028, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `book`
--
ALTER TABLE `book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `book_discipline`
--
ALTER TABLE `book_discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_id` (`book_id`),
  ADD KEY `discipline_id` (`discipline_id`);

--
-- Индексы таблицы `bookmark`
--
ALTER TABLE `bookmark`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `book_id` (`book_id`);

--
-- Индексы таблицы `checkup`
--
ALTER TABLE `checkup`
  ADD PRIMARY KEY (`id`),
  ADD KEY `done_task_id` (`done_task_id`),
  ADD KEY `checkup_ibfk_2` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `class_discipline`
--
ALTER TABLE `class_discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `discipline_id` (`discipline_id`);

--
-- Индексы таблицы `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `control`
--
ALTER TABLE `control`
  ADD PRIMARY KEY (`id`),
  ADD KEY `checkup_id` (`checkup_id`),
  ADD KEY `control_ibfk_2` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `discipline`
--
ALTER TABLE `discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `done_task`
--
ALTER TABLE `done_task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `done_task_ibfk_3` (`updated_by`),
  ADD KEY `created_by` (`created_by`);

--
-- Индексы таблицы `eng_dictionary`
--
ALTER TABLE `eng_dictionary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `level`
--
ALTER TABLE `level`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `my_book`
--
ALTER TABLE `my_book`
  ADD PRIMARY KEY (`id`),
  ADD KEY `my_discipline_id` (`book_discipline_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `my_class`
--
ALTER TABLE `my_class`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `my_discipline`
--
ALTER TABLE `my_discipline`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `discipline_id` (`discipline_id`);

--
-- Индексы таблицы `my_pupil`
--
ALTER TABLE `my_pupil`
  ADD PRIMARY KEY (`id`),
  ADD KEY `pupil_id` (`pupil_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `region`
--
ALTER TABLE `region`
  ADD PRIMARY KEY (`id`),
  ADD KEY `country_id` (`country_id`);

--
-- Индексы таблицы `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Индексы таблицы `school`
--
ALTER TABLE `school`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sity_id` (`sity_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `section`
--
ALTER TABLE `section`
  ADD PRIMARY KEY (`id`),
  ADD KEY `book_id` (`book_id`),
  ADD KEY `id` (`id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `sity`
--
ALTER TABLE `sity`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`),
  ADD KEY `region_id` (`region_id`);

--
-- Индексы таблицы `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `transfer_id` (`transfer_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_at` (`updated_at`),
  ADD KEY `my_pupil_id` (`my_pupil_id`);

--
-- Индексы таблицы `subtask`
--
ALTER TABLE `subtask`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`);

--
-- Индексы таблицы `task`
--
ALTER TABLE `task`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_id` (`created_by`),
  ADD KEY `level_id` (`level_id`),
  ADD KEY `theme_id` (`theme_id`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `theme`
--
ALTER TABLE `theme`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section_id` (`section_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `theory`
--
ALTER TABLE `theory`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transfer`
--
ALTER TABLE `transfer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `translate`
--
ALTER TABLE `translate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `english_id` (`english_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `updated_by` (`updated_by`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `school_id` (`school_id`),
  ADD KEY `sity_id` (`sity_id`),
  ADD KEY `role_id` (`role_id`);

--
-- Индексы таблицы `user_dictionary`
--
ALTER TABLE `user_dictionary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `translate_id` (`translate_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `book`
--
ALTER TABLE `book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT для таблицы `book_discipline`
--
ALTER TABLE `book_discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT для таблицы `bookmark`
--
ALTER TABLE `bookmark`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=122;
--
-- AUTO_INCREMENT для таблицы `checkup`
--
ALTER TABLE `checkup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `class_discipline`
--
ALTER TABLE `class_discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT для таблицы `comment`
--
ALTER TABLE `comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT для таблицы `control`
--
ALTER TABLE `control`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `discipline`
--
ALTER TABLE `discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT для таблицы `done_task`
--
ALTER TABLE `done_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;
--
-- AUTO_INCREMENT для таблицы `eng_dictionary`
--
ALTER TABLE `eng_dictionary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `file`
--
ALTER TABLE `file`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1028;
--
-- AUTO_INCREMENT для таблицы `level`
--
ALTER TABLE `level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT для таблицы `my_book`
--
ALTER TABLE `my_book`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT для таблицы `my_class`
--
ALTER TABLE `my_class`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;
--
-- AUTO_INCREMENT для таблицы `my_discipline`
--
ALTER TABLE `my_discipline`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT для таблицы `my_pupil`
--
ALTER TABLE `my_pupil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `region`
--
ALTER TABLE `region`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `school`
--
ALTER TABLE `school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT для таблицы `section`
--
ALTER TABLE `section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT для таблицы `sity`
--
ALTER TABLE `sity`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT для таблицы `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `subtask`
--
ALTER TABLE `subtask`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1105;
--
-- AUTO_INCREMENT для таблицы `task`
--
ALTER TABLE `task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=520;
--
-- AUTO_INCREMENT для таблицы `theme`
--
ALTER TABLE `theme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;
--
-- AUTO_INCREMENT для таблицы `theory`
--
ALTER TABLE `theory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `transfer`
--
ALTER TABLE `transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `translate`
--
ALTER TABLE `translate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;
--
-- AUTO_INCREMENT для таблицы `user_dictionary`
--
ALTER TABLE `user_dictionary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `book`
--
ALTER TABLE `book`
  ADD CONSTRAINT `book_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `book_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `book_discipline`
--
ALTER TABLE `book_discipline`
  ADD CONSTRAINT `book_discipline_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  ADD CONSTRAINT `book_discipline_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`);

--
-- Ограничения внешнего ключа таблицы `bookmark`
--
ALTER TABLE `bookmark`
  ADD CONSTRAINT `bookmark_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `bookmark_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `bookmark_ibfk_3` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

--
-- Ограничения внешнего ключа таблицы `checkup`
--
ALTER TABLE `checkup`
  ADD CONSTRAINT `checkup_ibfk_1` FOREIGN KEY (`done_task_id`) REFERENCES `done_task` (`id`),
  ADD CONSTRAINT `checkup_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `checkup_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `class_discipline`
--
ALTER TABLE `class_discipline`
  ADD CONSTRAINT `class_discipline_ibfk_1` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`);

--
-- Ограничения внешнего ключа таблицы `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `comment_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `comment_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `comment_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `control`
--
ALTER TABLE `control`
  ADD CONSTRAINT `control_ibfk_1` FOREIGN KEY (`checkup_id`) REFERENCES `checkup` (`id`),
  ADD CONSTRAINT `control_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `control_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `discipline`
--
ALTER TABLE `discipline`
  ADD CONSTRAINT `discipline_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `discipline_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `done_task`
--
ALTER TABLE `done_task`
  ADD CONSTRAINT `done_task_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`),
  ADD CONSTRAINT `done_task_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `done_task_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `eng_dictionary`
--
ALTER TABLE `eng_dictionary`
  ADD CONSTRAINT `eng_dictionary_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `eng_dictionary_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `file_ibfk_2` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_book`
--
ALTER TABLE `my_book`
  ADD CONSTRAINT `my_book_ibfk_1` FOREIGN KEY (`book_discipline_id`) REFERENCES `book_discipline` (`id`),
  ADD CONSTRAINT `my_book_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_class`
--
ALTER TABLE `my_class`
  ADD CONSTRAINT `my_class_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_discipline`
--
ALTER TABLE `my_discipline`
  ADD CONSTRAINT `my_discipline_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `my_discipline_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_pupil`
--
ALTER TABLE `my_pupil`
  ADD CONSTRAINT `my_pupil_ibfk_1` FOREIGN KEY (`pupil_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `my_pupil_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `region`
--
ALTER TABLE `region`
  ADD CONSTRAINT `region_ibfk_1` FOREIGN KEY (`country_id`) REFERENCES `country` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Ограничения внешнего ключа таблицы `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_ibfk_1` FOREIGN KEY (`sity_id`) REFERENCES `sity` (`id`),
  ADD CONSTRAINT `school_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `school_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  ADD CONSTRAINT `section_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `section_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `sity`
--
ALTER TABLE `sity`
  ADD CONSTRAINT `sity_ibfk_1` FOREIGN KEY (`region_id`) REFERENCES `region` (`id`);

--
-- Ограничения внешнего ключа таблицы `subscription`
--
ALTER TABLE `subscription`
  ADD CONSTRAINT `subscription_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_ibfk_2` FOREIGN KEY (`transfer_id`) REFERENCES `transfer` (`id`),
  ADD CONSTRAINT `subscription_ibfk_3` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_ibfk_4` FOREIGN KEY (`updated_at`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `subscription_ibfk_5` FOREIGN KEY (`my_pupil_id`) REFERENCES `my_pupil` (`id`);

--
-- Ограничения внешнего ключа таблицы `subtask`
--
ALTER TABLE `subtask`
  ADD CONSTRAINT `subtask_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);

--
-- Ограничения внешнего ключа таблицы `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`),
  ADD CONSTRAINT `task_ibfk_3` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`),
  ADD CONSTRAINT `task_ibfk_4` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `task_ibfk_5` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`),
  ADD CONSTRAINT `theme_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `theme_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `transfer`
--
ALTER TABLE `transfer`
  ADD CONSTRAINT `transfer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `transfer_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `translate`
--
ALTER TABLE `translate`
  ADD CONSTRAINT `translate_ibfk_1` FOREIGN KEY (`english_id`) REFERENCES `eng_dictionary` (`id`),
  ADD CONSTRAINT `translate_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `translate_ibfk_3` FOREIGN KEY (`updated_by`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`sity_id`) REFERENCES `sity` (`id`),
  ADD CONSTRAINT `user_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_dictionary`
--
ALTER TABLE `user_dictionary`
  ADD CONSTRAINT `user_dictionary_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `user_dictionary_ibfk_2` FOREIGN KEY (`translate_id`) REFERENCES `translate` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
