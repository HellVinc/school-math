<?php
//return [
//    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
//    'components' => [
//        'cache' => [
//            'class' => 'yii\caching\FileCache',
//        ],
//        'urlManager' => [
//            'enablePrettyUrl' => true,
//            'showScriptName' => false,
//            'rules' => [
//                '<_c:[\w\-]+>/<id:\d+>' => '<_c>/view',
//                '<_c:[\w\-]+>' => '<_c>/index',
//                '<_c:[\w\-]+>/<_a:[\w\-]+>/<id:\d+>' => '<_c>/<_a>'
//            ]
//        ],
//    ],
//];


return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'en-US',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errors' => [
            'class' => 'common\components\Errors',
        ],
        'uploadFile' => [
            'class' => 'common\components\UploadFile',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            //здесь прописываем роли
            'defaultRoles' => [
                'learner',
                'teacher',
                'tutor',
                'curator',
                'manager',
                'admin',
            ],
            //зададим куда будут сохраняться наши файлы конфигураций RBAC
            'itemFile' => '@common/components/rbac/items.php',
            'assignmentFile' => '@common/components/rbac/assignments.php',
            'ruleFile' => '@common/components/rbac/rules.php'
        ],
        'i18n' => [
            'translations' => [
                'msg*' => [ // другие заголовки
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',// путь к файлам перевода
                    'sourceLanguage' => 'en-US', // // Язык с которого переводиться (данный язык использован в текстах сообщений).
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<module>/<controller:\w+>/<id:\d+>' => '<module>/<controller>/index',
                '<module>/<controller>/<action>' => '<module>/<controller>/<action>',
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],

        'request' => [

            'parsers' => [

                'application/json' => 'yii\web\JsonParser'

            ]

        ],

    ],
];