<?php
use yii\helpers\Html;
use yii\helpers\Url;


/* @var $this yii\web\View */
/* @var $user common\models\User */

$resetLink = Yii::$app->request->hostInfo."/#/login";

;
?>
<div class="password-reset">
    <p>Здравствуйте <?= Html::encode($user->first_name) ?> <?= Html::encode($user->last_name) ?>,</p>

    <p>Новый пароль для входа на сайт :</p>

    <p><?= Html::encode($new_pass) ?></p>

    <p><?= Html::a($resetLink, $resetLink) ?></p>
</div>
