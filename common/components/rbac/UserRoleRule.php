<?php
namespace common\components\rbac;

use Yii;
use yii\rbac\Rule;
use yii\helpers\ArrayHelper;
use common\models\User;
use common\models\Role;

class UserRoleRule extends Rule
{
    public $name = 'userRole';
    public function execute($user, $item, $params)
    {
        //Получаем массив пользователя из базы
        $user = ArrayHelper::getValue($params, 'user', User::findOne($user));
        if ($user) {
            $role = $user->role_id; //Значение из поля role базы данных
            switch($item->name) {

                case 'learner':
                    return $role ==  Role::LEARNER;
                    break;
                case 'tutor':
                    return $role ==  Role::TUTOR ||$role == Role::LEARNER;
                    break;
                case 'teacher':
                    return $role ==  Role::TEACHER;
                    break;
                case 'curator':
                    return $role ==  Role::CURATOR;
                    break;
                case 'manager':
                    return $role == Role::MANAGER;
                    break;
                case 'admin':
                    return $role ==  Role::ADMIN;
                    break;

                default :
                    return false;
            }
        }
    }
}