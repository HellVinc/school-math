<?php
return [
    'admin' => [
        'type' => 1,
        'description' => 'Admin',
        'ruleName' => 'userRole',
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Manager',
        'ruleName' => 'userRole',
    ],
    'curator' => [
        'type' => 1,
        'description' => 'Curator',
        'ruleName' => 'userRole',
    ],
    'tutor' => [
        'type' => 1,
        'description' => 'Tutor',
        'ruleName' => 'userRole',
        'children' => [
            'learner',
        ],
    ],
    'teacher' => [
        'type' => 1,
        'description' => 'Teacher',
        'ruleName' => 'userRole',
    ],
    'learner' => [
        'type' => 1,
        'description' => 'Learner',
        'ruleName' => 'userRole',
    ],
];
