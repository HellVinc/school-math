<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;
use common\components\traits\modelWithFiles;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "control".
 *
 * @property integer $id
 * @property integer $checkup_id
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property User $updater
 * @property Checkup $checkup
 */
class Control extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use modelWithFiles;

    const WRONG = 0;
    const RIGHT = 1;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'control';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'checkup_id', 'curator_id', 'status', 'created_at'], 'required'],
            [['checkup_id', 'curator_id', 'status', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['checkup_id'], 'exist', 'skipOnError' => true, 'targetClass' => Checkup::className(), 'targetAttribute' => ['checkup_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            ['status', 'default', 'value' => self::WRONG],
            ['status', 'in', 'range' => [self::WRONG, self::RIGHT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/control", "ID"),
            'checkup_id' => Yii::t("msg/control", 'Checkup ID'),
            'curator_id' => Yii::t("msg/control", 'Curator ID'),
            'status' => Yii::t("msg/control", 'Status'),
            'created_by' => Yii::t("msg/control", 'Create by'),
            'updated_by' => Yii::t("msg/control", 'Update by'),
            'created_at' => Yii::t("msg/control", 'Date create'),
            'updated_at' => Yii::t("msg/control", 'Date update'),
            'deleted' => Yii::t("msg/control", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'checkup_id' => $this->checkup_id,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name. " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name. " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                [
                    'common\models\Control' => [
                        'id',
                        'checkup_id',
                        'created_by',
                        'creator' => function ($this) {
                            return $this->creator->first_name . " " . $this->creator->last_name;
                        },
                        'updated_by',
                        'updater' => function ($model) {
                            return $model->creator->first_name;
                        },
                        'created_at',
                        'updated_at',
                        'status',
                    ],
                ],
            ]
        );
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckup()
    {
        return $this->hasOne(Checkup::className(), ['id' => 'checkup_id'])->andOnCondition(['checkup.deleted' => self::NOT_DELETED]);
    }
}
