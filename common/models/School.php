<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "school".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sity_id
 * @property string $house
 * @property integer $street
 * @property integer $phone
 * @property string $email
 * @property integer $director
 * @property integer $notes
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property User $updater
 * @property Sity $sity
 * @property User[] $users
 */
class School extends \yii\db\ActiveRecord
{

    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
    public $sity_name;
    public $region_name;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'school';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'name', 'sity_id',], 'required'],
            [['sity_id', 'deleted', 'phone', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['email'], 'string', 'max' => 55],
            [['name','house', 'street', 'email', 'director', 'notes'], 'string', 'max' => 255],
            [['sity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sity::className(), 'targetAttribute' => ['sity_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['sity_name', 'region_name'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/school", "ID"),
            'name' => Yii::t("msg/school", 'Name'),
            'sity_id' => Yii::t("msg/school", 'Sity ID'),
            'house' => Yii::t("msg/school", 'House'),
            'street' => Yii::t("msg/school", 'Street'),
            'phone' => Yii::t("msg/school", 'Phone'),
            'email' => Yii::t("msg/school", 'Email'),
            'director' => Yii::t("msg/school", 'director'),
            'notes' => Yii::t("msg/school", 'notes'),
            'created_by' => Yii::t("msg/school", 'Create by'),
            'updated_by' => Yii::t("msg/school", 'Update by'),
            'created_at' => Yii::t("msg/school", 'Date create'),
            'updated_at' => Yii::t("msg/school", 'Date update'),
        ];
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'sity_id' => $this->sity_id,
            'sity' => $this->sity->name,
            'region' => $this->sity->region->name,
            'street' => $this->street,
            'house' => $this->house,
            'phone' => $this->phone,
            'email' => $this->email,
            'director' => $this->director,
            'notes' => $this->notes,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }


    /**
     * search all models
     * @param $result
     * @return mixed
     */
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\School' => [
                    'id',
                    'name',
                    'sity_id',
//                        'sity' => function ($model) {
//                            return $model->sity->name;
//                        },
//                        'street',
//                        'house',
//                        'phone',
//                        'email',
//                        'director',
//                        'notes',
                ],
            ]
        );
        return $result;

    }

    /**
     * @return $this
     */
    public function saveModel()
    {
        if ($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
//            $this->created_at = time();
        } else {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = time();
        }
        if ($this->sity_name) {
            $sity = Sity::checkModel($this->sity_name, $this->region_name);
            if ($sity->getErrors()) {
                $this->addError('sity_name', $sity->getErrors());
                return $this;
            }
            $this->sity_id = $sity->id;
        }
        return $this->saveWithCheck();
    }

    public function findModel()
    {
        return self::findOne([
            'name' => $this->name,
            'sity_id' => $this->sity_id,
            'deleted' => self::NOT_DELETED
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSity()
    {
        return $this->hasOne(Sity::className(), ['id' => 'sity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['school_id' => 'id']);
    }


}
