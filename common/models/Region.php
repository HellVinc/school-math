<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "territory".
 *
 * @property integer $id
 * @property integer $country_id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property Country $country
 */
class Region extends \yii\db\ActiveRecord
{
    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'region';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'created_at'], 'required'],
            [['country_id', 'deleted', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
            'deleted' => 'Deleted',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public static function checkModel($name)
    {
        $model = self::findOne(['name' => $name]);
        if (!$model) {
            $model = new self;
            $model->name = $name;
            $model->country_id = Sity::COUNTRY;
            $model->created_at = time();
        } else if ($model->deleted = self::DELETED) {
            $model->updated_at = time();
            $model->deleted = self::NOT_DELETED;
        }
        $model->save();
        return $model;
    }
}
