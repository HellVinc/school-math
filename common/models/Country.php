<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $name
 * @property integer $deleted
 *
 * @property Sity[] $sities
 */
class Country extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'deleted'], 'required'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSities()
    {
        return $this->hasMany(Sity::className(), ['country_id' => 'id']);
    }

    public static function findCountry($id = null, $all = false, $deleted = false){
        if($all){
            if($deleted){
                $results = self::find()->all();
            }else{
                $results = self::find()->where(['deleted'=>0])->all();
            }
            if($results){
                foreach($results as $k=>$result){
                    $results[$k] = $result->fields();
                }
                return $results;
            }
        }else{
            if($deleted) {
                $result = self::findOne(['id' => $id]);
            }else{
                $result = self::findOne(['id' => $id, 'deleted' => 0]);
            }
            if($result){
                return $result;
            }
        }

        return false;
    }
}
