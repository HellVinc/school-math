<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "role".
 *
 * @property integer $id
 * @property string $name
 * @property integer $deleted
 *
 * @property User[] $users
 */
class Role extends \yii\db\ActiveRecord
{

    const LEARNER = 1;
    const TUTOR = 2;
    const TEACHER = 3;
    const CURATOR = 4;
    const MANAGER = 5;
    const ADMIN = 6;


    const NOT_DELETED = 0;
    const DELETED = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'name', 'deleted'], 'required'],
            [['id', 'deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/role", "ID"),
            'name' => Yii::t("msg/role", 'Name'),
            'deleted' => Yii::t("msg/role", 'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['role_id' => 'id']);
    }
}
