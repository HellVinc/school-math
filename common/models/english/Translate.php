<?php

namespace common\models\english;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use common\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "translate".
 *
 * @property integer $id
 * @property integer $english_id
 * @property string $translate
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $updater
 * @property EngDictionary $english
 * @property User $creator
 * @property UserDictionary[] $userDictionaries
 */
class Translate extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'translate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['english_id', 'translate', 'created_by', 'created_at'], 'required'],
            [['english_id', 'created_by', 'created_at', 'updated_by', 'updated_at', 'deleted'], 'integer'],
            [['translate'], 'string', 'max' => 255],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['english_id'], 'exist', 'skipOnError' => true, 'targetClass' => EngDictionary::className(), 'targetAttribute' => ['english_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'english_id' => 'English ID',
            'translate' => 'Translate',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'updated_by' => 'Updated By',
            'updated_at' => 'Updated At',
            'deleted' => 'Deleted',
        ];
    }
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Book' => [
                    'id',
                    'english_id',
                    'translate',
                    'created_by',
                    'created_at',
                    'updated_by',
                    'updated_at',
                    'deleted',
                ],
            ]
        );
        return $result;
    }

    public function one_fields()
    {
        return [
            'id' =>$this->id,
            'english_id'=>$this->english_id,
            'english' => $this->english->value,
            'translate'=>$this->translate,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name. " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name. " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEnglish()
    {
        return $this->hasOne(EngDictionary::className(), ['id' => 'english_id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserDictionaries()
    {
        return $this->hasMany(UserDictionary::className(), ['translate_id' => 'id']);
    }
}
