<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "theme".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $name
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 *
 * @property User $creator
 * @property User $updater
 * @property Task[] $tasks
 * @property Section $section
 */
class Theme extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theme';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'section_id', 'name'], 'required'],
            [['section_id', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => Section::className(), 'targetAttribute' => ['section_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/theme", "ID"),
            'section_id' => Yii::t("msg/theme", 'Section ID'),
            'name' => Yii::t("msg/theme", 'Name'),
            'deleted' => Yii::t("msg/theme", 'Deleted'),
            'created_by' => Yii::t("msg/theme", 'Create by'),
            'updated_by' => Yii::t("msg/theme", 'Update by'),
            'created_at' => Yii::t("msg/theme", 'Date create'),
            'updated_at' => Yii::t("msg/theme", 'Date update'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['theme_id' => 'theme.id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(Section::className(), ['id' => 'section_id']);
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'section_id' => $this->section_id,
            'section' => $this->section->name,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Theme' => [
                    'id',
                    'name',
                    'section_id',
                ]
            ]
        );
        return $result;
    }
}
