<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "level".
 *
 * @property integer $id
 * @property string $name
 * @property integer $deleted
 *
 * @property Task[] $tasks
 */
class Level extends \yii\db\ActiveRecord
{

    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['deleted'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdocYii::t("msg/level",
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/level", "ID"),
            'name' => Yii::t("msg/level",'Name'),
            'deleted' => Yii::t("msg/level",'Deleted'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['level_id' => 'id']);
    }
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Level' => [
                    'id',
                    'name',
                ],
            ]
        );
        return $result;
    }


    public function findModel()
    {
        return self::findOne([
            'name' => $this->name,
            'deleted' => self::NOT_DELETED
        ]);
    }
}