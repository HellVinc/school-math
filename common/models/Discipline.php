<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "discipline".
 *
 * @property integer $id
 * @property string $name
 * @property $classes
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property User $updater
 * @property BookDiscipline[] $bookDisciplines
 * @property ClassDiscipline[] $classDisciplines
 * @property MyDiscipline[] $myDisciplines
 * @property  $photoPath

 */
class Discipline extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
    public $classes;
    public $photo;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_by', 'created_at'], 'required'],
            [['deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['classes'], 'safe']
        ];
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/discipline", "ID"),
            'name' => Yii::t("msg/discipline", 'Name'),
            'created_by' => Yii::t("msg/discipline", 'Create by'),
            'updated_by' => Yii::t("msg/discipline", 'Update by'),
            'created_at' => Yii::t("msg/discipline", 'Date create'),
            'updated_at' => Yii::t("msg/discipline", 'Date update'),
            'deleted' => Yii::t("msg/discipline", 'Deleted'),
        ];
    }


    public function getPhotoPath()
    {
        if ($this->photo) {
            return Yii::$app->request->getHostInfo() . "/photo/disciplines" . $this->id . "/" . $this->photo;
        } else {
            return Yii::$app->request->getHostInfo() . "/photo/disciplines/empty_discipline.jpg";
        }
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
//            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'classes' => $this->classDisciplines,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Discipline' => [
                    'id',
                    'name',
                    'image' => function($model){
                        return $model->photoPath;
                    },
                    'classes' => function ($model) {
                        return $model->classDisciplines;
                    }
                ],
            ]
        );
        return $result;
    }

    public function checkModel()
    {

        $trans = Yii::$app->db->beginTransaction();
        $new = false;
        if ($this->isNewRecord) {
            $new = true;
        }
        if ($this->saveModel()) {
            if ($this->classes) {
                if ($new) {
                    $my_class = ClassDiscipline::saveAll($this->id, $this->classes);
                } else {
                    $my_class = ClassDiscipline::checkModel($this->id, $this->classes);
                }
                if ($my_class != true && $my_class->getErrors()) {
                    $trans->rollBack();
                    $this->addError('name', $my_class->getErrors());
                }

            }
            $trans->commit();
        }else{
            $trans->rollBack();
        }
        return $this;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getBookDisciplines()
    {
        return $this->hasMany(BookDiscipline::className(), ['discipline_id' => 'id']);
    }

    public
    function getClassDisciplines()
    {
        return $this->hasMany(ClassDiscipline::className(), ['discipline_id' => 'id'])->andOnCondition(['class_discipline.deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getMyDisciplines()
    {
        return $this->hasMany(MyDiscipline::className(), ['discipline_id' => 'id']);
    }
}
