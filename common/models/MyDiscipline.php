<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "my_discipline".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $discipline_id
 * @property string $teacher
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property Discipline $discipline
 * @property User $user
 */
class MyDiscipline extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'my_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'discipline_id','created_at'], 'required'],
            [['user_id', 'discipline_id', 'deleted','created_at'], 'integer'],
            [['teacher'], 'string', 'max' => 255],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['discipline_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/myDiscipline", "ID"),
            'user' => Yii::t("msg/myDiscipline", 'User'),
            'user_id' => Yii::t("msg/myDiscipline", 'User ID'),
            'discipline' => Yii::t("msg/myDiscipline", 'Discipline'),
            'discipline_id' => Yii::t("msg/myDiscipline", 'Discipline ID'),
            'teacher' => Yii::t("msg/myDiscipline", 'Teacher'),
            'created_at' => Yii::t("msg/myDiscipline", 'Date create'),
            'deleted' => Yii::t("msg/myDiscipline", 'Deleted'),
        ];
    }


    public function one_fields()
    {
        return [
            'id' =>$this->id,
            'user' => function ($this) {
                return $this->user->first_name . " " . $this->user->last_name;
            },
            'image' => $this->photoPath,
            'user_id' =>$this->user_id,
            'discipline' => $this->discipline->name,
            'discipline_id' =>$this->discipline_id,
            'teacher' =>$this->teacher,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                [
                    'common\models\MyDiscipline' => [
                        'id',
                        'user' => function ($model) {
                            return $model->user->first_name . " " . $model->user->last_name;
                        },
                        'user_id',
                        'discipline' => function ($model) {
                            return $model->discipline->name;
                        },
                        'discipline_id',
                        'teacher'
                    ],
                ],
            ]
        );
        return $result;
    }


    public function findModel()
    {
        return self::findOne([
            'user_id' => $this->user_id,
            'discipline_id' => $this->discipline_id,
            'teacher' => $this->teacher,
            'deleted' => self::NOT_DELETED
        ]);
    }

    public static function saveDiscipline($user_id, $all, $update = false)
    {
        //если редактируем профиль, то проверить старые записи
        if ($update) {
            $result = self::findOldSpecializations($all, $user_id);
            if ($result->getError()) {
                return $result;
            }
            $all = $result;
        }

        foreach ($all as $one) {
            $discipline = new self();
            $discipline->user_id = $user_id;
            $discipline->discipline_id = intval($one);
            if (!$discipline->saveWithCheckAndRestore() || $discipline->getErrors()) {
                return $discipline;
            }
        }
        return true;
    }

    public static function findOldSpecializations($new_arr, $user_id)
    {
        $all = self::find()->where(['user_id' => $user_id, 'deleted' => self::NOT_DELETED])->all();
        if ($all) {
            $all_arr = [];
            //записываэм все специализации в массив
            foreach ($all as $one) {
                $all_arr[] = $one->discipline_id;
            }

            //сравниваем новый массив с старым

            $diffs = array_diff($all_arr, $new_arr);
            if ($diffs) {
                $trans = Yii::$app->db->beginTransaction();
                foreach ($diffs as $diff) {
                    //удаление тех записей, которых нет в новом массиве
                    $result = self::deleteSpecialization($diff, $user_id);
                    if ($result->getError()) {
                        $trans->rollback();
                        return $result;
                    }
                }
                $trans->commit();
            }
            $only_new = array_diff($new_arr, $all_arr);
            return $only_new;
        }
        return true;
    }

    //удаление спициализаций
    public static function deleteSpecialization($id, $user_id)
    {
        $model = new self;
        $model->user_id = $user_id;
        $model->discipline_id = $id;
        $result = $model->findModel();
        if ($result) {
            return $result->remove();
        }
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['id' => 'discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookDiscipline()
    {
        return $this->hasMany(User::className(), ['my_discipline_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
