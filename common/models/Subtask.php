<?php

namespace common\models;

use Yii;
use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\modelWithFiles;
use common\components\traits\findRecords;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subtask".
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $text
 * @property string $result
 * @property integer $number
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $updated_by
 * @property integer $deleted
 *
 * @property Task $task
 * @property File[] $files
 * @property User $creator
 * @property User $updater
 * @property DoneTask $doneTask
 */
class Subtask extends ActiveRecord
{
    use soft;
    use modelWithFiles;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subtask';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'text'], 'required'],
            [['task_id', 'number', 'created_by', 'created_at', 'updated_at', 'updated_by', 'deleted'], 'integer'],
            [['text'], 'string'],
            [['result'], 'string', 'max' => 255],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ],
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/task", "ID"),
            'task_id' => Yii::t("msg/task_id", "Task ID"),
            'text' => Yii::t("msg/task", 'Text'),
            'result' => Yii::t("msg/task", 'Result'),
            'number' => Yii::t("msg/task", 'Number'),
            'created_by' => Yii::t("msg/task", 'Create by'),
            'updated_by' => Yii::t("msg/task", 'Update by'),
            'created_at' => Yii::t("msg/task", 'Date create'),
            'updated_at' => Yii::t("msg/task", 'Date update'),
            'deleted' => Yii::t("msg/task", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'done' => $this->doneTask ? $this->doneTask->one_fields() : false,
            'number' => $this->number,
            'text' => $this->text,
            'result' => $this->result,
            'created_by' => $this->created_by,
//            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
//            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'files' => $this->files,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Subtask' => [
                    'id',
                    'done' => function ($model) {
                        if ($model->doneTask) {
                            /** @var $model Subtask */
                            return $model->doneTask->one_fields();
                        }
                        return false;
                    },
                    'number',
                    'text',
                    'result',
                    'files'
                ],
            ]
        );
        return $result;
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    public function getDoneTask()
    {
        return $this->hasOne(DoneTask::className(), ['subtask_id' => 'id'])->andOnCondition(['deleted' => self::NOT_DELETED]);
    }

    public function getFiles()
    {
        return $this->hasMany(File::className(), ['parent_id' => 'id'])->andOnCondition(['file.deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
