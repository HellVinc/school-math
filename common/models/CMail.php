<?php
namespace common\models;

use Yii;
use yii\base\Model;

class CMail extends Model
{

    public static function User(){

        $HTML = \Yii::$app->view->render('@common/mail/test', [
        ]);
        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Регистрация пользователя')
            ->setHtmlBody( $HTML )
            ->send();
    }
     public static function UserRegister($user){

         $HTML = \Yii::$app->view->render('@common/mail/userRegister', [
             'user' => $user,
         ]);
         Yii::$app->mailer->compose()
             ->setFrom(Yii::$app->params['supportEmail'])
             ->setTo($user->email)
             ->setSubject('Регистрация пользователя')
             ->setHtmlBody( $HTML )
             ->send();
     }



    public static function ForgotPassword($user, $new_pass){

        $HTML = \Yii::$app->view->render('@common/mail/forgotPassword', [
            'user' => $user,
            'new_pass' =>$new_pass
        ]);

        $res = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['supportEmail'])
            ->setTo($user->email)
            ->setSubject('Восстановление пароля')
            ->setHtmlBody( $HTML )
            ->send();
        return $res;
    }


}