<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bookmark".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $book_id
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property Task $task
 */
class Bookmark extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
    public $explanation;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bookmark';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'book_id', 'created_by', 'created_at'], 'required'],
            [['task_id', 'created_by', 'created_at', 'updated_at', 'deleted', 'book_id'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/bookmark", "ID"),
            'task_id' => Yii::t("msg/bookmark", 'Task ID'),
            'created_by' => Yii::t("msg/bookmark", 'Created By'),
            'created_at' => Yii::t("msg/bookmark", 'Created At'),
            'updated_at' => Yii::t("msg/bookmark", 'Updated At'),
            'deleted' => Yii::t("msg/bookmark", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'book_id' => $this->book_id,
            'task_id' => $this->task_id,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }

    public function findModel($task_id)
    {
        $res = self::findOne([
            'created_by' => $this->created_by,
            'book_id' => $this->book_id,
            'task_id' => $task_id
        ]);
        return $res;
    }
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Bookmark' => [
                    'id' => function($model){
                        return $model->task->id;
                    },
                    'book_id',
                    'text' =>function($model){
                        return $model->task->text;
                    },
                    'created_by',
                    'created_at',
                    'updated_at',
                ],
            ]
        );
        return $result;
    }

    public static function saveOne($book_id, $task_id)
    {
        $model = new Bookmark();

        $model->created_by = Yii::$app->user->id;
        $model->book_id = $book_id;
        //проверяем. существует ли такая запись
        $mark = $model->findModel($task_id);
        if ($mark) {
            if ($mark->deleted == self::DELETED) {
                $mark->deleted = self::NOT_DELETED;
            }
            $mark->task_id = $task_id;
            $mark->updated_at = time();
            $mark->save();
            return $mark;
//            $this->addError(['number' => Yii::t('msg/error', 'Record was added before')]);
        } else {
            // сохраняем новую запись
            $model->task_id = $task_id;
            $model->created_at = time();
            $model->save();
            return $model;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

//    public function getExplanation()
//    {
//        $task = Task::findOne(['id' => $this->task_id]);
//        return $task['explanation'];
//    }
    public function setExplanation()
    {
        return Task::find()->select('explanation')->where(['id' => $this->task_id])->all();
    }
}
