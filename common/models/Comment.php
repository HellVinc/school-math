<?php

namespace common\models;

use common\models\search\MessageSearch;
use Yii;
use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "comment".
 *
 * @property integer $id
 * @property integer $task_id
 * @property boolean $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted
 *
 * @property User $updater
 * @property Task $task
 * @property User $creator
 * @property Message $messages
 */
class Comment extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const STATUS_CREATE = 0;
    const STATUS_READ = 1;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'comment';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'created_at', 'created_by'], 'required'],
            [['task_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted', 'status'], 'integer'],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            ['status', 'default', 'value' => self::STATUS_CREATE],
            ['status', 'in', 'range' => [self::STATUS_CREATE, self::STATUS_READ]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task ID',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
            'deleted' => 'Deleted',
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'task_id' => $this->task_id,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'role' => $this->creator->role_id,
            'created_at' => $this->created_at,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'updated_at' => $this->updated_at,
            'deleted' => $this->deleted,

        ];
    }

    public function all_fields($result)
    {

        $result['models'] = ArrayHelper::toArray($result['models'],

            [
                'common\models\Comment' => [
                    'id',
                    'task_id',
                    'status',
                    'creator' => function ($model) {
                        return $model->creator->first_name . " " . $model->creator->last_name;
                    },
                    'role' => function ($model) {
                        return $model->creator->role_id;
                    },
                    'created_by',
                    'avatar' => function ($model) {
                        return $model->creator->photoPath;
                    }
                ],

            ]
        );
        return $result;
    }

    public function findModel()
    {
        return self::findOne([
            'created_by' => Yii::$app->user->id,
            'task_id' => $this->task_id,
            'deleted' => self::NOT_DELETED
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMessages()
    {
        return $this->hasMany(Message::className(), ['comment_id' => 'id']);
    }

}
