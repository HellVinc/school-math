<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;


/**
 * This is the model class for table "class_discipline".
 *
 * @property integer $id
 * @property integer $discipline_id
 * @property integer $class
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property Discipline $discipline
 */
class ClassDiscipline extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'class_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['discipline_id', 'class', 'created_at'], 'required'],
            [['discipline_id', 'class', 'deleted', 'created_at'], 'integer'],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['discipline_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['class', 'in', 'range' => [5, 6, 7, 8, 9, 10, 11]],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'discipline_id' => 'Discipline ID',
            'class' => 'Class',
            'deleted' => 'Deleted',
        ];
    }


    public function findModel()
    {
        return self::findOne([
            'class' => $this->class,
            'discipline_id' => $this->discipline_id,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['id' => 'discipline_id']);
    }

    public static function checkModel($discipline_id, $new_classes)
    {
        $my_classes = self::find()->select('class')->where(['discipline_id' => $discipline_id, 'deleted' => self::NOT_DELETED])->all();
        if ($my_classes) {
            $my_arr = [];
            foreach ($my_classes as $one) {
                $my_arr[] = $one->class;
            }
            $diff_for_delete = array_diff($my_arr, $new_classes);
            if ($diff_for_delete) {
                $result = self::deleteOld($discipline_id, $diff_for_delete);
                if ($result != true && $result->getErrors()) {
                    return $result;
                }
            }
            $diff_for_save = array_diff($new_classes, $my_arr);
            if ($diff_for_save) {
                $result = self::saveAll($discipline_id, $diff_for_save);
                if ($result != true && $result->getErrors()) {
                    return $result;
                }
            }

        }
        return true;
    }

    public static function saveAll($discipline_id, $new_classes)
    {
        foreach ($new_classes as $one) {
            $class = new self();
            $class->discipline_id = $discipline_id;
            $class->class = $one;
            if ($class->saveWithCheckAndRestore() && $class->getErrors()) {
                return $class;
            }
        }
        return true;
    }

    public static function deleteOld($discipline_id, $new_classes)
    {
        foreach ($new_classes as $one) {
            $model = new self();
            $model->discipline_id = $discipline_id;
            $model->class = $one;
            $class = $model->findModel();
            if ($class->remove() && $class->getErrors()) {
                return $class;
            }
        }
        return true;
    }
}
