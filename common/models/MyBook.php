<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "my_book".
 *
 * @property integer $id
 * @property integer $book_discipline_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property BookDiscipline $bookDiscipline
 */
class MyBook extends \yii\db\ActiveRecord
{

    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'my_book';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_discipline_id', 'created_at','user_id'], 'required'],
            [['book_discipline_id', 'deleted', 'created_at','user_id'], 'integer'],
            [['book_discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => BookDiscipline::className(), 'targetAttribute' => ['book_discipline_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/myBook", "ID"),

            'book_discipline_id' => Yii::t("msg/myBook", 'Discipline ID'),
            'created_at' => Yii::t("msg/myBook", 'Date create'),
            'deleted' => Yii::t("msg/myBook", 'Deleted'),
        ];
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'book_discipline_id' => $this->book_discipline_id,
            'user_id' => $this->user_id,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\MyBook' => [
                    'id',
                    'book' => function ($model) {
                        /** @var $model MyBook */
                        return $model->bookDiscipline->book->one_fields();
                    },
                    'book_discipline_id',
                    'user_id',
                ],
            ]
        );
        return $result;
    }


    public function findModel()
    {
        return self::findOne([
            'book_discipline_id' => $this->book_discipline_id,
            'user_id' => Yii::$app->user->id,
            'deleted' => self::NOT_DELETED
        ]);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookDiscipline()
    {
        return $this->hasOne(BookDiscipline::className(), ['id' => 'book_discipline_id']);
    }


}
