<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "book_discipline".
 *
 * @property integer $id
 * @property integer $book_id
 * @property integer $discipline_id
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property Discipline $discipline
 * @property Book $book
 */
class BookDiscipline extends ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'book_discipline';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['book_id', 'discipline_id', 'created_at'], 'required'],
            [['book_id', 'discipline_id', 'deleted', 'created_at'], 'integer'],
            [['discipline_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discipline::className(), 'targetAttribute' => ['discipline_id' => 'id']],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => Book::className(), 'targetAttribute' => ['book_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'updated_at'
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/bookDiscipline", "ID"),
            'book' => 'Book',
            'book_id' => 'Book ID',
            'discipline' => 'Discipline',
            'discipline_id' => 'Discipline ID',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * search all models
     * @param $result
     * @return array
     */

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\BookDiscipline' => [
                    'id',
                    'book' => function ($model) {
                        /** @var $model BookDiscipline */
                        return $model->book->one_fields();
                    },
                    'discipline' => function ($model) {
                        return $model->discipline->name;
                    },
                    'discipline_id',
                ],
            ]
        );
        return $result;

    }

    public function findModel()
    {
        return self::findOne([
            'book_id' => $this->book_id,
            'discipline_id' => $this->discipline_id,
            'deleted' => self::NOT_DELETED
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscipline()
    {
        return $this->hasOne(Discipline::className(), ['id' => 'discipline_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBook()
    {
        return $this->hasOne(Book::className(), ['id' => 'book_id']);
    }

}
