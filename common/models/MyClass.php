<?php

namespace common\models;

use Yii;

use common\components\traits\errors;
use common\components\traits\soft;

use common\models\search\MyClassSearch;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "my_class".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $number
 * @property string $letter
 * @property string $type
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property User $user
 * @property ClassDiscipline $classDiscipline
 */
class MyClass extends \yii\db\ActiveRecord
{

    use soft;
    use errors;

    const TYPE_PUPIL = 0;
    const TYPE_SELF = 1;


    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'my_class';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'number','created_at'], 'required'],
            [['user_id', 'number', 'type', 'deleted','created_at'], 'integer'],
            [['letter'], 'string', 'max' => 5],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            ['deleted', 'default', 'value' => self::TYPE_PUPIL],
            ['type', 'in', 'range' => [self::TYPE_PUPIL, self::TYPE_SELF]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/myClass", "ID"),
            'user_id' => Yii::t("msg/myClass", 'User ID'),
            'number' => Yii::t("msg/myClass", 'Number'),
            'letter' => Yii::t("msg/myClass", 'Class'),
            'created_at' => Yii::t("msg/myClass", 'Date create'),
            'deleted' => Yii::t("msg/myClass", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' =>$this->id,
//            'user_id',
//            'user' => function($model){
//                return $model->user->first_name . " " . $model->user->last_name;
//            },
            'number' =>$this->number,
            'letter' =>$this->letter
        ];
    }

    public static function searchModel($request)
    {
        $searchModel = new MyClassSearch();
        $searchModel->deleted = self::NOT_DELETED;
        if (!$searchModel->load(['MyClassSearch' => $request]) || !$searchModel->validate()) {
            return false;
        }
        if (!$request['discipline_id']) {
            $dataProvider = $searchModel->search();
        } else {
            $dataProvider = $searchModel->searchByDiscipline();
        }
        $models = $dataProvider->getModels();
        return [
            'models' => $models,
            'count_page' => $dataProvider->pagination->pageSize,
            'count_model' => $dataProvider->getTotalCount()
        ];

    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                [
                    'common\models\MyClass' => [
                        'id',
                        'number',
                        'letter'

                    ],
                ]
            ]
        );
        return $result;
    }


    public function findModel()
    {
        return self::findOne([
            'user_id' => $this->user_id,
            'number' => $this->number,
            'letter' => $this->letter,
            'type' => $this->type
        ]);
    }


    public static function saveClasses($user_id,  $classes)
    {
        foreach ($classes as $one) {
            $class = new MyClass();
            if ($class->saveOne($user_id,  self::TYPE_PUPIL, $one) != true) {
                return $class;
            }
        }
    }

    public function saveOne($user_id, $type, $number){
        $this->user_id = $user_id;
        $this->type = $type;
        $this->number = $number;
        return $this->saveWithCheckAndRestore();
    }

    public static function checkModel($user_id, $new_classes)
    {
        $my_classes = self::find()->select('number')->where(['user_id' => $user_id, 'type' => MyClass::TYPE_PUPIL, 'deleted' => self::NOT_DELETED])->all();
        if ($my_classes) {
            $my_arr = [];
            foreach ($my_classes as $one) {
                $my_arr[] = $one->number;
            }
            $diff_for_delete = array_diff($my_arr, $new_classes);
            if ($diff_for_delete) {
                $result = self::deleteOld($user_id, $diff_for_delete);
                if ($result && $result->getErrors()) {
                    return $result;
                }
            }
            $diff_for_save = array_diff($new_classes, $my_arr);
            if ($diff_for_save) {
                $result = self::saveClasses($user_id, $diff_for_save);
                if ($result && $result->getErrors()) {
                    return $result;
                }
            }
        }else{
            return self::saveClasses($user_id, $new_classes);
        }
    }


    public static function deleteOld($user_id, $new_classes)
    {
        foreach ($new_classes as $one) {
            $model = new self();
            $model->user_id = $user_id;
            $model->number = $one;
            $model->type = MyClass::TYPE_PUPIL;
            $class = $model->findModel();
            if ($class->remove() && $class->getErrors()) {
                return $class;
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getClassDiscipline()
    {
        return $this->hasOne(ClassDiscipline::className(), ['class' => 'number'])->andOnCondition(['class_discipline.deleted' => self::NOT_DELETED]);
    }

}
