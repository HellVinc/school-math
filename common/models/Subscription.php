<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "subscription".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $transfer_id
 * @property integer $code
 * @property integer $date_from
 * @property integer $date_to
 * @property integer $my_pupil_id
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 *
 * @property User $creator
 * @property User $updater
 * @property Transfer $transfer
 * @property User $user
 */
class Subscription extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'subscription';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'user_id', 'transfer_id'], 'required'],

            [['user_id', 'transfer_id', 'date_from', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['code'], 'string', 'max' => 50],
            [['transfer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transfer::className(), 'targetAttribute' => ['transfer_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['my_pupil_id'], 'exist', 'skipOnError' => true, 'targetClass' => MyPupil::className(), 'targetAttribute' => ['my_pupil_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/subscription", "ID"),
            'user_id' => 'User ID',
            'transfer_id' => 'Transfer ID',
            'date_from' => 'From',
            'date_to' => 'To',
            'deleted' => 'Deleted',
            'created_by' => Yii::t("msg/subscription", 'Create by'),
            'updated_by' => Yii::t("msg/subscription", 'Update by'),
            'created_at' => Yii::t("msg/subscription", 'Date create'),
            'updated_at' => Yii::t("msg/subscription", 'Date update'),
        ];
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => $this->user->first_name . " " . $this->user->last_name,
//            'transfer' =>  function($this){
//                return $this->transfer;
//            },
            'date_from' => $this->date_from,
            'date_to' => $this->date_to,
            'deleted' => $this->deleted,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],

            [
                'common\models\Subscription' => [
                    'id',
                    'user_id',
                    'user' => function ($model) {
                        return $model->user->first_name . " " . $model->user->last_name;
                    },
//                            'transfer' =>  function($model){
//                                return $model->transfer;
//                            },
                    'date_from',
                    'image' => function ($model) {
                        /** @var $model Subscription */
                        return $model->creator->photoPath;
                    },
                    'date_to',
                    'deleted',

                ],
            ]

        );
        return $result;
    }

    public function findModel()
    {
        return self::find()->where([
            'user_id' => $this->user_id,
            'transfer_id' => $this->transfer_id,
            'deleted' => self::NOT_DELETED
        ])->one();
    }

    public function saveModel($transf)
    {
        //TODO
        $subsc = new Subscription();
        $subsc->transfer_id = $transf->id;
        $subsc->user_id = $transf->user_id;

        //проверяем. существует ли такая запись
        $model = $subsc->findModel();
        if ($model) {
            return ['error' => Yii::t('msg/error', 'Record was added before')];
        }
        //ищем подписку
        $subscription = $subsc->checkSubscription();

        if ($subscription) {
            $subscription->updated_by = Yii::$app->user->id;
            $subscription->updated_at = time();
            //добавляєм период к подписке
            $subscription->date_to = $subsc->checkDays($transf->summa);
            return $subscription->save();

        } else {

            $subsc->created_by = Yii::$app->user->id;
            $subsc->created_at = time();
            // сохраняем новую запись
//            $subsc->code = substr(uniqid(rand(1, 6)), 0, 16);
            $subsc->code = Yii::$app->request->post('code');
            $subsc->date_from = time();
            $subsc->date_to = $subsc->checkDays($transf->summa);
            if (!$subsc->save()) {
                return $subsc->errors;
            }
            return $subsc->save();
        }
    }

    //проверить дествительность подписки

    public function checkSubscription()
    {
        return self::find()->where([
            'user_id' => Yii::$app->user->id,
            'deleted' => self::NOT_DELETED])
            ->andWhere(['>=', 'date_to', time()])
//            ->andOnCondition('DATE(FROM_UNIXTIME(subscription.date_to) >= DATE(UNIX_TIMESTAMP())')
            ->one();
    }

    //выщитать количество дней для подписки
    public function checkDays($summa)
    {
        $date = new \DateTime($this->date_to);
        $date->modify(" + " . intval($summa) . " day");
        return $this->date_to = $date->getTimestamp();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTransfer()
    {
        return $this->hasOne(Transfer::className(), ['id' => 'transfer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMyPupil()
    {
        return $this->hasOne(User::className(), ['id' => 'my_pupil_id']);
    }


}
