<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;


use Yii;
use yii\helpers\ArrayHelper;
use yii\validators\SafeValidator;

/**
 * This is the model class for table "sity".
 *
 * @property integer $id
 * @property string $name
 * @property string $region_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property School[] $schools
 * @property User[] $users
 * @property Region $region
 */
class Sity extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const COUNTRY = 1;
    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'region_id', 'created_at'], 'required'],
            [['name',], 'unique'],
            [['id', 'deleted', 'region_id', 'created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/sity", "ID"),
            'name' => Yii::t("msg/sity", 'Name'),
            'deleted' => Yii::t("msg/sity", 'Deleted'),
            'created_by' => Yii::t("msg/sity", 'Create by'),
            'updated_by' => Yii::t("msg/sity", 'Update by'),
            'created_at' => Yii::t("msg/sity", 'Date create'),
            'updated_at' => Yii::t("msg/sity", 'Date update'),
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Sity' => [
                    'id',
                    'name',
                    'schools' => function ($model) {
                        /** @var $model Sity */
                        return $model->schools;
                    }
                ]
            ]
        );
        return $result;
    }


    public static function checkModel($sity_name, $region_name)
    {
        $model = self::findOne(['name' => $sity_name]);
        if (!$model) {

            $region = Region::checkModel($region_name);
            if ($region->getErrors()) {
                $model->addError('sity_name', $region->getErrors());
                return $model;
            }
            $model = new Sity;
            $model->name = $sity_name;
            $model->region_id = $region->id;
            $model->created_at = time();


        } else if ($model->deleted = self::DELETED) {
            $model->updated_at = time();
            $model->deleted = self::NOT_DELETED;
        }
        $model->save();
        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getSchools()
    {
        return $this->hasMany(School::className(), ['sity_id' => 'id'])->andOnCondition(['deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getRegion()
    {
        return $this->hasOne(Region::className(), ['id' => 'region_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getUsers()
    {
        return $this->hasMany(User::className(), ['sity_id' => 'id']);
    }
}
