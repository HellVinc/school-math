<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\modelWithFiles;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "done_task".
 *
 * @property integer $id
 * @property integer $task_id
 * @property integer $subtask_id
 * @property integer $result
 * @property integer $status
 * @property string $answer
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property Checkup[] $checkups
 * @property User $user
 * @property User $creator
 * @property User $updater
 * @property Task $task
 * @property Subtask $subtask
 * @property File[] $files
 */
class DoneTask extends \yii\db\ActiveRecord
{
    use soft;
    use modelWithFiles;
    use findRecords;
    use errors;

    const STATUS_NONE = 0;
    const STATUS_NEED_CHECKUP = 1;
    const STATUS_CHECKUP = 2;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const ANSWER_RIGHT = 1;
    const ANSWER_WRONG = 0;

    public $error;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'done_task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'answer'], 'required'],
            [['status', 'created_at', 'updated_at', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at', 'result'], 'integer'],
            [['answer'], 'string'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
            [['subtask_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subtask::className(), 'targetAttribute' => ['subtask_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            ['status', 'default', 'value' => self::STATUS_NONE],
            ['status', 'in', 'range' => [self::STATUS_NONE, self::STATUS_NEED_CHECKUP, self::STATUS_CHECKUP]],


            ['result', 'default', 'value' => self::ANSWER_RIGHT],
            ['result', 'in', 'range' => [self::ANSWER_WRONG, self::ANSWER_RIGHT]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/doneTask", "ID"),
            'task_id' => Yii::t("msg/doneTask", 'Task'),
            'subtask_id' => Yii::t("msg/doneTask", 'Subtask'),
            'result' => Yii::t("msg/doneTask", 'Result'),
            'status' => Yii::t("msg/doneTask", 'Check tutor'),
            'answer' => Yii::t("msg/doneTask", 'Answer'),
            'created_by' => Yii::t("msg/task", 'Create by'),
            'updated_by' => Yii::t("msg/task", 'Update by'),
            'created_at' => Yii::t("msg/task", 'Date create'),
            'updated_at' => Yii::t("msg/task", 'Date update'),
            'deleted' => Yii::t("msg/doneTask", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'task_id' => $this->task_id,
            'subtask_id' => $this->subtask_id,
            'result' => $this->result,
            'status' => $this->status,
            'answer' => $this->answer,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
//            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'files' => $this->files,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                [
                    DoneTask::className() => [
                        'id',
                        'task_id',
                        'subtask_id',
                        'created_by',
                        'result',
                        'answer',
                        'creator',
                        'image' => function($model){
                            return $model->creator->photoPath;
                        },
                        'status',
                    ],
                ],
            ]
        );
        return $result;
    }

    public function findModel()
    {
        return self::findOne(['task_id' => $this->task_id, 'created_by' => Yii::$app->user->id]);
    }

    public function checkModel()
    {
        $model = $this->findModel();
        if ($model) {
            if ($model->deleted == self::DELETED) {
                $model->deleted = self::NOT_DELETED;
            }
            $model->checkResult();
            $model->saveModel();
            return $model;

        } else {
            // сохраняем новую запись
            $this->created_at = time();
            $this->checkResult();
            $this->saveModel();
        }
        return $this;
    }

    public function checkResult()
    {
        $answer = $this->task->result;
        if (isset($this->subtask->result)) {
            $answer = $this->subtask->result;
        }
        if ($this->answer == $answer) {
            $this->result = self::ANSWER_RIGHT;
            //добавляем комент - нотатку
        } else {
            $this->result = self::ANSWER_WRONG;
        }
        $res = Message::createNote($this->task_id, $this->answer);
        return true;
    }

    /**
     *  изменение статуса работы
     * @param $id
     * @param $status
     * @return $this
     */
    public function changeStatus($status)
    {
        $this->status = $status;
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckups()
    {
        return $this->hasMany(Checkup::className(), ['done_task_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubtask()
    {
        return $this->hasOne(Subtask::className(), ['id' => 'subtask_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['parent_id' => 'id'])->andOnCondition(['file.deleted' => self::NOT_DELETED]);
    }
}
