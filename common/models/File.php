<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;


/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $table
 * @property string $extension
 * @property string $url
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 * @property string $filePath
 * @property string $fileDir
 *
 * @property User $creator
 * @property User $updater
 *
 * @property Checkup $parent
 * @property Task $parent0
 * @property DoneTask $parent1
 */
class File extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'table', 'extension', 'url', 'cr ceated_by', 'created_at',], 'required'],
            [['parent_id', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['table', 'extension', 'url'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
//            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Checkup::className(), 'targetAttribute' => ['parent_id' => 'id']],
//            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['parent_id' => 'id']],
//            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoneTask::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/file", "ID"),
            'parent_id' => Yii::t("msg/file", 'Parent ID'),
            'table' => Yii::t("msg/file", 'Table'),
            'extension' => Yii::t("msg/file", 'Extension'),
            'url' => Yii::t("msg/file", 'Url'),
            'deleted' => Yii::t("msg/file", 'Deleted'),
            'created_by' => Yii::t("msg/file", 'Create by'),
            'updated_by' => Yii::t("msg/file", 'Update by'),
            'created_at' => Yii::t("msg/file", 'Date create'),
            'updated_at' => Yii::t("msg/file", 'Date update'),
        ];
    }


    public function fields()
{
    $this->url  = $this->filePath;
    return parent::fields();
}


    public static function uploadOne($name, $id, $table)
    {
        $file = new self();
        $result = Yii::$app->uploadFile->upload($name, $id, $table);
        if (!$result) {
            return $file->addError('error', 'File not saved');
        }
        $file->parent_id = $id;
        $file->table = $table;
        $file->created_at = time();
        $file->created_by = Yii::$app->user->id;
        $file->extension = $result->file->extension;
        $file->url = $result->name . '.' . $result->file->extension;
        $file->save();
        return $file;
    }

    public function saveModel($model)
    {
        if (is_array($_FILES)) {
            foreach ($_FILES as $name => $one) {
                $file = self::uploadOne($name, $model->id, $model->tableName());
                if ($file && $file->getErrors()) {
                    return $file;
                }
            }
            return $this;
        } else {
            //TODO : test with Vova

            return self::uploadOne('file', $this->id, $this->tableName());
        }
    }

    public static function removeWithParent($all)
    {
        foreach ($all as $one) {
            $file = File::findOne($one->id);
            if (!$file->remove()) {
                return $file;
            }
            unlink($file->fileDir); 
        }
        return true;
    }

    public function getFilePath()
    {
        return Yii::$app->request->hostInfo . "/files/" . $this->table . "/" . $this->parent_id ."/". $this->url;
    }

    public function getFileDir()
    {

        return dirname(Yii::getAlias('@app')) . '/files/' . $this->table . "/" . $this->parent_id ."/". $this->url;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
//    public function getParent()
//    {
//        return $this->hasOne(Checkup::className(), ['id' => 'parent_id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getParent0()
//    {
//        return $this->hasOne(Task::className(), ['id' => 'parent_id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getParent1()
//    {
//        return $this->hasOne(DoneTask::className(), ['id' => 'parent_id']);
//    }


}
