<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Book;

/**
 * BookSearch represents the model behind the search form about `common\models\Book`.
 */
class BookSearch extends Book
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    public $discipline_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'year', 'deleted','class',  'page', 'page_size','paid_content','created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['discipline_id', 'name', 'author', 'notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Book::find();
        $query->joinWith('bookDisciplines');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else{
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'book.year' => $this->year,
            'book.class' => $this->class,
            'book.paid_content' => $this->paid_content,
            'book.deleted' => $this->deleted,
            'book.created_by' => $this->created_by,
            'book.updated_by' => $this->updated_by,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`book`.created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        $query->andFilterWhere([
            'book_discipline.discipline_id' => $this->discipline_id,

        ]);

        $query->andFilterWhere(['like', 'book.name', $this->name])
            ->andFilterWhere(['like', 'book.author', $this->author])
            ->andFilterWhere(['like', 'book.notes', $this->notes]);

       return $dataProvider;
    }

    public function free()
    {
        $query = Book::find();
        $query->joinWith('bookDisciplines');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $query->andFilterWhere([
            'book.year' => $this->year,
            'book.class' => $this->class,
            'book.deleted' => $this->deleted,
        ]);

        $query->andOnCondition('book_discipline.discipline_id is null OR book_discipline.deleted = 1');

        return $dataProvider;
    }
}
