<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MyPupil;

/**
 * MyPupilSearch represents the model behind the search form about `common\models\MyPupil`.
 */
class MyPupilSearch extends MyPupil
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'pupil_id', 'user_id', 'type', 'created_at', 'deleted',  'deleted', 'page', 'page_size'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = MyPupil::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else{
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([

            'pupil_id' => $this->pupil_id,
            'user_id' => $this->user_id,
            'type' => $this->type,
            'deleted' => $this->deleted,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        return $dataProvider;
    }
}
