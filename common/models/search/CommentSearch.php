<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Comment;


/**
 * CommentSearch represents the model behind the search form about `common\models\Comment`.
 */
class CommentSearch extends Comment
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'task_id', 'created_at', 'updated_at', 'created_by', 'updated_by', 'deleted', 'page', 'page_size','status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Comment::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        } else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([

            'task_id' => $this->task_id,
            'created_by' => $this->created_by,
            'status' => $this->status,
            'deleted' => $this->deleted,
        ]);

        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))', $this->created_at, date('d-m-Y', time())]);
        }


        return $dataProvider;
    }
}
