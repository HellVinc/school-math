<?php

namespace common\models\search;

use common\models\Role;
use common\models\MyClass;
use common\models\Sity;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\User;
use yii\db\Query;

/**
 * UserSearch represents the model behind the search form about `common\models\User`.
 */
class UserSearch extends User
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $number;
    public $letter;
    public $city_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'role_id', 'confirm', 'balance', 'phone', 'school_id', 'sity_id', 'updated_at', 'created_at', 'deleted', 'page', 'page_size', 'number', 'push_status'], 'integer'],
            [['email', 'first_name', 'second_name', 'last_name', 'photo', 'letter', 'city_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = User::find();

//        $query->joinWith('myClasses');
        $query->joinWith('sity');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'user.role_id' => $this->role_id,
            'user.confirm' => $this->confirm,
            'user.balance' => $this->balance,
            'user.phone' => $this->phone,
            'user.school_id' => $this->school_id,
            'user.sity_id' => $this->sity_id,
            'user.deleted' => $this->deleted,
            'user.push_status' => $this->push_status,
            'my_class.number' => $this->number,
            'my_class.letter' => $this->letter,
        ]);
        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))', $this->created_at, date('d-m-Y', time())]);
        }

        if ($this->first_name && $this->last_name) {
            $query->andFilterWhere(['or',
                ['and',
                    ['like', 'user.first_name', "$this->last_name%", false],
                    ['like', 'user.last_name', "$this->first_name%", false],
                ],
                ['and',
                    ['like', 'user.first_name', "$this->first_name%", false],
                    ['like', 'user.last_name', "$this->last_name%", false],
                ]
            ]);
        } else {
            $query->andFilterWhere(['or',
                ['like', 'user.first_name', "$this->first_name%", false],
                ['like', 'user.last_name', "$this->first_name%", false]
            ]);
        }

        $query->andFilterWhere(['like', 'user.email', $this->email])
//            ->andFilterWhere(['like', 'user.first_name', $this->first_name])
            ->andFilterWhere(['like', 'user.second_name', $this->second_name])
//            ->andFilterWhere(['like', 'user.last_name', $this->last_name])
//            ->andFilterWhere(['like', 'school.sity_id', $this->school_id])
            ->andFilterWhere(['like', 'sity.name', $this->city_name]);

        return $dataProvider;
    }

    public function searchPupil()
    {
        $query = User::find();

        $query->joinWith('myClasses');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        } else {
            $dataProvider->pagination = false;
        }

        $query->andOnCondition('user.role_id = ' . Role::LEARNER . ' OR user.role_id = ' . Role::TUTOR);


        // grid filtering conditions
        $query->andFilterWhere([
            'user.school_id' => $this->school_id,
            'user.deleted' => self::NOT_DELETED,
            'my_class.number' => $this->number,
            'my_class.letter' => $this->letter,
//            'user.sity_id' => $this->sity_id,
        ]);

        $query->andFilterWhere(['like', 'user.first_name', $this->first_name])
            ->andFilterWhere(['like', 'user.second_name', $this->second_name])
            ->andFilterWhere(['like', 'user.last_name', $this->last_name]);

        return $dataProvider;
    }

    public function searchAllForClass()
    {
        if ($this->load(Yii::$app->request->get())) {
            $classes = [];
            for ($i = 1; $i <= 11; $i++) {
                //ищем букви для класов в базе
                $all = MyClass::find()->joinWith('user')
                    ->select('my_class.letter')
                    ->where(['my_class.number' => $i, 'my_class.deleted' => 0])
                    ->andFilterWhere(['user.school_id' => $this->school_id])
                    ->groupBy('my_class.letter')->all();
                //если запись есть в таблице my_class
                if (count($all) > 0) {
                    $classes[$i] = $all;
                }
            }
            $arr = [];
            //перебираэм все класы
            foreach ($classes as $key => $one) {
                $this->number = $key;
                //перебираем буквы этого класа
                foreach ($one as $j => $let) {
                    //если указано букву класа
                    if ($let->letter) {
                        $this->letter = $let->letter;
                    }
                    $arr[$key] = ['number' => $key];
                    $result = $this->searchPupil();
                    $models = $result->getModels();

                    if ($models) {
                        //записываем найденые модели
                        $arr[$key]['classes'][$j] = [
                            'letter' => $let->letter,
                            'users' => $this->all_fields_class($models)
                        ];
                    }
                    $this->letter = null;
                }
                //добавляем клас в масив результатов


            }
            return $arr;

        }
        return $this->getErrors();
    }

    public function searchAllForCity()
    {
        if ($this->load(Yii::$app->request->get())) {
            $this->role_id = Role::TUTOR;

            if ($this->city_name) {
                $result = $this->search();
                $models = $result->getModels();
                return ['1' => ['tutors' => $this->all_fields_class($models)]];
            } else {
                $cities = User::find()->select('user.sity_id')->joinWith('role')->where(['role.id' => Role::TUTOR, 'user.deleted' => User::NOT_DELETED])->groupBy('user.sity_id')->all();
                $arr = [];
                foreach ($cities as $key => $one) {
                    $this->sity_id = $one->sity_id;
                    $result = $this->search();
                    $models = $result->getModels();
                    $arr [$key] = [
                        'sity' => Sity::findOne($this->sity_id)->name,
                        'sity_id' => $this->sity_id,
                        'tutors' => $this->all_fields_class($models)
                    ];
                }
                return $arr;
            }
        }
        return $this->getErrors();
    }
}
