<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MyClass;
use common\models\User;

/**
 * MyClassSearch represents the model behind the search form about `common\models\MyClass`.
 */
class MyClassSearch extends MyClass
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $school_id;
    public $discipline_id;
    public $arr;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'school_id', 'discipline_id', 'number', 'deleted', 'page', 'page_size','created_at'], 'integer'],
            [['letter', 'arr'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = MyClass::find();

        $query->select('my_class.number, my_class.letter');
        $query->joinWith('user');
        $query->joinWith('classDiscipline');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        } 
//        else {
//            $dataProvider->pagination = false;
//        }
        // grid filtering conditions
        $query->andFilterWhere([
            'my_class.number' => $this->number,
            'my_class.user_id' => $this->user_id,
            'my_class.deleted' => $this->deleted,
            'user.school_id' => $this->school_id,

            'class_discipline.discipline_id' => $this->discipline_id,
        ]);
        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`class_discipline`.created_at))', $this->created_at, date('d-m-Y', time())]);
        }
        $query->andFilterWhere(['like', 'my_class.letter', $this->letter]);

        $query->orderBy('my_class.number asc, my_class.letter asc');

//        $query->distinct();
        $query->groupBy('my_class.number, my_class.letter');


        return $dataProvider;
    }

    public function searchByDiscipline()
    {
        $query = MyClass::find();

        $query->joinWith('user');
        $query->joinWith('classDiscipline');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        } else {
            $dataProvider->pagination = false;
        }
        // grid filtering conditions
        $query->andFilterWhere([
            'my_class.id' => $this->id,
            'my_class.number' => $this->number,
            'my_class.user_id' => Yii::$app->user->id,
            'my_class.deleted' => $this->deleted,
            'user.school_id' => $this->school_id,
            'class_discipline.discipline_id' => $this->discipline_id,
        ]);

        $query->orderBy('number asc, letter asc');
        $query->orderBy('number asc, letter asc');

        $query->andFilterWhere(['like', 'my_class.letter', $this->letter]);

        return $dataProvider;
    }
}
