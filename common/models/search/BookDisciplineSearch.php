<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BookDiscipline;

/**
 * BookDisciplineSearch represents the model behind the search form about `common\models\BookDiscipline`.
 */
class BookDisciplineSearch extends BookDiscipline
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'book_id', 'discipline_id', 'deleted', 'page', 'page_size','created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = BookDiscipline::find()->where([$this->id => 0]);
        $query->joinWith('book');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else{
//            $dataProvider->pagination = false;
//        }
        // grid filtering conditions
        $query->andFilterWhere([
            'book_discipline.book_id' => $this->book_id,
            'book_discipline.discipline_id' => $this->discipline_id,
            'book_discipline.deleted' => $this->deleted,
        ]);

        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`book_discipline`.created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        $query->andOnCondition(['book.deleted' => self::NOT_DELETED]);

        return $dataProvider;
    }
}
