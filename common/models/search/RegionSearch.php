<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Region;

/**
 * RegionSearch represents the model behind the search form about `common\models\Region`.
 */
class RegionSearch extends Region
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'country_id', 'deleted','page', 'page_size','created_at', 'updated_at'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Region::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        } 
//        else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'country_id' => $this->country_id,
            'deleted' => $this->deleted,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }


        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }

    public static function findByName($name)
    {
        return self::findOne(['name' => $name]);
    }
}
