<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Subscription;

/**
 * SubscriptionSearch represents the model behind the search form about `common\models\Subscription`.
 */
class SubscriptionSearch extends Subscription
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $min;
    public $max;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'transfer_id', 'date_from', 'date_to', 'deleted', 'page', 'page_size','created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['code', 'min', 'max'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Subscription::find()->where(['my_pupil_id' => null]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }else{
            $dataProvider->pagination = false;
        }

        if($this->min){
            if($this->date_from) {
                $query->andOnCondition('`date_from` <= ' . $this->date_from);
            }
            if($this->date_to){
                $query->andOnCondition('"date_to" <='.$this->date_to);
            }
        }elseif($this->max){
            if($this->date_from) {
                $query->andOnCondition('`date_from`  >=' . $this->date_from);
            }
            if($this->date_to){
                $query->andOnCondition('"date_to" >='.$this->date_to);
            }
        }else{
            $query->andFilterWhere([
                'date_from' => $this->date_from,
                'date_to' => $this->date_to,
            ]);
        }


        // grid filtering conditions
        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'transfer_id' => $this->transfer_id,
            'code' => $this->code,
            'deleted' => $this->deleted,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }



        return $dataProvider;
    }
}
