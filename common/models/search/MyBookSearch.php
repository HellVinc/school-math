<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\MyBook;

/**
 * MyBookSearch represents the model behind the search form about `common\models\MyBook`.
 */
class MyBookSearch extends MyBook
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    public $discipline_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'page', 'page_size', 'created_at','discipline_id','user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = MyBook::find();

        $query->joinWith('bookDiscipline');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'my_book.book_discipline_id' => $this->book_discipline_id,
            'my_book.deleted' => $this->deleted,
            'my_book.user_id' => Yii::$app->user->id,
            'book_discipline.discipline_id' => $this->discipline_id,
        ]);
        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`my_book`.created_at))', $this->created_at, date('d-m-Y', time())]);
        }
        $query->andOnCondition(['book_discipline.deleted' => self::NOT_DELETED]);


        return $dataProvider;
    }
}
