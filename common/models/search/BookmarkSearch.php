<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Bookmark;

/**
 * BookmarkSearch represents the model behind the search form about `common\models\Bookmark`.
 */
class BookmarkSearch extends Bookmark
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $sort = [
        'updated_at' => SORT_DESC
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'task_id', 'created_by', 'created_at', 'updated_at', 'deleted','page', 'page_size'], 'integer'],
            [['sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Bookmark::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->defaultOrder = $this->sort;
        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else{
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'task_id' => $this->task_id,
            'created_by' => $this->created_by,
            'deleted' => $this->deleted,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

        return $dataProvider;
    }
}
