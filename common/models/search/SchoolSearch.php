<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\School;

/**
 * SchoolSearch represents the model behind the search form about `common\models\School`.
 */
class SchoolSearch extends School
{

    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;

    public $sity_name;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sity_id', 'house', 'deleted', 'page', 'page_size', 'phone', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name', 'sity_name', 'email', 'director', 'notes'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = School::find();

        $query->joinWith(['sity']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        } 
//        else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'school.sity_id' => $this->sity_id,
            'school.house' => $this->house,
            'school.deleted' => $this->deleted,
            'school.phone' => $this->phone,
            'school.email' => $this->email,
            'school.director' => $this->director,
            'school.notes' => $this->notes,
            'school.created_by' => $this->created_by,
            'school.updated_by' => $this->updated_by,
        ]);
        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`school`.created_at))', $this->created_at, date('d-m-Y', time())]);
        }

        $query->andFilterWhere(['like', 'school.name', $this->name]);

        $query->andFilterWhere(['like', 'sity.name', $this->sity_name]);

        return $dataProvider;
    }
}
