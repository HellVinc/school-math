<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Task;

/**
 * TaskSearch represents the model behind the search form about `common\models\Task`.
 */
class TaskSearch extends Task
{

    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $sort = [
        'number' => SORT_ASC
    ];


    public $theme_name;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['class', 'level_id', 'theme_id', 'deleted', 'type', 'number', 'sheet', 'page', 'page_size','push_status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['text', 'explanation', 'sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *

     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Task::find();

//        $query->joinWith(['theme']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
        else{
            $dataProvider->pagination = false;
        }
        $dataProvider->sort->defaultOrder = $this->sort;
        // grid filtering conditions
        $query->andFilterWhere([
            'task.class' => $this->class,
            'task.level_id' => $this->level_id,
            'task.theme_id' => $this->theme_id,
            'task.deleted' => $this->deleted,
            'task.type' => $this->type,
            'task.number' => $this->number,
            'task.sheet' => $this->sheet,
            'task.push_status' => $this->push_status,
            'task.created_by' => $this->created_by,
            'task.updated_by' => $this->updated_by,
        ]);
        if($this->created_at){
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`task`.created_at))',  $this->created_at,  date('d-m-Y', time())]);
        }

//        $query->andFilterWhere(['like', 'theme.name', $this->name]);

        return $dataProvider;
    }
}
