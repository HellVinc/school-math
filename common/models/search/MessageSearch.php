<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Message;
use common\models\Role;
use yii\db\Query;

/**
 * MessageSearch represents the model behind the search form about `common\models\Message`.
 */
class MessageSearch extends Message
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $messages;
    public $flag;
    public $sort = [
        'created_at' => SORT_DESC,
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'comment_id', 'send_from', 'send_to', 'status', 'deleted', 'page', 'page_size', 'type', 'created_at', 'messages', 'flag'], 'integer'],
            [['text', 'sort'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        if ($this->flag){
            $query = Message::find()->where(['type' => 1]);
        }else{
            $query = Message::find();
        }


        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {
            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else {
//            $dataProvider->pagination = false;
//        }

        if (null !== $this->messages) {
            $query->groupBy('comment_id');
        }
        $dataProvider->sort->defaultOrder = $this->sort;

        // grid filtering conditions
        $query->andFilterWhere([
            'comment_id' => $this->comment_id,
            'send_from' => $this->send_from,
            'send_to' => $this->send_to,
            'status' => $this->status,
            'deleted' => $this->deleted,
            'created_at' => $this->created_at
        ]);

//        if (Yii::$app->user->identity->role_id == Role::LEARNER) {
//            $query->andOnCondition('adress_to =' . Yii::$app->user->id . ' OR created_by =' . Yii::$app->user->id);
//        } elseif (Yii::$app->user->identity->role_id == Role::TUTOR) {
//            if ($this->adress_to) {
//                $query->orOnCondition('created_by =' . $this->adress_to . ' OR ( adress_to =' . $this->adress_to . ' AND created_by = ' . Yii::$app->user->id . ')');
//            }else{
//                $query->orOnCondition('created_by =' . $this->adress_to . ' OR ( adress_to =' . $this->adress_to . ' AND created_by = ' . Yii::$app->user->id . ')');
//            }
//        }

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }

}
