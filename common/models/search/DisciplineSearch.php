<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Discipline;

/**
 * DisciplineSearch represents the model behind the search form about `common\models\Discipline`.
 */
class DisciplineSearch extends Discipline
{
    const DEFAULT_COUNT = 20;

    public $page;
    public $page_size;
    public $class;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'deleted', 'page', 'page_size', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['name', 'class'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search()
    {
        $query = Discipline::find();

        $query->joinWith('classDisciplines');

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($this->page_size || $this->page) {

            $dataProvider->pagination->pageSize = $this->page_size ? $this->page_size : self::DEFAULT_COUNT;
            $dataProvider->pagination->page = $this->page;
        }
//        else {
//            $dataProvider->pagination = false;
//        }

        // grid filtering conditions
        $query->andFilterWhere([
            'discipline.deleted' => $this->deleted,
            'class_discipline.class' => $this->class,
            'discipline.created_by' => $this->created_by,
            'discipline.updated_by' => $this->updated_by,
        ]);
        if ($this->created_at) {
            $query->andWhere(['between', 'DATE(FROM_UNIXTIME(`discipline`.created_at))', $this->created_at, date('d-m-Y', time())]);
        }

//        $query->andFilterWhere(['like', 'discipline.name', $this->name]);

        return $dataProvider;
    }
}
