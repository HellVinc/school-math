<?php

namespace common\models;

use Yii;
use common\models\search\TransferSearch;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "transfer".
 *
 * @property integer $id
 * @property integer $user_id
 * @property double $summa
 * @property integer $type
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $user
 * @property User $creator
 * @property User $updater
 * @property Subscription $subscription
 */
class Transfer extends \yii\db\ActiveRecord
{
    const SUBSCRIBTION = 1;
    const ADD_MONEY = 2;
    const DELETE_MONEY = 3;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'transfer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['summa', 'type', 'created_by', 'created_at',], 'required'],
            [['user_id', 'type', 'data', 'deleted', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['summa'], 'number'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/transfer", "ID"),
            'user_id' => Yii::t("msg/transfer", 'User ID'),
            'summa' => Yii::t("msg/transfer", 'Summa'),
            'type' => Yii::t("msg/transfer", 'Type'),
            'created_by' => Yii::t("msg/transfer", 'Create by'),
            'updated_by' => Yii::t("msg/transfer", 'Update by'),
            'created_at' => Yii::t("msg/transfer", 'Date create'),
            'updated_at' => Yii::t("msg/transfer", 'Date update'),
            'deleted' => Yii::t("msg/transfer", 'Deleted'),
        ];
    }


    public function one_fields()
    {
        $result = [
            'id'=>$this->id,
            'user_id'=>$this->user_id,
            'user' => $this->user->first_name . " " . $this->user->last_name,
            'summa'=>$this->summa,
            'type'=>$this->type,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'subscription' => $this->subscription
        ];

        return $result;
    }

    /**
     * @return $this
     */
    public
    function saveModel()
    {

        //TODO

        if ($this->isNewRecord) {
            $this->created_by = Yii::$app->user->id;
            $this->created_at = time();
        } else {
            $this->updated_by = Yii::$app->user->id;
            $this->updated_at = time();
        }

        if ($this->save()) {
            $user = User::findOne($this->user_id);
            if ($this->type == Transfer::SUBSCRIBTION || $this->type == Transfer::ADD_MONEY) {
                $user->balance += $this->summa;
            } else if ($this->type == Transfer::DELETE_MONEY) {
                $user->balance -= $this->summa;

                if ($user->balance < 0) {
                    return ['error' => Yii::t("msg/transfer", 'The sum exceeds admissible')];
                }
            }
            if (!$user->save(false)) {
                return Yii::$app->errors->modelError($user);
            }

            return $this;
        }
        return Yii::$app->errors->modelError($this);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public
    function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public
    function getSubscription()
    {
        return $this->hasOne(Subscription::className(), ['transfer_id' => 'id'])->andOnCondition(['subscription.deleted' => self::NOT_DELETED]);
    }
}
