<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;
use common\components\traits\modelWithFiles;

use Yii;
use common\models\search\CheckupSearch;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "checkup".
 *
 * @property integer $id
 * @property integer $done_task_id
 * @property string $text
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 *
 * @property User $creator
 * @property User $updater
 * @property DoneTask $doneTask
 * @property Control[] $controls
 * @property File[] $files
 */
class Checkup extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use modelWithFiles;

    const MIN_RATING = 4;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const NOT_VIEWED = 0;
    const VIEWED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checkup';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['done_task_id', 'created_by', 'created_at','status'], 'required'],
            [['updated_by', 'updated_at'], 'required', 'on' => 'update'],
            [['done_task_id', 'created_by', 'created_at','updated_by','updated_at', 'deleted','status'], 'integer'],
            [['text'], 'string'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['done_task_id'], 'exist', 'skipOnError' => true, 'targetClass' => DoneTask::className(), 'targetAttribute' => ['done_task_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            ['status', 'default', 'value' => self::NOT_VIEWED],
            ['status', 'in', 'range' => [self::NOT_VIEWED, self::VIEWED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/checkup", "ID"),
            'done_task_id' => Yii::t("msg/checkup", 'Done Task ID'),
            'text' => Yii::t("msg/checkup", 'Text'),
            'status' => Yii::t("msg/checkup", 'status'),
            'created_by' => Yii::t("msg/checkup", 'Create by'),
            'updated_by' => Yii::t("msg/checkup", 'Update by'),
            'created_at' => Yii::t("msg/checkup", 'Date create'),
            'updated_at' => Yii::t("msg/checkup", 'Date update'),
            'deleted' => Yii::t("msg/checkup", 'Deleted'),
        ];
    }


    public function one_fields()
    {
        return [
            'id'=> $this->id,
            'done_task_id'=> $this->done_task_id,
            'text'=> $this->text,
            'status'=> $this->status,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name. " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
            'updater' => $this->updater->first_name. " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'files' => $this->files
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                [
                    'common\models\Checkup' => [
                        'id',
                        'done_task_id',
                        'status',
                        'created_by',
                        'user' => function ($model) {
                            return $model->user->first_name . " " . $model->user->last_name;
                        },
                        'text',
                    ],
                ],
            ]
        );
        return $result;

    }

    public static function searchLowest($request)
    {
        $searchModel = new CheckupSearch();
        $searchModel->deleted = Checkup::NOT_DELETED;
        if (!$searchModel->load(['CheckupSearch' => !$request]) && $searchModel->validate()) {
            return false;
        }
        $dataProvider = $searchModel->search();
        $models = $dataProvider->getModels();
        if (!$models['error']) {
            $find = [];
            //ищем записи пользователей с низким рейтингом
            foreach ($models as $one) {
                //считаем количество правильно провереных работ
                $rating = Checkup::countRating($one, Control::RIGHT);
                //если количество провереных работ(рейтинг) меньше минимального
                if ($rating < Checkup::MIN_RATING) {
                    //добавляем запись в масив
                    $find [] = $one;
                }
            }
            return $find;
        }
    }

    public static function countRating($user_id, $status)
    {
        $query = self::find();
        $query->joinWith('controls');
        $query->where([
            'checkup.created_by' => $user_id,
            'checkup.deleted' => self::NOT_DELETED,
            'control.status' => $status
        ]);
        $result = $query->all();
        return count($result);

    }

    /**проверка рейтинга репетитора для изменения статуса исполененого задания
     * @return $this
     */
    public function checkByRating()
    {
        //считаем количество правильно провереных работ
        $rating = self::countRating($this->user_id, Control::RIGHT);
        //если количество провереных работ(рейтинг) больше минимального
        if ($rating >= self::MIN_RATING) {
            //изменяем статус работы на прооверенный
            $done_task = $this->doneTask;
            if ($done_task->changeStatus(DoneTask::STATUS_CHECKUP) && $done_task->getErrors()) {
                $this->addError('done_task_id', $done_task->getErrors());
                return $this;
            }
        }
        return true;
    }

    public function statusViewed()
    {
        $this->status = self::VIEWED;
        return $this->save();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoneTask()
    {
        return $this->hasOne(DoneTask::className(), ['id' => 'done_task_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getControls()
    {
        return $this->hasMany(Control::className(), ['checkup_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['parent_id' => 'id'])->andOnCondition(['file.deleted' => self::NOT_DELETED]);
    }


}
