<?php

namespace common\models;

use Yii;
use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;
use common\components\traits\modelWithFiles;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $comment_id
 * @property integer $send_from
 * @property integer $send_to
 * @property string $text
 * @property integer $status
 * @property integer $type
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property User $sender
 * @property User $receiver
 * @property Comment $comment
 * @property File[] $files
 */
class Message extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;
    use modelWithFiles;

    const STATUS_CREATE = 1;
    const STATUS_READ = 2;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const TYPE_MESSAGE = 0;
    const TYPE_NOTES = 1;

    public $error;
    public $task_id;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['comment_id', 'send_from', 'status', 'created_at'], 'required'],
            [['send_to'], 'required', 'on' => 'not_learner'],
            [['comment_id', 'send_from', 'send_to', 'status', 'deleted'], 'integer'],
            [['text'], 'string'],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],

            ['status', 'default', 'value' => self::STATUS_CREATE],
            ['status', 'in', 'range' => [self::STATUS_CREATE, self::STATUS_READ]],

            ['type', 'default', 'value' => self::TYPE_MESSAGE],
            ['type', 'in', 'range' => [self::TYPE_MESSAGE, self::TYPE_NOTES]],

            [['task_id'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('msg/message', 'ID'),
            'comment_id' => Yii::t("msg/message", 'Comment ID'),
            'send_from' => Yii::t("msg/message", 'Send From'),
            'send_to' => Yii::t("msg/message", 'Send To'),
            'text' => Yii::t("msg/message", 'Text'),
            'status' => Yii::t("msg/message", 'Status'),
            'deleted' => Yii::t("msg/message", 'Deleted'),
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'comment_id' => $this->comment_id,
            'text' => $this->text,
            'status' => $this->status,
            'sender' => $this->sender->first_name . " " . $this->sender->last_name,
            //TODO fix in future
//            'send_to' => $this->receiver->first_name . " " . $this->receiver->last_name,
            'send_from' => $this->send_from,
            'created_at' => $this->created_at,
            'deleted' => $this->deleted,
            'files' => $this->files,
            'avatar_sender' => $this->sender->photoPath
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Message' => [
                    'id',
                    'comment_id',
                    'status',
                    'text',
                    'send_from',
                    'send_to',
                    'sender' => function ($model) {
                        return $model->sender->first_name . " " . $model->sender->last_name;
                    },
                    'role' => function ($model) {
                        return $model->sender->role_id;
                    },
                    'created_at',
                    'files',
                    'avatar' => function ($model) {
                        /** @var $model Message */
                        return $model->sender->photoPath;
                    },
                    'type',
                    'task_id' => function($model) {
                        /** @var $model Message */
                        return $model->comment->task_id;
                    }
                ],
            ]
        );

        return $result;
    }
    
//    public function chats($result)
//    {
//        $result['models'] = ArrayHelper::toArray($result['models'], [
//            'common\models\Message' => [
//                'id',
//                'comment_id',
//                'text',
//                'send_from',
//                'sender' => function ($model) {
//                    return $model->sender->first_name . " " . $model->sender->last_name;
//                },
//                'send_to',
//                'receiver' => function ($model) {
//                    if (isset($model->receiver->first_name)){
//                        return $model->receiver->first_name . " " . $model->receiver->last_name;
//                    }
//                    return '';
//                },
//                'role_sender' => function ($model) {
//                    /** @var $model Message */
//                    return $model->sender->role->name;
//                },
//                'role_receiver' => function ($model) {
//                    /** @var $model Message */
//                    if (isset($model->receiver->role->name)){
//                        return $model->receiver->role->name;
//                    }else{
//                        return ' ';
//                    }
//                },
//                'time',
//                'files',
//                'avatar_sender' => function ($model) {
//                    /** @var $model Message */
//                    return $model->sender->photoPath;
//                },
//                'avatar_reciever' => function ($model) {
//                    /** @var $model Message */
//                    if (isset($model->receiver->photoPath)){
//                        return $model->receiver->photoPath;
//                    }else{
//                        return Yii::$app->request->getHostInfo() . "/photo/users/empty.jpg";
//                    }
//                },
//                'type',
//                'task_id' => function($model) {
//                    /** @var $model Message */
//                    return $model->comment->task_id;
//                }
//            ],
//        ]);
//
//        return $result;
//    }

    

    public function saveModel($scenario = true)
    {
        $trans = Yii::$app->db->beginTransaction();
        if (!$this->comment_id) {
            if (Yii::$app->user->identity->role_id == Role::TUTOR) {
                $scenario = false;
            }
            $comment = new Comment();
            $comment->task_id = $this->task_id;
            $comment->created_by = Yii::$app->user->id;

            $find_comment = $comment->findModel();
            if (!$find_comment) {

                $comment->status = Comment::STATUS_CREATE;

                $comment->created_at = time();
                if (!$comment->save()) {
                    $trans->rollBack();
                    return $this->addError('comment_id', $comment->getErrors());
                }
                $this->comment_id = $comment->id;
            } else {
                $this->comment_id = $find_comment->id;
            }
        }
        $this->created_at = time();
        $this->send_from = Yii::$app->user->id;
        $this->status = self::STATUS_CREATE;
        if (Yii::$app->user->identity->role_id != Role::LEARNER && $scenario) {
            $this->setScenario('not_learner');
        }
        if (!$this->save()) {
            $trans->rollBack();
        } else {
            $trans->commit();
        }
        return $this;
    }

    public static function createNote($task_id, $answer)
    {
        $model = new Message();
        $model->text = "Ви відправили відповідь " . $answer;
        $model->task_id = $task_id;
        $model->type = self::TYPE_NOTES;
        return $model->saveModel(false);
    }

    public static function changeStatus($comment_id)
    {
        $messages = Message::find()
            ->where([
                'comment_id' => $comment_id,
//                'send_to' => Yii::$app->user->id,
                'status' => self::STATUS_CREATE])
            ->all();
        foreach ($messages as $message) {
            $message['status'] = self::STATUS_READ;
            if (!$message->save()) {
                return $message->errors;
            }
        }
    }

    public function getTime()
    {
        return date('d-m-y H:i:s', $this->created_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComment()
    {
        return $this->hasOne(Comment::className(), ['id' => 'comment_id'])->andWhere(['comment.deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'send_from']);
    }

    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'send_to']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['parent_id' => 'id'])->andOnCondition(['file.deleted' => self::NOT_DELETED]);
    }

}
