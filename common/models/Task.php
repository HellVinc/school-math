<?php

namespace common\models;

use common\models\search\CommentSearch;
use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\modelWithFiles;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $user_id
 * @property integer $class
 * @property integer $level_id
 * @property integer $theme_id
 * @property string $number
 * @property string $type
 * @property string $sheet
 * @property string $text
 * @property string $result
 * @property string $explanation
 * @property integer $push_status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property File[] $files
 * @property Comment[] $comments
 * @property Theme $theme
 * @property User $creator
 * @property User $updater
 * @property Level $level
 * @property DoneTask $doneTask
 * @property DoneTask $doneTaskPush
 * @property Subtask $subtasks[]
 * @property Message $messages[]
 */
class Task extends \yii\db\ActiveRecord
{

    use soft;
    use modelWithFiles;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    const TYPE_TASK = 1;
    const TYPE_ADDITION = 2;

    public $error;
    public $done;
    public $user_id;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by', 'created_at', 'level_id', 'theme_id','type', 'number', 'sheet'], 'required'],
//            [['number', 'sheet'], 'required'],,
            [['created_by', 'class', 'level_id', 'theme_id', 'deleted', 'type', 'number', 'sheet', 'push_status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['text', 'explanation', 'result'], 'string'],
            ['type', 'in', 'range' => [self::TYPE_TASK, self::TYPE_ADDITION]],
            ['type', 'default', 'value' => self::TYPE_TASK],

            [['theme_id'], 'exist', 'skipOnError' => true, 'targetClass' => Theme::className(), 'targetAttribute' => ['theme_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['level_id'], 'exist', 'skipOnError' => true, 'targetClass' => Level::className(), 'targetAttribute' => ['level_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
            [['error', 'done', 'user_id'], 'save'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/task", "ID"),
            'class' => Yii::t("msg/task", 'Class'),
            'level_id' => Yii::t("msg/task", 'Level ID'),
            'theme_id' => Yii::t("msg/task", 'Theme ID'),
            'text' => Yii::t("msg/task", 'Text'),
            'result' => Yii::t("msg/task", 'Result'),
            'explanation' => Yii::t("msg/task", 'Explanation'),
            'deleted' => Yii::t("msg/task", 'Deleted'),
            'type' => Yii::t("msg/task", 'Type'),
            'number' => Yii::t("msg/task", 'Number'),
            'sheet' => Yii::t("msg/task", 'Page'),
            'created_by' => Yii::t("msg/task", 'Create by'),
            'updated_by' => Yii::t("msg/task", 'Update by'),
            'created_at' => Yii::t("msg/task", 'Date create'),
            'updated_at' => Yii::t("msg/task", 'Date update'),
        ];
    }


    public function one_fields()
    {
        return [
            'id' => $this->id,
            'class' => $this->theme->section->book->class,
            'level_id' => $this->level_id,
            'level' => $this->level->name,
            'theme_id' => $this->theme_id,
            'theme' => $this->theme->name,
            'done' => $this->doneTask && !$this->subtasks ? $this->doneTask->one_fields() : false,
            'number' => $this->number,
            'type' => $this->type,
            'sheet' => $this->sheet,
            'text' => $this->text,
            'result' => $this->result,
            'explanation' => $this->explanation,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name . " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
//            'updater' => $this->updater->first_name . " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'files' => $this->files,
            'comments' => $this->comments,
            'subtasks' => $this->subtasks ? $this->subtasks : false,
//            'unreadMessages'=> $this->messages,
        ];
    }


    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\Task' => [
                    'id',

                    'class',
                    'level_id',
                    'level' => function ($model) {
                        return $model->level->name;
                    },
                    'theme_id',
                    'theme' => function ($model) {
                        return $model->theme->name;
                    },
                    'done' => function ($model) {
                        /** @var $model Task */
                        if($model->subtasks) {
                            $check = 0;
                            // 0 = не решали подзадания
                            // 1 = все ответы верны
                            // 2 = есть ошибка
                            foreach ($model->subtasks as $subtask) {
                                if($subtask->doneTask) {
                                    if ($check != 2) {
                                        $check = 1;
                                    }
                                    if ($subtask->doneTask->result == 0) {
                                        $check = 2;
                                    }
                                }
                            }
                            if ($check > 0){
                                return [
                                    'result' => !($check-1)
                                ];
                            }
                        } elseif ($model->doneTask) {
                            return $model->doneTask;
                        }
                        return false;
                    },
                    'number',
                    'type',
//                            'type' => function ($model) {
//                                if ($model->type == self::TYPE_ADDITION) {
//                                    return 'addition';
//                                } elseif ($model->type == self::TYPE_TASK) {
//                                    return 'task';
//                                }
//                                return false;
//                            },
                    'sheet',
                    'text',
                    'result',
                    'explanation',
                    'subtasks' => function($model) {
                        /** @var $model Task */
                        return ArrayHelper::toArray($model->subtasks, [
                            'common\models\Subtask' => [
                                'doneTask'
                            ]
                        ]);
                    },
                    'messages'
                ],
            ]
        );
        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['parent_id' => 'id'])->andOnCondition(['file.table' => 'task'])->andOnCondition(['file.deleted' => self::NOT_DELETED]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookmarks()
    {
        return $this->hasMany(Bookmark::className(), ['task_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        $model = new Comment();
        $result['models'] = $model->find()->where(['task_id' => $this->id])->andOnCondition(['comment.deleted' => self::NOT_DELETED])->all();
        $comment = $model->all_fields($result)['models'];
        return $comment;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTheme()
    {
        return $this->hasOne(Theme::className(), ['id' => 'theme_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['id' => 'level_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoneTask()
    {
        if (!$this->user_id) {
            $this->user_id = Yii::$app->user->id;
        }
        return $this->hasOne(DoneTask::className(), ['task_id' => 'id'])->where([
            'done_task.created_by' => $this->user_id,
            'done_task.deleted' => self::NOT_DELETED
        ]);
    }

    public function getDoneTaskPush()
    {
        return $this->hasOne(DoneTask::className(), ['task_id' => 'id']);
    }

    public function getSubtasks()
    {
        return $this->hasMany(Subtask::className(), ['task_id' => 'id'])->andOnCondition(['subtask.deleted' => self::NOT_DELETED]);
    }

    public function getMessages()
    {
        return Message::find()
            ->joinWith('comment')
            ->where(['send_to' => Yii::$app->user->id, 'message.status' => 1])
            ->andWhere(['task_id' => $this->id])
            ->andOnCondition(['message.deleted' => self::NOT_DELETED])
            ->count()*1;
    }

}
