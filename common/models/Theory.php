<?php

namespace common\models;

use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\modelWithFiles;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "theory".
 *
 * @property integer $id
 * @property string $table
 * @property integer $row_id
 * @property string $title
 * @property string $text
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $deleted
 *
 * @property User $creator
 * @property User $updater
 */
class Theory extends \yii\db\ActiveRecord
{

    use soft;
    use modelWithFiles;
    use findRecords;
    use errors;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'theory';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['table', 'row_id', 'title', 'created_by','created_at'], 'required'],
            [['row_id', 'deleted', 'created_by','created_at','updated_by', 'updated_at'], 'integer'],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
            [['table', 'title', 'text'], 'string', 'max' => 255],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/theory", "ID"),
            'table' => Yii::t("msg/theory", 'Table'),
            'row_id' => Yii::t("msg/theory", 'Row ID'),
            'title' => Yii::t("msg/theory", 'Title'),
            'text' => Yii::t("msg/theory", 'Text'),
            'created_by' => Yii::t("msg/task", 'Create by'),
            'updated_by' => Yii::t("msg/task", 'Update by'),
            'created_at' => Yii::t("msg/task", 'Date create'),
            'updated_at' => Yii::t("msg/task", 'Date update'),
            'deleted' =>Yii::t("msg/theory",  'Deleted'),
        ];
    }
    public function one_fields()
    {
        return [
            'id' => $this->id,
            'table' => $this->table,
            'row_id' => $this->row_id,
            'title' => $this->title,
            'text' => $this->text,
            'created_by' => $this->created_by,
            'creator' => $this->creator->first_name. " " . $this->creator->last_name,
            'updated_by' => $this->updated_by,
//            'updater' => $this->updater->first_name. " " . $this->updater->last_name,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::ToArray($result['models'],
            [
                'common\models\Theory' => [
                    'id',
                    'table',
                    'row_id',
                    'title',
//                    'text',
                ]
            ]
        );
        return $result;
    }

    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    public function getUpdater()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
}
