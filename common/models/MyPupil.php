<?php

namespace common\models;


use common\components\traits\errors;
use common\components\traits\soft;
use common\components\traits\findRecords;

use Yii;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "my_pupil".
 *
 * @property integer $id
 * @property integer $pupil_id
 * @property integer $user_id
 * @property integer $type
 * @property integer $created_at
 * @property integer $deleted
 *
 * @property User $user
 * @property User $pupil
 */
class MyPupil extends \yii\db\ActiveRecord
{
    use soft;
    use findRecords;
    use errors;

    const PUPIL_TUTOR = 1;
    const TUTOR_CURATOR = 2;

    const NOT_DELETED = 0;
    const DELETED = 1;

    public $error;
//    public $checkup;
//    public $control;


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'my_pupil';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pupil_id', 'user_id', 'created_at'], 'required'],
            [['pupil_id', 'user_id', 'type', 'created_at', 'deleted'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            [['pupil_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['pupil_id' => 'id']],
            ['deleted', 'default', 'value' => self::NOT_DELETED],
            ['deleted', 'in', 'range' => [self::NOT_DELETED, self::DELETED]],
//            [['checkup','control'],'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t("msg/myPupil", "ID"),
            'created_at' => Yii::t("msg/myPupil", 'Date create'),
            'pupil_id' => 'Pupil ID',
            'user_id' => 'User ID',
            'type' => 'Type',
            'deleted' => 'Deleted',
        ];
    }

    public function one_fields()
    {
        return [
            'id' => $this->id,
            'user' => $this->user->first_name . " " . $this->user->last_name,
            'user_id' => $this->user_id,
            'pupil' => $this->pupil->first_name . " " . $this->pupil->last_name,
            'pupil_id' => $this->pupil_id,
        ];
    }

    public function all_fields($result)
    {
        $result['models'] = ArrayHelper::toArray($result['models'],
            [
                'common\models\MyPupil' => [
                    'id',
                    'user' => function ($model) {
                        return $model->user->first_name . " " . $model->user->last_name;
                    },
                    'user_id',
                    'pupil' => function ($model) {
                        return $model->pupil->first_name . " " . $model->pupil->last_name;
                    },
                    'pupil_id',
                    'checkup' => function ($model) {
                        return $model->countCheckup();
                    },
                    'control' => function ($model) {
                        return $model->countControl();
                    }
                ],
            ]
        );
        return $result;
    }

    public function findModel()
    {
        return self::findOne([
            'user_id' => $this->user_id,
            'pupil_id' => $this->pupil_id,
            'deleted' => self::NOT_DELETED
        ]);
    }

    public function saveModel()
    {
        //проверяем. существует ли такая запись
        if ($this->findModel()) {
            return ['error' => Yii::t('msg/error', 'Record was added before')];
        }

        if ($this->type === self::PUPIL_TUTOR && $this->pupil->role_id !== Role::LEARNER) {
            return ['error' => Yii::t('msg/error', 'Error')];
        }

        if ($this->type === self::TUTOR_CURATOR && $this->pupil->role_id !== Role::TUTOR) {
            return ['error' => Yii::t('msg/error', 'Error')];
        }
        // сохраняем новую запись

        if ($this->isNewRecord) {
            $this->created_at = time();
        }

        $model = $this->save();

        $subsc = Subscription::findOne(['created_by' => $this->pupil_id]);
        $subsc->my_pupil_id = $this->id;
        $subsc->save();

        return $model;
    }

    public function countCheckup()
    {
        $user = User::findOne($this->pupil_id);
        $mod = $user->checkups;
        return count($mod);
    }

    public function countControl()
    {
        $query = Control::find();
        $query->select('control.checkup_id');
        $query->joinWith('checkup');
        $query->where(['checkup.created_by' => $this->pupil_id, 'control.created_by' => 4, 'control.deleted' => self::NOT_DELETED]);
        $query->groupBy('control.checkup_id');
        $mod = $query->all();
        return count($mod);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPupil()
    {
        return $this->hasOne(User::className(), ['id' => 'pupil_id']);
    }
}
