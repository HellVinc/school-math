-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Авг 31 2016 г., 18:57
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `math`
--

-- --------------------------------------------------------

--
-- Структура таблицы `book`
--

CREATE TABLE IF NOT EXISTS `book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `edition` varchar(255) NOT NULL,
  `year` int(11) NOT NULL,
  `notes` text,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `book`
--

INSERT INTO `book` (`id`, `name`, `author`, `edition`, `year`, `notes`, `deleted`) VALUES
(1, 'Pozdil_1', 'Author_1', 'kyiv', 1998, 'good book', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `book_discipline`
--

CREATE TABLE IF NOT EXISTS `book_discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `discipline_id` (`discipline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `book_discipline`
--

INSERT INTO `book_discipline` (`id`, `book_id`, `discipline_id`, `deleted`) VALUES
(1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `checkup`
--

CREATE TABLE IF NOT EXISTS `checkup` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `done_task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `text` text,
  `date` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `done_task_id` (`done_task_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп данных таблицы `checkup`
--

INSERT INTO `checkup` (`id`, `done_task_id`, `user_id`, `text`, `date`, `deleted`) VALUES
(1, 1, 2, 'text', 1472481136, 0),
(2, 4, 2, 'text2', 1472550741, 0),
(3, 4, 2, 'text3', 1472550788, 0),
(4, 4, 2, 'text4', 1472550833, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `control`
--

CREATE TABLE IF NOT EXISTS `control` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `checkup_id` int(11) NOT NULL,
  `curator_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `date` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `checkup_id` (`checkup_id`),
  KEY `curator_id` (`curator_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

--
-- Дамп данных таблицы `control`
--

INSERT INTO `control` (`id`, `checkup_id`, `curator_id`, `status`, `date`, `deleted`) VALUES
(1, 1, 4, 0, 1472485107, 0),
(2, 1, 4, 1, 1472548265, 0),
(3, 2, 4, 1, 1472550907, 0),
(4, 3, 4, 1, 1472550911, 0),
(10, 4, 4, 1, 1472560201, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `discipline`
--

CREATE TABLE IF NOT EXISTS `discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Дамп данных таблицы `discipline`
--

INSERT INTO `discipline` (`id`, `name`, `deleted`) VALUES
(1, 'Математика', 0),
(2, 'Геометрія', 0),
(3, 'Алгебра', 0),
(4, 'Арифметика', 0),
(5, 'Тригонометрия', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `done_task`
--

CREATE TABLE IF NOT EXISTS `done_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `result` double(11,0) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `date` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `task_id` (`task_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Дамп данных таблицы `done_task`
--

INSERT INTO `done_task` (`id`, `task_id`, `user_id`, `result`, `status`, `date`, `deleted`) VALUES
(1, 1, 5, 11, 1, 1472475417, 0),
(4, 1, 2, 9, 2, 1472479487, 1),
(5, 2, 2, 0, 1, 1472560441, 0),
(6, 3, 2, 6, 1, 1472560448, 0),
(7, 4, 2, 8, 1, 1472560455, 0),
(8, 1, 14, 1, 1, 1472560485, 1),
(9, 2, 14, 2, 1, 1472560488, 0),
(10, 3, 14, 333, 1, 1472560492, 0),
(11, 4, 14, 4300, 1, 1472560498, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) NOT NULL,
  `table` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `file`
--

INSERT INTO `file` (`id`, `parent_id`, `table`, `extension`, `url`, `deleted`) VALUES
(2, 12, 'task', 'jpg', 'C:\\OpenServer\\domains\\school-maths/files/task/12', 0),
(3, 17, 'task', 'jpg', 'C:\\OpenServer\\domains\\school-maths/files/task/17', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `level`
--

CREATE TABLE IF NOT EXISTS `level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `level`
--

INSERT INTO `level` (`id`, `name`, `deleted`) VALUES
(1, 'level_1', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `my_book`
--

CREATE TABLE IF NOT EXISTS `my_book` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `my_discipline_id` int(11) NOT NULL,
  `book_id` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `my_class`
--

CREATE TABLE IF NOT EXISTS `my_class` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `number` tinyint(2) NOT NULL,
  `letter` varchar(5) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `my_class`
--

INSERT INTO `my_class` (`id`, `user_id`, `number`, `letter`, `deleted`) VALUES
(2, 12, 5, 'A', 0),
(3, 13, 5, 'A', 0),
(4, 14, 5, 'A', 0),
(5, 14, 3, 'A', 0),
(6, 14, 4, 'A', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `my_discipline`
--

CREATE TABLE IF NOT EXISTS `my_discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `discipline_id` int(11) NOT NULL,
  `teacher` varchar(255) DEFAULT NULL,
  `deleted` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `discipline_id` (`discipline_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `my_discipline`
--

INSERT INTO `my_discipline` (`id`, `user_id`, `discipline_id`, `teacher`, `deleted`) VALUES
(1, 5, 1, 'Тетяна Анатоліївнa', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `role`
--

CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Дамп данных таблицы `role`
--

INSERT INTO `role` (`id`, `name`, `deleted`) VALUES
(1, 'learner', 0),
(2, 'tutor', 0),
(3, 'teacher', 0),
(4, 'curator', 0),
(5, 'manager', 0),
(6, 'admin', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `school`
--

CREATE TABLE IF NOT EXISTS `school` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `sity_id` int(11) NOT NULL,
  `street` varchar(255) DEFAULT NULL,
  `house` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sity_id` (`sity_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `school`
--

INSERT INTO `school` (`id`, `name`, `sity_id`, `street`, `house`, `deleted`) VALUES
(1, 'school 1', 1, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `section`
--

CREATE TABLE IF NOT EXISTS `section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `book_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `book_id` (`book_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `section`
--

INSERT INTO `section` (`id`, `book_id`, `name`, `deleted`) VALUES
(1, 1, 'Pozdil_1', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `sity`
--

CREATE TABLE IF NOT EXISTS `sity` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `sity`
--

INSERT INTO `sity` (`id`, `name`, `deleted`) VALUES
(1, 'Полтавa', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `task`
--

CREATE TABLE IF NOT EXISTS `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creator_id` int(11) NOT NULL,
  `class` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `theme_id` int(11) NOT NULL,
  `text` text NOT NULL,
  `result` double(11,0) DEFAULT NULL,
  `explanation` text,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `creator_id` (`creator_id`),
  KEY `level_id` (`level_id`),
  KEY `theme_id` (`theme_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Дамп данных таблицы `task`
--

INSERT INTO `task` (`id`, `creator_id`, `class`, `level_id`, `theme_id`, `text`, `result`, `explanation`, `deleted`) VALUES
(1, 5, 5, 1, 1, '2+8', 11, NULL, 0),
(2, 5, 6, 1, 1, '2+8', 12, NULL, 0),
(3, 5, 8, 1, 1, '4+4', 8, NULL, 0),
(4, 5, 9, 1, 1, '4+999', 1003, NULL, 0),
(12, 5, 10, 1, 1, '1010', 1010101, NULL, 0),
(17, 5, 10, 1, 1, '1010', 1010101, NULL, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `theme`
--

CREATE TABLE IF NOT EXISTS `theme` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `section_id` (`section_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `theme`
--

INSERT INTO `theme` (`id`, `section_id`, `name`, `deleted`) VALUES
(1, 1, 'theme_1', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `transfer`
--

CREATE TABLE IF NOT EXISTS `transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `summa` double(11,0) NOT NULL,
  `type` int(11) NOT NULL,
  `data` int(11) NOT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `confirm` tinyint(1) DEFAULT NULL,
  `balance` int(11) DEFAULT NULL,
  `phone` int(12) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `first_name` varchar(55) NOT NULL,
  `second_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(55) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `auth_key` varchar(32) NOT NULL,
  `school_id` int(11) DEFAULT NULL,
  `sity_id` int(11) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `deleted` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `school_id` (`school_id`),
  KEY `sity_id` (`sity_id`),
  KEY `role_id` (`role_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `role_id`, `confirm`, `balance`, `phone`, `email`, `first_name`, `second_name`, `last_name`, `photo`, `auth_key`, `school_id`, `sity_id`, `password_hash`, `updated_at`, `created_at`, `deleted`) VALUES
(1, 1, NULL, NULL, 1234567891, '1@1.c', '1', NULL, '1', NULL, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU4i', NULL, NULL, '$2y$13$pTT6GdYCuHtzCf6bK0kgmemcN6LaqGOiCT.XEpaFu9W9.XyOdqh7W', 1472127985, 1471450288, 0),
(2, 2, NULL, NULL, 1234567892, '', '2', NULL, '2', NULL, 'ucqN16-chnGJGN0-a1y3IpBudtD__BOA', NULL, NULL, '$2y$13$izR3bPT56MQjisvDBQ.RgevWmBOirwOYuI6kxuhT2UzEA0LoWQL9y', 1471450630, 1471450630, 0),
(3, 3, NULL, NULL, 1234567893, '', '3', NULL, '3', NULL, 'pE6X5QJKcRPCSvHFh7PAaWSIiNsA90ZF', NULL, NULL, '$2y$13$BIBWYUMYpXsCAqLoScds5equtwegb5u2oeiFxm6f/L5CcMbQjQZj.', 1471450693, 1471450693, 0),
(4, 4, NULL, NULL, 1234567894, '', '4', NULL, '4', NULL, 'VHDzWqTBGpdqlsbWrp9M0a618moki--C', NULL, NULL, '$2y$13$9R3.UnqY.Tizd2RRxLXs9O30Kux2IS8L.3DjCEe7RqIYUm6A5RxWe', 1471450733, 1471450733, 0),
(5, 5, NULL, NULL, 1234567895, '', '5', NULL, '5', NULL, 'ZUCNeFIfLG52YaoOMCYn5Mt6ANNvUDNo', NULL, NULL, '$2y$13$5ez6lDL8sspJKDNPc.1iYudXooQF.7R9xOh8s5PkKdNV1Pa8G2sau', 1471450830, 1471450830, 0),
(6, 6, NULL, NULL, 1234567896, '', '6', NULL, '6', NULL, '0ERxfRvpMjU_A8wWvXfFJcl3v5cfAU45', NULL, NULL, '$2y$13$gjioOYB2/f8QiSTsF/WQ/eogJxGeb5VKbYAxTXuHE5sKB/HM1LO6u', 1471450896, 1471450896, 0),
(7, 1, NULL, NULL, 1234567811, 'tkalenko.inka@gmail.com', 'www', NULL, 'www', '7.jpg', 'gFyWAxTFjwvUG6AYiC1H_5hXCEfmZSw9', NULL, NULL, '$2y$13$96ijlSxTuPaYlLUqGCpmI.TJ92lA3hnXjKK/58u2yG/tAOtSZDYQO', 1471943148, 1471942913, 0),
(12, 1, NULL, NULL, 1234561111, 'w@w.c1', 'aaa', NULL, 'bbb', NULL, '34NClnX9ix7-vidh2O8NIxOgNW3vrWUk', NULL, NULL, '$2y$13$whKNh0P2i5vFLfP7Tn1IdurUJ8k3zCQpyUZ6rBmnx9q3VOTC27kXm', 1471963877, 1471963877, 0),
(13, 1, NULL, NULL, 1234561112, 'w@w.c2', 'aaa', NULL, 'bbb', NULL, 'FgRCY8StoJ92HVfF_XIlXTle8KtwayhX', NULL, NULL, '$2y$13$NHuLl6vbkzAyTs4PQtHns.Es5.MXTVYgJ787VNbqFoaCMR/GDJzqa', 1472123082, 1472123082, 0),
(14, 1, NULL, NULL, 1234561113, 'w@w.c3', 'aaa', NULL, 'bbb', NULL, 'HonGG7s5qYmQdUdDot-AxC-gB5fTRgz5', NULL, NULL, '$2y$13$RZyaQLMRJpLC54Is78Qp7u9U3Ecj1Tlni6z3CAYazcI/O7IpJXKyO', 1472126052, 1472123136, 0);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `book_discipline`
--
ALTER TABLE `book_discipline`
  ADD CONSTRAINT `book_discipline_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`),
  ADD CONSTRAINT `book_discipline_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`);

--
-- Ограничения внешнего ключа таблицы `checkup`
--
ALTER TABLE `checkup`
  ADD CONSTRAINT `checkup_ibfk_1` FOREIGN KEY (`done_task_id`) REFERENCES `done_task` (`id`),
  ADD CONSTRAINT `checkup_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `control`
--
ALTER TABLE `control`
  ADD CONSTRAINT `control_ibfk_1` FOREIGN KEY (`checkup_id`) REFERENCES `checkup` (`id`),
  ADD CONSTRAINT `control_ibfk_2` FOREIGN KEY (`curator_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `done_task`
--
ALTER TABLE `done_task`
  ADD CONSTRAINT `done_task_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `done_task_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `task` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_class`
--
ALTER TABLE `my_class`
  ADD CONSTRAINT `my_class_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `my_discipline`
--
ALTER TABLE `my_discipline`
  ADD CONSTRAINT `my_discipline_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `my_discipline_ibfk_2` FOREIGN KEY (`discipline_id`) REFERENCES `discipline` (`id`);

--
-- Ограничения внешнего ключа таблицы `school`
--
ALTER TABLE `school`
  ADD CONSTRAINT `school_ibfk_1` FOREIGN KEY (`sity_id`) REFERENCES `sity` (`id`);

--
-- Ограничения внешнего ключа таблицы `section`
--
ALTER TABLE `section`
  ADD CONSTRAINT `section_ibfk_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`);

--
-- Ограничения внешнего ключа таблицы `task`
--
ALTER TABLE `task`
  ADD CONSTRAINT `task_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `task_ibfk_2` FOREIGN KEY (`level_id`) REFERENCES `level` (`id`),
  ADD CONSTRAINT `task_ibfk_3` FOREIGN KEY (`theme_id`) REFERENCES `theme` (`id`);

--
-- Ограничения внешнего ключа таблицы `theme`
--
ALTER TABLE `theme`
  ADD CONSTRAINT `theme_ibfk_1` FOREIGN KEY (`section_id`) REFERENCES `section` (`id`);

--
-- Ограничения внешнего ключа таблицы `transfer`
--
ALTER TABLE `transfer`
  ADD CONSTRAINT `transfer_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Ограничения внешнего ключа таблицы `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`school_id`) REFERENCES `school` (`id`),
  ADD CONSTRAINT `user_ibfk_2` FOREIGN KEY (`sity_id`) REFERENCES `sity` (`id`),
  ADD CONSTRAINT `user_ibfk_3` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
